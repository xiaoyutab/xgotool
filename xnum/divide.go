package xnum

// 金额 除以
//
//	sum1	被除数
//	sum2	除数
func Divide(sum1, sum2 float64) float64 {
	sum_int := int(sum2 * 100)
	eq, _ := Equally(sum1*100, sum_int)
	return eq
}
