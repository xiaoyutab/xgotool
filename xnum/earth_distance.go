package xnum

import "math"

// 计算经纬度之间的距离【返回单位：m】
// 计算公式：C = sin(LatA*Pi/180)*sin(LatB*Pi/180) + cos(LatA*Pi/180)*cos(LatB*Pi/180)*cos((MLonA-MLonB)*Pi/180)
//
//	lat1	坐标点1的纬度
//	lng1	坐标点1的经度
//	lat1	坐标点2的纬度
//	lng2	坐标点2的经度
func EarthDistance(lat1, lng1, lat2, lng2 float64) float64 {
	rad := math.Pi / 180
	lat1 = lat1 * rad
	lng1 = lng1 * rad
	lat2 = lat2 * rad
	lng2 = lng2 * rad
	theta := lng2 - lng1
	dist := math.Acos(math.Sin(lat1)*math.Sin(lat2) + math.Cos(lat1)*math.Cos(lat2)*math.Cos(theta))
	return dist * EARTH_RADIUS * 1000
}

// 计算两点的距离【返回单位：m】
//
//	a1	坐标点a1的经纬度
//	a2	坐标点a2的经纬度
func EarthDistanceGps(a1, a2 *GPS) float64 {
	return EarthDistance(a1.Latitude, a1.Longitude, a2.Latitude, a2.Longitude)
}
