package xnum

// 金额平分拆分
// 小于 等于 1 的均直接返回money
//
//	money	金额
//	len		拆分份数
func Equally(money float64, len int) (float64, float64) {
	if len <= 1 {
		return money, money
	}
	if money == 0 {
		return 0, 0
	}
	// 金额转为整形
	moneys := uint64(money * 100)
	// 金额计算
	pre := moneys / uint64(len)
	// 最后金额追加
	last := moneys - pre*uint64(len-1)
	return float64(pre) / 100, float64(last) / 100
}
