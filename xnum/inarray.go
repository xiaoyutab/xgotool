package xnum

// 判断s是否在arr数组中
// PS: 类型不统一的话也会返回false，如s为int，但是arr不为[]int则也会返回false
//
//	s	待判断的变量，支持类型：bool,string,float64,float32,int,uint,int8,uint8,int16,uint16,int32,uint32,int64,uint64
//	arr	待判断的数组，需要对应s的类型，即s为string时arr为[]string
func InArray(s any, arr any) bool {
	switch s := s.(type) {
	case float64:
		if val, ok := arr.([]float64); ok {
			for _, v := range val {
				if IsEqualFloat(s, v) {
					return true
				}
			}
		}
	case float32:
		if val, ok := arr.([]float32); ok {
			for _, v := range val {
				if IsEqualFloat(s, v) {
					return true
				}
			}
		}
	case bool:
		if val, ok := arr.([]bool); ok {
			for _, v := range val {
				if s == v {
					return true
				}
			}
		}
	case string:
		if val, ok := arr.([]string); ok {
			for _, v := range val {
				if s == v {
					return true
				}
			}
		}
	case int64:
		if val, ok := arr.([]int64); ok {
			for _, v := range val {
				if s == v {
					return true
				}
			}
		}
	case uint64:
		if val, ok := arr.([]uint64); ok {
			for _, v := range val {
				if s == v {
					return true
				}
			}
		}
	case int:
		if val, ok := arr.([]int); ok {
			for _, v := range val {
				if s == v {
					return true
				}
			}
		}
	case uint:
		if val, ok := arr.([]uint); ok {
			for _, v := range val {
				if s == v {
					return true
				}
			}
		}
	case int32:
		if val, ok := arr.([]int32); ok {
			for _, v := range val {
				if s == v {
					return true
				}
			}
		}
	case uint32:
		if val, ok := arr.([]uint32); ok {
			for _, v := range val {
				if s == v {
					return true
				}
			}
		}
	case int16:
		if val, ok := arr.([]int16); ok {
			for _, v := range val {
				if s == v {
					return true
				}
			}
		}
	case uint16:
		if val, ok := arr.([]uint16); ok {
			for _, v := range val {
				if s == v {
					return true
				}
			}
		}
	case int8:
		if val, ok := arr.([]int8); ok {
			for _, v := range val {
				if s == v {
					return true
				}
			}
		}
	case uint8:
		if val, ok := arr.([]uint8); ok {
			for _, v := range val {
				if s == v {
					return true
				}
			}
		}
	}
	return false
}
