package xnum_test

import (
	"strings"
	"testing"

	"gitee.com/xiaoyutab/xgotool/xnum"
	"gitee.com/xiaoyutab/xgotool/xstring"
)

func TestDiscount(t *testing.T) {
	t.Log("100/100%:", xnum.Discount(100, 100))
	t.Log("100/80%:", xnum.Discount(100, 80))
	t.Log("100/99%:", xnum.Discount(100, 99))
	t.Log("9.9/100%:", xnum.Discount(9.9, 100))
	t.Log("9.9/80%:", xnum.Discount(9.9, 80))
	t.Log("9.9/99%:", xnum.Discount(9.9, 99))
}

// 计算金额部分（保留两位小数）
func TestPrice(t *testing.T) {
	t.Log(xnum.Price(0.95 * 1.23 / 100))
}

// 测试坐标点是否在多边形范围内
func TestInDistance(t *testing.T) {
	dic := []xnum.GPS{}
	dis := []string{
		"116.818921,39.802276",
		"116.819093,39.799704",
		"116.827504,39.799754",
		"116.827611,39.802325",
	}
	for _, v := range dis {
		tmp := strings.Split(v, ",")
		g := xnum.GPS{
			Longitude: xstring.ToFloat64(tmp[0]),
			Latitude:  xstring.ToFloat64(tmp[1]),
		}
		dic = append(dic, g)
	}
	t.Log(xnum.InDistance(xnum.GPS{Longitude: 116.822676, Latitude: 39.801369}, dic)) // true
	t.Log(xnum.InDistance(xnum.GPS{Longitude: 116.8295, Latitude: 39.801551}, dic))   // false
}

func TestIsEqualFloat(t *testing.T) {
	t.Log(xnum.IsEqualFloat(uint(1), int(3)))
}

func TestPriceHalf(t *testing.T) {
	t.Log(xnum.PriceHalf(-0.09999999999999964))
	t.Log(-0.09999999999999964 * 100 / 100)
}
