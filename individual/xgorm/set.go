package xgorm

// 设置 匹配查找 的方法别名，此方法会生成 多组 where 条件进行搜索
//
//	fun	自定义的 find_in_set 函数，该函数格式为：`func(field, value string) (string, []any)`
func (xdb *XDB) SetAliasFindInSet(fun func(field, value string) (string, []any)) *XDB {
	xdb.findinset = fun
	return xdb
}
