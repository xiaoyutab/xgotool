package xgorm

import "gorm.io/gorm"

// 定制查询
// 用于载入当前gorm暂不支持的高级查询特性
//
//	f	自定义查询函数(直接使用gorm进行查询)
func (db *XDB) Gorm(f func(conn *gorm.DB) *gorm.DB) *XDB {
	if db == nil || db.DB == nil {
		return db
	}
	db.DB = f(db.DB)
	return db
}
