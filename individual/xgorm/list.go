package xgorm

import "errors"

// 搜索列表数据
//
//	offset	跳过条数
//	limit	查询条数，<0表示查询全部
//	count	int64格式的条数搜索【需要为指针】
//	dest	搜索的字段列表【需要为指针】
func (c *XDB) List(offset, limit int, count *int64, dest any) error {
	if c == nil {
		return errors.New("数据库未连接")
	}
	if c.DB == nil {
		return errors.New("数据库未连接")
	}
	db := c.DB
	err := db.Count(count).Error
	if err != nil {
		return err
	}
	if len(c.preloads) > 0 {
		for _, preload := range c.preloads {
			db = db.Preload(preload.filed, preload.query...)
		}
	}
	if limit < 0 {
		// 如果小于0则标识查询全部
		err = db.Find(dest).Error
	} else {
		err = db.Offset(offset).Limit(limit).Find(dest).Error
	}
	if err != nil {
		return err
	}
	return nil
}
