package xgorm

import (
	"database/sql"
	"errors"

	"gorm.io/gorm"
)

// 事务相关的支持函数，代理gorm的事务相关方法

// 开启事务，代理gorm的Begin方法
//
//	opts	事务选项
func (db *XDB) Begin(opts ...*sql.TxOptions) *XDB {
	if db == nil || db.DB == nil {
		return db
	}
	db.DB = db.DB.Begin(opts...)
	return db
}

// 事务提交
func (db *XDB) Commit() *XDB {
	if db == nil || db.DB == nil {
		return db
	}
	db.DB = db.DB.Commit()
	return db
}

// 事务回退
func (db *XDB) Rollback() *XDB {
	if db == nil || db.DB == nil {
		return db
	}
	db.DB = db.DB.Rollback()
	return db
}

// 函数式事务处理
func (db *XDB) Transaction(fc func(txs *XDB) error, opts ...*sql.TxOptions) error {
	if db == nil || db.DB == nil {
		return errors.New("数据库未连接")
	}
	return db.DB.Transaction(func(tx *gorm.DB) error { return fc(To(tx)) }, opts...)
}
