package xgorm

// 预加载操作逻辑
//
//	query	预加载查询
//	args	预加载参数
func (db *XDB) Preloads(query string, args ...any) *XDB {
	if db == nil || db.DB == nil {
		return db
	}
	db.preloads = append(db.preloads, preload{
		filed: query,
		query: args,
	})
	return db
}
