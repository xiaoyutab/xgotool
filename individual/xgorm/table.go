package xgorm

// 设置表格【等同于gorm的table】
func (db *XDB) Table(name string, args ...any) *XDB {
	if db == nil || db.DB == nil {
		return db
	}
	if db.Baks == nil {
		db.Baks = db.DB
	} else {
		db.DB = db.Baks
	}
	db.DB = db.DB.Table(name, args...)
	return db
}
