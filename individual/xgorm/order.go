package xgorm

// Order排序
//
//	value	排序条件
func (db *XDB) Order(value any) *XDB {
	if db == nil || db.DB == nil {
		return db
	}
	db.DB = db.DB.Order(value)
	return db
}
