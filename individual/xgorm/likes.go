package xgorm

import (
	"fmt"
	"strings"
)

// 追加Likes搜索条件
//
//	fd	查询的字段【使用like进行匹配】
//	key	查询的字符串/数字等，请手动追加前后的%或者其他的%规则
//	op	查询条件，默认为 "LIKE ?"，可传入其他值，如：" = ?" / " > ? and  type = 1"......(字段在最前面，前后会追加括号，且仅第一个生效，所以写条件时需要注意位置)
func (c *XDB) Likes(fd []string, key any, op ...string) *XDB {
	if c == nil || c.DB == nil {
		return c
	}
	kkk := []any{}
	where_fields := []string{}
	tmp := "LIKE ?"
	if len(op) > 0 {
		tmp = op[0]
	}
	for i := 0; i < len(fd); i++ {
		where_fields = append(where_fields, fmt.Sprintf("( %s %s )", fd[i], tmp))
		kkk = append(kkk, key)
	}
	c.DB = c.DB.Where(strings.Join(where_fields, " OR "), kkk...)
	return c
}

// 追加Like查询
//
//	fd	查询的字段【使用like进行匹配】
//	key	查询的字符串/数字等
func (c *XDB) Like(fd []string, key string) *XDB {
	return c.Likes(fd, "%"+key+"%")
}

// when like搜索
//
//	w	是否执行此搜索条件
//	fd	查询的字段【使用like进行匹配】
//	key	查询的字符串/数字等
func (c *XDB) WhenLike(w bool, fd []string, key string) *XDB {
	if !w {
		return c
	}
	return c.Likes(fd, "%"+key+"%")
}
