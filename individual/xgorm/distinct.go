package xgorm

// 检索某个列或一组列的唯一值。当您指定要查询的独特字段时，SQL 将返回这些字段的独特值，并按照指定的顺序进行检索。
// 等同于gorm的Distinct方法，此处仅为内部的一个封装
//
//	args	检索条件
func (db *XDB) Distinct(args ...any) *XDB {
	if db == nil || db.DB == nil {
		return db
	}
	db.DB = db.DB.Distinct(args...)
	return db
}
