package xgorm

// join关联
// 等同于gorm的joins
//
//	query	关联表
//	args	关联参数
func (c *XDB) Joins(query string, args ...any) *XDB {
	if c == nil || c.DB == nil {
		return c
	}
	c.DB = c.DB.Joins(query, args...)
	return c
}
