package xgorm

// 设置查询字段
//
//	query	查询条件
//	args	附加参数
func (db *XDB) Select(query any, args ...any) *XDB {
	if db == nil || db.DB == nil {
		return db
	}
	db.DB = db.DB.Select(query, args...)
	return db
}

// 设置忽略字段，和Select取反
//
//	columns	要忽略的字段
func (db *XDB) Omit(columns ...string) *XDB {
	if db == nil || db.DB == nil {
		return db
	}
	db.DB = db.DB.Omit(columns...)
	return db
}
