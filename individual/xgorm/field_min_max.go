package xgorm

import (
	"strings"
)

// 设置字段的范围信息
//
//	min_field	数据库中的最小ID/时间字段
//	max_field	数据库中的最大ID/时间字段
//	min			搜索的最小条件
//	max			搜索的最大条件
//	type_field	搜索模式，0-包含min和max 1-包含min，不包含max 2-包含max，不包含min 3-都不包含
func (db *XDB) FieldMinMax(min_field, max_field string, min, max any, type_field uint8) *XDB {
	if db == nil || db.DB == nil {
		return db
	}
	fids := []string{
		"( " + min_field + " > ? and " + max_field + " < ? )",
	}
	wheres := []any{
		min, max,
	}
	if type_field == 0 || type_field == 1 {
		// 包含min
		fids = append(fids, "( "+min_field+" <= ? AND "+max_field+" >= ? )")
		wheres = append(wheres, min, min)
	}
	if type_field == 0 || type_field == 2 {
		// 包含max
		fids = append(fids, "( "+min_field+" <= ? AND "+max_field+" >= ? )")
		wheres = append(wheres, max, max)
	}
	return db.Where(strings.Join(fids, " OR "), wheres...)
}
