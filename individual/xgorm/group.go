package xgorm

// 分组
//
//	name	分组字段
func (db *XDB) Group(name string) *XDB {
	if db == nil || db.DB == nil {
		return db
	}
	db.DB = db.DB.Group(name)
	return db
}

// 排重
//
//	query	排重条件
//	args	附加参数
func (db *XDB) Having(query any, args ...any) *XDB {
	if db == nil || db.DB == nil {
		return db
	}
	db.DB = db.DB.Having(query, args...)
	return db
}
