package xgorm_test

import (
	"fmt"
	"sort"
	"strings"
	"testing"

	"gitee.com/xiaoyutab/xgotool/individual/xgorm"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func TestTimes(t *testing.T) {
	db, _ := gorm.Open(mysql.Open("huifabj:lTjwvucAi)Q5Fyv1@tcp(114.113.144.175:65533)/xinfu_express?charset=utf8"))
	ks, err := xgorm.To(db).Table("express_code").PluckStringInt("code_name", "code")
	if err != nil {
		t.Error(err)
	}
	t.Log(ks)
}

func TestFors(t *testing.T) {
	s := []int{1, 2, 1, 4, 2, 0, 67, 0, 6, 5, 65}
	sort.Ints(s)
	pet := len(s) * 2 / 3
	for i := 0; i < pet; i++ {
		if s[i] == 0 {
			pet++
			continue
		}
		t.Log(s[i])
	}
}

func TestAsasd(t *testing.T) {
	// 示例字符串
	input := `"Hello, 'World!'"`

	// 替换双引号为单引号
	output := strings.Replace(input, `"`, `'`, -1)
	// 替换单引号为双引号
	output = strings.Replace(output, `'`, `"`, -1)

	fmt.Println(output) // 输出: 'Hello, "World!"'
}
