// 个人扩展到的一些GROM的常用方法
package xgorm

import (
	"strings"

	"gorm.io/gorm"
)

type preload struct {
	filed string
	query []any
}

// 扩展结构体
// 使用Alias和Dns来区分不同的数据库连接，即相同的Alias连接相同
type XDB struct {
	Baks      *gorm.DB                                  // 旧的数据库驱动，用于多Table拆分
	preloads  []preload                                 // 预加载的结构体
	findinset func(field, value string) (string, []any) // 自定义的 find_in_set 函数
	*gorm.DB
}

// 将gorm数据库连接转换为此处扩展的连接信息【条件会在原指针上进行添加，所以此处可以单独使用】
//
//	c	待转换的结构体
//	fun	附加选项
func To(c *gorm.DB) *XDB {
	dbs := XDB{
		DB:        c,
		findinset: find_in_set,
	}
	return &dbs
}

// 自定义的 find_in_set 函数，此处会使用 AND 连接多个条件，若要使用 OR 连接的话，请额外替换 ` AND ` 为 ` OR `
// 另，为了便于外部替换，此处的 AND 为大写，且两侧均有一个空格，替换后的结果请也按照此方法进行替换
//
//	field	字段名
//	value	值，多个值用逗号分隔
func find_in_set(field, value string) (string, []any) {
	spl := []string{}
	vals := []any{}
	for _, v := range strings.Split(value, ",") {
		spl = append(spl, "FIND_IN_SET(?, field)")
		vals = append(vals, v)
	}
	return strings.Join(spl, " AND "), vals
}
