package xgorm

// Where搜索
// 此搜索条件和gorm的where等同
//
//	query	查询条件
//	args	附加参数
func (c *XDB) Where(query any, args ...any) *XDB {
	if c == nil || c.DB == nil {
		return c
	}
	if query == "" {
		return c
	}
	c.DB = c.DB.Where(query, args...)
	return c
}

// When搜索
// 此搜索条件和gorm的where等同，最前方多了一层If bool的判断
//
//	when	判断注入的When条件，只有此处为true时才会注入后方的where条件
//	query	查询条件
//	args	附加参数
func (c *XDB) When(when bool, query any, args ...any) *XDB {
	if !when {
		return c
	}
	if query == "" {
		return c
	}
	if c == nil || c.DB == nil {
		return c
	}
	c.DB = c.DB.Where(query, args...)
	return c
}

// Where条件
//
//	query	查询条件
//	args	附加参数
func (db *XDB) Not(query interface{}, args ...interface{}) *XDB {
	if db == nil || db.DB == nil {
		return db
	}
	db.DB = db.DB.Not(query, args...)
	return db
}

// Where条件
//
//	query	查询条件
//	args	附加参数
func (db *XDB) Or(query interface{}, args ...interface{}) *XDB {
	if db == nil || db.DB == nil {
		return db
	}
	db.DB = db.DB.Or(query, args...)
	return db
}

// Between 范围检索
//
//	field	待检索的条件
//	start	开始条件
//	end		结束条件
func (db *XDB) Between(field string, start, end any) *XDB {
	if field == "" {
		return db
	}
	if db == nil || db.DB == nil {
		return db
	}
	db.DB = db.DB.Where(field+" BETWEEN ? AND ?", start, end)
	return db
}

// When Between 范围检索
//
//	when	判断注入的When条件，只有此处为true时才会注入后方的where条件
//	field	待检索的条件
//	start	开始条件
//	end		结束条件
func (db *XDB) WhenBetween(when bool, field string, start, end any) *XDB {
	if !when {
		return db
	}
	if field == "" {
		return db
	}
	if db == nil || db.DB == nil {
		return db
	}
	db.DB = db.DB.Where(field+" BETWEEN ? AND ?", start, end)
	return db
}

// FindInSet 自定义的 find_in_set 函数进行Where替换
// 此函数默认使用 MySQL 的 find_in_set 函数进行拼接替换
// 如：("name", "a,b,c") -生成-> 以下两个参数
// 1. "FIND_IN_SET(?, name) AND FIND_IN_SET(?, name) AND FIND_IN_SET(?, name)"
// 2. []any{"a", "b", "c"}
// 此外，可以使用 db.SetAliasFindInSet() 方法进行替换以上的生成步骤，例如：
// Sqlite数据库中，因不支持此函数，可以设置函数拼接为以下两种方法进行替换
// ("name", "a") => "(name LIKE ? OR name LIKE ? OR name LIKE ? OR name = ?)", []any{"%,a,%", "a,%", "%,a", "a"}
// ("name", "a") => "( ',' || name || ',' )  LIKE ?", []any{"%,a,%"}
//
//	field	待检索的条件
//	value	待检索的条件值，多个使用,分割
func (db *XDB) WhereFindInSet(field, value string) *XDB {
	if db == nil || db.DB == nil || value == "" {
		return db
	}
	w, args := db.findinset(field, value)
	if w == "" {
		return db
	}
	db.DB = db.DB.Where(w, args...)
	return db
}
