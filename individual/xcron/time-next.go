package xcron

import "fmt"

// 添加x小时执行的任务[不重启]
//
//	h,m	几时几分
//	job	待运行的任务配置结构
func TimeNext(h, m int, job CronTab) {
	SpecNext(fmt.Sprintf("%d %d * * *", h, m), job)
}
