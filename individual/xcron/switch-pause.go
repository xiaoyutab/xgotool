package xcron

// 切换任务key的暂停/启用状态
//
//	key		Crontab任务的下标关键词key
//	pause	暂停状态 true-暂停此任务 false-继续运行此任务
func SwitchPause(key string, pause bool) {
	for i := 0; i < len(_default.Tables); i++ {
		if _default.Tables[i].Key == key {
			_default.Tables[i].IsPause = pause
			Restart()
			return
		}
	}
}
