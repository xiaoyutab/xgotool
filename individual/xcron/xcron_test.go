package xcron_test

import (
	"encoding/json"
	"testing"

	"gitee.com/xiaoyutab/xgotool/individual/xcron"
)

func TestJson(t *testing.T) {
	a := []xcron.CronTab{
		{
			Error: "测试错误原因哦",
		},
	}
	b, _ := json.Marshal(a)
	t.Log(string(b))
}
