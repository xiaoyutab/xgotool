package xcron

import "gitee.com/xiaoyutab/xgotool/xnum"

// 按照指定的spec进行执行任务[不重启]
//
//	spec	指定运行的规则：分 时 日 月份 周几（具体规则参考Linux系统的crontab）
//	job		要运行的任务的Tab结构
func SpecNext(spec string, job CronTab) {
	_default.Lock()
	defer _default.Unlock()
	job.Spec = spec
	if !xnum.InArray(job.Key, _default.Keys) {
		_default.Tables = append(_default.Tables, job)
		_default.Keys = append(_default.Keys, job.Key)
	}
}
