package xcron

// 按照指定的spec进行执行任务
//
//	spec	指定运行的规则：分 时 日 月份 周几（具体规则参考Linux系统的crontab）
//	job		要运行的任务的Tab结构配置
func Spec(spec string, job CronTab) {
	SpecNext(spec, job)
	Restart()
}
