package xcron

import "fmt"

// 添加x小时执行的任务[不重启]
//
//	h	多少小时执行一次
//	job	待运行的任务配置结构
func HoursNext(h int, job CronTab) {
	SpecNext(fmt.Sprintf("0 */%d * * *", h), job)
}
