package xcron

import "fmt"

// 添加x小时执行的任务
//
//	h	多少小时执行一次
//	job	待运行的任务结构配置
func Hours(h int, job CronTab) {
	Spec(fmt.Sprintf("0 */%d * * *", h), job)
}
