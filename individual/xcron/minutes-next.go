package xcron

import "fmt"

// 添加每X分钟执行的任务[不重启]
//
//	min	多少分钟执行一次
//	job	待运行的任务配置结构
func MinutesNext(min int, job CronTab) {
	SpecNext(fmt.Sprintf("*/%d * * * *", min), job)
}
