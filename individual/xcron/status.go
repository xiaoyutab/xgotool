package xcron

// 状态结构数据返回
type Statusstruct struct {
	Status   bool      `json:"status"`    // 运行状态
	Count    int       `json:"count"`     // 总注册条数
	RunCount int       `json:"run_count"` // 运行条数
	Tables   []CronTab `json:"tables"`    // 当前配置的任务规则表
}

// 获取xcron的运行状态
func Status() *Statusstruct {
	st := Statusstruct{
		Count:  len(_default.Tables),
		Tables: []CronTab{},
	}
	for _, v := range _default.Tables {
		if v.ExecId > 0 {
			st.RunCount++
		}
		st.Tables = append(st.Tables, v)
	}
	if st.RunCount > 0 {
		st.Status = true
	}
	return &st
}
