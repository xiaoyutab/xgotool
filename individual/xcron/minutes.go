package xcron

import "fmt"

// 添加每X分钟执行的任务
//
//	min	多少分钟执行一次
//	job	待运行的任务配置结构
func Minutes(min int, job CronTab) {
	SpecNext(fmt.Sprintf("*/%d * * * *", min), job)
}
