package xcron

import (
	"time"

	"github.com/robfig/cron/v3"
)

// 重启定时任务【会移除所有的定时任务ID，然后重新插入】
//
//	此处采用停止清空的形式来重启
func Restart() {
	// 停止定时任务
	_default.Lock()
	defer _default.Unlock()
	if _default.cron != nil {
		_default.cron.Stop()
		for i := 0; i < len(_default.Tables); i++ {
			_default.Tables[i].ExecId = 0
		}
		_default.cron = nil
	}
	// 开启新的定时任务
	_default.cron = cron.New()
	for k, v := range _default.Tables {
		if v.IsPause {
			continue
		}
		_default.Tables[k].StartTime = time.Now().Format(time.DateTime)
		id, err := _default.cron.AddFunc(v.Spec, v.Func)
		if err != nil {
			_default.Tables[k].Error = err.Error()
		} else {
			_default.Tables[k].ExecId = id
		}
	}
	_default.cron.Start()
}
