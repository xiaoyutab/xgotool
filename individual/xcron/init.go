// 定时任务脚本
package xcron

import (
	"sync"

	"github.com/robfig/cron/v3"
)

// 定时任务的配置字段标识
type CronTab struct {
	Spec      string       `json:"spec"`       // 定时任务的运行规则
	Key       string       `json:"key"`        // 定时任务的运行下标
	Name      string       `json:"name"`       // 定时任务的运行名称
	Desc      string       `json:"desc"`       // 定时任务的运行描述
	Func      func()       `json:"-"`          // 运行函数
	ExecId    cron.EntryID `json:"exec_id"`    // 运行ID
	Error     string       `json:"error"`      // 运行错误原因
	IsPause   bool         `json:"is_pause"`   // 是否暂停
	StartTime string       `json:"start_time"` // 开始分派时间
}

// 配置项结构体
type Config struct {
	cron   *cron.Cron // 定时任务索引
	Tables []CronTab  // 定时任务列表
	Keys   []string   // 已注册的key下标
	sync.Mutex
}

// 默认配置
var _default Config = Config{
	Tables: []CronTab{},
}
