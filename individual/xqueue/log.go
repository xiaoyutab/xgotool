package xqueue

import (
	"encoding/json"
	"fmt"
	"time"
)

// 队列执行日志模型
type QueueLog struct {
	Id        uint   `gorm:"column:id" xml:"id" form:"id" json:"id"`
	Key       string `gorm:"column:key;size:200" xml:"key" form:"key" json:"key"`                    // 队列key
	Name      string `gorm:"column:name;size:200" xml:"name" form:"name" json:"name"`                // 队列name名称
	Desc      string `gorm:"column:desc;size:200" xml:"desc" form:"desc" json:"desc"`                // 队列帮助描述
	Type      uint8  `gorm:"column:type" xml:"type" form:"type" json:"type"`                         // 操作类型 1-入队 2-执行
	IsError   int8   `gorm:"column:is_error" xml:"is_error" form:"is_error" json:"is_error"`         // 是否产生错误 1-产生了错误
	Error     string `gorm:"column:error" xml:"error" form:"error" json:"error"`                     // 错误描述
	Param     string `gorm:"column:param" xml:"param" form:"param" json:"param"`                     // 队列执行参数
	CreatedAt int64  `gorm:"column:created_at" xml:"created_at" form:"created_at" json:"created_at"` // 操作时间
}

func (c *QueueLog) TableName() string {
	return "queue_log"
}

// 队列日志写入
var log_insert func(c *QueueLog) = func(c *QueueLog) {
	b, _ := json.Marshal(c)
	fmt.Println(string(b))
}

// 内部快速记录日志
//
//	key	队列下标
//	isr	是否是执行操作
//	pm	传入参数
//	err	错误信息
func log(key string, isr bool, pm any, err error) {
	mod := QueueLog{
		Key:       key,
		Type:      1,
		Param:     asJson(pm),
		CreatedAt: time.Now().Unix(),
	}
	if v, ok := queue_list[key]; ok {
		mod.Name = v.Name
		mod.Desc = v.Desc
	}
	if isr {
		mod.Type = 2
	}
	if err != nil {
		mod.IsError = 1
		mod.Error = err.Error()
	}
	go log_insert(&mod)
}
