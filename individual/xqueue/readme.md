# xQueue 队列简易使用组件

使用此组件可以简单、快捷的快速使用队列组件，但是和其他未使用此组件的队列通信时会造成一定的困扰，所以请谨慎使用。

> PS: 日志记录时，方法名为 `_._` 时表示此次发送队列为原生二进制发送，可视情况进行记录；另，延迟队列的原生二进制发送的方法名为： `_._.xxxx` 其中xxxx为延迟时间，如：2min2s

## 驱动

此程序依赖队列驱动来进行使用，目前支持的驱动有以下几种，使用时请注意识别

| 驱动名称 | 定制驱动位置 | 备注 |
| ------ | ------ | ------ |
| 默认 | -- | 默认直接使用go携程进行驱动支持，该驱动仅限程序内调用，且容易造成数据丢失等问题 |
| nsq | individual/xqueue/dervice/xqueuensq | 使用go开发的nsq驱动进行队列识别开发 |

## 使用方法

本组件默认直接使用 `go` 携程进行执行，所以可以不使用外部队列程序进行支持；不过鉴于数据安全性考量，还是建议使用外部队列程序进行支持。

1. 配置外部队列
```go
// 设置外部驱动
// 此处需要传入驱动类型，暂定义为使用nsq进行队列传输<暂定使用Open(监听/写入双工)>
xqueue.SetDervice(xqueuensq.Open("127.0.0.1:4161", "test-topic", "test-channel", "127.0.0.1:4150", "test-topic", 10))
```
2. 设置队列日志记录
```go
xqueue.SetLogInsert(func(log *xqueue.QueueLog){
    // 日志相关操作
})
```
3. 设置程序监听
```go
// 直接进行添加(含说明方法)
xqueue.Add("<监听方法>",&xqueue.FuncConf{})
// 使用Map的形式进行批量添加
xqueue.AddMap(map[string]func(param []byte) error{
    // "<队列标识[:队列标识名称][:队列标识描述]>": <具体执行的方法>
})
```
4. 设置队列监听(启动队列)(最后)
```go
go xqueue.Listen()
```

## 使用本组件的队列发送事件

同依赖下发送队列时较为简单，直接使用 `xqueue.Set()` 方法进行发送即可。向其他主题进行发放时也较为简单，使用以下方法即可进行发送

```go
// 声明其他队列信息
// 此处需要传入驱动类型，暂定义为使用nsq进行队列传输<暂定使用OpenSet(仅推送)>
xq := xqueue.WithDervice(xqueuensq.OpenSet("127.0.0.1:4150","test-topic"))
// 发送任务
xq.Set("方法名",map[string]any{"desc":"xxxx参数"})
```

## 其他函数/程序向本组件发送队列

```js
// 因各程序的队列声明方式不同，所以此处仅提供参数的传递结构
// 向本程序配置的监听topic中发送消息，具体结构如下
// PS: 本组件直接向其他程序发送消息时也会按照此格式进行发送，若格式不同的话，请调用其他方法进行发送
{
    "func": "listen.func",// 程序配置的监听方法
    "param": {},// 方法传递的具体参数
    "send_time": 0,// 队列的入队时间，非必填 Unix时间戳
    "run_time": 0// 期望运行时间 非必填 Unix时间戳 默认立即运行
}
```