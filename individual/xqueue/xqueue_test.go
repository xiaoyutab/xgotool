package xqueue_test

import (
	"errors"
	"testing"
	"time"

	"gitee.com/xiaoyutab/xgotool/individual/xqueue"
	"gitee.com/xiaoyutab/xgotool/individual/xqueue/dervice/xqueuensq"
	"gitee.com/xiaoyutab/xgotool/individual/xqueue/dervice/xqueueredis"
)

func TestQueueRedis(t *testing.T) {
	xqueue.SetDervice(xqueueredis.OpenList("127.0.0.1:6379", 10, "", "test-queue"))
	xqueue.Add("echo", &xqueue.FuncConf{
		Func: func(param []byte) error {
			t.Log(string(param))
			return errors.New("测试错误")
		},
	})
	go xqueue.Listen()
	xqueue.Set("echo", "demo——测试")
	time.Sleep(time.Second * 10)
}

func TestQlSet(t *testing.T) {
	q1 := xqueuensq.Open("127.0.0.1:4161", "test-topic", "test-channel", "127.0.0.1:4150", "test-topic", 10)
	xqueue.SetDervice(q1)
	xqueue.Add("echo", &xqueue.FuncConf{
		Name: "输出内容",
		Desc: "输出内容信息",
		Func: func(param []byte) error {
			t.Log("queue", string(param))
			return nil
		},
	})
	go xqueue.Listen()
	// xqueue.WithDervice(q1).Set("echo", "输出测试内容信息ces测试内容")
	time.Sleep(time.Second * 10)
}
