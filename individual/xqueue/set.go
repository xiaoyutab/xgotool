package xqueue

// 设置事务驱动
//
//	c	事务驱动
func SetDervice(c Queue) {
	_default = c
}

// 重新设置日志写入事件
//
//	c	写入日志函数操作
func SetLogInsert(c func(log *QueueLog)) {
	log_insert = c
}

// 停止队列监听
func Stop() {
	if _default != nil {
		_default.Stop()
	}
}
