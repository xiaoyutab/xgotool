// 通用队列组件（此组件会牺牲部分队列的执行效率，用以建立相对统一的队列处理方案）
package xqueue

import (
	"sync"
	"time"
)

// 队列实现驱动
type Queue interface {
	Set(dat []byte) error                     // 推送队列任务
	SetDef(dat []byte, t time.Duration) error // 推送延时任务
	Listen(f func(dat []byte) error) error    // 队列监听任务
	Stop()                                    // 停止队列监听
}

// 事件驱动 nil表示直接使用 go xxx 进行调用执行
var _default Queue

// 监听队列列表
var queue_list = map[string]FuncConf{}

// 队列操作锁
var queue_list_lock sync.Mutex

// 队列中实际存储的数据结构信息
type queue_struct struct {
	Func     string `json:"func"`      // 调用方法
	Param    any    `json:"param"`     // 传入参数
	SendTime int64  `json:"send_time"` // 入队时间
	RunTime  int64  `json:"run_time"`  // 期望运行时间
}

// 队列配置结构
type FuncConf struct {
	Name string                   // 队列任务名称
	Desc string                   // 队列任务描述
	Func func(param []byte) error // 执行的函数操作
}
