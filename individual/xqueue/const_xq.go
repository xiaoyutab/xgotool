package xqueue

import (
	"time"
)

// 队列中继操作
type Xq struct {
	qs Queue // 实现队列的操作驱动
}

// 写入队列【若未开启队列的话，将不管队列方法执行的结果，即程序会运行0~1次，即使是失败，所以建议开启NSQ队列，用以保证任务能成功执行】
//
//	fun		写入的服务名称，对应读取时的名称配置
//	param	传入的参数，对应读取时的参数结构
func (xq *Xq) Set(funs string, param any) error {
	return queue_set(funs, param, xq.qs)
}

// 写入队列 直接写入二进制的方法，不会进行外层格式封装，一般用于外部程序通信使用
//
//	byt	待发送的消息内容
func (xq *Xq) SetByte(byt []byte) error {
	return queue_set_byte(byt, xq.qs)
}

// 写入队列-延迟执行
//
//	fun		写入的服务名称，对应读取时的名称配置
//	param	传入的参数，对应读取时的参数结构
//	def		延迟时间
func (xq *Xq) SetDef(funs string, param any, def time.Duration) error {
	return queue_set_def(funs, param, def, xq.qs)
}

// 写入队列-延迟队列 直接写入二进制的方法，不会进行外层格式封装，一般用于外部程序通信使用
//
//	byt	待发送的消息内容
//	def	延迟时间
func (xq *Xq) SetDefByte(byt []byte, def time.Duration) error {
	return queue_set_def_byte(byt, def, xq.qs)
}
