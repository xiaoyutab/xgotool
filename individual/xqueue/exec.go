package xqueue

import (
	"strings"
	"time"
)

// 对外提供的执行操作函数

// 添加Queue队列
//
//	funs	队列下标
//	conf	推入队列列表
func Add(funs string, conf *FuncConf) {
	queue_add(funs, conf)
}

// 循环添加Queue队列
//
//	funs	map[<队列标识[:队列标识名称][:队列标识描述]>]运行的队列监听函数
func AddMap(funs map[string]func(param []byte) error) {
	for k, v := range funs {
		spl := strings.Split(k, ":")
		conf := FuncConf{
			Func: v,
		}
		if len(spl) == 2 {
			// 仅下标+名称
			conf.Name = spl[1]
			k = spl[0]
		} else if len(spl) > 2 {
			// 存在下标、名称、描述等
			conf.Name = spl[1]
			conf.Desc = strings.Join(spl[2:], ":")
			k = spl[0]
		}
		Add(k, &conf)
	}
}

// 获取已插入的队列列表
func List() map[string]FuncConf {
	return queue_list
}

// 移除队列任务
//
//	fun	队列下标
func Remove(fun string) {
	queue_remove(fun)
}

// 写入队列【若未开启队列的话，将不管队列方法执行的结果，即程序会运行0~1次，即使是失败，所以建议开启NSQ队列，用以保证任务能成功执行】
//
//	fun		写入的服务名称，对应读取时的名称配置
//	param	传入的参数，对应读取时的参数结构
func Set(funs string, param any) error {
	return queue_set(funs, param, _default)
}

// 写入队列-延迟执行
//
//	fun		写入的服务名称，对应读取时的名称配置
//	param	传入的参数，对应读取时的参数结构
//	def		延迟时间
func SetDef(funs string, param any, def time.Duration) error {
	return queue_set_def(funs, param, def, _default)
}

// 队列监听操作
// 队列最终的监听操作，此处通过执行 go Listen() 来进行队列的监听
// 若不需要监听，只需要推送的话，则不需要执行此函数
func Listen() {
	queue_listen()
}

// 以指定驱动运行操作
//
//	c	指定的运行队列
func WithDervice(c Queue) *Xq {
	return &Xq{
		qs: c,
	}
}
