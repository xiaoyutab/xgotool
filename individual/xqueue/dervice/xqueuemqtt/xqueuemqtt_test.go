package xqueuemqtt_test

import (
	"testing"

	"gitee.com/xiaoyutab/xgotool/individual/xqueue"
	"gitee.com/xiaoyutab/xgotool/individual/xqueue/dervice/xqueuemqtt"
)

func TestMqtt(t *testing.T) {
	cli := xqueuemqtt.Open(&xqueuemqtt.Mqtts{
		Addr:  "tcp://127.0.0.1:1883",
		Topic: "test/demo",
		User:  "golang",
		Pass:  "123456",
	})
	xqueue.SetDervice(cli)
	xqueue.Set("xque", "222222")
}
func TestMqttListen(t *testing.T) {
	cli := xqueuemqtt.Open(&xqueuemqtt.Mqtts{
		Addr:  "tcp://127.0.0.1:1883",
		Topic: "test/demo",
		User:  "golang",
		Pass:  "123456",
	})
	xqueue.SetDervice(cli)
	xqueue.AddMap(map[string]func(param []byte) error{
		"xque": func(param []byte) error {
			t.Log(string(param))
			return nil
		},
	})
	xqueue.Listen()
}
