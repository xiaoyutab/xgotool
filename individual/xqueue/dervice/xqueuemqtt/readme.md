# MQTT消息队列服务

此服务为基于MQTT协议实现的消息队列服务。

## 功能

- 发布消息
- 订阅消息

## 使用
此服务支持Get和Set两种模式，分别用于发布消息和订阅消息，且因为读写配置相同，所以此处暂未拆分出读写模式，只在使用时进行识别（使用了Linten即使用了监听模式）

打开方式： `xqueuemqtt.Open()`

参数释义(包含GET和SET的全部参数)：
```go
// 获取MQTT协议客户端配置
func Open(&xqueuemqtt.Mqtts{
    Addr:  "tcp://127.0.0.1:1883",// MQTT服务器地址
    Topic: "test/demo",// 主题
    User:  "golang",// 用户名
    Pass:  "123456",// 密码
    ClientId: "",// 客户端ID，传空则使用xqueue_mqtt_<时间>作为客户端ID
    Type: "tcp",// 连接类型，空则从Addr中进行提取判断
    Qos: 0,// 服务质量，0/1/2，0表示最多一次，1表示至少一次，2表示只有一次
    Retain: false,// 是否保留消息，true表示保留，false表示不保留
}) xqueue.Queue{}
```

此外，此服务还支持直接获取客户端进行个性化操作的方式，如：
```go
cli := xqueuemqttOpen(&xqueuemqtt.Mqtts{
    Addr:  "tcp://127.0.0.1:1883",// MQTT服务器地址
    Topic: "test/demo",// 主题
    User:  "golang",// 用户名
    Pass:  "123456",// 密码
    ClientId: "",// 客户端ID，传空则使用xqueue_mqtt_<时间>作为客户端ID
    Type: "tcp",// 连接类型，空则从Addr中进行提取判断
    Qos: 0,// 服务质量，0/1/2，0表示最多一次，1表示至少一次，2表示只有一次
    Retain: false,// 是否保留消息，true表示保留，false表示不保留
})

// 获取MQTT客户端
client := cli.Client()

// 关闭客户端
cli.Close()
```