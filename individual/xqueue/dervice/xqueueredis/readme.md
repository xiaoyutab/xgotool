# xqueue 队列支持 redis驱动

使用 redis 作为队列的驱动，实现消息队列功能。

redis支持的数据中，存在多种数据结构支持队列的实现

## list 列表式队列实现

使用redis的list结构实现队列，redis的list结构是一个双向链表，可以从两端插入和删除元素，因此可以实现队列的先进先出特性。

此处采用的为左进右出，即先进先出。

左进：lpush key value
右出：rpop key

```go
// 打开队列
//
//	addr	Redis地址
//	db		Redis数据库
//	pass	Redis密码
//	qname	队列名称
//	push	推送/弹出方向，默认为0(只传1个参数则只修改push方向，超过两个参数则忽略)
//			push[0]为推送方向，0为左侧，1为右侧
//			push[1]为弹出方向，0为右侧，1为左侧
func OpenList(addr string, db int, pass string, qname string, push ...int) xqueue.Queue {}

// 打开先进后出的队列(左进左出，也可以改为 1,0-右进右出)
dev := OpenList("",0,"","<quest_name>",0,1)
```
