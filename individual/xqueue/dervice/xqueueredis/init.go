package xqueueredis

import (
	"sync"
	"time"
)

// 错误重试次数
const ErrorNum = 6

// 次数缓存确定
var _cache_redis_client sync.Map

// 重试间隔时间
var _sleep_time []time.Duration = []time.Duration{
	0,
	time.Second,      // 1s (第一次重试)
	time.Second * 2,  // 3s (第二次重试)
	time.Second * 3,  // 6s (第三次重试)
	time.Second * 4,  // 10s (第四次重试)
	time.Second * 5,  // 15s (第五次重试)
	time.Second * 15, // 30s (第六次重试)
}
