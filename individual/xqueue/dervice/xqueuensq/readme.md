# NSQ定制化驱动

此驱动实现了 `xqueue.Queue` 接口，可嵌入到 `xqueue` 中进行使用。

支持以下几种打开方法

## 只读模式

只读模式使用 `xqueuensq.OpenGet()` 进行打开，此方法会返回一个 `xqueuensq.Queue` 接口，但是仅实现了接口中的 `listen` 监听操作，在调用 `Set` 方法时会返回对应错误

参数释义：
```go
// 获取仅监听NSQ的xqueue驱动
//
//	get			监听队列端口 一般为127.0.0.1:4161 (此处填写的是NSQ的HTTP端口，为组件 nsqlookupd  监听的端口)
//	gettopic	监听队列的通道
//	getchannel	监听队列的客户端
//	max_error	最大错误重试次数
func OpenGet(get, gettopic, getchannel string, max_error uint16) xqueue.Queue{}
```

## 只写模式

同理，只写模式只支持 `Set` 的方法，其他方法会返回对应错误信息

打开方式： `xqueuensq.OpenSet()`

参数释义：
```go
// 获取仅推送的Nsq配置的xqueue驱动
//
//	set			发送队列的端口 一般为127.0.0.1:4150 (此处填写的为NSQ的 TCP 端口，而非HTTP端口，为组件 nsqd 监听的端口)
//	settopic	发送队列的通道
func OpenSet(set, settopic string) xqueue.Queue {}
```

## 读写模式

读写模式支持 `Set` 和 `Listen` 方法，基本支持 `xqueue` 的全部队列操作，一般为项目中主体使用

打开方式： `xqueuensq.Open()`

参数释义(包含GET和SET的全部参数)：
```go
// 获取完整的NSQ的xqueue驱动
//
//	get			监听队列端口 一般为127.0.0.1:4161 (此处填写的是NSQ的HTTP端口，为组件 nsqlookupd  监听的端口)
//	gettopic	监听队列的通道
//	getchannel	监听队列的客户端
//	set			发送队列的端口 一般为127.0.0.1:4150 (此处填写的为NSQ的 TCP 端口，而非HTTP端口，为组件 nsqd 监听的端口)
//	settopic	发送队列的通道
//	max_error	最大错误重试次数
func Open(get, gettopic, getchannel, set, settopic string, max_error uint16) xqueue.Queue {}
```