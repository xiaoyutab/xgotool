package xginplus

// 设置默认配置项
//
//	opt	配置项选项，WithXXX选项
func SetDefault(opt ...OptionFunc) {
	for i := 0; i < len(opt); i++ {
		opt[i](&_default)
	}
}
