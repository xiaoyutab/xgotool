package xginplus

import "time"

type ContextOptionFunc func(c *Context)

// 设置当前接口启用/禁用抖动过滤
//
//	jit	是否启用抖动过滤
func WithContextJitter(jit bool) ContextOptionFunc {
	return func(c *Context) {
		c.Jitter = jit
	}
}

// 设置缓存时间(默认缓存1秒)
// 此设置值会影响网络抖动的时间间隔
// 原始配置：1s内同样的结果只处理一次
// 修改时长后，会变成 指定时长内同样的请求结果只会处理一次
// PS: 此处的缓存命中方式涉及到：请求方式、请求路径、客户端IP、客户端UA、客户端ContentType、客户端发送的POST原始参数
//
//	ts	指定时长
func WithContextJitterTime(ts time.Duration) ContextOptionFunc {
	return func(c *Context) {
		c.JitterTime = ts
	}
}

// 设置接口的超时时间
//
//	ts	指定时长 0-不限制超时时间
func WithContextTimeOut(ts time.Duration) ContextOptionFunc {
	return func(c *Context) {
		c.TimeOut = ts
	}
}
