package xginplus_test

import (
	"testing"

	"gitee.com/xiaoyutab/xgotool/individual/xginplus"
)

func TestRBind(t *testing.T) {
	type temp_struct struct {
		UserName string `json:"user_name" form:"user_name" xml:"user_name"` // 登录用户名
		Password string `json:"password" form:"password" xml:"password"`    // 用户密码
		Res      int    `json:"res" form:"res" xml:"res"`
	}
	temp := temp_struct{}
	res := []byte(`{"user_name": "admin","password": 123123, "res": 2}`)
	err := xginplus.Bind(res, &temp)
	if err != nil {
		t.Error(err)
	}
	t.Log(temp)
}
