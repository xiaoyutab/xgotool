// 长连接相关依赖支持
// 继承外部 xginplus 组件
package xginplusws

import (
	"gitee.com/xiaoyutab/xgotool/individual/xginplus"
)

// WS消息结构体支持
type Websocket interface {
	// WS链接打开事件
	Open(*xginplus.Context) error
	// WS链接接收消息事件
	Pull(*xginplus.Context, []byte) error
	// WS链接发送消息事件
	// 此消息的返回值为给对应客户端发送的消息
	// any 为json串行化后发送给客户端的消息，bool来判断是否需要发送给客户端
	Push(*xginplus.Context) (any, bool)
	// WS链接意外关闭事件
	Close(*xginplus.Context)
}
