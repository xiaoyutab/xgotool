// Gin组件个人增强部分
package xginplus

import (
	"encoding/json"
	"time"

	"github.com/gin-gonic/gin"
)

// 插件配置项
type Config struct {
	SuccessData   func(c any) (int, any)                                        // 成功数据结构返回
	SuccessPage   func(c *Context, count int64, data any, ext gin.H) (int, any) // 成功的列表页返回的数据(此处传入 Context 的目的是为了方便扩展时的附加分页计算)
	ErrorData     func(err error) (int, any)                                    // 失败数据结构返回
	ErrorDataCode func(code int, err error) (int, any)                          // 失败数据结构返回
	Jitter        bool                                                          // 是否启用网络抖动过滤
	LimitOriginal int64                                                         // limit传入多少时原样返回
	DefaultTime   time.Duration                                                 // 默认超时时间 0-不限制
	CodeTimeOut   int                                                           // 超时返回码
}

// 默认配置项值
var _default Config = Config{
	CodeTimeOut:   500, // 默认超时返回码
	LimitOriginal: 1,   // 默认值1表示忽略此参数值（因为正数本来就是会原样返回的）
	SuccessData: func(c any) (int, any) {
		return 200, gin.H{
			"flag": true,
			"data": c,
		}
	},
	SuccessPage: func(c *Context, count int64, data any, ext gin.H) (int, any) {
		if len(ext) == 0 {
			return 200, gin.H{
				"flag":  true,
				"count": count,
				"data":  data,
			}
		}
		return 200, gin.H{
			"flag":  true,
			"count": count,
			"data":  data,
			"ext":   ext,
		}
	},
	ErrorData: func(err error) (int, any) {
		if err != nil {
			return 500, gin.H{
				"flag": false,
				"msg":  err.Error(),
			}
		}
		return 500, gin.H{
			"flag": false,
			"msg":  "系统错误",
		}
	},
	ErrorDataCode: func(code int, err error) (int, any) {
		if err != nil {
			return 500, gin.H{
				"flag": false,
				"msg":  err.Error(),
			}
		}
		return 500, gin.H{
			"flag": false,
			"msg":  "系统错误",
		}
	},
} // 默认配置项

// json.Number 转 int 类型(含小于0判断，用于分页处使用)
//
//	c	传入的数字
//	cs	是否判定附加值
func jint(c json.Number, cs bool) int {
	n, err := c.Int64()
	if err != nil {
		return 0
	}
	if cs && n == _default.LimitOriginal {
		return int(n)
	} else if n <= 0 {
		return 0
	}
	return int(n)
}
