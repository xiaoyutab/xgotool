package xginplus

import "sync"

type locks struct {
	base sync.Mutex      // 基础锁
	mps  map[string]bool // 对锁(因 sync.Mutex 锁具有不可复制属性，所以此处使用 bool 来实现对锁结构)
}

var cnt_lock locks = locks{}

// 锁定
// 因对锁的不可复制属性，所以此处需要使用处进行配合，检测到返回 false 时进行等待，直到返回true
func (c *locks) Lock(key string) bool {
	// 基础锁操作
	c.base.Lock()
	defer c.base.Unlock()
	// 对锁操作
	if _, ok := c.mps[key]; ok {
		return false
	}
	if c.mps == nil {
		c.mps = map[string]bool{
			key: true,
		}
	} else {
		c.mps[key] = true
	}
	return true
}

// 解锁
func (c *locks) UnLock(key string) {
	// 基础锁操作
	c.base.Lock()
	defer c.base.Unlock()
	// 对锁操作
	delete(c.mps, key)
}
