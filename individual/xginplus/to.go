package xginplus

import "github.com/gin-gonic/gin"

// 将 gin 的 Context 结构体转化为本框架的 Context 结构体
func To(c *gin.Context) *Context {
	return &Context{
		Context: c,
		TimeOut: _default.DefaultTime,
	}
}
