package xginplus

import (
	"time"

	"github.com/gin-gonic/gin"
)

type OptionFunc func(c *Config)

// 设置全接口启用/禁用抖动过滤
// 网络抖动的含义为同一秒内重复的请求
// 此处的抖动过滤的方案为：
// 1. 设置第一个请求的请求锁
// 2. 等待第一个请求的结果（若此期间进来第二个请求，第二个请求将会等待）
// 3. 第一个请求结果出来后，返回结果，并将结果进行缓存
// 4. 第n个请求进来后，直接读取缓存并返回
//
//	jit	是否启用抖动过滤
func WithJitter(jit bool) OptionFunc {
	return func(c *Config) {
		c.Jitter = jit
	}
}

// 一般成功数据处理结构
//
//	f	一般成功的处理函数
func WithSuccessData(f func(c any) (int, any)) OptionFunc {
	return func(c *Config) {
		c.SuccessData = f
	}
}

// 分页成功数据处理结构
//
//	f	一般成功的处理函数
func WithSuccessPage(f func(c *Context, count int64, data any, ext gin.H) (int, any)) OptionFunc {
	return func(c *Config) {
		c.SuccessPage = f
	}
}

// 错误数据格式化
//
//	f	处理错误数据的函数信息
func WithErrorData(f func(err error) (int, any)) OptionFunc {
	return func(c *Config) {
		_default.ErrorData = f
	}
}

// 设置Limit查询全部的条件
// 即用户传多少的时候才会查询全部（指 c.Limit() 原样返回）
//
//	mits	传入值，当传入limit为此值时， c.Limit() 原样返回
func WithLimitOriginal(mits int64) OptionFunc {
	return func(c *Config) {
		c.LimitOriginal = mits
	}
}

// 设置接口的默认超时时间
//
//	ts	指定时长 0-不限制超时时间
func WithDefaultTimeOut(ts time.Duration) OptionFunc {
	return func(c *Config) {
		c.DefaultTime = ts
	}
}

// 设置接口的指定状态码的超时时间
//
//	code	状态码
//	ts	指定时长 0-不限制超时时间
func WithCodeTimeOut(code int, ts time.Duration) OptionFunc {
	return func(c *Config) {
		c.CodeTimeOut = code
		c.DefaultTime = ts
	}
}
