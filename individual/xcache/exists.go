package xcache

// 判断变量是否存在
//
//	keys	待判断的变量名
func Exists(keys string) bool {
	// redis中可以使用 EXISTS 来进行判断，但是此处因为统一处理成string类型的json串，所以可直接使用GET来进行识别
	if c, err := Get(keys); err == nil && c != "" {
		return true
	}
	return false
}
