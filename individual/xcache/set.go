package xcache

import (
	"encoding/json"
	"strconv"
	"time"
)

// 设置Key的值
//
//	key	下标
//	val	值
//	t	延时删除时间
func SetExt(key, val string, t time.Duration) error {
	// 定时移除
	go func() {
		if t.Hours() > 12 {
			time.Sleep(time.Hour * 12)
			Remove(key)
		} else {
			time.Sleep(t)
			Remove(key)
		}
	}()
	if _default.Inteface != nil {
		return _default.Inteface.Store(key, val, t)
	}
	_default.CacheSync.Store(key, val) // 存储到运行内存中
	return nil
}

// 设置Key的值
//
//	key	下标
//	val	值
func Set(key, val string) error {
	return SetExt(key, val, _default.DefaultTime)
}

// 设置数字类型缓存，避免使用时再进行fmt.Sprintf转化(因any类型可能为多种数字类型，所以此处使用fmt进行转化)
// 使用SetNumber存储，读取时也只能使用GetString进行读取，然后再通过xstring.Toxxxx进行转化【或者使用json.Number进行转化】
//
//	key	缓存下标
//	val	缓存值
func SetNumber(key string, val any) error {
	switch v := val.(type) {
	case int:
		return Set(key, strconv.Itoa(v))
	case int64:
		return Set(key, strconv.FormatInt(v, 10))
	case uint64:
		return Set(key, strconv.FormatUint(v, 10))
	case float64:
		return Set(key, strconv.FormatFloat(v, 'f', -1, 64))
	case float32:
		return Set(key, strconv.FormatFloat(float64(v), 'f', -1, 32))
	case int8:
		return Set(key, strconv.FormatInt(int64(v), 10))
	case uint8:
		return Set(key, strconv.FormatInt(int64(v), 10))
	case int16:
		return Set(key, strconv.FormatInt(int64(v), 10))
	case uint16:
		return Set(key, strconv.FormatInt(int64(v), 10))
	case int32:
		return Set(key, strconv.FormatInt(int64(v), 10))
	case uint32:
		return Set(key, strconv.FormatInt(int64(v), 10))
	case uint:
		return Set(key, strconv.FormatInt(int64(v), 10))
	case string:
		return Set(key, v)
	default:
		return SetStruct(key, v)
	}
}

// 设置结构体
//
//	key	缓存的key名称
//	val	设置的缓存值类型，使用json串行化进行解析存储
func SetStruct(key string, val any) error {
	str, err := json.Marshal(val)
	if err != nil {
		return err
	}
	// 默认设置2小时缓存
	return Set(key, string(str))
}

// 设置结构体
//
//	key	缓存的key名称
//	val	设置的缓存值类型，使用json串行化进行解析存储
//	t	缓存时间
func SetStructExt(key string, val any, t time.Duration) error {
	str, err := json.Marshal(val)
	if err != nil {
		return err
	}
	// 默认设置2小时缓存
	return SetExt(key, string(str), t)
}
