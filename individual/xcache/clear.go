package xcache

import "sync"

// 清空缓存数据
func Clear() error {
	_default.CacheSync = sync.Map{}
	if _default.Inteface != nil {
		return _default.Inteface.Clear()
	}
	return nil
}
