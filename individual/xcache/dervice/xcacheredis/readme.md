# xcache-redis

基于redis的缓存实现。

此驱动使用方法：

```go
// 第一种方法
der1 := xcacheredis.Redis{
    Host: "127.0.0.1",
    Port: 6379,
    Password: "",
    DB: 0,
}
xcache.SetInteface(&der1)

// 第二种方法
der2 := xcacheredis.Open("127.0.0.1", 6379, "", 0)
xcache.SetInteface(&der2)


// 此外，还支持直接提取客户端信息
cli := der2.GetCli()
```