package xcache_test

import (
	"encoding/json"
	"errors"
	"fmt"
	"runtime"
	"testing"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
)

// 测试缓存模块逻辑信息
func TestXcache(t *testing.T) {
	key := xcache.Key("asdasfasdascfescs")
	t.Log(key)
	// 存储缓存
	// xcache.Set(key, "这是存储的值")
	// 读取缓存
	t.Log(xcache.Get(key))
	// 判断缓存
	t.Log(xcache.Exists(key))
}

func TestJsonStruct(t *testing.T) {
	type ja struct {
		A int    `json:"a"`
		B string `json:"b"`
	}
	type jb struct {
		B json.Number `json:"b"`
		ja
	}

	j := `{"a":1,"b":"1"}`
	inf := jb{}
	if err := json.Unmarshal([]byte(j), &inf); err != nil {
		t.Error(err)
	}
	t.Logf("%T: %v", inf, inf)
	b, _ := json.Marshal(inf)
	t.Log(string(b))
	inf = jb{
		ja: ja{
			A: 1,
			B: "1",
		},
		B: json.Number("2"),
	}
	t.Logf("%T: %v", inf, inf)
	b, _ = json.Marshal(inf)
	t.Log(string(b))
}

type MyError struct {
	File string // 错误文件
	Line int    // 错误行号
	Msg  string // 错误描述
}

func (c *MyError) Error() string {
	return fmt.Sprintf("%s[%d]: %s", c.File, c.Line, c.Msg)
}

// 生成错误消息
func New(msg string) *MyError {
	_, file, line, ok := runtime.Caller(1)
	if !ok {
		return &MyError{Msg: msg}
	}
	return &MyError{
		File: file,
		Line: line,
		Msg:  msg,
	}
}

func Newerr(err error) *MyError {
	_, file, line, ok := runtime.Caller(1)
	if !ok {
		return &MyError{Msg: err.Error()}
	}
	return &MyError{
		File: file,
		Line: line,
		Msg:  err.Error(),
	}
}

func TestErrors(t *testing.T) {
	err := a()
	err1 := fmt.Errorf("封装一层以后：%w", err)
	e1 := errors.Unwrap(errors.Unwrap(errors.Unwrap(err1)))
	fmt.Println(e1)
	var e *MyError
	if errors.As(err1, &e) {
		t.Log(e.Error(), err1.Error())
	}
	t.Log(err)
}

func a() error {
	return New("这是测试错误信息")
}

func TestAaaaa(t *testing.T) {
	a := 10.234
	t.Log(int(a))
}

func TestGroup(t *testing.T) {
	cache_key := xcache.Key("asdasfas")
	xcache.Group("a", cache_key)
}

func TestKeys(t *testing.T) {
	t.Log(xcache.Key("5", map[string]any{
		"b":  "3",
		"aa": "2",
		"a":  "1",
		"bb": 4.1,
		"c":  5.0001,
		"cc": "6",
		"g": map[string]any{
			"z": 121213.901,
			"a": 111,
		},
	}, 1, 2, 33, 444))
}
