// 缓存模块
package xcache

import (
	"encoding/json"
	"sync"
	"time"
)

// 基础缓存接口实现
type CacheInteface interface {
	Store(name, value string, ext time.Duration) error // 存储缓存信息
	Load(name string) (string, error)                  // 读取缓存信息
	Delete(name string) error                          // 删除缓存信息【会先调用Load判断是否存在，基本不存在删除空数据问题】
	Clear() error                                      // 清空全部缓存
}

// 缓存操作配置项信息
type Config struct {
	Inteface      CacheInteface // 缓存读写的操作类
	DefaultTime   time.Duration // 默认缓存时长 2小时
	CacheSync     sync.Map      // 程序内缓存
	groupMaxLimit int           // 分组内缓存最大数量，超过该数量后将会触发整个分组的清空操作
	groupCounts   int           // 分组数量限制，即缓存中允许存在最多多少个分组
}

var _default Config = Config{
	CacheSync:     sync.Map{},
	DefaultTime:   time.Hour * 2,
	groupMaxLimit: 100,
	groupCounts:   100,
}

// 设置存储驱动
// 默认使用 sync.Map 进行缓存读写，若无特殊需求，不建议使用此操作迁出缓存操作类
//
//	c	存储缓存的驱动标识
func SetInteface(c CacheInteface) {
	_default.Inteface = c
}

// 设置默认缓存时长
//
//	c	默认的缓存时长，默认0-不超时
func SetDefaultTime(c time.Duration) {
	_default.DefaultTime = c
}

// 设置缓存组中的数量限制
//
//	child	分组内列表的最大数量限制
//	self	外层组数量最大限制
func SetGroup(child, self int) {
	_default.groupMaxLimit = child
	_default.groupCounts = self
}

// 通过JSON形式将类型转换
//
//	in	输入的类型
//	out	输出的类型
func any2any(in, out any) error {
	b, err := json.Marshal(in)
	if err != nil {
		return err
	}
	err = json.Unmarshal(b, out)
	if err != nil {
		return err
	}
	return nil
}
