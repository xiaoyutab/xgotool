package xcache

import (
	"encoding/json"
	"errors"
)

// 获取key的值
//
//	key	要获取的key下标
func Get(key string) (string, error) {
	if _default.Inteface != nil {
		return _default.Inteface.Load(key)
	}
	if v, ok := _default.CacheSync.Load(key); ok {
		return v.(string), nil
	}
	return "", nil
}

// 获取KEY设置的值
//
//	key	要获取的key下标
func GetString(key string) string {
	if v, err := Get(key); err == nil {
		if len(v) > 1 && v[0] == '"' {
			// 如果字符串是以"开头和结尾的，则去除两侧的"
			v = v[1 : len(v)-1]
		}
		return v
	}
	return ""
}

// 读取结构体
//
//	key	缓存的key名称
//	val	预期读取的类型【使用json串行化进行反解析】
func GetStruct(key string, val any) error {
	str := GetString(key)
	if str == "" {
		return errors.New("缓存为空或缓存不存在")
	}
	return json.Unmarshal([]byte(str), val)
}
