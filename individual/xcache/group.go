package xcache

import (
	"sync"
)

var groupsLock sync.RWMutex
var groupsValue map[string][]string = map[string][]string{}

// 将缓存下标追加到缓存组中，并返回该下标
//
//	g	分组名称
//	arg	任意类型参数，用于进行MD5加密，返回缓存下标信息
func Gkey(g string, arg ...any) string {
	key := Key(arg...)
	Group(g, key)
	return key
}

// 将属性追加到分组中，以便后续的分组清理操作
//
//	g	分组名称
//	key	追加的key列表
func Group(g, key string) {
	groupsLock.Lock()
	defer groupsLock.Unlock()
	// 判断，如果数组为空的话，先一步从缓存中进行读取
	if len(groupsValue) == 0 {
		if err := GetStruct("__xcache_group", &groupsValue); err != nil {
			groupsValue = map[string][]string{
				"xcache": {}, // 占位组，避免下次判定还走缓存读取
			}
		}
	}
	defer func() {
		// 该函数执行完成后，将 gv 重新写入到缓存中去
		go SetStruct("__xcache_group", &groupsValue)
	}()
	if v, ok := groupsValue[g]; ok {
		if inString(key, v) {
			return
		}
		if len(v) < _default.groupMaxLimit {
			v = append(v, key)
			groupsValue[g] = v
			return
		}
		// 如果到达上限了，就清空该分组缓存，进行重新添加
		for i := 0; i < len(v); i++ {
			Remove(v[i])
		}
		groupsValue[g] = []string{key}
		return
	}
	// 在group列表中不存在的话
	if len(groupsValue) > _default.groupCounts {
		// 随机移除掉一半分组，再行追加
		cnt := 0
		for k, v := range groupsValue {
			if cnt >= _default.groupCounts/2 {
				break
			}
			delete(groupsValue, k)
			for i := 0; i < len(v); i++ {
				Remove(v[i])
			}
			cnt++
		}
	} // 直接进行添加
	groupsValue[g] = []string{key}
}

// 分组清理
//
//	g	分组名称
func GroupRemove(g string) {
	groupsLock.RLock()
	defer groupsLock.RUnlock()
	if v, ok := groupsValue[g]; ok {
		for i := 0; i < len(v); i++ {
			Remove(v[i])
		}
		delete(groupsValue, g)
	}
}

// 判断k在v中是否存在
//
//	k	待判断下标
//	v	目标数组
func inString(k string, v []string) bool {
	for i := 0; i < len(v); i++ {
		if k == v[i] {
			return true
		}
	}
	return false
}
