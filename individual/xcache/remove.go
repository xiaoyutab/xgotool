package xcache

// 移除缓存下标信息
//
//	key	要移除的缓存下标
func Remove(key string) error {
	if Exists(key) {
		if _default.Inteface != nil {
			return _default.Inteface.Delete(key)
		}
		_default.CacheSync.Delete(key)
	}
	return nil
}
