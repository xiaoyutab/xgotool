package xlog

// 记录详情日志
//
//	msg	消息备注
//	err	错误详情
func Debug(msg string, err error) {
	saveLog('D', "", 0, msg, err, true)
}

// 记录详情日志
//
//	msg	消息备注
//	err	错误详情
func DebugError(msg string, err error) error {
	return saveLog('D', "", 0, msg, err, true)
}

// 记录详情日志
//
//	msg	消息备注
//	err	错误详情
func DE(msg string, err error) error {
	return saveLog('D', "", 0, msg, err, true)
}
