package xlog

// 记录详情日志
//
//	msg	消息备注
//	err	错误详情
func Error(msg string, err error) {
	saveLog('E', "", 0, msg, err, true)
}

// 记录详情日志
//
//	msg	消息备注
//	err	错误详情
func ErrorError(msg string, err error) error {
	return saveLog('E', "", 0, msg, err, true)
}

// 记录详情日志
//
//	msg	消息备注
//	err	错误详情
func EE(msg string, err error) error {
	return saveLog('E', "", 0, msg, err, true)
}
