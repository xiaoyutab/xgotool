package xlog

// 记录详情日志
//
//	msg	消息备注
//	err	错误详情
func Info(msg string, err error) {
	saveLog('I', "", 0, msg, err, true)
}

// 记录详情日志
//
//	msg	消息备注
//	err	错误详情
func InfoError(msg string, err error) error {
	return saveLog('I', "", 0, msg, err, true)
}

// 记录详情日志
//
//	msg	消息备注
//	err	错误详情
func IE(msg string, err error) error {
	return saveLog('I', "", 0, msg, err, true)
}
