package xlog

// 记录详情日志
//
//	msg	消息备注
//	err	错误详情
func Crit(msg string, err error) {
	saveLog('C', "", 0, msg, err, true)
}

// 记录详情日志
//
//	msg	消息备注
//	err	错误详情
func CritError(msg string, err error) error {
	return saveLog('C', "", 0, msg, err, true)
}

// 记录详情日志
//
//	msg	消息备注
//	err	错误详情
func CE(msg string, err error) error {
	return saveLog('C', "", 0, msg, err, true)
}
