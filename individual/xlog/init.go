// 日志记录模块
package xlog

// 日志记录模块使用GO携程进行日志记录（此形式可能会存在退出的时候谢程未全部退出的问题，该问题会造成部分日志记录失败的情况）
// 错误等级：Info -> Notice -> Warning -> Error -> Crit -> Alert -> Panic
// 错误等级简写：I -> N -> W -> E -> C -> A -> P
// 备注：此等级中，Crit、Alert、Panic错误为影响到程序正常运行的错误，Error为需要注意，即将影响到正常运行的错误
//
//	Panic等级的错误也只是记录到数据库中，而不是直接将程序进行panic退出
//	记录函数的执行时常
//	调用方法为：defer xlog.FTime(time.Now())
//
// 配置项
type Config struct {
	SaveInterface LogSave // 不记录到DB数据库中，而是直接调用接口进行记录【一旦传入此接口，数据库中将不再进行插入】
	Console       bool    // 是否输出到控制台中
}

// 日志写入接口
type LogSave interface {
	Log(c *LogStruct)     // 记录正常日志信息
	Quest(c *QuestStruct) // 记录Quest的Http请求日志
	Func(c *FuncLog)      // 记录Func的函数调用耗时日志
}

// 默认配置
var _default Config = Config{}

// 错误信息统计表
type LogStruct struct {
	Id        uint64 `gorm:"column:id;primaryKey;autoIncrement" json:"id" form:"id"`                   // 条目ID
	Type      string `gorm:"column:type;size:1;comment:错误类型;index:error_type" json:"type" form:"type"` //错误类型，如：I、D等
	File      string `gorm:"column:file;size:200;comment:错误文件" json:"file" form:"file"`                //错误文件
	Line      uint   `gorm:"column:line;comment:文件行数;index:error_type" json:"line" form:"line"`        //错误行数
	Msg       string `gorm:"column:msg;size:200;comment:消息概述" json:"msg" form:"msg"`                   //错误消息概述
	Content   string `gorm:"column:content;comment:错误消息内容" json:"content" form:"content"`              //错误信息内容
	CreatedAt string `gorm:"column:created_at;comment:创建时间" json:"created_at" form:"created_at"`       //错误发生时间
}

// 函数耗时日志表
type FuncLog struct {
	Id        uint64 `gorm:"column:id;primaryKey;autoIncrement" json:"id" form:"id"`             // 条目ID
	Func      string `gorm:"column:func;size:200;comment:函数名称" json:"func" form:"func"`          //函数名称
	Line      uint   `gorm:"column:line;comment:调用行数" json:"line" form:"line"`                   //调用行数
	Runtime   uint64 `gorm:"column:runtime;comment:耗时：ns" json:"runtime" form:"runtime"`         // 耗时：ns
	CreatedAt string `gorm:"column:created_at;comment:调用时间" json:"created_at" form:"created_at"` //调用时间
}

// https请求日志记录
type QuestStruct struct {
	Id        uint   `gorm:"column:id;primaryKey;autoIncrement" form:"id" json:"id"`
	Uri       string `gorm:"column:uri;size:200;comment:HTTP请求网址" form:"uri" json:"uri"`                        //HTTP请求网址
	Param     string `gorm:"column:param;comment:请求参数[JSON格式存储]" form:"param" json:"param"`                     //请求参数[JSON格式存储]
	HttpCode  int    `gorm:"column:http_code;comment:HTTP请求返回的Code值" form:"http_code" json:"http_code"`         //HTTP请求返回的Code值
	Body      string `gorm:"column:body;comment:HTTP请求返回值" form:"body" json:"body"`                             //HTTP请求返回值
	Error     string `gorm:"column:error;size:200;comment:HTTP请求报错信息" form:"error" json:"error"`                //HTTP请求报错信息
	Header    string `gorm:"column:header;comment:HTTP请求时发送的Header请求头" form:"header" json:"header"`             //HTTP请求时发送的Header请求头
	StartTime string `gorm:"column:start_time;comment:HTTP请求开始时间" form:"start_time" json:"start_time"`          //HTTP请求开始时间
	EndTime   string `gorm:"column:end_time;comment:HTTP请求结束时间" form:"end_time" json:"end_time"`                //HTTP请求结束时间
	QuestSec  uint   `gorm:"column:quest_sec;comment:请求耗时 单位：毫秒" form:"quest_sec" json:"quest_sec"`             //请求耗时 单位：毫秒
	ClientIp  string `gorm:"column:client_ip;size:60;comment:客户端IP地址，兼容IPV6" form:"client_ip" json:"client_ip"` // 客户端IP地址，兼容IPV6
}
