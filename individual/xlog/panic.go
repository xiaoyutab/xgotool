package xlog

// 记录详情日志
//
//	msg	消息备注
//	err	错误详情
func Panic(msg string, err error) {
	saveLog('P', "", 0, msg, err, true)
}

// 记录详情日志
//
//	msg	消息备注
//	err	错误详情
func PanicError(msg string, err error) error {
	return saveLog('P', "", 0, msg, err, true)
}

// 记录详情日志
//
//	msg	消息备注
//	err	错误详情
func PE(msg string, err error) error {
	return saveLog('P', "", 0, msg, err, true)
}
