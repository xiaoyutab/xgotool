package xlog

import "errors"

// 记录用户日志
//
//	uid		用户ID
//	typ		记录类型
//	info	记录的详细信息
func User(uid uint, typ, info string) {
	saveLog('U', "", uid, typ, errors.New(info), false)
}
