package xlog

// 记录详情日志
//
//	msg	消息备注
//	err	错误详情
func Warning(msg string, err error) {
	saveLog('W', "", 0, msg, err, true)
}

// 记录详情日志
//
//	msg	消息备注
//	err	错误详情
func WarningError(msg string, err error) error {
	return saveLog('W', "", 0, msg, err, true)
}

// 记录详情日志
//
//	msg	消息备注
//	err	错误详情
func WE(msg string, err error) error {
	return saveLog('W', "", 0, msg, err, true)
}
