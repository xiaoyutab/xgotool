package xlog

// 设置日志存储的接口信息
//
//	c	日志存储实现类
func SetInteface(c LogSave) {
	_default.SaveInterface = c
}

// 设置是否显示日志在控制台
//
//	c	是否在控制台显示
func SetShow(c bool) {
	_default.Console = c
}
