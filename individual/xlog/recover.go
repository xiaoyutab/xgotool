package xlog

import "errors"

// 程序Recover恢复及记录相关信息
// 此程序为放置在defer中，避免程序强退导致异常的消息记录，所以使用时请直接：defer xlog.Recover()
//
//	f	Recover时调用的函数列表，一般留空，最常用作用为panic时的特殊日志记录（除xlog.Panic记录外的其他记录）
func Recover(f ...func()) {
	e := recover()
	if e != nil {
		switch err := e.(type) {
		case error:
			Panic("程序异常", err)
		case string:
			// 因此处为defer记录，所以runtime.caller时需要再次往上调用一层
			Panic("程序异常", errors.New(err))
		}
	}
	// 调用传入的f函数
	for i := 0; i < len(f); i++ {
		f[i]()
	}
}
