package xlog

// 记录详情日志
//
//	msg	消息备注
//	err	错误详情
func Alert(msg string, err error) {
	saveLog('A', "", 0, msg, err, true)
}

// 记录详情日志
//
//	msg	消息备注
//	err	错误详情
func AlertError(msg string, err error) error {
	return saveLog('A', "", 0, msg, err, true)
}

// 记录详情日志[AlertError的简称]
//
//	msg	消息备注
//	err	错误详情
func AE(msg string, err error) error {
	return saveLog('A', "", 0, msg, err, true)
}
