package xlog

import (
	"encoding/json"
	"strings"
	"time"

	"gitee.com/xiaoyutab/xgotool/https"
)

// 记录HTTPS请求日志
//
//	注册HTTPS服务时将此函数注入进去
func Quest(h *https.CURL) {
	defer Ftime(time.Now())
	// 基础数据赋值
	q := QuestStruct{
		Uri:      h.URI,
		HttpCode: h.HttpCode,
		Body:     h.Body,
	}
	// 特殊数组赋值
	if len(h.ParamQuest) > 0 {
		// 参数不为空
		temp, err := json.Marshal(h.ParamQuest)
		if err != nil {
			Error("Quest日志存储-Param JSON转化失败", err)
			return
		}
		q.Param = string(temp)
	}
	if len(h.ParamJsonQuest) > 0 {
		// 参数不为空
		temp, err := json.Marshal(h.ParamJsonQuest)
		if err != nil {
			Error("Quest日志存储-ParamJson JSON转化失败", err)
			return
		}
		q.Param = string(temp)
	}
	if len(h.HeaderQuest) > 0 {
		// 参数不为空
		temp, err := json.Marshal(h.HeaderQuest)
		if err != nil {
			Error("Quest日志存储-Header JSON转化失败", err)
			return
		}
		q.Header = string(temp)
	}
	q.StartTime = h.StartTime.Format(time.DateTime)
	q.EndTime = h.EndTime.Format(time.DateTime)
	q.QuestSec = uint(h.EndTime.Sub(h.StartTime).Milliseconds())
	// 空参数清除
	if q.Param == "{}" || q.Param == "{\"_\":{}}" {
		q.Param = ""
	} else if strings.Contains(q.Param, "\"_\":") {
		// 如果参数中存在{"_":xxxx}，就在此处去除外部的下标
		q.Param = q.Param[5 : len(q.Param)-1]
	}
	if q.Body == "{}" || q.Body == "null" {
		q.Body = ""
	}
	if q.Header == "{}" || q.Header == "null" {
		q.Header = ""
	}
	q.ClientIp = h.ClientIP
	if _default.SaveInterface != nil {
		_default.SaveInterface.Quest(&q)
		return
	}
}

// 记录其他符合规则的HTTP请求，JSON格式化后结构需要为https.CURL结构
//
//	s	JSON字节码
func QuestJson(s []byte) {
	h := https.CURL{}
	if err := json.Unmarshal(s, &h); err == nil {
		Quest(&h)
	}
}
