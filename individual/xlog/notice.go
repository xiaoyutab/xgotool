package xlog

import "errors"

// 记录详情日志
//
//	msg	消息备注
func Notice(msg string) {
	saveLog('N', "", 0, msg, nil, true)
}

// 记录详情日志，返回error类型，以便其他方法调用
func NoticeError(msg string) error {
	saveLog('N', "", 0, msg, nil, true)
	return errors.New(msg)
}

// 记录详情日志，返回error类型，以便其他方法调用
func NE(msg string) error {
	saveLog('N', "", 0, msg, nil, true)
	return errors.New(msg)
}

// 记录详情日志，返回nil的error类型，以便其他方法调用
func NN(msg string) error {
	return saveLog('N', "", 0, msg, nil, true)
}
