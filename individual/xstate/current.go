package xstate

// 获取当前状态字段，如果为空则说明出现错误
func (c *Xstate) Current() string {
	if c == nil || c.Error != nil {
		return ""
	}
	return c.current
}

// 获取当前状态对应的备注，如果为空则说明出现错误
func (c *Xstate) CurrentName() string {
	if c == nil || c.Error != nil {
		return ""
	}
	if v, ok := c.statusMap[c.current]; ok {
		return v
	}
	return c.current
}

// 追加所携带的参数信息
//
//	s	追加的参数信息
func (c *Xstate) SetData(s ...string) *Xstate {
	c.Lock()
	defer c.Unlock()
	c.data = append(c.data, s...)
	return c
}

// 移除所携带的参数信息
//
//	s	移除的参数信息
func (c *Xstate) RemoveData(s string) *Xstate {
	c.Lock()
	defer c.Unlock()
	for i := 0; i < len(c.data); i++ {
		if c.data[i] == s {
			c.data = append(c.data[:i], c.data[i+1:]...)
			break
		}
	}
	return c
}

// 清空所携带的参数信息
func (c *Xstate) CleanData() *Xstate {
	c.Lock()
	defer c.Unlock()
	c.data = nil
	return c
}
