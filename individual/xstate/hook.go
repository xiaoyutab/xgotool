package xstate

// 链式操作支持

// 链式切换开始的钩子监听
//
//	f	监听函数
func (c *Xstate) HookSwitch(f func(current, target string, dat []string)) *Xstate {
	if c.Error != nil {
		return c
	}
	c.Lock()
	defer c.Unlock()
	c.hookSwitch = append(c.hookSwitch, f)
	return c
}

// 事件监听，监听刚进入事件时的调用通知
//
//	e	待监听事件
//	f	监听回调
func (c *Xstate) HookEvent(e string, f func(e string, dat []string)) *Xstate {
	if c.Error != nil {
		return c
	}
	c.Lock()
	defer c.Unlock()
	if !c.hasEnevt(e) {
		// 如果事件不存在就直接进行跳过
		return c
	}
	if _, ok := c.hookEvent[e]; !ok {
		c.hookEvent = map[string][]func(e string, dat []string){}
	}
	c.hookEvent[e] = append(c.hookEvent[e], f)
	return c
}
