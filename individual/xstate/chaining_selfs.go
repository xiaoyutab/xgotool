package xstate

import (
	"errors"
)

// 内部功能支持函数

// 判断状态机中是否存在某事件
//
//	e	待判断的事件
func (c *Xstate) hasEnevt(e string) bool {
	for i := 0; i < len(c.events); i++ {
		if c.events[i].Name == e {
			return true
		}
	}
	return false
}

// 判断当前状态是否允许该操作
//
//	e	待操作的事件
func (c *Xstate) checkStatus(e string) (string, error) {
	event := event{}
	for i := 0; i < len(c.events); i++ {
		if c.events[i].Name == e {
			event = c.events[i]
			break
		}
	}
	if event.Name == "" {
		return "", errors.New("事件" + e + "不存在")
	}
	if !inArrayString(c.current, event.Origin) {
		return "", errors.New("当前状态不允许 " + e + " 操作")
	}
	return event.Target, nil
}
