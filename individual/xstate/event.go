package xstate

import "errors"

// 事件调用
//
//	e	调用事件标识
func (c *Xstate) Event(e string) error {
	if c == nil {
		return errors.New("对象内数据为空")
	}
	if c.Error != nil {
		return c.Error
	}
	var target string
	var err error
	// 判断状态是否允许操作[含判断事件是否存在]
	if target, err = c.checkStatus(e); err != nil {
		return err
	}
	// 打开读写锁
	c.Lock()
	defer c.Unlock()
	// 调用事件回调
	if _, ok := c.hookEvent[e]; ok {
		for i := 0; i < len(c.hookEvent[e]); i++ {
			go c.hookEvent[e][i](c.current, c.data)
		}
	}
	// 调用状态变更回调
	for i := 0; i < len(c.hookSwitch); i++ {
		go c.hookSwitch[i](c.current, target, c.data)
	}
	// 判断target状态是否在允许调整的状态内
	if _, ok := c.statusMap[target]; !ok {
		return errors.New("切换的目标状态不在允许的范围内，请重新初始化状态机以确保事件配置正确")
	}
	// 状态变更
	c.current = target
	return nil
}
