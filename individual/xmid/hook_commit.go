package xmid

import "gorm.io/gorm"

// 将全部的数据库对象进行提交
func hook_commit(c *Context) {
	c.db.Range(func(key, value any) bool {
		switch db := value.(type) {
		case *gorm.DB:
			db.Commit()
		}
		return true
	})
}
