package xmid_test

import (
	"errors"
	"fmt"
	"testing"

	"gitee.com/xiaoyutab/xgotool/individual/xmid"
)

func TestXmidOrder(t *testing.T) {
	// 测试订单计算逻辑
	r := xmid.DefaultEmpty()
	r.Use(func(x *xmid.Context) {
		// 基础信息填充
		mps := []string{}
		x.Set("order_info", 100)
		x.Set("order_list", mps)
		x.Next()
	})
	r.Use(func(ctx *xmid.Context) {
		// 优惠券减免
		ps, err := ctx.Get("order_info")
		if err != nil {
			ctx.AbortWithError(fmt.Errorf("获取订单信息失败"))
			return
		}
		if ps.(int) >= 100 {
			ctx.Set("order_info", ps.(int)-1)
			mps, err := ctx.Get("order_list")
			if err != nil {
				ctx.AbortWithError(fmt.Errorf("获取订单列表失败"))
				return
			}
			mpsa := mps.([]string)
			mpsa = append(mpsa, "优惠券减免")
			ctx.Set("order_list", mpsa)
		}
		ctx.Next()
	})
	r.HookCommit(func(ctx *xmid.Context) {
		// 最终结果
		if pc, err := ctx.Get("order_info"); err == nil {
			t.Log("订单金额：", pc)
		}
		if mps, err := ctx.Get("order_list"); err == nil {
			t.Log("条目列表：", mps)
		}
	})
	r.HookRollback(func(ctx *xmid.Context) {
		// 回滚操作
		t.Log("订单计算失败，订单金额回退")
		t.Log(ctx.Error())
	})
	r.Run(nil)
}

func TestXmidUse(t *testing.T) {
	r := xmid.Default()
	r.Use(func(x *xmid.Context) {
		fmt.Println("中间件1开始执行")
		x.Next()
		fmt.Println("中间件1执行结束")
	}).Use(func(x *xmid.Context) {
		fmt.Println("中间件2开始执行")
		x.AbortWithError(errors.New("携带错误信息"))
		fmt.Println("中间件2执行结束")
	})
	r.HookStart(func(ctx *xmid.Context) {
		fmt.Println("开始运行")
		ctx.Set("a1", 111)
	})
	r.HookEnd(func(ctx *xmid.Context) {
		if inf, err := ctx.Get("a1"); err == nil {
			t.Log(inf)
		}
		fmt.Println("结束运行")
	})
	r.HookCommit(func(ctx *xmid.Context) {
		fmt.Println("提交数据")
	})
	r.HookRollback(func(ctx *xmid.Context) {
		fmt.Println("数据回退")
	})
	err := r.Run(func(ctx *xmid.Context) {
		fmt.Println("程序开始运行")
	})
	t.Log(err)
}
