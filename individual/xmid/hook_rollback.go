package xmid

import "gorm.io/gorm"

// 将全部的数据库对象进行回滚
func hook_rollback(c *Context) {
	c.db.Range(func(key, value any) bool {
		switch db := value.(type) {
		case *gorm.DB:
			db.Rollback()
		}
		return true
	})
}
