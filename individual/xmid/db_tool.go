package xmid

import "gorm.io/gorm"

// 直接进行数据库操作
//
//	dbs	操作的数据库下标，使用SetDB进行设置
//	f	操作的函数标识，该函数会传入db和ctx进行协助处理
func DbTool(dbs string, f func(db *gorm.DB, c *Context) error) HandlerFunc {
	return func(ctx *Context) {
		// 插入余额
		db, err := ctx.GetDBS(dbs)
		if err != nil {
			ctx.AbortWithError(err)
			return
		}
		err = f(db, ctx)
		if err != nil {
			ctx.AbortWithError(err)
			return
		}
	}
}

// 直接进行数据库操作
//
//	f	操作的函数标识，该函数会传入db和ctx进行协助处理
func DbToolDefault(f func(d *gorm.DB, c *Context) error) HandlerFunc {
	return DbTool("default", f)
}

// Use中间件增强组件（用于连写操作）
//
//	dbs	操作的数据库下标，使用SetDB进行设置
//	f	操作的函数标识，该函数会传入db和ctx进行协助处理
func (c *Context) UseDB(dbs string, f func(db *gorm.DB, c *Context) error) *Context {
	return c.Use(DbTool(dbs, f))
}

// Use中间件增强组件（用于连写操作）
//
//	f	操作的函数标识，该函数会传入db和ctx进行协助处理
func (c *Context) DbToolDefault(f func(d *gorm.DB, c *Context) error) *Context {
	return c.Use(DbTool("default", f))
}
