// 自定义中间件处理依赖
package xmid

import (
	"math"
)

// 定义abort停止处理的最大值，用于判断到达此值以后将不再往下执行
const abortConst int = math.MaxInt >> 1

// 中间件需要传入的函数体
type HandlerFunc func(*Context)

// 获取一个空的中间事务支持
func DefaultEmpty() *Context {
	return &Context{}
}

// 获取默认中间件事务支持
// 此事务已存在回滚事务和提交事务
func Default() *Context {
	return &Context{
		hookCommit:   []HandlerFunc{hook_commit},   // 默认提交钩子
		hookRollback: []HandlerFunc{hook_rollback}, // 默认回滚钩子
	}
}
