package https

import (
	"context"
	"net/http/cookiejar"
	"time"
)

// 设置跳过https证书验证
func WithHttpsContinue() OptionFunc {
	return func(c *Option) {
		c.httpsContinue = true
	}
}

// 设置是否启用重试机制
// 此操作会在请求失败时自动重试，重试次数暂为指定的11次，即共请求12次结果
// 重试间隔：1s、2s、5s、10s、20s、30s、1min、3min、5min、10min、30min
// 重试报告不会记录请求日志，也不会使用重试机制，即报告一旦接收失败，则此次报告不会再次重传
// 报告提交：
// - 成功请求：curl -I -X POST -H "Content-Type: application/json" -d '{"msg": "SUCCESS","report": ["返回的Body内容"]}' <URI>
// - 失败请求：curl -I -X POST -H "Content-Type: application/json" -d '{"msg": "FAILED", "report": ["网络请求Code返回错误：500"]}' <URI>
// - 失败+成功请求：curl -I -X POST -H "Content-Type: application/json" -d '{"msg": "ATTEMPTED SUCCESS", "report": ["网络请求Code返回错误：500","返回的Body内容"]}' <URI>
//
//	uri	报告URI地址(空则表示不提交错误报告)
//	cnt	成功的返回内容(空表示只验证http-code在200~299之间，非空则额外验证返回内容包含此内容)
func WithRetry(uri, cnt string) OptionFunc {
	return func(c *Option) {
		c.retry = 1
		c.retryUri = uri
		c.successContent = cnt
	}
}

// 设置请求的超时时间
//
//	t	设置超时时间
func WithTimeOut(t time.Duration) OptionFunc {
	return func(c *Option) {
		c.timeOut = t
	}
}

// 设置缓存时间
//
//	t	设置缓存时间，0表示不设置缓存
func WithCache(t time.Duration) OptionFunc {
	return func(c *Option) {
		c.cacheTime = t
	}
}

// 设置忽略header头日志
//
//	k	header请求头的下标标识
func WithIgnoreHeader(k string) OptionFunc {
	return func(c *Option) {
		c.ignoreHeader = append(c.ignoreHeader, k)
	}
}

// 设置此记录不记录日志标识
func WithNoLog() OptionFunc {
	return func(c *Option) {
		c.noLog = true
	}
}

// 使用不同的cookiejar包
//
//	jar	存储cookie的依赖结构[传入nil表示不存储cookie]
func WithCookie(jar *cookiejar.Jar) OptionFunc {
	return func(c *Option) {
		c.jar = jar
	}
}

// 使用context.Context来控制进程请求
//
//	ctx	控制请求的context结构
func WithContext(ctx context.Context) OptionFunc {
	return func(c *Option) {
		c.context = ctx
	}
}

// 导入程序指定的host对（此处的域名、IP对应关系和host文件中的对应关系）
// 如果host中写入端口的话，域名和IP都需要对应上端口才行，如："xiaoyutab.cn:443": "120.24.39.208:443"
//
//	host	待导入的主机对，格式为   "域名[:端口]": "主机IP[:端口]"
func WithHosts(host map[string]string) OptionFunc {
	return func(c *Option) {
		c.hosts = host
	}
}
