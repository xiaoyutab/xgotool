# 网址请求增强包

## 功能

- 支持自定义请求头
- 支持自定义请求超时时间
- 支持请求错误重试
- 支持请求重定向
- 支持请求日志
- 支持请求的前/后钩子处理
- 支持请求缓存
- 支持分组并发请求
- 支持直接下载文件
- 支持上传文件
- 支持缓存自定义
- 支持https请求不验证证书

## 使用

```go
package main

import (
	"fmt"
	"github.com/xiaoyutab/xgotool/https"
)

func main() {
    // 基础配置项，用于处理请求的基础信息表述

    // 设置是否存储 cookie 默认不存储
    https.SetDefaultUnCookieJar(true)

    // 设置 cookie 的存储结构
    https.SetJar(nil)

    // 设置默认的超时时间，针对所有请求均生效
    https.SetDefaultTimeOut(time.Second * 10)

    // 设置默认的缓存实现，需要实现 https.CacheInterface 接口
    https.SetDefaultCacheFunc(nil)

    // 设置默认的缓存时间，针对所有请求均生效，所以一般不建议配置此值
    https.SetDefaultCacheTime(time.Second * 3)

    // 设置默认请求头
    https.SetDefaultAc("") // 设置请求头 access
    https.SetDefaultUA("") // 设置请求头 user-agent
    https.SetDefaultCT("") // 设置请求头 content-type
    https.SetDefaultAcL("") // 设置请求头 access-language

    // 日志回调函数，用于记录请求日志
    // 此请求会在请求全都完成后才会调用，可用于记录请求日志
    // 多次调用会设置多个日志回调钩子
    // 如果触发了缓存命中，则不会触发日志记录
    https.SetQuestLog(func(c *https.CURL) {})

    // 设置请求前的钩子处理，一般用于token验证；此处传入了空域名，表示验证所有请求
	// 第一个参数为域名参数
	// 传入空字符串``、`*` 均表示所有请求均走此处理函数
	https.SetBeforeHook("", func(c *https.CURL) {
		if len(c.URI) <= 4 || (len(c.URI) > 4 && c.URI[:4] == "http") {
			// 未使用http开头，即不是完整的域名
			c.URI = "http://xiaoyutab.cn/" + c.URI
		}
	})

    // 请求后钩子处理，可用于请求结果的处理(一般用于处理200状态下的统计返回结果，此处以code进行示例)
    https.SetAfterHook("xiaoyutab.cn", func(c *https.CURL) {
        // 定义临时结构体，解析返回结果（建议将此结构体定义于外部，此处定义内部仅作为举例示意）
		type temp_struct struct {
			Code int    `json:"code"`
			Flag bool   `json:"flag"`
			Msg  string `json:"msg"`
			Data any    `json:"data"`
		}
        temp := temp_struct{}
        // 解析返回结果
        err := json.Unmarshal([]byte(c.Body), &temp)
        if err != nil {
            c.Error = fmt.Errorf("解析返回结果失败：%w", err)
            return 
        }
        if temp.Code!= 0 {
            c.Error = fmt.Errorf("请求失败：%s", temp.Msg)
            return 
        }
        // 处理返回结果
        b,_ := json.Marshal(temp.Data)
        c.Body = string(b)
    })

	// 创建请求对象
	err := https.New("https://www.baidu.com").
        HeaderKV("content-type", "application/json").// 设置header请求头
        Header(map[string]string{
			"Authorization": "Bearer 1234567890",
		}). // 批量设置header头信息
        WithOption(
            https.WithTimeOut(time.Second * 10), // 设置请求超时时间
            https.WithCache(time.Second*10), // 设置请求缓存时间，0-不缓存(默认)
            https.WithRetry("", ""), // 错误重试，第一个参数为重试依然失败的回调网址，第二个参数为验证body中的信息包含
            https.WithContext(context.Background()),// 请求上下文控制，可用于超时、取消请求等，用于配合其他程序的并发处理
            https.WithHttpsContinue(), // 支持https请求的证书跳过
            https.WithCookie(nil),// 使用自定义的 cookie-jar 组件
            https.WithNoLog(), // 不记录请求日志(https请求日志会统一记录在外部)
        ).// 增强功能插入
        Get().
        Replace("","").// 请求结果替换，第一个参数为需要替换的字符串，第二个参数为替换后的字符串(此方法为直接替换curl.Body字符串，需要在请求完成后执行)
        Json() // 直接解析到JSON对象中
}
```