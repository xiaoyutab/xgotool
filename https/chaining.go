package https

import (
	"errors"
	"fmt"
	"os"
)

// 链式操作支持

// 追加请求参数
//
//	m	待追加的参数信息
func (c *CURL) Param(m map[string]string) *CURL {
	if m == nil || c.Error != nil {
		return c
	}
	if c.ParamQuest == nil {
		c.ParamQuest = make(map[string]string)
	}
	for k, v := range m {
		c.ParamQuest[k] = v
	}
	return c
}

// 追加请求参数
//
//	k	请求参数的参数名
//	v	请求参数的参数值
func (c *CURL) ParamKV(k, v string) *CURL {
	return c.Param(map[string]string{k: v})
}

// JSON格式的请求参数追加
//
//	m	追加的参数列表（可直接传入map[string]any{"_":xxx}(会在map最外层有且仅有_下标时才会直接取其内容进行传输)）
func (c *CURL) ParamJson(m map[string]any) *CURL {
	if m == nil || c.Error != nil {
		return c
	}
	if c.ParamJsonQuest == nil {
		c.ParamJsonQuest = make(map[string]any)
	}
	for k, v := range m {
		c.ParamJsonQuest[k] = v
	}
	return c
}

// JSON格式的请求参数追加
//
//	m	追加的参数列表(此处为简写map[string]any{"_":any}的写法)
func (c *CURL) ParamJsonAny(m any) *CURL {
	return c.ParamJson(map[string]any{"_": m})
}

// 设置Header请求头
//
//	k	header下标
//	v	header下标对应的值
func (c *CURL) HeaderKV(k, v string) *CURL {
	return c.Header(map[string]string{k: v})
}

// 设置Header请求头
//
//	h	header请求头的键值对信息
func (c *CURL) Header(h map[string]string) *CURL {
	if h == nil || c.Error != nil {
		return c
	}
	if c.HeaderQuest == nil {
		c.HeaderQuest = make(map[string]string)
	}
	for k, v := range h {
		c.HeaderQuest[k] = v
	}
	return c
}

// 追加Header请求头信息
//
//	token	JWT编码的Token加密信息
func (c *CURL) Authorization(token string) *CURL {
	if token == "" {
		c.Error = errors.New("传入的Authorization值为空")
		return c
	}
	return c.headerDefault("Authorization", "Bearer "+token)
}

// 添加待上传的文件
//
//	form_name	form表单名
//	file_name	文件原始路径
func (c *CURL) AddFile(form_name, file_name string) *CURL {
	if c == nil || c.Error != nil {
		return c
	}
	if c.files == nil {
		c.files = make(map[string]string)
	}
	c.files[form_name] = file_name
	return c
}

// 添加待上传的文件-以字节流的形式进行添加
// PS: 因暂未找到直接写入字节流的形式上传，所以此函数调用了临时文件进行写入，然后再请求的时候进行读取，效率会稍有降低，建议正常上传时直接读取文件进行上传
//
//	form_name	form表单名
//	file_byte	文件字节流
func (c *CURL) AddFileByte(form_name string, file_byte []byte) *CURL {
	if c == nil || c.Error != nil {
		return c
	}
	// 获取临时文件
	temp_file, err := os.CreateTemp(os.TempDir(), "curl_file_")
	if err != nil {
		c.Error = fmt.Errorf("临时文件创建失败: %w", err)
		return c
	}
	defer temp_file.Close()
	c.filesRemoves = append(c.filesRemoves, temp_file.Name())
	if _, err := temp_file.Write(file_byte); err != nil {
		c.Error = fmt.Errorf("临时文件写入失败: %w", err)
		return c
	}
	c.AddFile(form_name, temp_file.Name())
	// 将文件写入到上传文件中
	return c
}
