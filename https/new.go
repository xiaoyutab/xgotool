package https

import (
	"context"
	"regexp"
	"strings"
	"time"
)

// 声明URL请求结构信息
//
//	uri		要请求的网址信息
//	opts	可选参数，针对此网址进行的配置项追加
func New(uri string, opts ...OptionFunc) *CURL {
	// 默认配置项加载
	opt := Option{
		timeOut:   _default.timeOut,
		cacheTime: _default.cacheTime,
		jar:       getJar(),
		context:   context.TODO(),
	}
	// 配置项设置
	for _, optFunc := range opts {
		optFunc(&opt)
	}
	// 检测URL是否有多余的斜杠，如果有则进行处理
	if strings.Contains(uri, "//") {
		inf, err := regexp.Compile(`\/+`)
		if err == nil {
			// 去除多余的斜杠
			uri = inf.ReplaceAllString(uri, "/")
			// 补全协议头
			uri = strings.ReplaceAll(uri, ":/", "://")
		}
	}
	curls := CURL{
		URI:        uri,
		option:     &opt,
		CreateTime: time.Now(),
	}
	// 声明配置项的值
	return &curls
}

// 设置Option选项[此参数需要在Get/Post/PostJson之前进行调用，若在之后调用的话会失去作用]
//
//	opts	可选参数，针对此网址进行的配置项追加
func (c *CURL) WithOption(opts ...OptionFunc) *CURL {
	// 配置项设置
	for _, optFunc := range opts {
		optFunc(c.option)
	}
	return c
}
