package https

import (
	"fmt"
	"net/http/cookiejar"
	"net/url"
	"os"
	"time"
)

// 请求开始前的钩子操作
func (c *CURL) hookBefore() *CURL {
	if c == nil {
		c = &CURL{
			Error: fmt.Errorf("http实例未正常初始化"),
		}
	}
	c.
		headerDefault("accept", _default.AC).
		headerDefault("Content-Type", _default.CT).
		headerDefault("accept-language", _default.ACL).
		headerDefault("user-agent", _default.UA)
	// 缓存判定，如果存在缓存的话就直接追加响应值，且配置error错误对象
	ck, err := url.QueryUnescape(_default.cacheInterface.Get(c.getKey()))
	if err == nil && ck != "" {
		c.Body = ck
		c.option.cacheHit = true
	}
	return c
}

// 设置header默认值，且配置该值忽略日志记录
//
//	k	header下标
//	v	下标值
func (c *CURL) headerDefault(k, v string) *CURL {
	// 追加Header请求头
	if c.HeaderQuest == nil {
		c.HeaderQuest = map[string]string{}
	}
	if _, ok := c.HeaderQuest[k]; !ok {
		c.HeaderQuest[k] = v
		// 判断忽略参数里有无此head头
		for i := 0; i < len(c.option.ignoreHeader); i++ {
			if c.option.ignoreHeader[i] == k {
				return c
			}
		}
		c.option.ignoreHeader = append(c.option.ignoreHeader, k)
	}
	return c
}

// 请求结束的钩子操作
func (c *CURL) hookEnd() *CURL {
	c.EndTime = time.Now()
	// 清理缓存文件
	for i := 0; i < len(c.filesRemoves); i++ {
		os.Remove(c.filesRemoves[i])
	}
	// 记录缓存
	if c.option.cacheTime > 0 {
		// 将Body进行 url.QueryEscape 编码，避免中间出现"导致redis存储失败
		_default.cacheInterface.Set(c.getKey(), url.QueryEscape(c.Body), c.option.cacheTime)
	}
	// 移除忽略的请求头信息
	for i := 0; i < len(c.option.ignoreHeader); i++ {
		delete(c.HeaderQuest, c.option.ignoreHeader[i])
	}
	// 如果设置了日志记录并且未命中缓存，则调用日志函数进行对应记录
	if !c.option.noLog && !c.option.cacheHit {
		// 记录日志
		for i := 0; i < len(_default.LogFunc); i++ {
			_default.LogFunc[i](c)
		}
	}
	return c
}

// 获取cookie的jar信息
func getJar() *cookiejar.Jar {
	if !_default.saveCookie {
		return nil
	}
	if _default.jar != nil {
		return _default.jar
	}
	jar, err := cookiejar.New(nil)
	if err != nil {
		return nil
	}
	_default.jar = jar
	return jar
}
