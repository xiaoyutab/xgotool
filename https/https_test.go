package https_test

import (
	"context"
	"encoding/json"
	"fmt"
	"net/url"
	"os"
	"testing"
	"time"

	"gitee.com/xiaoyutab/xgotool/https"
)

func TestTempGet(t *testing.T) {
	c := https.New("https://xiaoyutab.cn").
		WithOption(https.WithHosts(map[string]string{"xiaoyutab.cn": "120.24.39.208"})).
		Get()
	t.Log(c.Body)
	t.Log(c.Error)
}

func TestGet(t *testing.T) {
	https.SetQuestLog(func(c *https.CURL) {
		fmt.Println("预请求耗时：", c.StartTime.Sub(c.CreateTime).String())
		fmt.Println("请求耗时：", c.EndTime.Sub(c.StartTime).String())
	})
	curl := https.New("http://www.lawxpapi.com").
		WithOption(https.WithHttpsContinue()).
		ParamKV("q", "这是GET").
		WithOption(https.WithCache(time.Second * 3)).
		Authorization("").
		Get()
	if curl.Error != nil {
		t.Error(curl.Error)
	}
	t.Log("请求完成")
	time.Sleep(time.Second)
	curl = https.New("http://www.lawxpapi.com").
		WithOption(https.WithHttpsContinue()).
		ParamKV("q", "这是GET").
		WithOption(https.WithCache(time.Second * 3)).
		Get()
	if curl.Error != nil {
		t.Error(curl.Error)
	}
	t.Log("再次请求完成")
}

func TestUrl(t *testing.T) {
	a := "aaa\"bbb\"ccc"
	t.Log(a)
	t.Log(url.QueryEscape(a))
	t.Log(url.QueryUnescape("aaa%22bbb%22ccc"))
}

func TestCookieJar(t *testing.T) {
	https.SetDefaultUnCookieJar(false)
	curl := https.New("https://fanyi.baidu.com/").Get()
	t.Log(curl.Cookie())
	curl = https.New("https://baidu.com/").Get()
	t.Log(curl.Cookie())
}

func TestPostParam(t *testing.T) {
	curl := https.New("https://my.ems.com.cn/pcpErp-web/a/pcp/orderFeeService/getOrderFee").
		Param(map[string]string{
			"dataDigest":  "ClNUfG7sJbbniuSitBD3nA==",
			"ecCompanyId": "1100137508152",
			"waybillNos":  "EB806415538CN",
		}).
		Post()
	t.Log(curl)
}

func TestGroupHttp(t *testing.T) {
	con, cal := context.WithTimeout(context.Background(), time.Second*10)
	defer cal()
	a := []int{}
	g := https.Group(func() {
		time.Sleep(time.Second)
		a = append(a, 1)
	}, func() {
		time.Sleep(time.Second + time.Millisecond*200)
		a = append(a, 3)
	}, func() {
		time.Sleep(time.Second)
		a = append(a, 6)
	}).AddError(func() error {
		// return errors.New("测试错误信息")
		return nil
	}).Add(func() {
		time.Sleep(time.Second * 2)
		a = append(a, 100)
	}).WithContext(con).Run()
	t.Log(a)
	t.Log(g.Error)
	t.Log(g.HasTimeOut)
}

func TestUploadFile(t *testing.T) {
	uri := "http://localhost:8089/api/tr-run/"
	file := "/mnt/c/Users/xiaoyutab/Downloads/60c95e12b63e75412.png_e1080.png"
	f, _ := os.ReadFile(file)
	// 测试临时文件有无写入
	curls := https.New(uri).ParamKV("is_draw", "0").AddFileByte("file", f).Post()
	if curls.Error != nil {
		t.Error(curls.Error)
	}
	t.Log(curls.Body)
}

func TestHasOver(t *testing.T) {
	cont, der := context.WithTimeout(context.Background(), time.Second*2)
	defer der()
	ts := https.
		Group(func() {
			time.Sleep(time.Second * 1)
		}).
		Add(func() {
			time.Sleep(1)
		}).
		WithContext(cont).
		Run()
	t.Log(ts.HasTimeOut)
}

func TestHget(t *testing.T) {
	code := 0
	err := https.New("https://api1.xinfushenghuo.cn/apiindex/get-navigation-location?&is_index=1").
		Get().
		Json(&code, "data", "0", "id")
	if err != nil {
		t.Error(err)
	}
	t.Log(code)
}

func TestGroupsError(t *testing.T) {
	wg := https.Group().AddError(func() error {
		return fmt.Errorf("测试错误")
	}).Run()
	if wg.Error != nil {
		t.Log(wg.Error)
	}
	t.Log("执行完成")
}

func TestHttpApi(t *testing.T) {
	https.SetAfterHook("43.138.16.245:9091", func(c *https.CURL) {
		type temp_struct struct {
			Code int    `json:"code"`
			Flag bool   `json:"flag"`
			Msg  string `json:"msg"`
			Data any    `json:"data"`
		}
		tmp := temp_struct{}
		err := c.Json(&tmp)
		if err != nil {
			t.Error(err)
		}
		if tmp.Code == 0 {
			c.Error = fmt.Errorf(tmp.Msg)
		}
		b, _ := json.Marshal(tmp.Data)
		c.Body = string(b)
	})
	type temp_struct struct {
		Data  []any `json:"data"`
		Page  any   `json:"page"`
		Total int   `json:"total"`
	}
	dats := temp_struct{}
	err := https.New("http://43.138.16.245:9091/dervice.list").
		Header(map[string]string{
			"Authorization": "Bearer 1234567890",
		}).
		HeaderKV("content-type", "application/json").
		Get().
		Replace("", "").
		Json(&dats)
	if err != nil {
		t.Error(err)
	}
	t.Log(dats.Total)
}
