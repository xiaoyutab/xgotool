package https

import (
	"sync"
	"time"
)

// 内建缓存组件
type cache struct {
	sync.Map
}

// 获取缓存的值
//
//	name	缓存名称
func (c *cache) Get(name string) string {
	if v, ok := c.Load(name); ok {
		if str, ok := v.(string); ok {
			return str
		}
	}
	return ""
}

// 设置缓存项的值
//
//	name	设置项名称
//	val		缓存项的值
//	t		缓存时间 -/0-不缓存
func (c *cache) Set(name string, val string, t time.Duration) {
	if t <= 0 {
		return
	}
	c.Store(name, val)
	go func(name string, t time.Duration) {
		time.Sleep(t)
		c.Delete(name)
	}(name, t)
}
