package xconsole

import (
	"fmt"
	"runtime"
)

// 输出当前程序版本信息
func version(help bool) string {
	if help {
		return "输出当前软件版本信息"
	}
	fmt.Println("当前软件版本为: ", _default.Version)
	fmt.Println("xgotool依赖版本: ", _default.v)
	fmt.Println("编译Go版本: ", runtime.Version())
	fmt.Println("系统版本: ", runtime.GOOS, runtime.GOARCH)
	fmt.Println("CPU核心数量: ", runtime.NumCPU())
	return ""
}
