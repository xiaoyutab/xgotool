package xconsole

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"sort"
	"strings"

	"gitee.com/xiaoyutab/xgotool/xnum"
)

// 开始监听运行命令行程序
func Listen() {
	Regedit(nil)
	// 参数提取
	commands := os.Args[1]
	// 命令移除
	os.Args = append(os.Args[:1], os.Args[2:]...)
	if xnum.InArray(commands, []string{"help", "-help", "--help", "h", "-h"}) {
		fmt.Println("现支持以下命令：")
		fmt.Println("PS: 命令程序中分级时采用 : 进行级别拆分，为便于使用，调用时 `/`、`\\`、`.`、`,`、`*`、`+` 参数等同于 `:` ")
		fmt.Println("PS2: 但是由于部分系统的命令行格式中, `\\` 常被作为转移符占用，所以若要输入 `\\` 时，一般是需要输入两次的 `\\\\`")
		helps()
		fmt.Println("\tv: 输出当前软件版本信息 等同于version、-v、-version、--version")
		fmt.Println("\th: 输出当前软件所支持的命令列表/帮助信息 等同于help、-h、-help、--help")
	} else if xnum.InArray(commands, []string{"v", "version", "-v", "-version", "--version"}) {
		version(false)
	}
	// commands 多分隔符兼容
	spl_inf := []string{"/", "\\", ".", ",", "*", "+"}
	for i := 0; i < len(spl_inf); i++ {
		if strings.Contains(commands, spl_inf[i]) {
			commands = strings.ReplaceAll(commands, spl_inf[i], ":")
		}
	}
	reg, err := regexp.Compile(":{1,}")
	if err != nil {
		panic(err)
	}
	commands = reg.ReplaceAllString(commands, ":")
	// 路由跳转
	if fun, ok := _default.r[commands]; ok {
		log.Println("开始运行程序：", fun(true))
		fun(false)
	} else {
		cmd_name := []string{}
		for i, _ := range _default.r {
			spl := strings.Split(i, ":")
			if spl[len(spl)-1] == commands {
				cmd_name = append(cmd_name, i)
			}
		}
		if len(cmd_name) > 1 {
			log.Panicln("检测到有以下命令符合规则，请确认待运行的命令是哪个：\n\n" + strings.Join(cmd_name, "\n"))
		} else if len(cmd_name) == 1 {
			log.Println("开始运行程序：", _default.r[cmd_name[0]](true))
			_default.r[cmd_name[0]](false)
		} else {
			log.Println("未检测到 " + commands + " 命令，请确认后再次尝试")
		}
	}
}

// 输出帮助文档信息
func helps() {
	no_str := []string{}
	strs := map[string][]string{}
	has := []string{}
	for command, fun := range _default.r {
		if !strings.Contains(command, ":") {
			no_str = append(no_str, "\t"+command+" ： "+fun(true))
		} else {
			tmps := strings.Split(command, ":")
			if !xnum.InArray(tmps[0], has) {
				has = append(has, tmps[0])
			}
			strs[tmps[0]] = append(strs[tmps[0]], command+" ： "+fun(true))
		}
	}
	sort.Strings(no_str)
	for i := 0; i < len(no_str); i++ {
		fmt.Println(no_str[i])
	}
	sort.Strings(has)
	for i := 0; i < len(has); i++ {
		fmt.Println("\t" + has[i] + ": ")
		sort.Strings(strs[has[i]])
		for j := 0; j < len(strs[has[i]]); j++ {
			fmt.Println("\t    " + strs[has[i]][j])
		}
	}
}
