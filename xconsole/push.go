package xconsole

import "errors"

// 推送任务到路由接口中
//
//	rou	执行的命令
//	fun	执行的命令内容
func Push(rou string, fun Command) error {
	if _, ok := _default.r[rou]; ok {
		return errors.New("命令 " + rou + " 已存在")
	}
	_default.Lock()
	defer _default.Unlock()
	_default.r[rou] = fun
	return nil
}

// 要移除的命令列表
func UnPush(rou []string) error {
	_default.Lock()
	defer _default.Unlock()
	for i := 0; i < len(rou); i++ {
		delete(_default.r, rou[i])
	}
	return nil
}

// 批量推送任务到路由接口中
//
//	m map结构的任务键值对
func PushMap(m map[string]Command) error {
	_default.Lock()
	defer _default.Unlock()
	for k, v := range m {
		if _, ok := _default.r[k]; ok {
			return errors.New("命令 " + k + " 已存在")
		}
		_default.r[k] = v
	}
	return nil
}
