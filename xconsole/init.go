// 命令行程序支持组件
// 此组件需要使用 xconsole.Listen() 来运行命令监听，即：
//
//	if len(os.Args) > 1 {
//		xconsole.Listen()
//		return
//	}
package xconsole

import (
	"sync"
)

// 命令支持的函数结构
type Command func(help bool) string

// 配置项
type Config struct {
	Version       string                  // 外部版本标识
	Rout          map[string]Command      // 注入时直接携带路由进行注入
	WriteFileFunc func(name, body string) // 写入文件的函数，外部注册时可直接注册为：xfile.WriteString
	FmtFunc       func(file ...string)    // 格式化文件的函数，若未注入则需要手动进行执行此命令
	v             string                  // 内部版本标识
	DNS           string                  // 默认DNS连接
	r             map[string]Command      // 内部路由
	sync.Mutex                            // 内部路由锁
}

// 默认配置
var _default Config = Config{
	r:       map[string]Command{},
	v:       "v0.1",
	Version: "v1.0.0",
	DNS:     "admin:admin@tcp(127.0.0.1:3306)/log_analysis?charset=utf8",
}

// 注入配置
func Regedit(conf *Config) {
	if conf == nil {
		return
	}
	if conf.Version != "" {
		_default.Version = conf.Version
	}
	if conf.Rout != nil {
		PushMap(conf.Rout)
	}
	if conf.WriteFileFunc != nil {
		_default.WriteFileFunc = conf.WriteFileFunc
	}
	if conf.FmtFunc != nil {
		_default.FmtFunc = conf.FmtFunc
	}
	if conf.DNS != "" {
		_default.DNS = conf.DNS
	}
}
