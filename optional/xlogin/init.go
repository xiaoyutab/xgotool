// 三方登录服务
//
// 提供三方登录的一些授权标识、认证标识等
// 目前支持三方登录：WPS
// 此服务会注册一个三方授权表，用于存储三方授权的一些必要信息
package xlogin

import (
	"gorm.io/gorm"
)

// 定义三方登录所支持的类型
const (
	TypesWps    uint8 = iota + 1 // WPS登录
	TypesWechat                  // 微信公众号登录【H5】
	// TypesQQ                      // QQ三方登录
)

type OpenConfig struct {
	AppId     string // 配置项的key(必填，不能为空，此值为空则会忽略掉此项配置)
	AppSecret string // 配置项的SECRET(部分平台中也叫AppKey)
}

type Config struct {
	DB        *gorm.DB             // 数据库连接
	TableName string               // 三方登录的表名信息
	Conf      map[uint8]OpenConfig // 三方登录的具体配置详情
}

var _default = Config{
	TableName: "user_open_login",
	Conf:      map[uint8]OpenConfig{},
}

// 注入配置项
func Regedit(conf *Config) {
	if conf == nil {
		return
	}
	if conf.DB != nil {
		_default.DB = conf.DB
	}
	if conf.TableName != "" {
		_default.TableName = conf.TableName
	}
	if conf.Conf != nil {
		for i, v := range conf.Conf {
			if v.AppId != "" {
				_default.Conf[i] = v
			}
		}
	}
	if _default.DB != nil {
		_default.DB.AutoMigrate(&OpenLogin{})
	}
}

// 三方登录的结构体类型
type OpenLogin struct {
	Id        uint64 `gorm:"column:id;primaryKey;type:BIGINT UNSIGNED;not null;autoIncrement" json:"id" form:"id"`              // 条目ID
	UserId    uint   `gorm:"column:user_id;not null;comment:绑定用户ID;index:user_id" json:"user_id" form:"user_id"`                // 绑定用户ID
	Nickname  string `gorm:"column:nickname;type:varchar(200);comment:三方昵称" json:"nickname" form:"nickname"`                    // 三方昵称
	Sex       string `gorm:"column:sex;type:varchar(50);comment:三方性别" json:"sex" form:"sex"`                                    // 三方性别
	Avatar    string `gorm:"column:avatar;type:varchar(200);comment:三方头像" json:"avatar" form:"avatar"`                          // 三方头像
	OpenID    string `gorm:"column:openid;type:VARCHAR(100);comment:三方返回的OPENID;index:openid" json:"openid" form:"openid"`      // OPENID
	UnionId   string `gorm:"column:unionid;type:VARCHAR(100);comment:三方返回的UNIONID;index:unionid" json:"unionid" form:"unionid"` // unionid
	Types     uint8  `gorm:"column:types;type:TINYINT UNSIGNED;comment:所属三方类型;index:types" json:"types" form:"types"`           // 所属三方类型
	CreatedAt string `gorm:"column:created_at;type:DATETIME;comment:创建时间" json:"created_at" form:"created_at"`                  //错误发生时间
}

// 三方登录的表名
func (a *OpenLogin) TableName() string {
	return _default.TableName
}
