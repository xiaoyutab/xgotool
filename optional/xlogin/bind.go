package xlogin

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
)

// Openid绑定
//
//	openid	三方唯一标识
//	typ		所属三方
//	user_id	待绑定的用户ID
func Bind(openid string, typ uint8, user_id uint) error {
	info, err := Openid2info(openid, typ)
	if err != nil {
		return err
	}
	if info.UserId != 0 {
		return errors.New("该三方标识已绑定，请先解绑，再进行绑定")
	}
	info.UserId = user_id
	// 入库
	err = _default.DB.Save(info).Error
	if err != nil {
		return err
	}
	// 入缓存
	xcache.SetStruct(xcache.Key("xlogin.openid2info-v1", openid, typ), info)
	return nil
}
