package xlogin

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
)

// 使用Openid换取Token【此服务依赖cache缓存和nsq刷新】
func Openid2Token(openid string, typ uint8) (*CodeToken, error) {
	tokens := CodeToken{}
	var err error
	cache_key := xcache.Key("xlogin.openid2token", openid, typ)
	if xcache.Exists(cache_key) {
		if err = xcache.GetStruct(cache_key, &tokens); err == nil {
			return &tokens, nil
		}
	}
	// 使用switch，避免某些服务可以直接使用openid换取token
	switch typ {
	default:
		err = errors.New("暂不支持该分类")
	}
	if err != nil {
		return nil, err
	}
	return &tokens, nil
}
