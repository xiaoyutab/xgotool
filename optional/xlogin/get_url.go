package xlogin

import (
	"fmt"
	"net/url"
)

// 根据三方类型返回三方授权码
//
//	urls	回调跳转地址【会携带code等参数进行回调】
//	typ		三方类型
//	state	附加参数
func GetUrl(urls string, typ uint8, state string) string {
	if state == "" {
		state = "STATE"
	}
	switch typ {
	case TypesWps:
		return fmt.Sprintf("https://openapi.wps.cn/oauthapi/v2/authorize?response_type=code&appid=%s&autologin=false&redirect_uri=%s&scope=user_info&state=%s",
			_default.Conf[TypesWps].AppId, url.QueryEscape(urls), state,
		)
	case TypesWechat:
		return fmt.Sprintf("https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&response_type=code&scope=snsapi_userinfo&redirect_uri=%s&state=%s#wechat_redirect",
			_default.Conf[TypesWechat].AppId, url.QueryEscape(urls), state,
		)
		// case TypesQQ:
		// 	return fmt.Sprintf("https://graph.qq.com/oauth2.0/authorize?response_type=code&client_id=%s&redirect_uri=%s&scope=%s",
		// 		_default.Conf[TypesQQ].AppId, url.QueryEscape(urls), state,
		// 	)
	}
	return ""
}
