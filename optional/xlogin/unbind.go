package xlogin

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
)

// Openid解绑
//
//	openid	三方唯一标识
//	typ		所属三方
func UnBind(openid string, typ uint8) error {
	info, err := Openid2info(openid, typ)
	if err != nil {
		return err
	}
	if info.UserId == 0 {
		return errors.New("该三方标识已解绑")
	}
	info.UserId = 0
	// 入库
	err = _default.DB.Save(info).Error
	if err != nil {
		return err
	}
	// 入缓存
	xcache.SetStruct(xcache.Key("xlogin.openid2info-v1", openid, typ), info)
	return nil
}
