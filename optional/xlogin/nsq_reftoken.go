package xlogin

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/https"
	"gitee.com/xiaoyutab/xgotool/individual/xcache"
)

// 刷新Token信息
//
//	pm	请求的token信息
func NsqRefToken(pm *CodeToken) error {
	tokens := CodeToken{}
	var err error
	// 进行刷新token
	switch pm.XloginType {
	case TypesWps:
		err = https.New("https://openapi.wps.cn/oauthapi/v2/token/refresh").Param(map[string]string{
			"appid":         _default.Conf[TypesWps].AppId,
			"appkey":        _default.Conf[TypesWps].AppSecret,
			"refresh_token": pm.RefreshToken,
		}).Get().Json(&tokens, "token")
	case TypesWechat:
		err = https.New("https://api.weixin.qq.com/sns/oauth2/refresh_token").Param(map[string]string{
			"appid":         _default.Conf[TypesWps].AppId,
			"grant_type":    "refresh_token",
			"refresh_token": pm.RefreshToken,
		}).Get().Json(&tokens)
	default:
		err = errors.New("该类型暂不支持刷新")
	}
	if err != nil {
		return err
	}
	if tokens.ExpiresIn == 0 {
		return errors.New("三方token获取失败")
	}
	tokens.XloginType = pm.XloginType
	xcache.SetStruct(xcache.Key("xlogin.openid2token", tokens.Openid, pm.XloginType), tokens)
	return nil
}
