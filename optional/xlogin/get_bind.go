package xlogin

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 获取我已绑定的账号信息
//
//	uid	用户ID
func GetBind(uid uint) ([]OpenLogin, error) {
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	mod := []OpenLogin{}
	err := _default.DB.Table(_default.TableName).Where("user_id", uid).Order("id DESC").Find(&mod).Error
	if err != nil {
		return nil, xlog.AE("用户绑定记录查询失败", err)
	}
	return mod, nil
}
