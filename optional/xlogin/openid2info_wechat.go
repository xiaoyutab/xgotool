package xlogin

import (
	"time"

	"gitee.com/xiaoyutab/xgotool/https"
)

// OPENID转微信的详情信息
func openid2info_wechat(op *OpenLogin, token, app_id, openid string) error {
	type wechat_info struct {
		Openid    string   `json:"openid"`
		Nickname  string   `json:"nickname"`
		Sex       int      `json:"sex"`
		Province  string   `json:"province"`
		City      string   `json:"city"`
		Country   string   `json:"country"`
		Avatar    string   `json:"headimgurl"`
		Privilege []string `json:"privilege"`
		Unionid   string   `json:"unionid"`
	}
	wps := wechat_info{}
	err := https.New("https://api.weixin.qq.com/sns/userinfo").Param(map[string]string{
		"access_token": token,
		"openid":       openid,
	}).Get().Json(&wps)
	if err != nil {
		return err
	}
	sex_list := []string{"未知", "男", "女"}
	op.Avatar = wps.Avatar
	op.OpenID = wps.Openid
	op.UnionId = wps.Unionid
	op.Nickname = wps.Nickname
	op.Sex = sex_list[wps.Sex]
	op.CreatedAt = time.Now().Format(time.DateTime)
	return nil
}
