package xlogin

import (
	"time"

	"gitee.com/xiaoyutab/xgotool/https"
)

// OPENID转WPS的详情信息
func openid2info_wps(op *OpenLogin, token, app_id, openid string) error {
	type wps_info struct {
		Nickname   string `json:"nickname"`
		Avatar     string `json:"avatar"`
		Sex        string `json:"sex"`
		Openid     string `json:"openid"`
		Unionid    string `json:"unionid"`
		CompanyID  string `json:"company_id"`
		CompanyUID string `json:"company_uid"`
	}
	wps := wps_info{}
	err := https.New("https://openapi.wps.cn/oauthapi/v3/user").Param(map[string]string{
		"access_token": token,
		"appid":        app_id,
		"openid":       openid,
	}).Get().Json(&wps, "user")
	if err != nil {
		return err
	}
	op.Avatar = wps.Avatar
	op.OpenID = wps.Openid
	op.UnionId = wps.Unionid
	op.Nickname = wps.Nickname
	op.Sex = wps.Sex
	op.CreatedAt = time.Now().Format(time.DateTime)
	return nil
}
