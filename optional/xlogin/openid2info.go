package xlogin

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
)

// openid换取个人信息【依次从缓存、数据库、三方进行读取，user_id为0表示未绑定】
//
//	openid	三方返回的唯一标识
//	typ		三方类型
func Openid2info(openid string, typ uint8) (*OpenLogin, error) {
	if openid == "" {
		return nil, errors.New("openid不允许为空")
	}
	op := OpenLogin{}
	// 从缓存中获取信息
	cache_key := xcache.Key("xlogin.openid2info-v1", openid, typ)
	if xcache.Exists(cache_key) {
		if err := xcache.GetStruct(cache_key, &op); err == nil {
			return &op, nil
		}
	}
	// 从数据库中获取
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	err := _default.DB.Table(_default.TableName).Where("openid", openid).Where("types", typ).Order("id ASC").Limit(1).Find(&op).Error
	if err != nil {
		return nil, err
	}
	if op.Id > 0 {
		// 写入缓存并返回
		xcache.SetStruct(cache_key, op)
		return &op, nil
	}
	// 数据库中也没有的话，就需要从三方进行获取信息了
	ops, err := openid2infos(openid, typ)
	if err != nil {
		return nil, err
	}
	// 入库
	err = _default.DB.Create(ops).Error
	if err != nil {
		return nil, err
	}
	// 入缓存
	xcache.SetStruct(cache_key, ops)
	return ops, nil
}

// openid换取个人信息
//
//	openid	三方返回的唯一标识
//	typ		三方类型
func openid2infos(openid string, typ uint8) (*OpenLogin, error) {
	// 获取token信息
	token, err := Openid2Token(openid, typ)
	if err != nil {
		return nil, err
	}
	op := OpenLogin{}
	switch typ {
	case TypesWps:
		err = openid2info_wps(&op, token.AccessToken, _default.Conf[TypesWps].AppId, openid)
		if err != nil {
			return nil, err
		}
		op.Types = TypesWps
	case TypesWechat:
		err = openid2info_wechat(&op, token.AccessToken, _default.Conf[TypesWps].AppId, openid)
		if err != nil {
			return nil, err
		}
		op.Types = TypesWechat
	default:
		return nil, errors.New("该三方暂不支持")
	}
	return &op, nil
}
