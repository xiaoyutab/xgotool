package xbank

// 获取银行的图标信息标识
//
//	typ	银行名称/银行简称
func GetIcon(typ string) *BankIcon {
	banks := GetList()
	for i := 0; i < len(banks); i++ {
		if banks[i].Name == typ || banks[i].ShortName == typ {
			return &banks[i]
		}
	}
	return &default_bank
}
