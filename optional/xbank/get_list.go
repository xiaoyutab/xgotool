package xbank

import (
	"encoding/json"
)

// 默认银行信息
var default_bank BankIcon = BankIcon{
	Name:       "其他银行",
	ShortName:  "other",
	Icon:       "other_icon.png",
	Background: "other.png",
}

// 获取全部银行信息
func GetList() []BankIcon {
	mod := []BankIcon{}
	if err := json.Unmarshal(CityCsv, &mod); err != nil {
		return nil
	}
	return mod
}
