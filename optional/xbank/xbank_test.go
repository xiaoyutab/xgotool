package xbank_test

import (
	"testing"
	"time"

	"gitee.com/xiaoyutab/xgotool/optional/xbank"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func TestXBankRegedit(t *testing.T) {
	db, _ := gorm.Open(mysql.Open("admin:admin@tcp(localhost:3306)/gatway?charset=utf8"), &gorm.Config{})
	// https.Regedit(&https.Config{
	// 	LogsFunc: func(c *https.CURL) {
	// 		b, _ := json.Marshal(c)
	// 		t.Log(string(b))
	// 	},
	// })
	xbank.Regedit(&xbank.Config{
		DB: db,
	})
	// 根据银行卡号长度和bin查询所属
	// t.Log(xbank.GetIcon("GYCB"))
	// t.Log(xbank.ToInfo("6221700000000000102"))
	err := xbank.Create(&xbank.BankUser{
		UserId:   1,
		CardNo:   "6221700000000000104",
		CardName: "测试",
	})
	// lis, err := xbank.UserList(1)
	if err != nil {
		t.Error(err)
	}
	// t.Log(lis)
	time.Sleep(time.Second * 1)
}
