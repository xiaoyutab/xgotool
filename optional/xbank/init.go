// 银行卡号BIN列表
package xbank

import (
	_ "embed"

	"gorm.io/gorm"
)

// 银行信息CSV为爬虫爬取CSV，当前CSV爬取时间：2023-08-09 19:04:21
// CSV文件要求：使用,分割各个列，然后使用换行符分割各个行，内容编码为GBK编码
// 此模块依赖xlog模块进行日志输出/入库
// 爬取地址：https://www.chakahao.com
// CSV格式：【银行卡BIN,卡分类,卡类型,卡号长度,卡所属】
// 卡分类：1-借记卡 2-贷记卡(信用卡) 3-预付费卡
// 卡类型：类似【工商银行牡丹灵通卡(银联卡)】的字符串
// 卡所属：icbc-工商银行 abc-农业银行 ccb-建设银行 boc-中国银行 psbc-邮政储蓄 comm-交通银行 cmb-招商银行 ceb-光大银行 other-其他银行
type Config struct {
	DB       *gorm.DB // 数据库连接
	BankName string   // 银行卡前缀存储表
	IconFix  string   // 图标资源前缀[带后面的/分割]
}

// 默认配置
var _default Config = Config{
	BankName: "bank",
}

// 配置项注入
func Regedit(c *Config) {
	if c == nil {
		return
	}
	if c.DB != nil {
		_default.DB = c.DB
	}
	if c.BankName != "" {
		_default.BankName = c.BankName
	}
	if _default.DB != nil {
		_default.DB.AutoMigrate(&BankUser{})
	}
}

// 银行结构标识
type BankIcon struct {
	CardType   uint8  `json:"card_type" form:"card_type"`   // 卡类型 1-借记卡 2-贷记卡(信用卡) 3-其他类型卡
	CardNo     string `json:"card_no" form:"card_no"`       // 卡号
	Name       string `json:"name" form:"name"`             // 银行名称
	ShortName  string `json:"short_name" form:"short_name"` // 银行简称
	Icon       string `json:"icon" form:"icon"`             // ICON图标
	Background string `json:"background" form:"background"` // 背景图
	Logo       string `json:"logo" form:"logo"`             // 银行LOGO图
	Validated  bool   `json:"validated" form:"validated"`   // 卡号验证是否通过
}

//go:embed bank.json
var CityCsv []byte

// 用户银行卡列表
type BankUser struct {
	Id          uint   `gorm:"column:id;primaryKey;not null;autoIncrement" json:"id" form:"id"`
	UserId      uint   `gorm:"column:user_id;index:user_id;comment:创建人" json:"user_id" form:"user_id"`                                 // 创建人ID
	CardName    string `gorm:"column:card_name;type:VARCHAR(200);comment:户主姓名" json:"card_name" form:"card_name"`                      // 户主姓名
	CardNo      string `gorm:"column:card_no;type:VARCHAR(20);comment:卡号" json:"card_no" form:"card_no"`                               // 卡号【目前卡号最大长度为20，所以此处使用VARCHAR(20)】
	CardType    uint8  `gorm:"column:card_type;type:TINYINT UNSIGNED;comment:卡类型 1-借记卡 2-贷记卡 3-其他卡" json:"card_type" form:"card_type"` // 卡类型 1-借记卡 2-贷记卡(信用卡) 3-其他卡
	CardPlat    string `gorm:"column:card_plat;type:VARCHAR(100);comment:所属银行" json:"card_plat" form:"card_plat"`                      // 所属银行
	Platform    string `gorm:"column:platform;type:VARCHAR(20);comment:银行简称" json:"platform" form:"platform"`                          // 银行简称
	CardOpening string `gorm:"column:card_opening;type:VARCHAR(200);comment:开户行" json:"card_opening" form:"card_opening"`              // 所属银行开户行
	IsSign      uint8  `gorm:"column:is_sign;type:TINYINT UNSIGNED;comment:是否签约" json:"is_sign" form:"is_sign"`                        // 是否已签约 0-否 1-是
	SignId      uint   `gorm:"column:sign_id;comment:签约ID" json:"sign_id" form:"sign_id"`                                              // 签约ID - 签约逻辑暂不在此处存储，以免造成业务干扰
	IsDeleted   uint8  `gorm:"column:is_deleted;type:TINYINT UNSIGNED;comment:是否删除 0-否 1-是" json:"is_deleted" form:"is_deleted"`       // 是否删除 0-否 1-是
	Icon        string `gorm:"-" json:"icon" form:"icon"`                                                                              // ICON图标
	Background  string `gorm:"-" json:"background" form:"background"`                                                                  // 背景图
	Logo        string `gorm:"-" json:"logo" form:"logo"`                                                                              // 银行LOGO图
	CreatedAt   string `gorm:"column:created_at;type:DATETIME;comment:创建时间" json:"created_at" form:"created_at"`                       // 创建时间
	UpdatedAt   string `gorm:"column:updated_at;type:DATETIME;comment:最后修改时间" json:"updated_at" form:"updated_at"`                     // 更新时间
}

func (c *BankUser) TableName() string {
	return _default.BankName
}
