package xbank

import (
	"errors"
	"strings"
	"time"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 创建银行归属信息
//
//	b	银行信息
func Create(b *BankUser) error {
	if b.UserId == 0 {
		return errors.New("用户ID不能为空")
	}
	if b.IsSign == 1 {
		if b.SignId == 0 {
			return errors.New("已签约信息的签约ID不能为空")
		}
	}
	b.CardNo = strings.ReplaceAll(b.CardNo, " ", "") // 卡号去除空格
	if b.CardNo == "" {
		return errors.New("卡号不能为空")
	}
	if b.CardName == "" {
		return errors.New("户主姓名不能为空")
	}
	b.UpdatedAt = time.Now().Format(time.DateTime)
	if b.CreatedAt == "" {
		b.CreatedAt = time.Now().Format(time.DateTime)
	}
	// 如果所属银行为空，就根据卡号获取所属银行等信息
	if b.Platform == "" || b.CardPlat == "" {
		inf := ToInfo(b.CardNo)
		if b.Platform == "" {
			b.Platform = inf.ShortName
		}
		if b.CardPlat == "" {
			b.CardPlat = inf.Name
		}
		if b.CardType == 0 {
			b.CardType = inf.CardType
		}
	}
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	err := _default.DB.Save(b).Error
	if err != nil {
		return xlog.AE("用户银行卡标识存储失败", err)
	}
	return nil
}
