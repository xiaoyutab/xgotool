package xbank

import "strings"

// 获取银行卡LOGO
//
//	ban	银行卡缩写
func GetLogo(ban string) string {
	return "https://apimg.alipay.com/combo.png?d=cashier&t=" + strings.ToUpper(ban)
}
