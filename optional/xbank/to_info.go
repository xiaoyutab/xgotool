package xbank

import (
	"strings"

	"gitee.com/xiaoyutab/xgotool/https"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 获取银行卡详情信息
//
//	no	卡号
func ToInfo(no string) *BankIcon {
	type ali_bank struct {
		CardType  string `json:"cardType"`
		Bank      string `json:"bank"`
		Key       string `json:"key"`
		Validated bool   `json:"validated"`
		Stat      string `json:"stat"`
	}
	no = strings.ReplaceAll(no, " ", "") // 卡号去除空格
	bak := ali_bank{}
	// 请求数据
	err := https.New("https://ccdcapi.alipay.com/validateAndCacheCardInfo.json").Param(map[string]string{
		"_input_charset": "utf-8",
		"cardNo":         no,
		"cardBinCheck":   "true",
	}).Get().Json(&bak)
	if err != nil {
		// 银行卡类型获取失败
		xlog.Alert("银行卡信息获取失败", err)
		inf := default_bank
		inf.CardNo = no
		return &inf
	}
	inf := GetIcon(bak.Bank)
	if bak.CardType == "DC" {
		// 储蓄卡
		inf.CardType = 1
	} else if bak.CardType == "CC" {
		inf.CardType = 2
	} else {
		inf.CardType = 3
	}
	inf.CardNo = bak.Key
	inf.Validated = bak.Validated
	return inf
}
