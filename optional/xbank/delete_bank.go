package xbank

// 删除银行卡标识
//
//	id	待删除的银行卡ID
func DeleteBank(id uint) error {
	inf, err := GetInfo(id)
	if err != nil {
		return err
	}
	inf.IsDeleted = 1
	return Create(inf)
}
