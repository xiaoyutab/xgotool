package xbank

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 获取银行卡详情信息
//
//	id	银行卡ID
func GetInfo(id uint) (*BankUser, error) {
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	// 查询列表数据
	mods := BankUser{}
	err := _default.DB.Table(_default.BankName).
		Where("id", id).
		Where("is_deleted", 0).
		Order("id DESC").
		Find(&mods).
		Error
	if err != nil {
		return nil, xlog.AE("用户银行卡列表获取失败", err)
	}
	if mods.Id == 0 {
		return nil, errors.New("银行卡未找到")
	}
	tmps := GetIcon(mods.Platform)
	mods.Background = tmps.Background
	mods.Icon = tmps.Icon
	mods.Logo = tmps.Logo
	return &mods, nil
}
