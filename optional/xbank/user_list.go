package xbank

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 获取用户银行卡列表
//
//	uid	用户ID
func UserList(uid uint) ([]BankUser, error) {
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	// 查询列表数据
	mods := []BankUser{}
	err := _default.DB.Table(_default.BankName).
		Where("user_id", uid).
		Where("is_deleted", 0).
		Order("id DESC").
		Find(&mods).
		Error
	if err != nil {
		return nil, xlog.AE("用户银行卡列表获取失败", err)
	}
	for i := 0; i < len(mods); i++ {
		tmps := GetIcon(mods[i].Platform)
		mods[i].Background = tmps.Background
		mods[i].Icon = tmps.Icon
		mods[i].Logo = tmps.Logo
	}
	return mods, nil
}
