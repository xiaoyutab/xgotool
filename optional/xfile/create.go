package xfile

import (
	"errors"
	"time"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
)

// 存储文件信息
// 其中，Md5、SHA1、FilePath、Size四项不允许为空
//
// 返回值中，bool类型的是否存在【存在的话将不会再次存储】
// PS: 若返回已存在的话，源文件可直接进行删除，而传入值f则会被替换为已存在的文件详情信息
func Create(f *File) (bool, error) {
	if f.Md5 == "" {
		return false, errors.New("MD5签名不能为空")
	}
	if f.Sha1 == "" {
		return false, errors.New("SHA1签名不能为空")
	}
	if f.FilePath == "" {
		return false, errors.New("文件路径不能为空")
	}
	if f.Size == 0 {
		return false, errors.New("文件大小不能为空")
	}
	if _default.DB == nil {
		return false, errors.New("数据库未连接")
	}
	if f.Id == 0 {
		// 检测文件是否存在
		old, err := FindSearch(f.Md5, f.Sha1, f.Size)
		if err == nil {
			f = old
			xcache.SetStruct(xcache.Key("xfile.find", f.Id), f)
			return true, nil
		}
	}
	// 缺省值填充
	if f.CreatedAt == "" {
		f.CreatedAt = time.Now().Format(time.DateTime)
	}
	err := _default.DB.Save(f).Error
	if err != nil {
		return false, err
	}
	xcache.SetStruct(xcache.Key("xfile.find", f.Id), f)
	return false, nil
}
