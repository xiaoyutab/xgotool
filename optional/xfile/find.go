package xfile

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
)

// 获取已存在的文件标识
//
//	md5		文件的MD5签名
//	sha1	文件的SHA1签名
//	size	文件大小，单位：B
func FindSearch(md5, sha1 string, size uint) (*File, error) {
	if md5 == "" || sha1 == "" || size == 0 {
		return nil, errors.New("查询参数错误")
	}
	f := File{}
	cache_key := xcache.Key("xfile.findSearch", md5, sha1, size)
	if xcache.Exists(cache_key) {
		if err := xcache.GetStruct(cache_key, &f); err == nil {
			return &f, nil
		}
	}
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	err := _default.DB.Table(_default.FileName).Where("md5", md5).Where("sha1", sha1).Where("size", size).Find(&f).Error
	if err != nil {
		return nil, err
	}
	if f.Id == 0 {
		return nil, errors.New("文件不存在")
	}
	xcache.SetStruct(cache_key, f)
	return &f, nil
}

// 查询文件详情信息
//
//		此方法会根据file表中的hash_id字段来获取上一级的文件信息，并进行覆盖
//
//	id	文件ID
func Find(id uint) (*File, error) {
	if id == 0 {
		return nil, errors.New("文件ID传输错误")
	}
	f := File{}
	cache_key := xcache.Key("xfile.find", id)
	if xcache.Exists(cache_key) {
		if err := xcache.GetStruct(cache_key, &f); err == nil {
			return &f, nil
		}
	}
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	err := _default.DB.Table(_default.FileName).Where("id", id).Find(&f).Error
	if err != nil {
		return nil, err
	}
	if f.Id == 0 {
		return nil, errors.New("文件不存在")
	}
	// 文件ID替换逻辑
	for {
		if f.HashId == 0 {
			break
		}
		err := _default.DB.Table(_default.FileName).Where("id", f.HashId).Find(&f).Error
		if err != nil {
			return nil, err
		}
		if f.Id == 0 {
			return nil, errors.New("上级文件未找到")
		}
		xcache.SetStruct(xcache.Key("xfile.find", f.Id), f)
	}
	xcache.SetStruct(cache_key, f)
	return &f, nil
}
