// 自建文件管理服务
package xfile

import "gorm.io/gorm"

// 配置项
type Config struct {
	DB           *gorm.DB
	FileName     string // file表存储表名
	FileDiffName string // diff表存储表名
}

// 默认配置项
var _default Config = Config{
	FileName:     "file",
	FileDiffName: "file_diff",
}

// 入口配置
func Regedit(c *Config) {
	if c == nil {
		return
	}
	if c.DB != nil {
		_default.DB = c.DB
	}
	if c.FileName != "" {
		_default.FileName = c.FileName
	}
	if c.FileDiffName != "" {
		_default.FileDiffName = c.FileDiffName
	}
	// 数据库/表注册/创建
	if _default.DB != nil {
		_default.DB.AutoMigrate(&File{}, &Diff{})
	}
}

// 文件管理服务，支持文件上传、查看、打开统计
// 其中，图片文件额外包含格式转换，均转换为webp格式进行存储
type File struct {
	Id          uint   `gorm:"column:id;primaryKey;not null;autoIncrement" json:"id" form:"id"`                                            // 自增ID
	Md5         string `gorm:"column:md5;type:CHAR(32);index:search_key;comment:文件MD5签名" json:"md5" form:"md5"`                            //文件MD5签名
	Sha1        string `gorm:"column:sha1;type:VARCHAR(100);index:search_key;comment:文件SHA1签名" json:"sha1" form:"sha1"`                    //文件SHA1签名
	Size        uint   `gorm:"column:size;index:search_key;comment:文件大小，单位：B" json:"size" form:"size"`                                     //文件大小，单位：B
	FilePath    string `gorm:"column:file_path;type:VARCHAR(200);comment:图片存放位置" json:"file_path" form:"file_path"`                        //图片存放位置
	QualityPath string `gorm:"column:quality_path;type:VARCHAR(200);comment:压缩后的图片存放位置，压缩质量暂定为10" json:"quality_path" form:"quality_path"` //压缩后的图片存放位置，压缩质量暂定为10
	Thumb100    string `gorm:"column:thumb100;type:VARCHAR(200);comment:100x100缩略图存放位置" json:"thumb100" form:"thumb100"`                   //100x100缩略图存放位置
	Thumb200    string `gorm:"column:thumb200;type:VARCHAR(200);comment:200x200缩略图存放位置" json:"thumb200" form:"thumb200"`                   //200x200缩略图存放位置
	Width       uint   `gorm:"column:width;comment:图片/视频宽度" json:"width" form:"width"`                                                     //图片/视频宽度
	Height      uint   `gorm:"column:height;comment:图片/视频高度" json:"height" form:"height"`                                                  //图片/视频高度
	Length      uint   `gorm:"column:length;comment:视频/音频时间长度，单位：ms" json:"length" form:"length"`                                          //视频/音频时间长度，单位：ms
	Ahash       uint64 `gorm:"column:ahash;type:BIGINT UNSIGNED;comment:A-Hash" json:"ahash" form:"ahash"`                                 //A-Hash
	Dhash       uint64 `gorm:"column:dhash;type:BIGINT UNSIGNED;comment:D-Hash" json:"dhash" form:"dhash"`                                 //D-Hash
	Phash       uint64 `gorm:"column:phash;type:BIGINT UNSIGNED;comment:P-Hash" json:"phash" form:"phash"`                                 //P-Hash
	HashId      uint   `gorm:"column:hash_id;comment:替换图ID，存在此ID的不再进行比对计算，此值不为0则表示原图已删除，使用此ID定位的图来进行展示" json:"hash_id" form:"hash_id"`     //替换图ID，存在此ID的不再进行比对计算，此值不为0则表示原图已删除，使用此ID定位的图来进行展示
	CreatedAt   string `gorm:"column:created_at;type:DATETIME;comment:创建时间" json:"created_at" form:"created_at"`                           // 创建时间
}

// 获取表名
func (c *File) TableName() string {
	return _default.FileName
}

// 文件hash比较表
type Diff struct {
	Id        uint64 `gorm:"column:id;primaryKey;not null;autoIncrement" json:"id" form:"id"`                              // 自增ID
	Fid1      uint   `gorm:"column:fid1;comment:文件1的ID" json:"fid1" form:"fid1"`                                           //文件1的ID
	Fid2      uint   `gorm:"column:fid2;comment:文件2的ID" json:"fid2" form:"fid2"`                                           //文件2的ID
	AhashDiff int    `gorm:"column:ahash_diff;comment:a hash的差别" json:"ahash_diff" form:"ahash_diff"`                      //a hash的差别
	DhashDiff int    `gorm:"column:dhash_diff;comment:d hash的差别" json:"dhash_diff" form:"dhash_diff"`                      //d hash的差别
	PhashDiff int    `gorm:"column:phash_diff;comment:p hash的差别" json:"phash_diff" form:"phash_diff"`                      //p hash的差别
	IsOver    uint8  `gorm:"column:is_over;type:TINYINT UNSIGNED;comment:是否已处理 0-未处理 1-已处理" json:"is_over" form:"is_over"` //是否已处理
	CreatedAt string `gorm:"column:created_at;type:DATETIME;comment:添加时间" json:"created_at" form:"created_at"`             //添加时间
	UpdatedAt string `gorm:"column:updated_at;type:DATETIME;comment:处理时间" json:"updated_at" form:"updated_at"`             //处理时间
}

// 获取表名
func (c *Diff) TableName() string {
	return _default.FileDiffName
}
