package xfile

import (
	"errors"
	"os"
)

// 写入文件
//
//	file_path	文件路径
//	cont		文件内容
func Write(file_path string, cont []byte) error {
	if file_path == "" {
		return errors.New("文件名不能为空")
	}
	file, err := os.OpenFile(file_path, os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		return err
	}
	defer file.Close()
	// 写入文件
	_, err = file.Write(cont)
	if err != nil {
		return err
	}
	return nil
}

// 写入文件(字符串)
//
//	file_path	文件路径
//	cont		文件内容
func WriteString(file_path, cont string) error {
	return Write(file_path, []byte(cont))
}
