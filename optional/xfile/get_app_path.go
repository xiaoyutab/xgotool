package xfile

import (
	"os"
	"os/user"
	"runtime"
	"strings"
)

// 获取程序的配置目录
//
//	name	子级目录名称，多级可以直接传入/，也可以传入多个参数
func GetAppPath(name ...string) (string, error) {
	u, err := user.Current()
	if err != nil {
		return "", err
	}
	path := u.HomeDir + "/.config/"
	if runtime.GOOS == "windows" {
		path = u.HomeDir + "/AppData/Local/"
	}
	if len(name) > 0 {
		// 拼接用户输入的路径
		pth := strings.Join(name, "/") + "/"
		// 特殊符号过滤
		for _, v := range []string{":", "\\", "*", "?", "\"", "'", "<", ">", "|", "&", ";", "{", "}", "(", ")"} {
			pth = strings.ReplaceAll(pth, v, "")
		}
		path += pth
	}
	err = os.MkdirAll(path, os.ModePerm)
	if err != nil {
		return "", err
	}
	return path, nil
}
