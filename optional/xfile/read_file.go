package xfile

import (
	"errors"
	"os"
)

// 读取文件的配置信息
type ReadFileOption struct {
	Child     bool   // 是否读取下级目录
	Joins     string // 多个文件拼接时的参数选项
	Hierarchy int    // 递归读取时最大层级，0-不限制
	nowLevel  int    // 当前读取层级
}

// 读取文件
//
//	path	待读取的文件/目录
//	opt		文件读取选项
func ReadFile(path string, opt *ReadFileOption) ([]byte, error) {
	if opt == nil {
		opt = &ReadFileOption{
			Joins: "\n",
		}
	}
	if opt.Hierarchy != 0 && opt.nowLevel >= opt.Hierarchy {
		return nil, errors.New("已经读取到最大层级了")
	}
	inf, err := os.Stat(path)
	if err != nil {
		return nil, err
	}
	opt.nowLevel++
	out := []byte{}
	if inf.IsDir() {
		if !opt.Child {
			return nil, errors.New("输入路径为目录，无法进行读取")
		}
		dir, err := os.ReadDir(path)
		if err != nil {
			return nil, err
		}
		for i := 0; i < len(dir); i++ {
			if dir[i].Name() == "." || dir[i].Name() == ".." {
				continue
			}
			inf, err := ReadFile(path+"/"+dir[i].Name(), opt)
			if err != nil {
				continue
			}
			out = append(out, []byte(opt.Joins)...)
			out = append(out, inf...)
		}
	} else {
		out, err = os.ReadFile(path)
		if err != nil {
			return nil, err
		}
	}
	return out, nil
}
