package xfile

import (
	"errors"
	"io"
	"os"
)

// 复制文件
//
//	from	源文件
//	to		目标文件
//	size	缓冲区大小(默认：1024*1024KB=1MB，建议大小：100K+)
func Copy(from, to string, size int) error {
	if size <= 0 {
		size = 1024 * 1024
	}
	if _, err := os.Stat(from); err != nil {
		return err
	}
	if _, err := os.Stat(to); err == nil {
		return errors.New("目标文件已存在")
	}
	source, err := os.Open(from)
	if err != nil {
		return err
	}
	destination, err := os.Create(to)
	if err != nil {
		return err
	}
	buf := make([]byte, size)
	for {
		n, err := source.Read(buf)
		if err != nil && err != io.EOF {
			return err
		}
		if n == 0 {
			break
		}

		if _, err := destination.Write(buf[:n]); err != nil {
			return err
		}
	}
	return nil
}
