package xfile

import "strings"

const (
	ExtNone     = iota // 未知文件类型
	ExtImage           // 图片类型
	ExtDocument        // 文档类型
	ExtAudio           // 音频类型
	ExtVideo           // 视频类型
	ExtConfig          // 配置文件
	ExtZip             // 压缩包
)

// 根据后缀判断文件类型
//
//	ext	文件后缀，使用path.Ext(f.Filename)获取到的文件后缀
func CheckType(ext string) int {
	if len(ext) <= 0 {
		return ExtNone
	}
	ext = strings.ToLower(ext)
	if ext[0] == '.' {
		ext = ext[1:]
	}
	switch ext {
	case "png", "jpg", "jpeg", "gif", "webp", "avif", "ico", "cur", "tga", "iff", "ani", "tiff", "bmp", "pcx", "jfif":
		return ExtImage
	case "txt", "doc", "docx", "xls", "xlsx", "ppt", "pptx", "pdf", "md", "mobi", "epub":
		return ExtDocument
	case "mp3", "aac", "wav", "wma", "cda", "flac", "m4a", "mid", "mka", "mp2", "mpa", "mpc", "ape", "ofr", "ogg", "ra",
		"wv", "tta", "ac3", "dts":
		return ExtAudio
	case "mpg", "mpeg", "avi", "rm", "rmvb", "mov", "wmv", "asf", "dat", "mp4", "asx", "wvx", "mpe":
		return ExtVideo
	case "ini", "yml", "env", "conf", "json":
		return ExtConfig
	case "zip", "rar", "7z", "tar", "bz", "gz", "tgz", "tbz", "bz2":
		return ExtZip
	default:
		return ExtNone
	}
}
