package xfile_test

import (
	"testing"

	"gitee.com/xiaoyutab/xgotool/optional/xfile"
)

// 判断文件类型
func TestFileExt(t *testing.T) {
	t.Log(xfile.CheckType("jpg") == xfile.ExtImage)
}
