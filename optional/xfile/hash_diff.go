package xfile

import (
	"errors"
	"math/bits"
	"time"
)

// 针对文件1和文件2进行hash比较，并将值存储到hash比较表 `Diff` 中
//
//	fid1	文件1的ID
//	fid2	文件2的ID
func HashDiff(fid1, fid2 uint) error {
	if fid1 == 0 || fid2 == 0 {
		return errors.New("文件ID传输错误")
	}
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	// 查询 Diff 表中是否存在该记录
	d := Diff{}
	err := _default.DB.Table(_default.FileDiffName).
		Where("( fid1 = ? and fid2 = ? ) or ( fid1 = ? and fid2 = ? )", fid1, fid2, fid1, fid2).
		Limit(1).
		Find(&d).
		Error
	if err != nil {
		return err
	}
	// 若存在记录则直接返回
	if d.Id > 0 {
		return nil
	}
	// 获取文件内容进行hash比较
	f1, err := Find(fid1)
	if err != nil {
		return err
	}
	f2, err := Find(fid2)
	if err != nil {
		return err
	}
	if f1.Ahash == 0 || f1.Phash == 0 || f1.Dhash == 0 || f2.Ahash == 0 || f2.Phash == 0 || f2.Dhash == 0 {
		return nil
	}
	fdiff := Diff{
		Fid1:      f1.Id,
		Fid2:      f2.Id,
		AhashDiff: bits.OnesCount64(f1.Ahash ^ f2.Ahash),
		DhashDiff: bits.OnesCount64(f1.Dhash ^ f2.Dhash),
		PhashDiff: bits.OnesCount64(f1.Phash ^ f2.Phash),
		IsOver:    0,
		CreatedAt: time.Now().Format(time.DateTime),
		UpdatedAt: time.Now().Format(time.DateTime),
	}
	// 如果文件差异较大则直接返回成功，将不插入比较区间
	if fdiff.AhashDiff > 5 && fdiff.DhashDiff > 5 && fdiff.PhashDiff > 5 {
		return nil
	}
	return _default.DB.Save(&fdiff).Error
}
