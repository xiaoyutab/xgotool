package xbook

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 获取书籍列表信息
//
//	keywords	搜索关键词
//	offset		跳过条数
//	limit		查询条数
func BookList(keywords string, offset, limit int) (int64, []Book, error) {
	if _default.DB == nil {
		return 0, nil, errors.New("数据库连接失败")
	}
	db := _default.DB.Table(_default.BooksName).
		Where("is_deleted", 0).
		Where("audit_status", 99).
		Where("( `title` LIKE ? OR `desc` LIKE ? OR author LIKE ? OR `ids` = ?)",
			"%"+keywords+"%", "%"+keywords+"%", "%"+keywords+"%", keywords,
		)
	var count int64
	err := db.Count(&count).Error
	if err != nil {
		return 0, nil, xlog.AE("书籍搜索错误", err)
	}
	lists := []Book{}
	err = db.Order("id DESC").Offset(offset).Limit(limit).Find(&lists).Error
	if err != nil {
		return 0, nil, xlog.AE("书籍搜索结果提取", err)
	}
	return count, lists, nil
}
