package xbook

import (
	"errors"
	"net/url"
)

// 获取爬取规则
//
//	domain	按域名搜索
//	offset	跳过条数
//	limit	查询条数
func DomainChapterList(domain string, offset, limit int) (int64, []BookDomainChapter, error) {
	if _default.DB == nil {
		return 0, nil, errors.New("数据库未连接")
	}
	db := _default.DB.Table(_default.BookDomainChapterName)
	if domain != "" {
		if len(domain) > 4 && domain[0:4] == "http" {
			u, err := url.Parse(domain)
			if err != nil {
				return 0, nil, err
			}
			domain = u.Host
		}
		db.Where("FIND_IN_SET(?,domain)", domain)
	}
	var count int64
	err := db.Count(&count).Error
	if err != nil {
		return 0, nil, err
	}
	if count == 0 {
		return 0, []BookDomainChapter{}, nil
	}
	lis := []BookDomainChapter{}
	err = db.Order("id DESC").Offset(offset).Limit(limit).Find(&lis).Error
	if err != nil {
		return 0, nil, err
	}
	for i := 0; i < len(lis); i++ {
		lis[i] = *lis[i].InitData()
	}
	return count, lis, nil
}
