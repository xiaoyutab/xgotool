// 书籍模组
// 其中的书籍查找功能请自行连接数据库编写，此模组暂不提供书籍查找功能
package xbook

import (
	"strings"

	"gorm.io/gorm"
)

// 配置项信息
type Config struct {
	DB                    *gorm.DB
	BooksName             string // 书籍所在数据表名称
	TitlesName            string // 书籍章节所在数据表名称
	ContentName           string // 书籍内容所在数据表名称
	BookDomainChapterName string // 书籍爬虫爬取规则表名称
}

// 默认配置项
var _default Config = Config{
	BooksName:             "book",
	TitlesName:            "book_menu",
	ContentName:           "book_content",
	BookDomainChapterName: "book_domain_chapter",
}

// 入口配置
func Regedit(c *Config) {
	if c == nil {
		return
	}
	if c.DB != nil {
		_default.DB = c.DB
	}
	if c.BooksName != "" {
		_default.BooksName = c.BooksName
	}
	if c.TitlesName != "" {
		_default.TitlesName = c.TitlesName
	}
	if c.ContentName != "" {
		_default.ContentName = c.ContentName
	}
	if _default.DB != nil {
		_default.DB.AutoMigrate(&Book{},
			&BookChapter{}, &BookContent{},
			&BookDomainChapter{},
		)
	}
}

// 书籍表，存储书籍简介等信息
type Book struct {
	Id                uint   `gorm:"column:id;primaryKey;autoIncrement;not null" form:"id" json:"id"`
	UserId            uint   `gorm:"column:user_id;index:user_id;comment:创建人" json:"user_id" form:"user_id"`                                            // 创建人ID
	Title             string `gorm:"column:title;type:varchar(200);comment:书籍标题" form:"title" json:"title"`                                             //书籍标题
	Desc              string `gorm:"column:desc;type:varchar(500);comment:书籍简单描述" form:"desc" json:"desc"`                                              //书籍简单描述
	Content           string `gorm:"column:content;type:text;comment:书籍简介" form:"content" json:"content"`                                               //书籍简介
	Thumb             uint   `gorm:"column:thumb;comment:书籍封面图ID" form:"thumb" json:"thumb"`                                                            //书籍封面图片（图片ID）
	Author            string `gorm:"column:author;type:varchar(100);comment:书籍作者" form:"author" json:"author"`                                          //书籍作者
	Ids               string `gorm:"column:ids;type:varchar(200);comment:书籍出版IDS编号" form:"ids" json:"ids"`                                              //书籍出版IDS编号
	PublishDate       string `gorm:"column:publish_date;type:date;comment:出版日期" form:"publish_date" json:"publish_date"`                                //出版日期
	PublishShop       string `gorm:"column:publish_shop;type:varchar(100);comment:出版商" form:"publish_shop" json:"publish_shop"`                         //出版商
	AuditStatus       uint8  `gorm:"column:audit_status;type:TINYINT UNSIGNED;index:audit_status;comment:审核状态" json:"audit_status" form:"audit_status"` // 审核状态 0-未审核 98-审核拒绝 99-审核通过 97-放弃审核(编辑以后再次编辑时，会将之前的编辑文章调整为放弃审核状态)
	OverStatus        uint8  `gorm:"column:over_status;type:TINYINT UNSIGNED;index:audit_status;comment:完结状态" json:"over_status" form:"over_status"`    // 完结状态 1-连载中 2-已完结 3-已太监
	IsDeleted         uint8  `gorm:"column:is_deleted;type:tinyint unsigned;comment:是否删除" form:"-" json:"-"`                                            //是否删除
	CollectionWebsite string `gorm:"column:collection_website;type:varchar(200)" form:"collection_website" json:"collection_website"`                   // 采集网址
	CreatedAt         string `gorm:"column:created_at;type:datetime" form:"created_at" json:"created_at"`
	UpdatedAt         string `gorm:"column:updated_at;type:datetime" form:"updated_at" json:"updated_at"`
}

// 返回所属表名信息
func (c *Book) TableName() string {
	return _default.BooksName
}

// 书籍章节内容表
type BookChapter struct {
	Id          uint   `gorm:"column:id;primaryKey;autoIncrement;not null" form:"id" json:"id"`
	BookId      uint   `gorm:"column:book_id;comment:书籍ID" form:"book_id" json:"book_id"`                                      //书籍ID
	Title       string `gorm:"column:title;type:varchar(200);comment:章节标题" form:"title" json:"title"`                          //章节标题
	Fid         uint   `gorm:"column:fid;comment:上级章节/册的ID" form:"fid" json:"fid"`                                             //上级章节/册的ID
	AuditStatus uint8  `gorm:"column:audit_status;type:TINYINT UNSIGNED;comment:审核状态" json:"audit_status" form:"audit_status"` // 审核状态 0-未审核 98-审核拒绝 99-审核通过 97-放弃审核(编辑以后再次编辑时，会将之前的编辑文章调整为放弃审核状态)
	OrderNum    uint   `gorm:"column:order_num;comment:排序字段" form:"order_num" json:"order_num"`                                //上级章节/册的ID
	CreatedAt   string `gorm:"column:created_at;type:datetime" form:"created_at" json:"created_at"`
	UpdatedAt   string `gorm:"column:updated_at;type:datetime" form:"updated_at" json:"updated_at"`
}

// 返回所属表名信息
func (c *BookChapter) TableName() string {
	return _default.TitlesName
}

// 书籍章节内容表
type BookContent struct {
	Id         uint   `gorm:"column:id;primaryKey;not null" form:"id" json:"id"`
	Content    string `gorm:"column:content;type:mediumtext;comment:章节内容" form:"content" json:"content"`        //章节内容
	Translate  string `gorm:"column:translate;type:mediumtext;comment:译文" form:"translate" json:"translate"`    //译文
	Annotation string `gorm:"column:annotation;type:mediumtext;comment:批注" form:"annotation" json:"annotation"` //批注
}

// 返回所属表名信息
func (c *BookContent) TableName() string {
	return _default.ContentName
}

// 爬虫小说抓取规则
type BookDomainChapter struct {
	Id          uint     `gorm:"column:id;primaryKey;autoIncrement;not null" form:"id" json:"id"`
	Domain      string   `gorm:"column:domain;type:text;comment:域名" form:"-" json:"-"`                                       //域名(多域名使用,拼接)
	DomainList  []string `gorm:"-"  form:"domain" json:"domain"`                                                             // 拆分好的域名列表
	ChapterRole string   `gorm:"column:chapter_role;type:varchar(200);comment:章节规则" form:"chapter_role" json:"chapter_role"` //章节规则
	ContentRole string   `gorm:"column:content_role;type:varchar(200);comment:章节规则" form:"content_role" json:"content_role"` //章节规则
	HasText     uint8    `gorm:"column:has_text;type:tinyint unsigned;comment:直接提取TXT？" form:"has_text" json:"has_text"`     //直接提取TXT？
	HasBr       string   `gorm:"column:has_br;type:varchar(100);comment:标签更换换行符" form:"-" json:"-"`                          //标签更换换行符(多个使用,拼接)
	HasBrList   []string `gorm:"-" form:"has_br" json:"has_br"`                                                              // 拆分好的换行符
	ConTrim     string   `gorm:"column:con_trim;type:text;comment:清除文本部分内容" form:"-" json:"-"`                               //清除文本部分内容(多个使用,拼接)
	ConTrimList []string `gorm:"-" form:"con_trim" json:"con_trim"`                                                          // 拆分好的待清除内容
	CreatedAt   string   `gorm:"column:created_at;type:datetime;comment:创建时间" form:"created_at" json:"created_at"`           //创建时间
}

// 返回所属表名信息
func (c *BookDomainChapter) TableName() string {
	return _default.BookDomainChapterName
}

// 数据初始化，用于重新分割内容到具体元素中去
func (c *BookDomainChapter) InitData() *BookDomainChapter {
	if c.Domain != "" {
		c.DomainList = strings.Split(c.Domain, ",")
		c.HasBrList = strings.Split(c.HasBr, ",")
		c.ConTrimList = strings.Split(c.ConTrim, ",")
	}
	return c
}
