package xbook_test

import (
	"net/url"
	"testing"

	"gitee.com/xiaoyutab/xgotool/optional/xbook"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func TestBookList(t *testing.T) {
	db, _ := gorm.Open(mysql.Open("admin:admin@tcp(localhost:3306)/gatway?charset=utf8"), &gorm.Config{})
	xbook.Regedit(&xbook.Config{
		DB: db,
	})
	t.Log(xbook.Gather())
	// xbook.DomainChapterCreate(&xbook.BookDomainChapter{
	// 	DomainList:  []string{"www.baidu.com", "www.biquge66.net"},
	// 	ChapterRole: "#list",
	// 	ContentRole: "#content",
	// })
	// c, lis, err := xbook.DomainChapterList("https://www.biquge66.net/book/1073/1814339.html", 0, 10)
	// if err != nil {
	// 	t.Error(err)
	// }
	// t.Log(c, lis)
	// xbook.BookList("", 0, 10)
}

// 测试域名分割
func TestDomain(t *testing.T) {
	d := "https://www.biquge66.net/book/1073/#info?id=1"
	u, err := url.Parse(d)
	if err != nil {
		t.Error(err)
	}
	t.Log(u.Host, u.Path, u.Fragment)
}
