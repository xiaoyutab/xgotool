package xbook

import "errors"

// 获取待采集的小说列表
func Gather() ([]Book, error) {
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	book := []Book{}
	err := _default.DB.Table(_default.BooksName).
		Where("collection_website <> ''").
		Where("is_deleted", 0).
		Where("over_status", 1).
		Where("audit_status", 99).
		Order("id DESC").
		Find(&book).
		Error
	if err != nil {
		return nil, err
	}
	return book, nil
}
