package xbook

import (
	"errors"
	"time"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 创建书籍信息
//
//	c	书籍的结构信息
func BookCreate(c *Book) error {
	if c.UserId == 0 {
		return errors.New("创建人不能为空")
	}
	if c.Title == "" {
		return errors.New("书籍标题不能为空")
	}
	if c.CreatedAt == "" {
		c.CreatedAt = time.Now().Format(time.DateTime)
	}
	c.UpdatedAt = time.Now().Format(time.DateTime)
	err := _default.DB.Create(c).Error
	if err != nil {
		return xlog.AE("书籍插入失败", err)
	}
	return nil
}
