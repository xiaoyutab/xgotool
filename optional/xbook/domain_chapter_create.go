package xbook

import (
	"errors"
	"strings"
	"time"
)

// 创建域名爬取规则
//
//	c	爬取规则
func DomainChapterCreate(c *BookDomainChapter) error {
	if c == nil {
		return errors.New("爬取规则为空")
	}
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	if len(c.DomainList) > 0 {
		c.Domain = strings.Join(c.DomainList, ",")
	}
	if c.Domain == "" {
		return errors.New("所属域名不能为空")
	}
	if c.ChapterRole == "" {
		return errors.New("章节规则不能为空")
	}
	if c.ContentRole == "" {
		return errors.New("内容规则不能为空")
	}
	if len(c.ConTrimList) > 0 {
		c.ConTrim = strings.Join(c.ConTrimList, ",")
	}
	if len(c.HasBrList) > 0 {
		c.HasBr = strings.Join(c.HasBrList, ",")
	}
	if c.CreatedAt == "" {
		c.CreatedAt = time.Now().Format(time.DateTime)
	}
	db := _default.DB.Table(c.TableName()).Save(c)
	if db.Error != nil {
		return db.Error
	}
	if db.RowsAffected == 0 {
		return errors.New("数据前后无变化")
	}
	if c.Id == 0 {
		return errors.New("系统存储错误")
	}
	return nil
}
