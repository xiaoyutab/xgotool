package xbook

import (
	"errors"
	"time"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 创建章节信息
//
//	c	章节信息
//	cn	内容信息（该项的ID字段会直接进行覆盖）
func MenuCreate(c *BookChapter, cn *BookContent) error {
	if c == nil {
		return errors.New("书籍章节为空")
	}
	if c.BookId <= 0 {
		return errors.New("所属书籍不能为空")
	}
	if c.Title == "" {
		return errors.New("章节标题不能为空")
	}
	c.UpdatedAt = time.Now().Format(time.DateTime)
	if c.CreatedAt != "" {
		c.CreatedAt = time.Now().Format(time.DateTime)
	}
	// 使用fid组无限分类式菜单，而不是使用type组分卷式菜单
	// 插入数据
	err := _default.DB.Table(_default.TitlesName).Save(c).Error
	if err != nil {
		return xlog.AE("章节信息存储失败", err)
	}
	// 更新内容信息
	if cn != nil {
		cn.Id = c.Id
		err = _default.DB.Table(_default.ContentName).Save(cn).Error
		if err != nil {
			return xlog.AE("章节内容信息存储失败", err)
		}
	}
	return nil
}
