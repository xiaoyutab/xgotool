package xsource

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 获取批次详情
//
//	id	批次ID
func Info(id uint) (*Source, error) {
	if id == 0 {
		return nil, errors.New("批次ID传输错误")
	}
	cache_key := xcache.Key("xsource.info", id)
	inf := Source{}
	if xcache.Exists(cache_key) {
		if err := xcache.GetStruct(cache_key, &inf); err == nil {
			return &inf, nil
		}
	}
	// 缓存中没有，从库中查询
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	err := _default.DB.Table(_default.SourceName).Where("id", id).Find(&inf).Error
	if err != nil {
		return nil, xlog.EE("溯源批次数据查询失败", err)
	}
	if inf.ID == 0 {
		return nil, errors.New("数据未找到")
	}
	// 存储缓存
	xcache.SetStruct(cache_key, inf)
	return &inf, nil
}
