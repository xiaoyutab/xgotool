package xsource

import (
	"fmt"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 创建批次描述信息
//
//	sid		批次ID
//	source	待创建的批次描述信息
func CreateDesc(sid uint, source []SourceDesc) error {
	// 因为是批量添加，所以此处使用for循环的形式进行添加
	for i := 0; i < len(source); i++ {
		// 批次验证
		if source[i].SourceId > 0 && source[i].SourceId != sid {
			return fmt.Errorf("第 %d 个列表数据的批次ID[%d]和传入sid[%d]不一致，请检查后再次尝试", i+1, source[i].SourceId, sid)
		}
		// 名称验证
		if source[i].TypesName == "" {
			return fmt.Errorf("第 %d 个列表数据的描述项名称为空，请检查后再次尝试", i+1)
		}
		if source[i].SourceId == 0 {
			source[i].SourceId = sid
		}
	}
	// 删除已有的描述信息，进行重新添加
	err := _default.DB.Table(_default.SourceDescName).Where("source_id", sid).Delete(&SourceDesc{}).Error
	if err != nil {
		return xlog.EE("批次描述信息存储失败", err)
	}
	xcache.Remove(xcache.Key("xsource.info_desc", sid))
	for i := 0; i < len(source); i++ {
		// 进行存储
		err := _default.DB.Save(&source[i]).Error
		if err != nil {
			return fmt.Errorf("第 %d 个列表数据插入失败，原因：%s", i+1, err.Error())
		}
	}
	return nil
}
