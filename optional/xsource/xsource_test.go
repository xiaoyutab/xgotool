package xsource_test

import (
	"testing"
	"time"

	"gitee.com/xiaoyutab/xgotool/optional/xsource"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func TestCreateChild(t *testing.T) {
	db, _ := gorm.Open(mysql.Open("admin:admin@tcp(localhost:3306)/gatway?charset=utf8"), &gorm.Config{})
	xsource.Regedit(&xsource.Config{
		DB: db,
	})
	// err := xsource.Create(&xsource.Source{
	// 	Name:   "测试批次",
	// 	UserId: 1,
	// })
	// err := xsource.CreateDesc(1, []xsource.SourceDesc{{TypesName: "测试名称", Value: "描述值"}})
	// err := xsource.CreateChild(1, 200)
	// err := xsource.CreateChildCode(1, []string{"a1b18e79-35d8-11ee-85b3-426e4af4371a"})
	inf, err := xsource.InfoGoods(1, "", "用户查询", "", "127.0.0.1", 1)
	if err != nil {
		t.Error(err)
	}
	log, err := xsource.LogList(1)
	if err != nil {
		t.Error(err)
	}
	t.Log(log, inf)
	time.Sleep(time.Second * 2)
}
