package xsource

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 获取批次商品详情
//
//	id		批次ID
//	offset	跳过条数
//	limit	查询条数
func InfoChild(id uint, offset, limit int) (int64, []SourceChild, error) {
	if id == 0 {
		return 0, nil, errors.New("批次ID传输错误")
	}
	// 缓存中没有，从库中查询
	if _default.DB == nil {
		return 0, nil, errors.New("数据库未连接")
	}
	inf := []SourceChild{}
	var count int64
	err := _default.DB.Table(_default.SourceChildName).Where("source_id", id).Count(&count).Error
	if err != nil {
		return 0, nil, xlog.EE("溯源批次商品条数查询失败", err)
	}
	err = _default.DB.Table(_default.SourceChildName).Where("source_id", id).Order("id DESC").Offset(offset).Limit(limit).Find(&inf).Error
	if err != nil {
		return 0, nil, xlog.EE("溯源批次商品数据查询失败", err)
	}
	return count, inf, nil
}
