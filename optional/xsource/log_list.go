package xsource

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 溯源商品查询日志
//
//	id	溯源产品ID
func LogList(id uint64) ([]SourceLog, error) {
	if id == 0 {
		return nil, errors.New("参数传递错误")
	}
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	mod := []SourceLog{}
	err := _default.DB.Table(_default.SourceLogName).Where("source_child_id", id).Order("id desc").Find(&mod).Error
	if err != nil {
		return nil, xlog.AE("溯源产品查询日志获取错误", err)
	}
	return mod, nil
}
