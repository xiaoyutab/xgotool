// 溯源模块
package xsource

import (
	"gorm.io/gorm"
)

// 溯源模块逻辑
// 1. 后台插入产品详情信息，插入产品名称、产品描述、产品图片等
// 2. 生产产品时，追加插入产品的附加信息
// 3.
//
// 此方法使用Create等方法插入初始数据，然后使用InfoGoods来查询/验证，使用LogList来确定之前查询的次数及IP等信息
type Config struct {
	DB              *gorm.DB // 数据库连接
	SourceName      string   // 溯源批次表名称
	SourceDescName  string   // 溯源批次附属信息表
	SourceChildName string   // 溯源详情表名称
	SourceLogName   string   // 流转表名称
}

var _default Config = Config{
	SourceName:      "source",
	SourceDescName:  "source_desc",
	SourceChildName: "source_child",
	SourceLogName:   "source_log",
}

// 注入配置项
func Regedit(c *Config) {
	if c == nil {
		return
	}
	if c.DB != nil {
		_default.DB = c.DB
	}
	if c.SourceName != "" {
		_default.SourceName = c.SourceName
	}
	if c.SourceLogName != "" {
		_default.SourceLogName = c.SourceLogName
	}
	if c.SourceDescName != "" {
		_default.SourceDescName = c.SourceDescName
	}
	if c.SourceChildName != "" {
		_default.SourceChildName = c.SourceChildName
	}
	if _default.DB != nil {
		_default.DB.AutoMigrate(&Source{}, &SourceDesc{}, &SourceChild{}, &SourceLog{})
	}
}

// 待溯源批次表
type Source struct {
	ID        uint   `gorm:"id;primaryKey;not null;autoIncrement" form:"id" json:"id"`                         // 待溯源商品ID
	Name      string `gorm:"column:name;type:VARCHAR(200);comment:待溯源商品名称" json:"name" form:"name"`            // 待溯源商品名称
	UserId    uint   `gorm:"column:user_id;comment:发布人ID" json:"user_id" form:"user_id"`                       // 发布人ID
	CreatedAt string `gorm:"column:created_at;type:datetime;comment:创建时间" json:"created_at" form:"created_at"` // 创建时间
	UpdatedAt string `gorm:"column:updated_at;type:datetime;comment:修改时间" json:"updated_at" form:"updated_at"` // 修改时间
}

// 表名
func (c *Source) TableName() string {
	return _default.SourceName
}

// 待溯源批次描述表
type SourceDesc struct {
	ID        uint   `gorm:"id;primaryKey;not null;autoIncrement" form:"id" json:"id"`                   // 待溯源商品ID
	SourceId  uint   `gorm:"source_id;comment:溯源批次ID;index:source_id" form:"source_id" json:"source_id"` // 所查询的批次ID
	TypesName string `gorm:"column:types_name" json:"types_name" form:"types_name"`                      // 描述项的名称，具体值
	Value     string `gorm:"column:value;type:VARCHAR(200);comment:描述项的值" json:"value" form:"value"`     // 描述项的值
}

// 表名
func (c *SourceDesc) TableName() string {
	return _default.SourceDescName
}

// 溯源商品表
type SourceChild struct {
	ID        uint64 `gorm:"id;primaryKey;type:BIGINT UNSIGNED;not null;autoIncrement" form:"id" json:"id"`                            // 待溯源商品ID
	SourceId  uint   `gorm:"source_id;comment:溯源批次ID" form:"source_id" json:"source_id"`                                               // 所查询的商品ID
	GoodsCode string `gorm:"column:goods_code;type:VARCHAR(50);comment:待溯源商品标识;UNIQUE:goods_code" json:"goods_code" form:"goods_code"` // 待溯源商品标识，如果未传入则自动生成一个uuid来作为标识
	CreatedAt string `gorm:"column:created_at;type:datetime;comment:创建时间" json:"created_at" form:"created_at"`                         // 创建时间
}

// 表名
func (c *SourceChild) TableName() string {
	return _default.SourceChildName
}

// 溯源入库/出库/运输/查询等的日志
type SourceLog struct {
	ID            uint64 `gorm:"id;primaryKey;type:BIGINT UNSIGNED;not null;autoIncrement" form:"id" json:"id"`                                           // 待溯源商品ID
	SourceChildId uint64 `gorm:"source_child_id;type:BIGINT UNSIGNED;comment:溯源商品ID;index:source_child_id" form:"source_child_id" json:"source_child_id"` // 所查询的商品ID
	Ip            string `gorm:"column:ip;size:50" json:"ip" form:"ip"`                                                                                   // 操作地址IP
	Msg           string `gorm:"column:msg;type:VARCHAR(200);comment:备注信息" json:"msg" form:"msg"`                                                         // 操作备注
	IsFind        uint8  `gorm:"column:is_find;type:TINYINT UNSIGNED;comment:是否为查询操作 0-内部流转 1-外部查询 2-产品作废" json:"is_find" form:"is_find"`                 // 是否是查询操作【用于区分内部流转还是外部查询】
	UserAgent     string `gorm:"column:user_agent;size:300" json:"user_agent" form:"user_agent"`                                                          // 操作用户的UA标识
	CreatedAt     string `gorm:"column:created_at;type:datetime;comment:创建时间" json:"created_at" form:"created_at"`                                        // 创建时间
}

// 表名
func (c *SourceLog) TableName() string {
	return _default.SourceLogName
}
