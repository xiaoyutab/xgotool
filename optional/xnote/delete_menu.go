package xnote

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 删除笔记菜单
//
//	id	笔记菜单ID
//	uid	用户ID
func DeleteMenu(id, uid uint) error {
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	err := _default.DB.Table(_default.MenuName).
		Where("id", id).
		Where("user_id", uid).
		Where("is_deleted", 0).
		Update("is_deleted", 1).
		Error
	if err != nil {
		return xlog.AE("用户笔记菜单删除失败", err)
	}
	return nil
}
