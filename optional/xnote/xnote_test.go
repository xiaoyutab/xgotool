package xnote_test

import (
	"testing"

	"gitee.com/xiaoyutab/xgotool/optional/xnote"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func TestXnode(t *testing.T) {
	db, _ := gorm.Open(mysql.Open("admin:admin@tcp(localhost:3306)/gatway?charset=utf8&"), &gorm.Config{})
	xnote.Regedit(&xnote.Config{
		DB: db,
	})
	// t.Log(xnote.CreateMenu(&xnote.NoteMenu{
	// 	UserId: 1,
	// 	Fid:    1,
	// 	Name:   "测试栏目111",
	// }))
	t.Log(xnote.GetMenu(1, 1))
	// 存储内容
	// xnote.SaveContent(1, "测试存储内容哦")
	t.Log(xnote.GetContent(1))
}
