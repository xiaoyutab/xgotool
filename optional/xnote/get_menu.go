package xnote

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 获取笔记菜单标识
//
//	uid	用户ID
//	fid	上级菜单ID
func GetMenu(uid, fid uint) ([]NoteMenu, error) {
	if uid <= 0 {
		return nil, errors.New("用户ID不能为空")
	}
	lis := []NoteMenu{}
	err := _default.DB.Table(_default.MenuName).
		Where("((`fid` = ? AND `type` = 1) OR (`type` = 2 AND FIND_IN_SET(?,`menu_id`)))", fid, fid).
		Where("user_id", uid).
		Where("is_deleted", 0).
		Find(&lis).
		Error
	if err != nil {
		return nil, xlog.AE("个人笔记菜单列表获取失败", err)
	}
	return lis, nil
}
