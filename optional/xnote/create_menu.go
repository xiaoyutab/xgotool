package xnote

import (
	"errors"
	"time"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
	"gitee.com/xiaoyutab/xgotool/xnum"
)

// 创建笔记菜单标识
//
//	c	笔记菜单结构
func CreateMenu(c *NoteMenu) error {
	if c.UserId == 0 {
		return errors.New("菜单所属人不能为空")
	}
	if !xnum.InArray(c.Type, []uint8{1, 2}) {
		return errors.New("分类选择错误")
	}
	if c.CreatedAt == "" {
		c.CreatedAt = time.Now().Format(time.DateTime)
	}
	c.UpdatedAt = time.Now().Format(time.DateTime)
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	err := _default.DB.Table(_default.MenuName).Save(c).Error
	if err != nil {
		return xlog.AE("个人笔记菜单记录失败", err)
	}
	return nil
}
