package xnote

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 获取菜单详情
//
//	id	菜单ID
func GetMenuInfo(id uint) (*NoteMenu, error) {
	if id == 0 {
		return nil, errors.New("详情ID不能为空")
	}
	menu := NoteMenu{}
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	err := _default.DB.Table(_default.MenuName).Where("id", id).Where("is_deleted", 0).Find(&menu).Error
	if err != nil {
		return nil, xlog.AE("笔记章节详情获取失败", err)
	}
	if menu.Id == 0 {
		return nil, xlog.NE("笔记未找到")
	}
	return &menu, nil
}
