package xnote

import "gitee.com/xiaoyutab/xgotool/individual/xlog"

// 获取菜单的目录树结构
type MenuTree struct {
	Id     uint   `json:"id" form:"id"`         // 菜单ID
	Expert string `json:"expert" form:"expert"` // 多级目录分隔符
	Value  string `json:"value" form:"value"`   // 拼接完成的目录结构
}

// 获取目录结构信息
//
//	ids	待获取的目录列表信息
func GetMenuTree(ids []uint) ([]MenuTree, error) {
	tmp := []MenuTree{}
	for i := 0; i < len(ids); i++ {
		if ids[i] == 0 {
			continue
		}
		menu := NoteMenu{}
		// 查询数据
		err := _default.DB.Table(_default.MenuName).Where("id", ids[i]).Where("is_deleted", 0).Where("type", 1).Find(&menu).Error
		if err != nil {
			return nil, xlog.AE("目录结构树获取失败", err)
		}
		if menu.Id == 0 {
			continue
		}
		// 无线递归 查询上级
		if menu.Id > 0 && menu.Fid > 0 {
			id := menu.Fid
			for {
				m := NoteMenu{}
				// 查询数据
				_default.DB.Table(_default.MenuName).Where("id", id).Where("type", 1).Where("is_deleted", 0).Find(&m)
				if m.Id == 0 {
					break
				}
				menu.Name = m.Name + " / " + menu.Name
				if m.Fid == 0 {
					break
				}
				id = m.Fid
			}
		}
		tmp = append(tmp, MenuTree{
			Id:     menu.Id,
			Expert: " / ",
			Value:  menu.Name,
		})
	}
	return tmp, nil
}
