package xnote

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 存储笔记内容信息
//
//	id		栏目ID
//	cont	栏目内容
func SaveContent(id uint, cont string) error {
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	con := NoteContent{
		Id:      id,
		Content: cont,
	}
	err := _default.DB.Table(_default.NoteContentName).Save(&con).Error
	if err != nil {
		return xlog.AE("笔记内容存储失败", err)
	}
	return nil
}
