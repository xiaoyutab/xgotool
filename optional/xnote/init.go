// 个人笔记模块
package xnote

import "gorm.io/gorm"

// 配置项信息
type Config struct {
	DB              *gorm.DB
	NoteContentName string // 笔记内容存储表名
	MenuName        string // 笔记标签/目录存储表明
}

// 默认配置项
var _default Config = Config{
	NoteContentName: "note_content",
	MenuName:        "note_menu",
}

// 入口配置
func Regedit(c *Config) {
	if c == nil {
		return
	}
	if c.DB != nil {
		_default.DB = c.DB
	}
	if c.NoteContentName != "" {
		_default.NoteContentName = c.NoteContentName
	}
	if c.MenuName != "" {
		_default.MenuName = c.MenuName
	}
	if _default.DB != nil {
		_default.DB.AutoMigrate(&NoteMenu{}, &NoteContent{})
	}
}

// 笔记菜单/标签名称表
type NoteMenu struct {
	Id        uint   `gorm:"column:id;primaryKey;autoIncrement;not null" form:"id" json:"id"`
	Fid       uint   `gorm:"column:fid;comment:上级ID" form:"fid" json:"fid"`                                         //上级ID
	UserId    uint   `gorm:"column:user_id;comment:所属用户ID" form:"user_id" json:"user_id"`                           //所属用户ID
	Name      string `gorm:"column:name;type:varchar(200);comment:标签/目录名称" form:"name" json:"name"`                 //标签/目录名称
	Type      uint8  `gorm:"column:type;comment:所属分类 1-菜单 2-内容" form:"type" json:"type"`                            //所属分类 1-菜单 2-内容
	MenuId    string `gorm:"column:menu_id;type:varchar(200);comment:所属目录/标签，关联menu" form:"menu_id" json:"menu_id"` //所属目录/标签，关联menu
	IsDeleted uint8  `gorm:"column:is_deleted;type:tinyint unsigned;comment:是否删除" form:"-" json:"-"`                //是否删除
	CreatedAt string `gorm:"column:created_at;type:datetime;comment:添加时间" form:"created_at" json:"created_at"`      //添加时间
	UpdatedAt string `gorm:"column:updated_at;type:datetime;comment:修改时间" form:"updated_at" json:"updated_at"`      //修改时间
}

// 返回所属表名信息
func (c *NoteMenu) TableName() string {
	return _default.MenuName
}

// 笔记内容存储表
type NoteContent struct {
	Id      uint   `gorm:"column:id;primaryKey;not null;comment:笔记ID，等同node_menu.id" form:"id" json:"id"` //笔记ID，等同node.id
	Content string `gorm:"column:content;type:longtext;comment:笔记内容" form:"content" json:"content"`       //笔记内容
}

// 返回所属表名信息
func (c *NoteContent) TableName() string {
	return _default.NoteContentName
}
