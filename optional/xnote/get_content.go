package xnote

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 获取笔记内容信息
//
//	id	栏目ID
func GetContent(id uint) (string, error) {
	if _default.DB == nil {
		return "", errors.New("数据库未连接")
	}
	con := NoteContent{}
	err := _default.DB.Table(_default.NoteContentName).Where("id", id).Find(&con).Error
	if err != nil {
		return "", xlog.AE("笔记内容获取失败", err)
	}
	return con.Content, nil
}
