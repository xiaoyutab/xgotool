package xdist

// 获取字典信息
//
//	name	字典的name名称
//	group	字典的所属分组
func GetValue(name, group string) string {
	if v, ok := _default.nvk.Load(name + group); ok {
		return v.(string)
	}
	temp := DistInfo{}
	err := _default.DB.Table(temp.TableName()).
		Where("groups", group).
		Where("name", name).
		Order("id ASC").
		Limit(1).
		Find(&temp).
		Error
	if err != nil {
		return ""
	}
	_default.nvk.Store(temp.Name+temp.Groups, temp.Value)
	_default.vnk.Store(temp.Value+temp.Groups, temp.Name)
	return temp.Value
}

// 获取字典信息
//
//	value	字典的value值
//	group	字典的所属分组
func GetName(value, group string) string {
	if v, ok := _default.vnk.Load(value + group); ok {
		return v.(string)
	}
	temp := DistInfo{}
	err := _default.DB.Table(temp.TableName()).
		Where("groups", group).
		Where("value", value).
		Order("id ASC").
		Limit(1).
		Find(&temp).
		Error
	if err != nil {
		return ""
	}
	_default.nvk.Store(temp.Name+temp.Groups, temp.Value)
	_default.vnk.Store(temp.Value+temp.Groups, temp.Name)
	return temp.Value
}
