// 数据库存储数据
package xdist

import (
	"sync"

	"gorm.io/gorm"
)

// 配置信息
type Config struct {
	DB        *gorm.DB // 数据库连接
	DistTable string   // 字典配置表名
	nvk       sync.Map // 字典的键值对缓存
	vnk       sync.Map // 字典的键值对对应的缓存
	gpk       sync.Map // 字典分组缓存
}

var _default Config = Config{
	DistTable: "xdist",
}

func Regedit(c *Config) {
	if c == nil {
		return
	}
	if c.DB != nil {
		_default.DB = c.DB
	}
	if c.DistTable != "" {
		_default.DistTable = c.DistTable
	}
	if _default.DB != nil {
		_default.DB.AutoMigrate(&DistInfo{})
	}
}

// 字典表结构信息
type DistInfo struct {
	Id        uint   `gorm:"column:id;type:int unsigned;primaryKey;autoIncrement;not null" form:"id" json:"id" xml:"id"`
	Groups    string `gorm:"column:groups;type:varchar(100);comment:所属分组" form:"groups" json:"groups" xml:"groups"`             // 所属分组
	Name      string `gorm:"column:name;type:varchar(100);comment:下标名称" form:"name" json:"name" xml:"name"`                     // 下标名称
	Value     string `gorm:"column:value;type:varchar(300);comment:存储值" form:"value" json:"value" xml:"value"`                  // 存储的值
	CreatedAt string `gorm:"column:created_at;type:datetime;comment:添加时间" form:"created_at" json:"created_at" xml:"created_at"` // 添加时间
}

func (c *DistInfo) TableName() string {
	return _default.DistTable
}
