package xdist

import (
	"time"

	"gorm.io/gorm"
)

// 设置字典下标表示
//
//	name	设置的name名称
//	group	所属分组
//	value	需要存储的值
func SetValue(name, group, value string) error {
	// 清理原缓存
	temp_nv := GetValue(name, group)
	_default.nvk.Delete(name + group)
	_default.vnk.Delete(temp_nv + group)
	_default.vnk.Delete(value + group)
	// 修改数据库中的值
	temp := DistInfo{}
	err := _default.DB.Table(temp.TableName()).Where("groups", group).Where("name", name).Order("id ASC").Limit(1).Find(&temp).Error
	if err != nil {
		return err
	}
	temp.Name = name
	temp.Groups = group
	temp.Value = value
	if temp.CreatedAt == "" {
		temp.CreatedAt = time.Now().Format(time.DateTime)
	}
	err = _default.DB.Table(temp.TableName()).Save(&temp).Error
	return err
}

// 设置数据库连接
//
//	db	数据库连接
func SetDB(db *gorm.DB) {
	_default.DB = db
}
