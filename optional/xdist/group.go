package xdist

// 获取分组列表[value => name]
//
//	group	待获取的分组下标
func GroupVN(group string) (map[string]string, error) {
	if v, ok := _default.gpk.Load(group); ok {
		return v.(map[string]string), nil
	}
	gps, gp := []DistInfo{}, DistInfo{}
	err := _default.DB.Table(gp.TableName()).Where("groups", group).Order("id ASC").Find(&gps).Error
	if err != nil {
		return nil, err
	}
	rets := map[string]string{}
	for i := 0; i < len(gps); i++ {
		rets[gps[i].Value] = gps[i].Name
	}
	// 写入缓存
	_default.gpk.Store(group, rets)
	// 进行返回
	return rets, nil
}

// 获取分组列表[name => value]
//
//	group	待获取的分组下标
func GroupNV(group string) (map[string]string, error) {
	tmp, err := GroupVN(group)
	if err != nil {
		return nil, err
	}
	out := map[string]string{}
	for i, v := range tmp {
		out[v] = i
	}
	return out, nil
}
