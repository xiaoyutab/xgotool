package xintegral

import "errors"

// 获取积分账户列表
//
//	uid	用户ID
//	typ	积分类型
func List(uid, typ uint) ([]Integral, error) {
	if uid <= 0 {
		return nil, errors.New("用户ID不能为空")
	}
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	db := _default.DB.Table(_default.IntegralName).Where("user_id", uid)
	if typ > 0 && typ < 5 {
		db = db.Where("type", typ)
	}
	inf := []Integral{}
	err := db.Where("((`type` = 2 and `end_date` > NOW() ) or `type` != 2)").Where("is_deleted", 0).Order("created_at ASC").Find(&inf).Error
	return inf, err
}
