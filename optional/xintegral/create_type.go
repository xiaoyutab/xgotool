package xintegral

import (
	"errors"
	"time"
)

// 创建/修改积分隔离类信息
//
//	c	积分隔离分类存储的信息
func CreateType(c *IntegralType) error {
	if c.UserId <= 0 {
		return errors.New("积分隔离类创建人不能为空")
	}
	if c.Name == "" {
		return errors.New("名称不能为空")
	}
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	if c.CreatedAt == "" {
		c.CreatedAt = time.Now().Format(time.DateTime)
	}
	c.UpdatedAt = time.Now().Format(time.DateTime)
	err := _default.DB.Save(c).Error
	if err != nil {
		return err
	}
	return nil
}
