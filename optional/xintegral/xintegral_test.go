package xintegral_test

import (
	"testing"

	"gitee.com/xiaoyutab/xgotool/optional/xintegral"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// 测试插入积分主要信息
func TestCreateIntegral(t *testing.T) {
	var err error
	db, _ := gorm.Open(mysql.Open("admin:admin@tcp(localhost:3306)/gatway?charset=utf8"), &gorm.Config{})
	xintegral.Regedit(&xintegral.Config{
		DB: db,
	})
	// err = xintegral.Cron()
	// err = xintegral.CreateIntegral(&xintegral.Integral{
	// 	Name:       "测试-3",
	// 	Desc:       "测试积分明细-3",
	// 	UserId:     1,
	// 	Type:       1,
	// 	Balance:    10,
	// 	ExpireDays: 1,
	// })
	// if err != nil {
	// 	t.Error(err)
	// }
	// inf, err := xintegral.Info(1)
	// if err != nil {
	// 	t.Error(err)
	// }
	// t.Log(inf)
	// err = xintegral.CreateHistory(1, 8, "赠送积分", true)
	// if err != nil {
	// 	t.Error(err)
	// }
	err = xintegral.CreateHistory(1, -2, "使用积分", true)
	if err != nil {
		t.Error(err)
	}
	t.Log("测试完成")
}

func TestMath(t *testing.T) {
	a := uint(10)
	a -= uint(0 - -100)
	t.Log(a)
}
