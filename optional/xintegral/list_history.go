package xintegral

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

func ListHistory(integral_id uint, offset, limit int) (int64, []IntegralHistory, error) {
	if integral_id == 0 {
		return 0, nil, errors.New("积分条目ID不能为空")
	}
	if _default.DB == nil {
		return 0, nil, errors.New("数据库未连接")
	}
	var count int64
	db := _default.DB.Table(_default.IntegralHistoryName).Where("integral_id", integral_id)
	err := db.Count(&count).Error
	if err != nil {
		return 0, nil, xlog.AE("积分明细条数统计失败", err)
	}
	// 查询列表记录
	lis := []IntegralHistory{}
	err = db.Order("id DESC").Offset(offset).Limit(limit).Error
	if err != nil {
		return 0, nil, xlog.AE("积分明细查询失败", err)
	}
	return count, lis, nil
}
