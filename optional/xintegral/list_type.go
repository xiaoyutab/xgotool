package xintegral

import "errors"

// 积分分类表
//
//	uid	用户ID
func ListType(uid uint) ([]IntegralType, error) {
	if uid <= 0 {
		return nil, errors.New("用户ID不能为空")
	}
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	db := _default.DB.Table(_default.IntegralTypeName).Where("user_id", uid)
	inf := []IntegralType{}
	err := db.Order("created_at ASC").Find(&inf).Error
	return inf, err
}
