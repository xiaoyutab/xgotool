// 用户积分模块
package xintegral

import (
	"gitee.com/xiaoyutab/xgotool/individual/xcron"
	"gorm.io/gorm"
)

// 积分模块特点：
//     1. 有效期逻辑【每条都有单独的有效期】
//     2. 固定时期【这一批的记录有统一的过期时间】
//     3. 无限期
//     4. x年后的 x月x日过期

// 配置项
type Config struct {
	DB                  *gorm.DB
	IntegralName        string // 积分主表名称
	IntegralHistoryName string // 积分明细名称
	IntegralTypeName    string // 积分隔离表表名，用于隔离自身积分或者群积分、VIP积分等
}

// 默认配置项
var _default Config = Config{
	IntegralName:        "integral",         // 积分表名称
	IntegralHistoryName: "integral_history", // 积分明细表名称
	IntegralTypeName:    "integral_type",    // 积分隔离表表名
}

// 入口配置
func Regedit(c *Config) {
	if c == nil {
		return
	}
	if c.DB != nil {
		_default.DB = c.DB
	}
	if c.IntegralName != "" {
		_default.IntegralName = c.IntegralName
	}
	if c.IntegralHistoryName != "" {
		_default.IntegralHistoryName = c.IntegralHistoryName
	}
	if c.IntegralTypeName != "" {
		_default.IntegralTypeName = c.IntegralTypeName
	}
	// 数据库/表注册/创建
	if _default.DB != nil {
		_default.DB.AutoMigrate(&Integral{}, &IntegralHistory{}, &IntegralType{})
	}
	// 挂载Crontab任务，每半小时运行一次
	xcron.Hours(2, xcron.CronTab{
		Key:  "xgotool.optional.xintegral.cron",
		Name: "定时整理积分内容结构",
		Desc: "每隔2小时运行一次，用于整理积分中的即将过期积分、即将失效积分以及积分过期后从积分余额中扣除积分",
		Func: Cron,
	})
}

// 积分主表
type Integral struct {
	Id            uint   `gorm:"column:id;primaryKey;not null;autoIncrement" json:"id" form:"id"`                                                 // 条目ID
	UserId        uint   `gorm:"column:user_id;comment:用户ID" json:"user_id" form:"user_id"`                                                       // 用户ID
	TypeId        uint   `gorm:"column:type_id;comment:积分隔离ID，用于隔离自身积分或者群积分、VIP积分等" json:"type_id" form:"type_id"`                                // 积分隔离ID
	Name          string `gorm:"column:name;type:VARCHAR(50);comment:积分名称" json:"name" form:"name"`                                               // 积分名称
	Desc          string `gorm:"column:desc;type:VARCHAR(200);comment:积分详情" json:"desc" form:"desc"`                                              // 积分详情
	Type          uint   `gorm:"column:type;type:TINYINT UNSIGNED;comment:积分类型 1-每一条有单独的有效期 2-固定期限 3-无限期 4-x年后的固定月、日有效期" json:"type" form:"type"` // 积分类型 1-每一条有单独的有效期 2-固定期限 3-无限期 4-x年后的固定月、日有效期
	Balance       uint   `gorm:"column:balance;comment:积分余额" json:"balance" form:"balance"`                                                       // 积分余额
	ExpireBalance uint   `gorm:"column:expire_balance;comment:即将过期积分【3天内最近的】" json:"expire_balance" form:"expire_balance"`                        // 即将过期积分【3天内最近的】
	ExpireDays    uint   `gorm:"column:expire_days;comment:默认过期时间【type=1-天 type=4-年】" json:"expire_days" form:"expire_days"`                      // 默认过期时间【type=1-天 type=4-年】
	IsDeleted     uint8  `gorm:"column:is_deleted;type:TINYINT UNSIGNED;comment:是否删除 0-否 1-是" json:"is_deleted" form:"is_deleted"`                // 是否删除 0-否 1-是
	EndDate       string `gorm:"column:end_date;type:DATE;comment:过期时间【2-统一有效期 4-x月x日有效】" json:"end_date" form:"end_date"`                        // 过期时间【2-统一有效期 4-x月x日有效】
	CreatedAt     string `gorm:"column:created_at;type:DATETIME;comment:创建时间" json:"created_at" form:"created_at"`                                // 创建时间
	UpdatedAt     string `gorm:"column:updated_at;type:DATETIME;comment:更新时间" json:"updated_at" form:"updated_at"`                                // 更新时间
}

// 获取表名
func (c *Integral) TableName() string {
	return _default.IntegralName
}

// 积分明细表
type IntegralHistory struct {
	Id         uint   `gorm:"column:id;primaryKey;not null;autoIncrement" json:"id" form:"id"`                  // 条目ID
	IntegralId uint   `gorm:"column:integral_id;comment:积分ID" json:"integral_id" form:"integral_id"`            // 积分ID
	Balance    int    `gorm:"column:balance;type:INT;comment:积分变动金额" json:"balance" form:"balance"`             // 积分变动金额
	EndDate    string `gorm:"column:end_date;type:DATE;comment:过期时间" json:"end_date" form:"end_date"`           // 过期时间
	EndBalance uint   `gorm:"column:end_balance;comment:剩余未使用积分" json:"end_balance" form:"end_balance"`         // 剩余未使用积分
	Desc       string `gorm:"column:desc;type:VARCHAR(200);comment:积分变动描述" json:"desc" form:"desc"`             // 积分变动描述
	CreatedAt  string `gorm:"column:created_at;type:DATETIME;comment:创建时间" json:"created_at" form:"created_at"` // 创建时间
}

// 获取表名
func (c *IntegralHistory) TableName() string {
	return _default.IntegralHistoryName
}

// 积分隔离备注表
type IntegralType struct {
	Id        uint   `gorm:"column:id;primaryKey;not null;autoIncrement" json:"id" form:"id"`                  // 条目ID
	Name      string `gorm:"column:name;type:VARCHAR(50);comment:积分名称" json:"name" form:"name"`                // 隔离类型名称
	Desc      string `gorm:"column:desc;type:VARCHAR(200);comment:积分详情" json:"desc" form:"desc"`               // 隔离类型详情
	FileId    uint   `gorm:"column:file_id;comment:文件ID" json:"file_id" form:"file_id"`                        // 所属文件ID
	UserId    uint   `gorm:"column:user_id;comment:用户ID" json:"user_id" form:"user_id"`                        // 创建人ID
	CreatedAt string `gorm:"column:created_at;type:DATETIME;comment:创建时间" json:"created_at" form:"created_at"` // 创建时间
	UpdatedAt string `gorm:"column:updated_at;type:DATETIME;comment:更新时间" json:"updated_at" form:"updated_at"` // 更新时间
}

// 获取表名
func (c *IntegralType) TableName() string {
	return _default.IntegralTypeName
}
