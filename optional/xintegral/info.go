package xintegral

import "errors"

// 获取积分详情信息
//
//	id	积分条目ID
func Info(id uint) (*Integral, error) {
	if id <= 0 {
		return nil, errors.New("详情ID不能为空")
	}
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	inf := Integral{
		Id:        id,
		IsDeleted: 0,
	}
	err := _default.DB.Find(&inf).Error
	if err != nil {
		return nil, err
	}
	return &inf, nil
}
