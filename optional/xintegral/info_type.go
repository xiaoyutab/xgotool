package xintegral

import "errors"

// 获取积分隔离详情信息
//
//	id	积分隔离条目ID
func InfoType(id uint) (*IntegralType, error) {
	if id <= 0 {
		return nil, errors.New("详情ID不能为空")
	}
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	inf := IntegralType{
		Id: id,
	}
	err := _default.DB.Find(&inf).Error
	if err != nil {
		return nil, err
	}
	return &inf, nil
}
