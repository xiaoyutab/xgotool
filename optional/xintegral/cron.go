package xintegral

import "gitee.com/xiaoyutab/xgotool/individual/xlog"

// 定时脚本，由外部挂载程序进行调用，用于重置积分中的即将过期的积分
func Cron() {
	if _default.DB == nil {
		return
	}
	// 获取所有即将过期的积分信息
	type sums struct {
		IntegralId  uint
		IntegralSum uint
	}
	_sum := []sums{}
	err := _default.DB.Table(_default.IntegralHistoryName).
		Select("integral_id", "SUM(end_balance) AS integral_sum").
		Where("end_date > DATE_SUB(NOW(),INTERVAL  3 DAY)").
		Group("integral_id").
		Find(&_sum).
		Error
	if err != nil {
		xlog.Alert("数据库运行错误", err)
		return
	}
	for i := 0; i < len(_sum); i++ {
		_default.DB.Table(_default.IntegralName).Where("`type` != 3").Where("id", _sum[i].IntegralId).Update("expire_balance", _sum[i].IntegralSum)
	}
}
