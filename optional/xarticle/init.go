// 文章模块
package xarticle

import (
	"gorm.io/gorm"
)

const (
	LikeArticleLike    uint = iota // 文章点赞 - 0
	LikeArticleCollect             // 文章收藏 - 1
	LikeCommentLike                // 评论点赞 - 2
)

// 文章模块中的文章逻辑如下：
// 1. 文章主表
// 2. 文章详情表（富文本）
// 3. 文章编辑表（同文章主表，加状态进行关联）
// 4. 文章编辑详情表（富文本）
// 5. 文章审核表，存储审核历史记录（含文章ID、编辑ID等）

type Config struct {
	DB                      *gorm.DB // 数据库操作句柄
	ArticleName             string   // 文章主表表名
	ArticleContentName      string   // 文章内容表表名
	ArticleCommentName      string   // 文章评论表表名
	ArticleEditsName        string   // 文章内容修改表表名
	ArticleEditsContentName string   // 文章内容修改表（内容）表名
	ArticleBrowseName       string   // 文章浏览记录表
	ArticleLikeName         string   // 文章点赞记录表
}

// 配置项存储
var _default Config = Config{
	ArticleName:             "article",
	ArticleContentName:      "article_content",
	ArticleCommentName:      "article_comment",
	ArticleEditsName:        "article_edit",
	ArticleEditsContentName: "article_edit_content",
	ArticleBrowseName:       "article_browse",
	ArticleLikeName:         "article_like",
}

// 文章模块注册
//
//	ac	配置项信息
func Regedit(ac *Config) {
	if ac != nil {
		if ac.DB != nil {
			_default.DB = ac.DB
		}
		if ac.ArticleName != "" {
			_default.ArticleName = ac.ArticleName
		}
		if ac.ArticleCommentName != "" {
			_default.ArticleCommentName = ac.ArticleCommentName
		}
		if ac.ArticleContentName != "" {
			_default.ArticleContentName = ac.ArticleContentName
		}
		if ac.ArticleEditsName != "" {
			_default.ArticleEditsName = ac.ArticleEditsName
		}
		if ac.ArticleEditsContentName != "" {
			_default.ArticleEditsContentName = ac.ArticleEditsContentName
		}
		if ac.ArticleBrowseName != "" {
			_default.ArticleBrowseName = ac.ArticleBrowseName
		}
		if ac.ArticleLikeName != "" {
			_default.ArticleLikeName = ac.ArticleLikeName
		}
	}
	if _default.DB != nil {
		// 注册生成表信息
		_default.DB.AutoMigrate(&Article{},
			&ArticleComment{}, &ArticleContent{}, &ArticleBrowse{}, &ArticleLike{},
			&ArticleEdits{}, &ArticleEditsContent{},
		)
	}
}

// 文章主表，用于展示文章的主要信息
type Article struct {
	Id           uint   `gorm:"column:id;primaryKey" json:"id" form:"id"`                                                                 // 文章ID
	Title        string `gorm:"column:title;type:varchar(200);comment:文章标题" json:"title" form:"title"`                                    // 文章标题
	Content      string `gorm:"-" json:"content" form:"content"`                                                                          // 文章内容，禁止orm直接创建
	UserId       uint   `gorm:"column:user_id;comment:创建人id" json:"user_id" form:"user_id"`                                               // 创建人id
	GiveLikeNum  uint   `gorm:"column:give_like_num;comment:点赞次数" json:"give_like_num" form:"give_like_num"`                              // 点赞次数
	BrowseNum    uint   `gorm:"column:browse_num;not null;comment:浏览次数" json:"browse_num" form:"browse_num"`                              // 浏览次数
	CollectNum   uint   `gorm:"column:collect_num;not null;comment:收藏次数" json:"collect_num" form:"collect_num"`                           // 收藏次数
	ShareNum     uint   `gorm:"column:share_num;not null;comment:分享量" json:"share_num" form:"share_num"`                                  // 分享量
	CommentNum   int    `gorm:"column:comment_num;not null;comment:评论数量" json:"comment_num" form:"comment_num"`                           // 评论数量
	DonotComment uint8  `gorm:"do_not_comment;type:TINYINT UNSIGNED;not null;comment:是否禁止评论" json:"do_not_comment" form:"do_not_comment"` // 是否禁止评论
	IsDelete     int8   `gorm:"column:is_delete;type:TINYINT UNSIGNED;comment:是否删除 1-删除 0-未删除" json:"is_delete" form:"is_delete"`         // 是否删除 1-删除 0-未删除
	CreatedAt    string `gorm:"column:created_at;type:datetime;comment:创建时间" json:"created_at" form:"created_at"`                         // 创建时间
	UpdatedAt    string `gorm:"column:updated_at;type:datetime;comment:更新时间" json:"updated_at" form:"updated_at"`                         // 更新时间
}

// 表名
func (c *Article) TableName() string {
	return _default.ArticleName
}

// 文章详情表，用于存储文章的详情内容
type ArticleContent struct {
	ArticleId uint   `gorm:"column:id;primaryKey;comment:文章ID" json:"id" form:"id"`                   // 文章ID
	Content   string `gorm:"column:content;type:longtext;comment:文章内容" json:"content" form:"content"` // 文章内容
}

// 表名
func (c *ArticleContent) TableName() string {
	return _default.ArticleContentName
}

// 文章评论表
type ArticleComment struct {
	Id          uint   `gorm:"column:id;primaryKey;not null;autoIncrement" json:"id" form:"id"`                                  // 评论ID
	ArticleId   uint   `gorm:"column:article_id;not null;comment:文章ID" json:"article_id" form:"article_id"`                      // 文章ID
	CommentId   uint   `gorm:"column:comment_id;not null;comment:回复评论ID" json:"comment_id" form:"comment_id"`                    // 回复评论ID
	UserId      uint   `gorm:"column:user_id;not null;comment:创建人id" json:"user_id" form:"user_id"`                              // 创建人id
	GiveLikeNum uint   `gorm:"column:give_like_num;not null;comment:点赞次数" json:"give_like_num" form:"give_like_num"`             // 点赞次数
	Content     string `gorm:"column:content;type:varchar(200);comment:文章评论 200字内" json:"content" form:"content"`                // 文章评论 200字内
	IsDelete    int8   `gorm:"column:is_delete;type:TINYINT UNSIGNED;comment:是否删除 1-删除 0-未删除" json:"is_delete" form:"is_delete"` // 是否删除 1-删除 0-未删除
	CreatedAt   string `gorm:"column:created_at;type:datetime;comment:创建时间" json:"created_at" form:"created_at"`                 // 创建时间
}

// 表名
func (c *ArticleComment) TableName() string {
	return _default.ArticleCommentName
}

// 文章编辑表，该表每次编辑都会重新生成一条记录
type ArticleEdits struct {
	Id          uint   `gorm:"column:id;primaryKey;not null;autoIncrement" json:"id" form:"id"`                                                                                            // 自增ID
	ArticleId   uint   `gorm:"column:article_id;comment:文章ID" json:"article_id" form:"article_id"`                                                                                         // 文章ID
	Title       string `gorm:"column:title;type:varchar(200);comment:文章标题" json:"title" form:"title"`                                                                                      // 文章标题
	Content     string `gorm:"-" json:"content" form:"content"`                                                                                                                            // 文章详情，不直接使用orm进行存储
	UserId      uint   `gorm:"column:user_id;comment:创建人id" json:"user_id" form:"user_id"`                                                                                                 // 创建人id
	AuditStatus uint8  `gorm:"column:audit_status;type:TINYINT UNSIGNED;comment:审核状态 0-未审核 98-审核拒绝 99-审核通过 97-放弃审核(编辑以后再次编辑时，会将之前的编辑文章调整为放弃审核状态)" json:"audit_status" form:"audit_status"` // 审核状态 0-未审核 98-审核拒绝 99-审核通过 97-放弃审核(编辑以后再次编辑时，会将之前的编辑文章调整为放弃审核状态)
	AuditError  string `gorm:"column:audit_error;type:VARCHAR(200);comment:审核通过/拒绝原因" json:"audit_error" form:"audit_error"`                                                               // 审核通过/拒绝原因
	AuditUserId uint   `gorm:"column:audit_user_id;comment:审核人id" json:"audit_user_id" form:"audit_user_id"`                                                                               // 审核人id
	CreatedAt   string `gorm:"column:created_at;type:datetime;comment:创建时间" json:"created_at" form:"created_at"`                                                                           // 创建时间
	UpdatedAt   string `gorm:"column:updated_at;type:datetime;comment:审核时间" json:"updated_at" form:"updated_at"`                                                                           // 审核时间
}

// 表名
func (c *ArticleEdits) TableName() string {
	return _default.ArticleEditsName
}

// 文章编辑表对应的内容，审核通过后会将该内容替换到文章表中
type ArticleEditsContent struct {
	ArticleEditsId uint   `gorm:"column:id;primaryKey;comment:文章编辑条目ID" json:"id" form:"id"`               // 文章编辑条目ID
	Content        string `gorm:"column:content;type:longtext;comment:文章内容" json:"content" form:"content"` // 文章内容
}

// 表名
func (c *ArticleEditsContent) TableName() string {
	return _default.ArticleEditsContentName
}

// 文章浏览记录表
type ArticleBrowse struct {
	Id        uint   `gorm:"column:id;not null;autoIncrement;primaryKey" json:"id" form:"id"`
	UserId    uint   `gorm:"column:user_id;index:user_id;comment:浏览用户id" json:"user_id" form:"user_id"`           //浏览用户id
	ArticleId uint   `gorm:"column:article_id;index:article_id;comment:文章id" json:"article_id" form:"article_id"` //文章id
	Ip        string `gorm:"column:ip;type:VARCHAR(20);comment:IPV4地址" json:"ip" form:"ip"`                       //IPV4地址
	CreatedAt string `gorm:"column:created_at;type:DATETIME;comment:查看时间" json:"created_at" form:"created_at"`    //查看时间
}

// 表名
func (c *ArticleBrowse) TableName() string {
	return _default.ArticleBrowseName
}

// 文章浏览记录表
type ArticleLike struct {
	Id        uint   `gorm:"column:id;not null;autoIncrement;primaryKey" json:"id" form:"id"`
	UserId    uint   `gorm:"column:user_id;index:like_index,unique;comment:点赞用户id" json:"user_id" form:"user_id"`               // 点赞用户id
	ArticleId uint   `gorm:"column:article_id;index:like_index,unique;comment:点赞对象id" json:"article_id" form:"article_id"`      // 点赞对象id
	LikeType  uint   `gorm:"column:like_type;index:like_index,unique;comment:点赞类型【0~10已被占用】" json:"like_type" form:"like_type"` // 点赞类型【0~10已被占用】
	Ip        string `gorm:"column:ip;type:VARCHAR(20);comment:IPV4地址" json:"ip" form:"ip"`                                     // IPV4地址
	CreatedAt string `gorm:"column:created_at;type:DATETIME;comment:查看时间" json:"created_at" form:"created_at"`                  // 查看时间
}

// 表名
func (c *ArticleLike) TableName() string {
	return _default.ArticleLikeName
}
