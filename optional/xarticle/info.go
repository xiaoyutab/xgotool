package xarticle

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
)

// 获取文章详情操作
//
//	id	文章ID
func Info(id uint) (*Article, error) {
	if id == 0 {
		return nil, errors.New("文章ID不能为0")
	}
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	ar := Article{}
	cache_key := xcache.Key("xarticle.Info", id)
	if xcache.Exists(cache_key) {
		if err := xcache.GetStruct(cache_key, &ar); err == nil {
			return &ar, nil
		}
	}
	err := _default.DB.Table(_default.ArticleName).Where("id", id).Where("is_delete", 0).Find(&ar).Error
	if err != nil {
		return nil, err
	}
	if ar.Id == 0 {
		return nil, errors.New("数据未找到")
	}
	// 获取文章详情
	ar_info := ArticleContent{}
	err = _default.DB.Table(_default.ArticleContentName).Where("id", id).Find(&ar_info).Error
	if err != nil {
		return nil, err
	}
	if ar_info.ArticleId == ar.Id {
		ar.Content = ar_info.Content
	}
	xcache.SetStruct(cache_key, ar)
	return &ar, nil
}
