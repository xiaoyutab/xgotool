package xarticle

import (
	"errors"
	"time"
)

// 创建文章信息
//
//	ar	创建的文章标识【只有审核通过后才会将文章发布到正式文章表中】
func Create(ar *ArticleEdits) error {
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	if ar.UserId == 0 {
		return errors.New("用户ID不能为空")
	}
	if ar.ArticleId > 0 {
		// 如果是修改文章的话，需要将其他的待审核状态文章修改为放弃审核
		err := _default.DB.Table(_default.ArticleEditsName).
			Where("audit_status", 0).
			Where("article_id", ar.ArticleId).
			Select("audit_status", "updated_at").
			Updates(&ArticleEdits{
				AuditStatus: 97,
				UpdatedAt:   time.Now().Format(time.DateTime),
			}).
			Error
		if err != nil {
			return err
		}
	}
	if ar.Id > 0 {
		ar.Id = 0
	}
	if ar.CreatedAt == "" {
		ar.CreatedAt = time.Now().Format(time.DateTime)
	}
	ar.UpdatedAt = time.Now().Format(time.DateTime)
	err := _default.DB.Table(_default.ArticleEditsName).Create(ar).Error
	if err != nil {
		return err
	}
	// 存储详情内容
	err = _default.DB.Table(_default.ArticleEditsContentName).Create(&ArticleEditsContent{
		ArticleEditsId: ar.Id,
		Content:        ar.Content,
	}).Error
	return err
}
