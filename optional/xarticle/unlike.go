package xarticle

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
	"gorm.io/gorm"
)

// 取消文章点赞量
//
//	id	文章ID
//	uid	点赞用户ID
//	ip	点赞用户IP地址
//	typ	点赞类型
func UnLike(id, uid, typ uint) error {
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	article := ArticleLike{
		ArticleId: id,
		UserId:    uid,
		LikeType:  typ,
	}
	// 插入文章浏览记录
	err := _default.DB.Find(&article).Error
	if err != nil {
		return err
	}
	if article.Id == 0 {
		return nil
	}
	// 删除对应记录
	err = _default.DB.Delete(&article).Error
	if err != nil {
		return err
	}
	if typ == LikeArticleLike || typ == LikeArticleCollect {
		// 修改文章浏览量
		ar, err := Info(id)
		if err != nil {
			return err
		}
		if typ == LikeArticleLike {
			ar.GiveLikeNum--
		} else {
			ar.CollectNum--
		}
		err = _default.DB.Table(_default.ArticleName).Select("give_like_num", "collect_num").Save(ar).Error
		if err != nil {
			return err
		}
		// 修改缓存的浏览量
		xcache.SetStruct(xcache.Key("xarticle.Info", id), ar)
	} else if typ == LikeCommentLike {
		// 评论取消点赞
		if err := _default.DB.Table(_default.ArticleCommentName).Update("give_like_num", gorm.Expr("give_like_num - ?", 1)).Error; err != nil {
			return err
		}
	}
	return nil
}
