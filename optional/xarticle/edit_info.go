package xarticle

import "errors"

// 获取edit详情信息
//
//	id	edit详情ID
//	con	是否要获取详情
func EditInfo(id uint, con bool) (*ArticleEdits, error) {
	if id == 0 {
		return nil, errors.New("编辑ID传输错误")
	}
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	mod := ArticleEdits{}
	err := _default.DB.Table(_default.ArticleEditsName).Where("id", id).Find(&mod).Error
	if err != nil {
		return nil, err
	}
	if !con {
		return &mod, nil
	}
	// 获取详情信息
	mods := ArticleEditsContent{}
	err = _default.DB.Table(_default.ArticleEditsContentName).Where("id", id).Find(&mods).Error
	if err != nil {
		return nil, err
	}
	if mods.ArticleEditsId == mod.Id {
		mod.Content = mods.Content
	}
	return &mod, nil
}
