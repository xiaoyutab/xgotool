package xarticle

import (
	"errors"
	"time"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
)

// 审核文章信息
//
//	edit_id	编辑文章的ID
//	audit	审核状态
//	msg		审核备注
//	uid		审核人ID
func Audit(edit_id uint, audit uint8, msg string, uid uint) error {
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	if audit == 0 {
		return errors.New("不能设置为待审核状态")
	}
	// 获取审核条目信息
	mod_edits, err := EditInfo(edit_id, true)
	if err != nil {
		return err
	}
	mod_edits.AuditStatus = audit
	mod_edits.AuditUserId = uid
	mod_edits.AuditError = msg
	mod_edits.UpdatedAt = time.Now().Format(time.DateTime)
	err = _default.DB.Table(_default.ArticleEditsName).Save(mod_edits).Error
	if err != nil {
		return err
	}
	// 如果是审核通过，需要同步到主文章表中
	if audit == 99 {
		ar := &Article{}
		if mod_edits.ArticleId > 0 {
			ar, err = Info(mod_edits.ArticleId)
			if err != nil {
				return err
			}
		}
		ar.UserId = mod_edits.UserId
		ar.Title = mod_edits.Title
		ar.UpdatedAt = time.Now().Format(time.DateTime)
		if ar.CreatedAt == "" {
			ar.CreatedAt = time.Now().Format(time.DateTime)
		}
		err = _default.DB.Table(_default.ArticleName).Save(ar).Error
		if err != nil {
			return err
		}
		err = _default.DB.Table(_default.ArticleContentName).Save(&ArticleContent{
			ArticleId: ar.Id,
			Content:   mod_edits.Content,
		}).Error
		if err != nil {
			return err
		}
		// 如果是创建的话，此处需要将审核的记录关联到文章中
		if mod_edits.ArticleId == 0 {
			mod_edits.ArticleId = ar.Id
			err = _default.DB.Table(_default.ArticleEditsName).Save(mod_edits).Error
			if err != nil {
				return err
			}
		}
		// 清空文章缓存
		xcache.Remove(xcache.Key("xarticle.Info", ar.Id))
	}
	return nil
}
