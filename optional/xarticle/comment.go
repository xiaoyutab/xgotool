package xarticle

import (
	"errors"
	"time"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
)

// 文章评论
//
//	article_id	文章ID
//	comment_id	回复的评论ID
//	user_id		用户ID
//	cont		评论内容
func Comment(article_id, comment_id, user_id uint, cont string) error {
	if article_id == 0 {
		return errors.New("文章ID不能为空")
	}
	if user_id == 0 {
		return errors.New("用户未登录")
	}
	// 获取文章详情
	ar_info, err := Info(article_id)
	if err != nil {
		return err
	}
	if ar_info.DonotComment == 1 {
		return errors.New("该文章已禁止评论")
	}
	// 只修改评论数量
	ar_info.CommentNum++
	err = _default.DB.Table(_default.ArticleName).Select("comment_num").Save(ar_info).Error
	if err != nil {
		return err
	}
	// 文章评论量添加【缓存更新】
	xcache.SetStruct(xcache.Key("xarticle.Info", ar_info.Id), ar_info)
	// 插入评论记录
	err = _default.DB.Table(_default.ArticleCommentName).Save(&ArticleComment{
		ArticleId: article_id,
		CommentId: comment_id,
		UserId:    user_id,
		Content:   cont,
		CreatedAt: time.Now().Format(time.DateTime),
	}).Error
	if err != nil {
		return err
	}
	return nil
}
