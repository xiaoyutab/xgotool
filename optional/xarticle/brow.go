package xarticle

import (
	"errors"
	"time"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
)

// 添加文章浏览量
//
//	id	文章ID
//	uid	浏览用户ID
//	ip	浏览用户IP地址
func Brow(id, uid uint, ip string) error {
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	article := ArticleBrowse{
		ArticleId: id,
		UserId:    uid,
		Ip:        ip,
		CreatedAt: time.Now().Format(time.DateTime),
	}
	// 插入文章浏览记录
	err := _default.DB.Create(&article).Error
	if err != nil {
		return err
	}
	// 修改文章浏览量
	ar, err := Info(article.ArticleId)
	if err != nil {
		return err
	}
	ar.BrowseNum++
	err = _default.DB.Table(_default.ArticleName).Select("browse_num").Save(ar).Error
	if err != nil {
		return err
	}
	// 修改缓存的浏览量
	xcache.SetStruct(xcache.Key("xarticle.Info", article.ArticleId), ar)
	return nil
}
