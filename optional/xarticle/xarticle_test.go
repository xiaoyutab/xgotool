package xarticle_test

import (
	"testing"

	"gitee.com/xiaoyutab/xgotool/optional/xarticle"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// 测试错误记录情况
func TestCreate(t *testing.T) {
	db, _ := gorm.Open(mysql.Open("admin:admin@tcp(localhost:3306)/gatway?charset=utf8"), &gorm.Config{})
	xarticle.Regedit(&xarticle.Config{
		DB: db,
	})
	// xarticle.Create(&xarticle.ArticleEdits{
	// 	Title:   "测试-2",
	// 	Content: "测试内容",
	// 	UserId:  1,
	// })
	// xarticle.Audit(3, 99, "测试通过", 10)
	// xarticle.Comment(1, 0, 1, "测试评论")
	err := xarticle.Like(1, 2, xarticle.LikeArticleLike, "127.0.0.1")
	if err != nil {
		t.Error(err)
	}
}
