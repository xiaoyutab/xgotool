package xbanner

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// Banner列表获取
//
//	dervice	设备类型
//	types	广告位位置 0-所有
func List(dervice, types uint8) ([]Banner, error) {
	if dervice == 0 {
		return nil, errors.New("所属设备类型禁止为空")
	}
	cache_key := xcache.Key("xbanner.list", dervice, types)
	lis := []Banner{}
	if xcache.Exists(cache_key) {
		if err := xcache.GetStruct(cache_key, &lis); err == nil {
			return lis, nil
		}
		lis = []Banner{}
	}
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	db := _default.DB.Table(_default.BannerName).Where("dervice", dervice)
	if types > 0 {
		db = db.Where("type", types)
	}
	// 从数据库中进行查询
	err := db.Where("is_deleted", 0).Order("`order` DESC").Find(&lis).Error
	if err != nil {
		return nil, xlog.EE("Banner列表查询失败", err)
	}
	// 写入缓存
	xcache.SetStruct(cache_key, lis)
	return lis, nil
}
