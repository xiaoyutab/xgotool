package xbanner

import (
	"errors"
	"time"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
)

// 创建Banner广告位
//
//	b	Banner广告位
func Create(b *Banner) error {
	if b.Dervice == 0 || b.Type == 0 {
		return errors.New("广告位置选择错误")
	}
	if b.Fid == 0 {
		return errors.New("Banner图片获取失败")
	}
	if b.UserId == 0 {
		return errors.New("用户ID传输错误")
	}
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	b.UpdatedAt = time.Now().Format(time.DateTime)
	if b.CreatedAt == "" {
		b.CreatedAt = time.Now().Format(time.DateTime)
	}
	err := _default.DB.Save(b).Error
	if err != nil {
		return err
	}
	// 清除对应列表的缓存信息
	xcache.Remove(xcache.Key("xbanner.list", b.Dervice, b.Type))
	xcache.Remove(xcache.Key("xbanner.list", b.Dervice, uint8(0)))
	return nil
}
