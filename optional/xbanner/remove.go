package xbanner

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 移除Banner信息，因删除操作仅需提供id即可，所以此处单独进行封装了一层，即直接根据ID删除对应Banner信息
//
//	id	BannerID
func Remove(id uint) error {
	if id == 0 {
		return errors.New("参数传输错误")
	}
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	ban := Banner{}
	err := _default.DB.Table(_default.BannerName).Where("id", id).Find(&ban).Error
	if err != nil {
		return xlog.AE("Banner信息查询失败", err)
	}
	if ban.IsDeleted == 1 {
		return errors.New("该Banner已被删除")
	}
	ban.IsDeleted = 1
	return Create(&ban)
}
