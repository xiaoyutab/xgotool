// Banner模块
package xbanner

import "gorm.io/gorm"

// Banner模块
// 通用模块配置，模块内使用uint形式存储文件ID
// 所以，若要使用此模块的话，建议将xfile模块也进行使用一下，或者自建一套file文件存储系统
// PS: 文件建议使用http://xxxx.cn/xx/xxx?id=<fid>&type=xxx&token=xxx&info=xxx的形式来进行拼接展示【若不要求严格的话建议直接使用fid+type来进行跳转】

// 配置信息
type Config struct {
	DB         *gorm.DB
	BannerName string       // Banner表存储名称
	DerviceMap []DerviceMap // 设备类型列表/索引
}

// 设备索引标识
type DerviceMap struct {
	Id    uint8        `json:"id"`    // 索引值
	Name  string       `json:"name"`  // 名称
	Child []DerviceMap `json:"child"` // 子索引
}

var _default Config = Config{
	BannerName: "banner",
	DerviceMap: []DerviceMap{{
		Id:   1,
		Name: "默认设备",
		Child: []DerviceMap{{
			Id:   1,
			Name: "默认广告位",
		}},
	}},
}

// 入口配置
func Regedit(c *Config) {
	if c == nil {
		return
	}
	if c.DB != nil {
		_default.DB = c.DB
	}
	if c.BannerName != "" {
		_default.BannerName = c.BannerName
	}
	if c.DerviceMap != nil {
		_default.DerviceMap = c.DerviceMap
	}
	// 数据库/表注册/创建
	if _default.DB != nil {
		_default.DB.AutoMigrate(&Banner{})
	}
}

// 配置通用Banner信息
type Banner struct {
	Id        uint64 `gorm:"column:id;primaryKey;type:BIGINT UNSIGNED;not null;autoIncrement" json:"id" form:"id"`
	Title     string `gorm:"column:title;type:VARCHAR(200);comment:Banner标题" json:"title" form:"title"`                                // Banner标题
	Fid       uint   `gorm:"column:fid;comment:文件ID" json:"fid" form:"fid"`                                                            // Banner图片信息
	Href      string `gorm:"column:href;type:VARCHAR(200);comment:Banner跳转地址，http开头标识网址，app开头标识APP内部跳转......" json:"href" form:"href"` // Banner跳转地址
	Dervice   uint8  `gorm:"column:dervice;type:TINYINT UNSIGNED;comment:Banner所属设备类型" json:"dervice" form:"dervice"`                  // 设备类型
	Type      uint8  `gorm:"column:type;type:TINYINT UNSIGNED;comment:Banner所属设备位置" json:"type" form:"type"`                           // Banner位置
	Order     uint8  `gorm:"column:order;type:TINYINT UNSIGNED;comment:Banner排序，数字越大排序越靠前" json:"order" form:"order"`                  // 排序，数字越大排序越靠前，最大200
	IsTarget  uint8  `gorm:"column:is_target;type:TINYINT UNSIGNED;comment:是否新窗口打开 0-否 1-是" json:"is_target" form:"is_target"`         // 是否新窗口打开，0否1是
	IsDeleted uint8  `gorm:"column:is_deleted;type:TINYINT UNSIGNED;comment:是否删除 0-否 1-是" json:"is_deleted" form:"is_deleted"`         // 是否删除 0-否 1-是
	UserId    uint   `gorm:"column:user_id;comment:Banner创建人" json:"user_id" form:"user_id"`                                           // 创建人ID
	CreatedAt string `gorm:"column:created_at;type:DATETIME;comment:创建时间" json:"created_at" form:"created_at"`                         // 创建时间
	UpdatedAt string `gorm:"column:updated_at;type:DATETIME;comment:最后修改时间" json:"updated_at" form:"updated_at"`                       // 更新时间
}

// 获取表名
func (c *Banner) TableName() string {
	return _default.BannerName
}
