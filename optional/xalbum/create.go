package xalbum

import (
	"errors"
	"sync"
	"time"

	"gitee.com/xiaoyutab/xgotool/individual/xgorm"
)

// 创建相册
//
//	c	修改后的相册内容
func Create(c *Albumn) error {
	if c == nil {
		return errors.New("相册内容为空")
	}
	if c.Uid == 0 {
		return errors.New("用户未登录")
	}
	if c.Name == "" {
		return errors.New("相册名不能为空")
	}
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	c.UpdatedAt = time.Now().Format(time.DateTime)
	if c.CreatedAt == "" {
		c.CreatedAt = time.Now().Format(time.DateTime)
	}
	err := xgorm.To(_default.DB).Table(c.TableName()).
		When(c.Id > 0, "id", c.Id).
		When(c.Id > 0, "is_deleted", 0).
		Save(c).
		Error
	if err != nil {
		return err
	}
	if c.IsDeleted == 1 {
		// 如果是删除操作
		_default.Delete(c.Id)
	} else {
		// 写入缓存
		_default.Store(c.Id, *c)
	}
	_default.child = sync.Map{} // 清空子相册ID列表
	return nil
}
