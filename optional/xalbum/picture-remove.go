package xalbum

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xgorm"
)

// 从相册中移除照片
//
//	id	相册ID
//	fid	照片文件ID
func PictureRemove(id, fid uint) error {
	if _default.DB == nil {
		return errors.New("数据库未链接")
	}
	if id == 0 || fid == 0 {
		return errors.New("参数错误")
	}
	err := xgorm.To(_default.DB).Table(_default.AlbumnPictureName).
		Where("fid", fid).
		Where("aid", id).
		Where("is_deleted", 0).
		Update("is_deleted", 1).
		Error
	if err != nil {
		return err
	}
	return nil
}
