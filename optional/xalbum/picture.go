package xalbum

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xgorm"
)

// 获取相册内的图片列表
//
//	id		相册ID
//	offset	跳过条数
//	limit	查询条数
func Picture(id uint, offset, limit int) (int64, []AlbumnPicture, error) {
	if _default.DB == nil {
		return 0, nil, errors.New("数据库未连接")
	}
	count, mod := int64(0), []AlbumnPicture{}
	cid := Childs(id)
	if len(cid) == 0 {
		cid = []uint{id}
	}
	err := xgorm.To(_default.DB).
		Table(_default.AlbumnPictureName).
		Where("aid IN ?", cid).
		Where("is_deleted", 0).
		Order("id DESC").
		List(offset, limit, &count, &mod)
	if err != nil {
		return 0, nil, err
	}
	return count, mod, nil
}
