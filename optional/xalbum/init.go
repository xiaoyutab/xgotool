// 用户相册组件
package xalbum

import (
	"sync"
	"time"

	"gorm.io/gorm"
)

// 配置项详情
type Config struct {
	DB                *gorm.DB
	AlbumnName        string   // 相册表名称
	AlbumnPictureName string   // 相册内照片表名称
	sync.Map                   // 缓存标识
	child             sync.Map // 子元素的缓存标识
	cachesClearStatus bool     // 是否已启动缓存清理标识
}

var _default = Config{
	AlbumnName:        "albumn",
	AlbumnPictureName: "albumn_picture",
}

// 相册模块注册
//
//	c	配置项信息
func Regedit(c *Config) {
	if c == nil {
		return
	}
	if c.DB != nil {
		_default.DB = c.DB
	}
	if c.AlbumnName != "" {
		_default.AlbumnName = c.AlbumnName
	}
	if c.AlbumnPictureName != "" {
		_default.AlbumnPictureName = c.AlbumnPictureName
	}
	if _default.DB != nil {
		_default.DB.AutoMigrate(&Albumn{}, &AlbumnPicture{})
	}
	if !_default.cachesClearStatus {
		_default.cachesClearStatus = true
		go func() {
			for {
				time.Sleep(time.Hour)
				_default.Map = sync.Map{}
			}
		}()
	}
}

// 相册表
type Albumn struct {
	Id        uint   `gorm:"column:id;type:int unsigned;primaryKey;autoIncrement;not null" form:"id" json:"id"`
	Uid       uint   `gorm:"column:uid;type:int unsigned;comment:用户ID" form:"uid" json:"uid"`                          // 用户ID
	Name      string `gorm:"column:name;type:varchar(100);comment:相册名称" form:"name" json:"name"`                       // 相册名称
	Fid       uint   `gorm:"column:fid;type:int unsigned;comment:上级ID" form:"fid" json:"fid"`                          // 上级ID
	Thumb     uint   `gorm:"column:thumb;type:int unsigned;comment:封面图ID" form:"thumb" json:"thumb"`                   // 封面图ID
	IsVideo   uint8  `gorm:"column:is_video;type:tinyint unsigned;comment:是否为视频相册" form:"is_video" json:"is_video"`    // 是否为视频相册
	IsPeople  uint8  `gorm:"column:is_people;type:tinyint unsigned;comment:是否为人物相册" form:"is_people" json:"is_people"` // 是否为人物相册
	IsStory   uint8  `gorm:"column:is_story;type:tinyint unsigned;comment:是否为故事集" form:"is_story" json:"is_story"`     // 是否为故事集
	IsMore    uint8  `gorm:"column:is_more;type:tinyint unsigned;comment:是否为更多相册" form:"is_more" json:"is_more"`       // 是否为更多相册
	IsDeleted uint8  `gorm:"column:is_deleted;type:tinyint unsigned;comment:是否删除" form:"is_deleted" json:"is_deleted"` // 是否删除
	CreatedAt string `gorm:"column:created_at;type:datetime;comment:添加时间" form:"created_at" json:"created_at"`         // 添加时间
	UpdatedAt string `gorm:"column:updated_at;type:datetime;comment:修改时间" form:"-" json:"-"`                           // 修改时间
}

// 返回所属表名信息
func (c *Albumn) TableName() string {
	return _default.AlbumnName
}

// 相册图片表
type AlbumnPicture struct {
	Id        uint   `gorm:"column:id;type:int unsigned;primaryKey;autoIncrement;not null" form:"id" json:"id"`
	Uid       uint   `gorm:"column:uid;type:int unsigned;comment:上传人" form:"uid" json:"uid"`                           //上传人
	Fid       uint   `gorm:"column:fid;type:int unsigned;comment:文件ID" form:"fid" json:"fid"`                          //文件ID
	Aid       uint   `gorm:"column:aid;type:int unsigned;comment:所属相册ID" form:"aid" json:"aid"`                        //所属相册ID
	Type      uint   `gorm:"column:type;type:int unsigned;comment:图片类型" form:"type" json:"type"`                       //图片类型 0-未标记 1-标记为图片 2-标记为视频 3-标记为音频 4-标记为电话录音 5-标记为文档文件
	CreatedAt string `gorm:"column:created_at;type:datetime;comment:添加时间" form:"created_at" json:"created_at"`         //添加时间
	IsDeleted uint8  `gorm:"column:is_deleted;type:tinyint unsigned;comment:是否删除" form:"is_deleted" json:"is_deleted"` //是否删除
}

// 返回所属表名信息
func (c *AlbumnPicture) TableName() string {
	return _default.AlbumnPictureName
}
