package xalbum

import (
	"fmt"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 获取相册的子ID列表
//
//	id	待获取的相册ID
func Childs(id uint) []uint {
	ids := []uint{id}
	defer xlog.Recover()
	if v, ok := _default.child.Load(id); ok {
		return v.([]uint)
	}
	if _default.DB == nil {
		return []uint{}
	}
	for {
		tmp := []uint{}
		_default.DB.Table(_default.AlbumnName).
			Where("id IN ? OR fid IN ?", ids, ids).
			Where("is_deleted", 0).
			Order("id ASC").
			Select("id").
			Find(&tmp)
		if len(tmp) == 0 {
			break
		}
		fmt.Println("查询结果：", ids, tmp)
		if len(tmp) == len(ids) && tmp[0] == ids[0] && tmp[len(tmp)-1] == ids[len(tmp)-1] {
			break
		}
		ids = tmp
	}
	_default.child.Store(id, ids)
	return ids
}
