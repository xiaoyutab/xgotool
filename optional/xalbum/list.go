package xalbum

import "errors"

// 获取相册列表
// 因个人相册列表数量不会太多，所以此处直接使用List进行获取全部
//
//	uid	用户ID
//	fid	上级相册ID
func List(uid, fid uint) ([]Albumn, error) {
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	mod := []Albumn{}
	err := _default.DB.Table(_default.AlbumnName).
		Where("uid", uid).
		Where("fid", fid).
		Where("is_deleted", 0).
		Order("id DESC").
		Find(&mod).
		Error
	if err != nil {
		return nil, err
	}
	// 缓存写入
	for i := 0; i < len(mod); i++ {
		_default.Store(mod[i].Id, mod[i])
	}
	return mod, nil
}
