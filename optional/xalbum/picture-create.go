package xalbum

import (
	"errors"
	"time"

	"gitee.com/xiaoyutab/xgotool/individual/xgorm"
)

// 创建相册内的照片
//
//	c	待创建的信息
func PictureCreate(c *AlbumnPicture) error {
	if c == nil {
		return errors.New("参数错误")
	}
	if c.Uid == 0 {
		return errors.New("用户未登录")
	}
	if c.Aid == 0 {
		return errors.New("所属相册为空")
	}
	if c.Fid == 0 {
		return errors.New("文件未找到")
	}
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	var count int64
	err := xgorm.To(_default.DB).Table(c.TableName()).
		Where("fid", c.Fid).
		Where("aid", c.Aid).
		Where("is_deleted", 0).
		Count(&count).
		Error
	if err != nil {
		return nil
	}
	if count > 0 {
		return errors.New("文件已存在与本相册内")
	}
	if c.CreatedAt == "" {
		c.CreatedAt = time.Now().Format(time.DateTime)
	}
	db := _default.DB.Table(c.TableName()).Save(c)
	if db.Error != nil {
		return db.Error
	}
	if db.RowsAffected <= 0 {
		return errors.New("存储失败")
	}
	return nil
}
