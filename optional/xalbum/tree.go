package xalbum

// 相册树
type TreeAlbumn struct {
	Id    uint         `json:"id" form:"id" xml:"id"`                   // 相册ID
	Fid   uint         `json:"fid" form:"fid" xml:"fid"`                // 上级相册ID
	Label string       `json:"label" form:"label" xml:"label"`          // 相册名称
	Child []TreeAlbumn `json:"children" form:"children" xml:"children"` // 子相册列表
}

// 获取相册目录树
func Tree(uid uint, fid uint) ([]TreeAlbumn, error) {
	fids := []TreeAlbumn{}
	lis, err := List(uid, fid)
	if err != nil {
		return nil, err
	}
	if len(lis) == 0 {
		return fids, nil
	}
	for i := 0; i < len(lis); i++ {
		childs, err := Tree(uid, lis[i].Id)
		if err != nil {
			continue
		}
		fids = append(fids, TreeAlbumn{
			Id:    lis[i].Id,
			Fid:   fid,
			Label: lis[i].Name,
			Child: childs,
		})
	}
	return fids, nil
}
