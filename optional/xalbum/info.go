package xalbum

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 获取相册详情信息
//
//	id	相册ID
func Info(id uint) (*Albumn, error) {
	if id <= 0 {
		return nil, errors.New("相册ID传输错误")
	}
	defer xlog.Recover()
	if v, ok := _default.Load(id); ok {
		tmp := v.(Albumn)
		return &tmp, nil
	}
	// 缓存中不存在就从数据库中进行读取
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	a := Albumn{}
	err := _default.DB.Table(a.TableName()).Where("id", id).Where("is_deleted", 0).Limit(1).Find(&a).Error
	if err != nil {
		return nil, err
	}
	if a.Id == 0 {
		return nil, errors.New("数据未找到")
	}
	_default.Store(a.Id, a)
	return &a, nil
}
