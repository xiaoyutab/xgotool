package xalbum

import (
	"errors"
	"time"

	"gitee.com/xiaoyutab/xgotool/individual/xgorm"
)

// 获取特殊相册中的文件列表
//
//	id		特殊相册类型
//		0	最近上传【最近一周上传的文件列表】
//		-1	所有图片【标记类型】
//		-2	所有视频【标记类型】
//		-3	所有音频【标记类型】
//		-4	电话录音【标记类型】
//		-5	文档附件【标记类型】
//	offset	跳过条数
//	limit	查询条数
func PictureSpecial(id, offset, limit int) (int64, []AlbumnPicture, error) {
	if _default.DB == nil {
		return 0, nil, errors.New("数据库未连接")
	}
	count, mod := int64(0), []AlbumnPicture{}
	err := xgorm.To(_default.DB).
		Table(_default.AlbumnPictureName).
		Where("is_deleted", 0).
		When(id == 0, "created_at > ?", time.Now().AddDate(0, 0, -7).Format(time.DateTime)).
		When(id == -1, "type", 1).
		When(id == -2, "type", 2).
		When(id == -3, "type", 3).
		When(id == -4, "type", 4).
		When(id == -5, "type", 5).
		Order("id DESC").
		List(offset, limit, &count, &mod)
	if err != nil {
		return 0, nil, err
	}
	return count, mod, nil
}
