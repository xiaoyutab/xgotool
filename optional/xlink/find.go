package xlink

import (
	"errors"
	"time"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
)

// 查询连接信息（只查询在有效期内的、有效的连接）
//
//	id		连接ID
//	code	连接code码
func Find(id uint, code string) (*Link, error) {
	if id == 0 && code == "" {
		return nil, errors.New("连接参数传输")
	}
	link := Link{}
	cache_key_id := xcache.Key("xlink.info", id)
	cache_key_code := xcache.Key("xlink.info", code)
	if xcache.Exists(cache_key_id) {
		if err := xcache.GetStruct(cache_key_id, &link); err == nil {
			return &link, nil
		}
	}
	if xcache.Exists(cache_key_code) {
		if err := xcache.GetStruct(cache_key_code, &link); err == nil {
			return &link, nil
		}
	}
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	err := _default.DB.Table(_default.LinkName).
		Where("end_date >= ?", time.Now().Format(time.DateOnly)).
		Where("status", 0).
		Where("is_deleted", 0).
		Where("( id = ? or codes = ? )", id, code).Find(&link).Error
	if err != nil {
		return nil, err
	}
	if link.Id == 0 {
		return nil, errors.New("连接不存在")
	}
	xcache.SetStruct(cache_key_id, link)
	xcache.SetStruct(cache_key_code, link)
	return &link, nil
}
