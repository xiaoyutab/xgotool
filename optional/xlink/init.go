// 短连接服务
package xlink

import (
	"gorm.io/gorm"
)

// 短连接服务支持自建短连、三方短链
// 三方短链实现逻辑：用户点击三方短连 -> 跳转到我方短连 -> 跳转到原始连接
// 此服务也可以作为友情链接来进行使用，使用流程如下：
//     1. 在网站中添加一个三方网站的首页连接
//     2. 创建一个跳转到自己网站首页的短链（用于统计短连请求次数等）
//     3. 在三方网站申请友情链接
//     4. 创建一个跳转到三方网站友情链接的短链（用于统计流出量）
//     5. 将上一步获取到的短连接加到网站中
// 使用方式：
//     1. 嵌套中间件，一旦匹配到短链以后，会直接由中间件进行接管，由中间件进行转发（匹配/xxxxxx格式的短链接）
//     2. 程序内进行匹配规则，然后根据code获取原始连接进行跳转

type Config struct {
	DB           *gorm.DB // 数据库连接
	LinkName     string   // 连接存储表
	LinkBrowName string   // 连接点击表
}

// 默认配置
var _default Config = Config{
	LinkName:     "link",
	LinkBrowName: "link_brow",
}

// 配置项注入
func Regedit(c *Config) {
	if c == nil {
		return
	}
	if c.DB != nil {
		_default.DB = c.DB
	}
	if c.LinkName != "" {
		_default.LinkName = c.LinkName
	}
	if c.LinkBrowName != "" {
		_default.LinkBrowName = c.LinkBrowName
	}
	if _default.DB != nil {
		_default.DB.AutoMigrate(&Link{}, &LinkBrow{})
	}
}

// 短链接存储表（配置短链接信息）
type Link struct {
	Id        uint64 `gorm:"column:id;primaryKey;type:BIGINT UNSIGNED;not null;autoIncrement" json:"id" form:"id"`
	UserId    uint   `gorm:"column:user_id;comment:创建人" json:"user_id" form:"user_id"`                                           //创建人
	Uri       string `gorm:"column:uri;type:VARCHAR(200);comment:源链接信息" json:"uri" form:"uri"`                                   //源链接信息
	EndDate   string `gorm:"column:end_date;type:DATE;comment:过期时间" json:"end_date" form:"end_date"`                             //过期时间
	Codes     string `gorm:"column:codes;type:CHAR(6);index:codes;comment:短链识别码" json:"codes" form:"codes"`                      //短链识别码
	Status    uint8  `gorm:"column:status;type:TINYINT UNSIGNED;comment:状态 0-正常 1-禁用" json:"status" form:"status"`               //状态 0-正常 1-禁用
	IsDeleted uint8  `gorm:"column:is_deleted;type:TINYINT UNSIGNED;comment:是否删除 0-正常 1-删除" json:"is_deleted" form:"is_deleted"` //是否删除 0-正常 1-删除
	CreatedAt string `gorm:"column:created_at;type:DATETIME;comment:创建时间" json:"created_at" form:"created_at"`                   // 创建时间
	UpdatedAt string `gorm:"column:updated_at;type:DATETIME;comment:最后更新时间" json:"updated_at" form:"updated_at"`                 // 最后更新时间
}

// 获取表名
func (c *Link) TableName() string {
	return _default.LinkName
}

// 短链接的访问PV记录表
type LinkBrow struct {
	Id          uint64 `gorm:"column:id;primaryKey;type:BIGINT UNSIGNED;not null;autoIncrement" json:"id" form:"id"`
	UrlId       uint64 `gorm:"column:url_id;type:BIGINT UNSIGNED;index:url_id;comment:短链接ID" json:"url_id" form:"url_id"` //短链接ID
	Ua          uint   `gorm:"column:ua;comment:UA，关联KV表" json:"ua" form:"ua"`                                            //UA，关联KV表
	System      uint   `gorm:"column:system;comment:系统" json:"system" form:"system"`                                      //系统
	Brow        uint   `gorm:"column:brow;comment:浏览器" json:"brow" form:"brow"`                                           //浏览器
	BrowVersion uint   `gorm:"column:brow_version;comment:浏览器版本" json:"brow_version" form:"brow_version"`                 //浏览器版本
	CreatedAt   string `gorm:"column:created_at;type:DATETIME;comment:浏览时间" json:"created_at" form:"created_at"`          // 浏览时间
	Referer     uint   `gorm:"column:referer;comment:referer来源网址，关联KV" json:"referer" form:"referer"`                     //来源网址，关联KV表
}

// 获取表名
func (c *LinkBrow) TableName() string {
	return _default.LinkBrowName
}
