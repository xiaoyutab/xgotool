package xlink

import (
	"errors"
	"time"
)

// 创建连接信息
//
//	bat	连接访问PV信息
func PvCreate(bat *LinkBrow) error {
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	if bat.UrlId == 0 {
		return errors.New("连接ID不能为空")
	}
	if bat.CreatedAt == "" {
		bat.CreatedAt = time.Now().Format(time.DateTime)
	}
	err := _default.DB.Save(bat).Error
	if err != nil {
		return err
	}
	return nil
}
