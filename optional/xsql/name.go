package xsql

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 根据文件名获取SQL内容
//
//	name	SQL文件名，基于打包变量所在位置的相对路径，忽略.sql后缀
func Name(name string) (string, error) {
	return _default.Name(name)
}

// 根据文件名获取SQL内容
//
//	name	SQL文件名，基于打包变量所在位置的相对路径，忽略.sql后缀
func (c *Config) Name(name string) (string, error) {
	if name == "" {
		return "", errors.New("文件名不能为空")
	}
	if c.SqlFile == nil {
		return "", errors.New("未传入SQL打包变量")
	}
	data, err := c.SqlFile.ReadFile(name + ".sql")
	if err != nil {
		return "", xlog.AE("SQL文件读取错误", err)
	}
	return string(data), nil
}
