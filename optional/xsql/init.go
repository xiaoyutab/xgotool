// SQL语句获取、SQL语句执行的方法
package xsql

import (
	"embed"

	"gorm.io/gorm"
)

// Sql查询模块
type Config struct {
	DB      *gorm.DB  // 数据库连接
	SqlFile *embed.FS // SQL文件的原生打包变量，需要使用//go:embed *.sql形式来存储文件
}

var _default Config = Config{}

// 注入配置项
func Regedit(c *Config) {
	if c.DB != nil {
		_default.DB = c.DB
	}
	if c.SqlFile != nil {
		_default.SqlFile = c.SqlFile
	}
}
