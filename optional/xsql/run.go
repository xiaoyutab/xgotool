package xsql

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 根据SQL语句进行运行【可使用?进行参数绑定和传入】
//
//	sql		要运行的SQL语句
//	arg		参数列表
func RunExec(sql string, arg ...any) error {
	return _default.RunExec(sql, arg...)
}

// 根据SQL语句进行运行【可使用?进行参数绑定和传入】
//
//	sql		要运行的SQL语句
//	arg		参数列表
func (c *Config) RunExec(sql string, arg ...any) error {
	if c.DB == nil {
		return errors.New("数据库未连接")
	}
	err := c.DB.Exec(sql, arg...).Error
	if err != nil {
		return xlog.EE("SQL语句执行失败", err)
	}
	return nil
}

// 根据SQL语句进行查询操作【可使用?进行参数绑定和传入】
//
//	sql		要查询的SQL语句
//	objs	查询结果回写到变量【指针】
//	arg		参数列表
func RunScan(sql string, objs any, arg ...any) error {
	return _default.RunScan(sql, objs, arg...)
}

// 根据SQL语句进行查询操作【可使用?进行参数绑定和传入】
//
//	sql		要查询的SQL语句
//	objs	查询结果回写到变量【指针】
//	arg		参数列表
func (c *Config) RunScan(sql string, objs any, arg ...any) error {
	if c.DB == nil {
		return errors.New("数据库未连接")
	}
	err := c.DB.Raw(sql, arg...).Scan(objs).Error
	if err != nil {
		return xlog.EE("SQL语句查询失败", err)
	}
	return nil
}
