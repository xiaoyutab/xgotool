package xsql

import (
	"errors"
	"strings"
)

// 读取SQL并进行替换操作
//
//	name	文件名
//	search	搜索内容
//	replace	替换内容
func Replace(name, search, replace string) (string, error) {
	return _default.Replace(name, search, replace)
}

// 读取SQL并进行替换操作
//
//	name	文件名
//	search	搜索内容
//	replace	替换内容
func ReplaceAll(name string, search, replace []string) (string, error) {
	return _default.ReplaceAll(name, search, replace)
}

// 读取SQL并进行替换操作
//
//	name	文件名
//	search	搜索内容
//	replace	替换内容
func (c *Config) Replace(name, search, replace string) (string, error) {
	return c.ReplaceAll(name, []string{search}, []string{replace})
}

// 读取SQL并进行替换操作
//
//	name	文件名
//	search	搜索内容
//	replace	替换内容
func (c *Config) ReplaceAll(name string, search, replace []string) (string, error) {
	sql, err := c.Name(name)
	if err != nil {
		return "", err
	}
	if len(search) != len(replace) {
		return "", errors.New("搜索内容和替换内容未对应")
	}
	for i := 0; i < len(search); i++ {
		sql = strings.ReplaceAll(sql, search[i], replace[i])
	}
	return sql, nil
}
