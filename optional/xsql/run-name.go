package xsql

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 根据SQL文件名进行运行【可使用?进行参数绑定和传入】
//
//	name	文件名
//	arg		参数列表
func RunNameExec(name string, arg ...any) error {
	return _default.RunNameExec(name, arg...)
}

// 根据SQL文件名进行运行【可使用?进行参数绑定和传入】
//
//	name	文件名
//	arg		参数列表
func (c *Config) RunNameExec(name string, arg ...any) error {
	sql, err := c.Name(name)
	if err != nil {
		return err
	}
	if c.DB == nil {
		return errors.New("数据库未连接")
	}
	err = c.DB.Exec(sql, arg...).Error
	if err != nil {
		return xlog.EE("SQL语句执行失败", err)
	}
	return nil
}

// 根据SQL文件名进行查询操作【可使用?进行参数绑定和传入】
//
//	name	文件名
//	objs	查询结果回写到变量【指针】
//	arg		参数列表
func RunNameScan(name string, objs any, arg ...any) error {
	return _default.RunNameScan(name, objs, arg...)
}

// 根据SQL文件名进行查询操作【可使用?进行参数绑定和传入】
//
//	name	文件名
//	objs	查询结果回写到变量【指针】
//	arg		参数列表
func (c *Config) RunNameScan(name string, objs any, arg ...any) error {
	sql, err := c.Name(name)
	if err != nil {
		return err
	}
	if c.DB == nil {
		return errors.New("数据库未连接")
	}
	err = c.DB.Raw(sql, arg...).Scan(objs).Error
	if err != nil {
		return xlog.EE("SQL语句查询失败", err)
	}
	return nil
}
