// 标签模块
package xtag

import "gorm.io/gorm"

type Config struct {
	DB      *gorm.DB
	TagName string // 标签表表名
	TagLink string // 标签连接表表名
}

var _default Config = Config{
	TagName: "tag",
	TagLink: "tag_link",
}

// 入口配置
func Regedit(c *Config) {
	if c == nil {
		return
	}
	if c.DB != nil {
		_default.DB = c.DB
	}
	if c.TagName != "" {
		_default.TagName = c.TagName
	}
	if c.TagLink != "" {
		_default.TagLink = c.TagLink
	}
	// 数据库/表注册/创建
	if _default.DB != nil {
		_default.DB.AutoMigrate(&Tag{}, &TagLink{})
	}
}

// 标签表
type Tag struct {
	Id        uint   `gorm:"column:id;primaryKey;not null;autoIncrement" json:"id" form:"id"`
	Name      string `gorm:"column:name;type:VARCHAR(200);comment:标签名称" json:"name" form:"name"`               // 标签名
	AliasId   uint   `gorm:"column:alias_id;comment:标签别名源ID 0-普通标签 xxx-标签别名" json:"alias_id" form:"alias_id"`  // 标签所属主签ID 0-普通标签
	Uid       uint   `gorm:"column:uid;comment:创建人ID 0-系统标签" json:"uid" form:"uid"`                            // 创建人ID 0-系统标签
	CreatedAt string `gorm:"column:created_at;type:DATETIME;comment:添加时间" json:"created_at" form:"created_at"` //添加时间
	UpdatedAt string `gorm:"column:updated_at;type:DATETIME;comment:处理时间" json:"updated_at" form:"updated_at"` //处理时间
}

// 获取表名
func (c *Tag) TableName() string {
	return _default.TagName
}

// 标签关联表
type TagLink struct {
	Id         uint64 `gorm:"column:id;primaryKey;type:BIGINT UNSIGNED;not null;autoIncrement" json:"id" form:"id"`
	TagId      uint   `gorm:"column:tag_id;comment:标签ID;index:tag_id" json:"tag_id" form:"tag_id"`                         //标签编号
	TargetId   uint   `gorm:"column:target_id;comment:连接对象ID;index:target_id" json:"target_id" form:"target_id"`           //链接对象表主键编号
	TargetName uint   `gorm:"column:target_name;comment:连接对象类型ID;index:target_name" json:"target_name" form:"target_name"` //链接对象表名称，KeyValue值
	CreatedAt  string `gorm:"column:created_at;type:DATETIME;comment:添加时间" json:"created_at" form:"created_at"`            //添加时间
}

// 获取表名
func (c *TagLink) TableName() string {
	return _default.TagLink
}
