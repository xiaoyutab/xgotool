package xtag

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 获取标签详情信息
//
//	tag_id	标签ID
func Info(tag_id uint) (*Tag, error) {
	if tag_id == 0 {
		return nil, errors.New("标签ID传输错误")
	}
	cache_key := xcache.Key("xtag.info", tag_id)
	tag_info := Tag{}
	if xcache.Exists(cache_key) {
		if err := xcache.GetStruct(cache_key, &tag_info); err == nil {
			return &tag_info, nil
		}
	}
	// 从数据库中进行读取
	err := _default.DB.Table(_default.TagName).Where("id", tag_id).Find(&tag_info).Error
	if err != nil {
		return nil, xlog.AE("标签信息获取失败", err)
	}
	if tag_info.Id == 0 {
		return nil, errors.New("标签不存在")
	}
	xcache.SetStruct(cache_key, &tag_info)
	return &tag_info, nil
}
