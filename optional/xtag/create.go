package xtag

import (
	"errors"
	"time"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
)

// 创建标签
//
//	c	标签结构内容
func Create(c *Tag) error {
	if c.Name == "" {
		return errors.New("标签名不能为空")
	}
	if c.CreatedAt == "" {
		c.CreatedAt = time.Now().Format(time.DateTime)
	}
	c.UpdatedAt = time.Now().Format(time.DateTime)
	err := _default.DB.Table(_default.TagName).Save(c).Error
	if err != nil {
		return err
	}
	xcache.SetStruct(xcache.Key("xtag.info", c.Id), c)
	return nil
}
