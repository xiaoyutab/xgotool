package xtag

import (
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 检测连接
// 将连接创建到普通tag标签上，而不是alias别名上
//
//	target_id	对象表名
//	target_name	对象名称
//	tag_id		标签ID
func IsLink(target_id, target_name, tag_id uint) bool {
	if target_id == 0 || target_name == 0 || tag_id == 0 {
		return false
	}
	for {
		// 获取标签详情
		// 如果是别名标签的话就追标签链，直接追到最底层的普通标签
		inf, err := Info(tag_id)
		if err != nil {
			return false
		}
		if inf.AliasId == 0 {
			break
		}
		tag_id = inf.AliasId
	}
	c := TagLink{
		TagId:      tag_id,
		TargetId:   target_id,
		TargetName: target_name,
	}
	err := _default.DB.Find(&c).Error
	if err != nil {
		xlog.Alert("连接检测操作失败", err)
		return false
	}
	if c.Id > 0 {
		return true
	}
	return false
}
