package xtag

import (
	"errors"
	"time"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 创建连接
// 将连接创建到普通tag标签上，而不是alias别名上
//
//	target_id	对象表名
//	target_name	对象名称
//	tag_id		标签ID
func Link(target_id, target_name, tag_id uint) error {
	if target_id == 0 || target_name == 0 || tag_id == 0 {
		return errors.New("连接参数错误")
	}
	for {
		// 获取标签详情
		// 如果是别名标签的话就追标签链，直接追到最底层的普通标签
		inf, err := Info(tag_id)
		if err != nil {
			return err
		}
		if inf.AliasId == 0 {
			break
		}
		tag_id = inf.AliasId
	}
	c := TagLink{
		TagId:      tag_id,
		TargetId:   target_id,
		TargetName: target_name,
		CreatedAt:  time.Now().Format(time.DateTime),
	}
	err := _default.DB.Table(_default.TagLink).Save(&c).Error
	if err != nil {
		return xlog.AE("用户连接保存失败", err)
	}
	return err
}
