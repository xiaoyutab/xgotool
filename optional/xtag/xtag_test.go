package xtag_test

import (
	"testing"

	"gitee.com/xiaoyutab/xgotool/optional/xtag"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func TestTag(t *testing.T) {
	db, _ := gorm.Open(mysql.Open("admin:admin@tcp(localhost:3306)/gatway?charset=utf8"), &gorm.Config{})
	xtag.Regedit(&xtag.Config{
		DB: db,
	})
	// xtag.Create(&xtag.Tag{
	// 	Id:   1,
	// 	Name: "atest1",
	// })
	// xtag.Create(&xtag.Tag{
	// 	Id:      2,
	// 	Name:    "atest2",
	// 	AliasId: 1,
	// })
	// xtag.Create(&xtag.Tag{
	// 	Id:      3,
	// 	Name:    "atest3",
	// 	AliasId: 2,
	// })
	// xtag.Link(1, 1, 1)
	// xtag.Link(1, 1, 2)
	// xtag.Link(1, 1, 3)
	// xtag.Link(1, 2, 2)
	// xtag.Unlink(1, 2, 2)
	xtag.Remove(3)
}
