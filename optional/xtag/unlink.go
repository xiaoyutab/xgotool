package xtag

import "errors"

// 删除连接信息
//
//	target_id	对象表名
//	target_name	对象名称
//	tag_id		标签ID
func Unlink(target_id, target_name, tag_id uint) error {
	if target_id == 0 || target_name == 0 || tag_id == 0 {
		return errors.New("连接参数错误")
	}
	return _default.DB.Table(_default.TagLink).
		Where("tag_id", tag_id).
		Where("target_id", target_id).
		Where("target_name", target_name).
		Delete(&TagLink{}).
		Error
}
