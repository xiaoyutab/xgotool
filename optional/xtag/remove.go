package xtag

import "gitee.com/xiaoyutab/xgotool/individual/xcache"

// 删除TAG
// 此操作会连带着删除taglink的数据和tag alias数据，使用时请多注意使用情况，另，建议每隔一段时间执行一次 `OPTIMIZE TABLE xxxx`，用于回收删除后的空间
// 如果删除的标签为别名标签，则此操作会将该别名标签、别名标签的别名给删除
//
//	id	标签ID
func Remove(id uint) error {
	if id == 0 {
		return nil
	}
	// 查找tag alias别名标签
	tag_del := []uint{id}
	tmp := []uint{}
	tmp_2 := []uint{}
	err := _default.DB.Table(_default.TagName).Where("alias_id", id).Select("id").Find(&tmp).Error
	if err != nil {
		return err
	}
	if len(tmp) > 0 {
		tag_del = append(tag_del, tmp...)
		// 查找别名的别名
		for {
			err := _default.DB.Table(_default.TagName).Where("alias_id in ?", tmp).Select("id").Find(&tmp_2).Error
			if err != nil {
				return err
			}
			if len(tmp) <= 0 {
				break
			}
			tag_del = append(tag_del, tmp...)
			tmp = tmp_2
			tmp_2 = []uint{}
		}
	}
	if len(tag_del) > 0 {
		// 执行删除操作
		err := _default.DB.Table(_default.TagName).Where("id in ?", tag_del).Delete(&Tag{}).Error
		if err != nil {
			return err
		}
		// 移除缓存
		for i := 0; i < len(tag_del); i++ {
			xcache.Remove(xcache.Key("xtag.info", tag_del[i]))
		}
		// 删除tag alias link
		err = _default.DB.Table(_default.TagLink).Where("tag_id in ?", tag_del).Delete(&Tag{}).Error
		if err != nil {
			return err
		}
	}
	return nil
}
