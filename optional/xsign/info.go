package xsign

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 获取签到批次详情信息
//
//	id	批次ID
func Info(id uint) (*SignType, error) {
	cache_key := xcache.Key("xgotool.xsign.info", id)
	sign := SignType{}
	if xcache.Exists(cache_key) {
		if err := xcache.GetStruct(cache_key, &sign); err == nil {
			return &sign, nil
		}
	}
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	err := _default.DB.Table(_default.SignTypeTable).Where("id", id).Find(&sign).Error
	if err != nil {
		return nil, xlog.AE("签到批次信息获取失败", err)
	}
	if sign.Id == 0 {
		return nil, errors.New("批次数据未找到")
	}
	xcache.SetStruct(cache_key, sign)
	return &sign, nil
}
