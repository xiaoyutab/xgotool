package xsign

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
	"gitee.com/xiaoyutab/xgotool/xnum"
)

// 创建/修改批次信息
//
//	c	批次信息
func Create(c *SignType) error {
	if c.Uid == 0 {
		return errors.New("创建人不允许为空")
	}
	if !xnum.InArray(c.Type, []uint8{SIGN_WEEK, SIGN_MONTH, SIGN_EVERY,
		SIGN_ONCE, SIGN_TWICE, SIGN_WORK,
	}) {
		return errors.New("批次类型选择错误")
	}
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	err := _default.DB.Table(_default.SignTypeTable).Save(c).Error
	if err != nil {
		return xlog.AE("签到批次信息存储错误", err)
	}
	xcache.SetStruct(xcache.Key("xgotool.xsign.info", c.Id), c)
	return nil
}
