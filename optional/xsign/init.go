// 签到管理模块
package xsign

import (
	"time"

	"gitee.com/xiaoyutab/xgotool/individual/xcron"
	"gorm.io/gorm"
)

const (
	SIGN_WEEK  = iota + uint8(1) // 周签到[每天只允许签到一次]
	SIGN_MONTH                   // 月签到[每天只允许签到一次]
	SIGN_EVERY                   // 每日签到[每天只允许签到一次]
	SIGN_ONCE                    // 单次签到[入场签到]
	SIGN_TWICE                   // 双次签到[入场签到+离场签到]
	SIGN_WORK                    // 双次签到[入场签到+离场签到][每天允许签到两次]
)

type Config struct {
	DB            *gorm.DB          // 数据库连接
	SignTypeTable string            // 签到批次配置表
	SignUserTable string            // 用户签到记录表
	HistoryTable  string            // 签到历史记录存储表
	SignTwiceTime time.Duration     // 双次签到模块中，两次签到的最小时间间隔[默认10分钟]
	SignFunc      func(c *SignUser) // 签到成功后的回调，c 签到回调标识
}

var _default Config = Config{
	SignTypeTable: "xsign",
	SignUserTable: "xsign_user",
	HistoryTable:  "xsign_history",
	SignTwiceTime: time.Minute * 10,
}

// 注入配置项
//
//	c	程序中需要使用的配置项信息
func Regedit(c *Config) {
	if c == nil {
		return
	}
	if c.DB != nil {
		_default.DB = c.DB
	}
	if c.SignTypeTable != "" {
		_default.SignTypeTable = c.SignTypeTable
	}
	if c.HistoryTable != "" {
		_default.HistoryTable = c.HistoryTable
	}
	if c.SignUserTable != "" {
		_default.SignUserTable = c.SignUserTable
	}
	if c.SignTwiceTime > 0 {
		_default.SignTwiceTime = c.SignTwiceTime
	}
	if _default.DB != nil {
		_default.DB.AutoMigrate(&SignType{}, &SignUser{}, &SignHistory{})
	}
	xcron.SpecNext("1 0 * * *", xcron.CronTab{
		Key:  "xsign.reset.pitch",
		Name: "根据任务类型重设批次数量信息",
		Desc: "根据签到的批次类型来确定是否需要重置次数",
		Func: ResetPitch,
	})
}

// 签到配置表
type SignType struct {
	Id        uint   `gorm:"column:id;primaryKey;autoIncrement;not null" form:"id" json:"id"`
	Uid       uint   `gorm:"column:uid;comment:创建人ID" form:"uid" json:"uid"`                                          //创建人ID
	Type      uint8  `gorm:"column:type;type:tinyint unsigned;comment:签到类型 xsign.SIGN_XXX类型" form:"type" json:"type"` //签到类型 xsign.SIGN_XXX类 型
	Title     string `gorm:"column:title;type:varchar(200);comment:签到批次标题" form:"title" json:"title"`                 //签到批次标题
	Desc      string `gorm:"column:desc;type:varchar(500);comment:签到批次描述" form:"desc" json:"desc"`                    //签到批次描述
	Status    uint8  `gorm:"column:status;type:tinyint unsigned;comment:批次状态" form:"status" json:"status"`            //批次状态0-待审核 1-审核通过 2-审核拒绝
	StartTime string `gorm:"column:start_time;type:datetime;comment:创建时间" form:"start_time" json:"start_time"`        //开始时间
	EndTime   string `gorm:"column:end_time;type:datetime;comment:修改时间" form:"end_time" json:"end_time"`              //结束时间
	CreatedAt string `gorm:"column:created_at;type:datetime;comment:创建时间" form:"created_at" json:"created_at"`        //创建时间
	UpdatedAt string `gorm:"column:updated_at;type:datetime;comment:修改时间" form:"updated_at" json:"updated_at"`        //修改时间
}

// 返回所属表名信息
func (c *SignType) TableName() string {
	return _default.SignTypeTable
}

// 用户签到详情记录
type SignUser struct {
	Id      uint   `gorm:"column:id;primaryKey;autoIncrement;not null" form:"id" json:"id"`
	Uid     uint   `gorm:"column:uid;comment:签到用户ID" form:"uid" json:"uid"`                              //签到用户ID
	Sid     uint   `gorm:"column:sid;comment:签到批次ID" form:"sid" json:"sid"`                              //签到批次ID
	PreDate string `gorm:"column:pre_date;type:datetime;comment:上次签到时间" form:"pre_date" json:"pre_date"` //上次签到时间
	PreOnce uint   `gorm:"column:pre_once;comment:周期内累计签到次数" form:"pre_once" json:"pre_once"`            //周期内累计签到次数
	PreTwce uint   `gorm:"column:pre_twce;comment:周期内连续签到次数" form:"pre_twce" json:"pre_twce"`            //周期内连续签到次数
}

// 返回所属表名信息
func (c *SignUser) TableName() string {
	return _default.SignUserTable
}

// 签到明细记录表
type SignHistory struct {
	Id        uint64 `gorm:"column:id;type:bigint unsigned;primaryKey;autoIncrement;not null" form:"id" json:"id"`
	Uid       uint   `gorm:"column:uid;comment:用户ID" form:"uid" json:"uid"`                                    //用户ID
	Sid       uint   `gorm:"column:sid;comment:批次ID" form:"sid" json:"sid"`                                    //批次ID
	Ip        uint   `gorm:"column:ip;comment:IP地址，KVID" form:"ip" json:"ip"`                                  //IP，KV表ID
	Ua        uint   `gorm:"column:ua;comment:UA头，KVID" form:"ua" json:"ua"`                                   //UA头，KV表ID
	CreatedAt string `gorm:"column:created_at;type:datetime;comment:签到时间" form:"created_at" json:"created_at"` //签到时间
}

// 返回所属表名信息
func (c *SignHistory) TableName() string {
	return _default.HistoryTable
}
