package xsign

import (
	"fmt"
	"time"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 重置批次信息标识
// SIGN_WEEK  = iota + uint8(1) // 周签到[每天只允许签到一次](每周一重置)
// SIGN_MONTH                   // 月签到[每天只允许签到一次](每月1号重置)
// SIGN_EVERY                   // 每日签到[每天只允许签到一次](每年1月1号重置)
// SIGN_ONCE                    // 单次签到[入场签到]
// SIGN_TWICE                   // 双次签到[入场签到+离场签到]
// SIGN_WORK                    // 双次签到[入场签到+离场签到][每天允许签到两次]
func ResetPitch() {
	// 查询目前在有效期内的批次ID信息
	mods := []SignType{}
	if _default.DB == nil {
		return
	}
	err := _default.DB.Table(_default.SignTypeTable).
		Where("status", 1). // 审核通过
		Where("start_time >= ?", time.Now().Format(time.DateTime)).
		Where("end_time <= ?", time.Now().Format(time.DateTime)).
		Find(&mods).
		Error
	if err != nil {
		xlog.Alert("生效内的批次列表查询失败", err)
		return
	}
	if len(mods) == 0 {
		return
	}
	// 循环批次信息进行重置
	for i := 0; i < len(mods); i++ {
		switch mods[i].Type {
		case SIGN_WEEK:
			// 周重置
			if time.Now().Weekday() == 1 {
				// 周一重置
				reset_pitch(mods[i].Id)
			}
		case SIGN_MONTH:
			// 月重置
			if time.Now().Day() == 1 {
				// 每月1号重置
				reset_pitch(mods[i].Id)
			}
		case SIGN_EVERY:
			// 年重置
			if time.Now().Month() == 1 && time.Now().Day() == 1 {
				reset_pitch(mods[i].Id)
			}
		case SIGN_WORK:
			// 每日重置
			reset_pitch(mods[i].Id)
		}
	}
}

// 批次重置
//
//	sid	批次ID
func reset_pitch(sid uint) {
	err := _default.DB.Table(_default.SignUserTable).
		Where("sid", sid).
		Select("pre_once", "pre_twce").
		Updates(&SignUser{}). // 因默认值为0，所以此处不再给对应变量进行赋值
		Error
	if err != nil {
		xlog.Alert("用户签到信息重置错误", fmt.Errorf("批次ID：%d,错误原因：%s", sid, err.Error()))
	}
}
