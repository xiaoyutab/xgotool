package xtype

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
)

// Adcode换取分类的详细信息
//
//	type_code	分类的TypeCode值【五~六位数字】
func Code2info(type_code uint) (*Types, error) {
	cache_key := xcache.Key("xtype.code2info", type_code)
	cit := Types{}
	if xcache.Exists(cache_key) {
		if err := xcache.GetStruct(cache_key, &cit); err == nil {
			return &cit, nil
		}
	}
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	cit.TypeCode = type_code
	// 从数据库中进行读取
	err := _default.DB.Table(_default.TypeName).Find(&cit).Error
	if err != nil {
		return nil, err
	}
	xcache.SetStruct(cache_key, cit)
	return &cit, nil
}
