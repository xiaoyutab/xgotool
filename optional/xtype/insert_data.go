package xtype

import (
	"fmt"
	"strings"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
	"gitee.com/xiaoyutab/xgotool/xnum"
	"gitee.com/xiaoyutab/xgotool/xstring"
)

// 插入数据信息
func insert_data() {
	xlog.Info("开始转换分类信息", nil)
	count := int64(0)
	lis := strings.Split(AmapCsv, "\n")
	for i := 0; i < len(lis); i++ {
		lis[i] = strings.TrimSpace(lis[i])
		if i == 0 {
			// 第一行不进行计算
			continue
		}
		if lis[i] == "" {
			// 空白行不进行计算
			continue
		}
		count++
	}
	xlog.Info("分类转换完成，开始探测数据库内文件", nil)
	var db_count int64
	if err := _default.DB.Table(_default.TypeName).Count(&db_count).Error; err != nil {
		xlog.Crit("分类条数获取错误", err)
		return
	}
	if db_count != count {
		// 数据库条数和查询到的条数不一致，查询数据库中的所有数据，重组不一致的列表，进行插入
		db_lis := []uint{}
		if err := _default.DB.Table(_default.TypeName).Select("type_code").Find(&db_lis).Error; err != nil {
			xlog.Crit("所有分类主键信息获取错误", err)
			return
		}
		new_lis := []Types{}
		for i := 0; i < len(lis); i++ {
			if i == 0 {
				// 第一行不进行计算
				continue
			}
			if lis[i] == "" {
				// 空白行不进行计算
				continue
			}
			// 使用,分割字符串
			tmp := strings.Split(lis[i], ",")
			if len(tmp) < 7 {
				// 跳过数据不正确的行
				xlog.Error("CSV行展示错误", fmt.Errorf("行：%d,内容：%s", i+1, tmp))
				continue
			}
			if !xnum.InArray(xstring.ToUint(tmp[1]), db_lis) {
				new_lis = append(new_lis, Types{
					TypeCode: xstring.ToUint(tmp[1]),
					Name:     tmp[4],
					NameEn:   tmp[7],
				})
			}
		}
		// 数据入库
		for i := 0; i < len(new_lis); i++ {
			if err := _default.DB.Table(_default.TypeName).Create(&new_lis[i]).Error; err != nil {
				xlog.Error("分类插入错误", fmt.Errorf("分类名：%s,Code：%d", new_lis[i].Name, new_lis[i].TypeCode))
				continue
			}
		}
	}
	xlog.Info("分类信息探测完成", nil)
}
