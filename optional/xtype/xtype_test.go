package xtype_test

import (
	"testing"

	"gitee.com/xiaoyutab/xgotool/optional/xtype"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// 测试POI分类编码
func TestXtypeRegedit(t *testing.T) {
	db, _ := gorm.Open(mysql.Open("admin:admin@tcp(localhost:3306)/gatway?charset=utf8"), &gorm.Config{})
	xtype.Regedit(&xtype.Config{
		DB: db,
	})
	// 获取顶级分类列表
	lis, err := xtype.FirstList()
	if err != nil {
		t.Error(err)
	}
	t.Log(lis)
	// 获取二级分类列表
	lis, err = xtype.SecondList(60000)
	if err != nil {
		t.Error(err)
	}
	t.Log(lis)
	// 获取三级分类列表
	lis, err = xtype.ThirdList(60600)
	if err != nil {
		t.Error(err)
	}
	t.Log(lis)
	// 获取三级分类详情
	inf, err := xtype.Code2info(60601)
	if err != nil {
		t.Error(err)
	}
	t.Log(inf)
}
