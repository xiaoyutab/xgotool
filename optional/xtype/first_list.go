package xtype

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
)

// 获取顶级分类信息
func FirstList() ([]Types, error) {
	cache_key := xcache.Key("xcity.first_list")
	cit := []Types{}
	if xcache.Exists(cache_key) {
		if err := xcache.GetStruct(cache_key, &cit); err == nil {
			return cit, nil
		}
		cit = []Types{}
	}
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	// 从数据库中进行读取
	err := _default.DB.Table(_default.TypeName).
		Where("type_code % 10000 = 0").
		Order("type_code ASC").
		Find(&cit).
		Error
	if err != nil {
		return nil, err
	}
	xcache.SetStruct(cache_key, cit)
	return cit, nil
}
