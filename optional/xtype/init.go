// 分类模块，高德POI分类
package xtype

import (
	_ "embed"

	"gorm.io/gorm"
)

// 分类为从高德进行下载的POI分类编码信息，然后另存为的CSV文件，更新时请重新下载保存到此目录下
// CSV文件要求：使用,分割各个列，然后使用换行符分割各个行，内容编码为GBK编码
// 此模块依赖xlog模块进行日志输出/入库
// 下载地址：https://lbs.amap.com/api/webservice/download

type Config struct {
	DB       *gorm.DB
	TypeName string // 省市区存储的表名
}

var _default Config = Config{
	TypeName: "types",
}

// 入口配置
func Regedit(c *Config) {
	if c == nil {
		return
	}
	if c.DB != nil {
		_default.DB = c.DB
	}
	if c.TypeName != "" {
		_default.TypeName = c.TypeName
	}
	// 数据库/表注册/创建
	if _default.DB != nil {
		_default.DB.AutoMigrate(&Types{})
		insert_data()
	}
}

// 城市的表结构信息
type Types struct {
	TypeCode uint   `gorm:"column:type_code;primaryKey;comment:分类的New Type;not null" json:"type_code" form:"type_code"` // 省市区的ADCode
	Name     string `gorm:"column:name;type:VARCHAR(100);comment:分类的中文名" json:"name" form:"name"`                       // 中文名
	NameEn   string `gorm:"column:name_en;type:VARCHAR(100);comment:分类的英文名" json:"name_en" form:"name_en"`              // 中文名
}

// 获取表名
func (c *Types) TableName() string {
	return _default.TypeName
}

//go:embed amap_poicode.csv
var AmapCsv string
