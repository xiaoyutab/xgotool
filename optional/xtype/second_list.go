package xtype

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
)

// 获取二级分类信息
//
//	type_code	一级分类code
func SecondList(type_code uint) ([]Types, error) {
	if type_code%10000 != 0 {
		return nil, errors.New("传入的一级分类code不正确")
	}
	cache_key := xcache.Key("xcity.SecondList", type_code)
	cit := []Types{}
	if xcache.Exists(cache_key) {
		if err := xcache.GetStruct(cache_key, &cit); err == nil {
			return cit, nil
		}
		cit = []Types{}
	}
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	// 从数据库中进行读取
	err := _default.DB.Table(_default.TypeName).
		Where("type_code > ? and type_code < ?", type_code, type_code+10000).
		Where("type_code % 100 = 0").
		Order("type_code ASC").
		Find(&cit).
		Error
	if err != nil {
		return nil, err
	}
	xcache.SetStruct(cache_key, cit)
	return cit, nil
}
