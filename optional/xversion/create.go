package xversion

import (
	"errors"
	"time"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 存储数据信息
//
//	c	存储的 versionAPP版本 表内容
func Create(c *Version) error {
	if c == nil {
		return errors.New("待存储的数据为空")
	}
	// 数据库连接检测
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	// 时间变量检测
	if c.CreatedAt == "" {
		c.CreatedAt = time.Now().Format(time.DateTime)
	}
	if c.UpdatedAt == "" {
		c.UpdatedAt = time.Now().Format(time.DateTime)
	}
	// 数据保存
	db := _default.DB.Table("version").Save(c)
	if db.Error != nil {
		return xlog.AE("表 version 的数据保存失败", db.Error)
	}
	// 更新写入缓存
	xcache.SetStruct(xcache.Key("mkdata.temp.version.info", c.Id), c)
	// 返回结果
	return nil
}
