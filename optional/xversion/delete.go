package xversion

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 查询数据信息
//
//	id	查询的 versionAPP版本 表主键ID
func Delete(id uint) error {
	if id <= 0 {
		return errors.New("参数查询错误")
	}
	// 数据库连接检测
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	// 执行删除操作
	db := _default.DB.Table("version").Where("id", id).Delete(&Version{})
	// 错误判断
	if db.Error != nil {
		return xlog.AE("表 version 的数据查询失败", db.Error)
	}
	// 空条数判断
	if db.RowsAffected <= 0 {
		return errors.New("数据未找到")
	}
	// 缓存清理
	xcache.Remove(xcache.Key("mkdata.temp.version.info", id))
	return nil
}
