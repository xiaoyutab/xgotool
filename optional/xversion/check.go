package xversion

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 检测当前APP是否需要更新
//
//	typ	所属APP
//	der	所属设备端
//	num	客户端内部版本号，随每次发布递增【Build版本号】
func CheckNum(typ, der int, num uint) (*Version, error) {
	defer xlog.Recover()
	var id uint
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	err := _default.DB.Table(_default.TableName).Where("client_num >= ?", num).Where("type", typ).Where("dervice", der).Order("client_num DESC").Select("id").Find(&id).Error
	if err != nil {
		return nil, err
	}
	if id == 0 {
		return nil, errors.New("数据未找到")
	}
	inf, err := Info(id)
	if err != nil {
		return nil, err
	}
	inf.HasUpdate = inf.ClientNum > num // 是否提示更新
	inf.HasForce = inf.MinNum > num     // 是否强制更新
	return inf, nil
}
