package xversion

import (
	"gorm.io/gorm"
)

// 配置项
type Config struct {
	DB        *gorm.DB       // 数据库连接
	Type      map[int]string // APP说明 1-默认
	Dervice   map[int]string // APP所属平台 1-安卓 2-IOS 3-华为 4-微信小程序
	TableName string         // 表名
}

var _default Config = Config{
	Type: map[int]string{
		1: "默认APP",
	},
	Dervice: map[int]string{
		1: "安卓端",
		2: "IOS端",
		3: "华为端",
		4: "微信小程序端",
	},
	TableName: "version",
}

// 配置项注入
//
//	c	待注入的配置项信息
func Regedit(c *Config) {
	if c == nil {
		return
	}
	if c.DB != nil {
		_default.DB = c.DB
	}
	if len(c.Type) > 0 {
		_default.Type = c.Type
	}
	if len(c.Dervice) > 0 {
		_default.Dervice = c.Dervice
	}
	if c.TableName != "" {
		_default.TableName = c.TableName
	}
}

// APP版本
type Version struct {
	Id            uint   `gorm:"column:id;type:int unsigned;primaryKey;autoIncrement;not null;comment:主键ID" form:"id" json:"id"`              // 主键ID
	Type          int    `gorm:"column:type;type:int;comment:所属APP" form:"type" json:"type"`                                                  // 所属APP
	Dervice       int    `gorm:"column:dervice;type:int;comment:所属设备端" form:"dervice" json:"dervice"`                                         // 所属设备端
	ClientVersion string `gorm:"column:client_version;type:varchar(20);not null;comment:客户端的版本号" form:"client_version" json:"client_version"` // 客户端的版本号
	ClientNum     uint   `gorm:"column:client_num;type:int unsigned;not null;comment:客户端内部版本号，随每次发布递增" form:"client_num" json:"client_num"`   // 客户端内部版本号，随每次发布递增
	UpdateNote    string `gorm:"column:update_note;type:text;not null;comment:更新说明" form:"update_note" json:"update_note"`                    // 更新说明
	AppLink       string `gorm:"column:app_link;type:varchar(255);not null;comment:app的下载地址" form:"app_link" json:"app_link"`                 // app的下载地址
	MinVersion    string `gorm:"column:min_version;type:varchar(20);comment:最小更新版本号" form:"min_version" json:"min_version"`                   // 最小更新版本号
	MinNum        uint   `gorm:"column:min_num;type:int unsigned;comment:最小客户端内部版本号" form:"min_num" json:"min_num"`                           // 最小客户端内部版本号
	Ext           string `gorm:"column:ext;type:text;comment:该版本对应的附加属性配置，JSON存储" form:"ext" json:"ext"`                                      // 该版本对应的附加属性配置，JSON存储
	CreatedAt     string `gorm:"column:created_at;type:datetime;comment:创建时间" form:"created_at" json:"created_at"`                            // 创建时间
	UpdatedAt     string `gorm:"column:updated_at;type:datetime;comment:最后更新时间" form:"updated_at" json:"updated_at"`                          // 最后更新时间
	HasUpdate     bool   `gorm:"-" json:"has_update" form:"-"`                                                                                // 是否需要更新
	HasForce      bool   `gorm:"-" json:"has_force" form:"-"`                                                                                 // 是否强制更新
}

// 返回所属表名信息
func (c *Version) TableName() string {
	return _default.TableName
}
