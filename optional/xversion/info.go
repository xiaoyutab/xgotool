package xversion

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 查询数据信息
//
//	id	查询的 versionAPP版本 表主键ID
func Info(id uint) (*Version, error) {
	// 数据库连接检测
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	// 缓存检测
	cache_key := xcache.Key("mkdata.temp.version.info", id)
	models := Version{}
	if xcache.Exists(cache_key) {
		if err := xcache.GetStruct(cache_key, &models); err == nil {
			return &models, nil
		}
	}
	// 从数据库中进行读取
	err := _default.DB.Table("version").Where("id", id).Find(&models).Error
	if err != nil {
		return nil, xlog.AE("表 version 的数据查询失败", err)
	}
	if models.Id == 0 {
		return nil, errors.New("数据未找到")
	}
	// 写入缓存
	xcache.SetStruct(cache_key, models)
	// 返回结果信息
	return &models, nil
}
