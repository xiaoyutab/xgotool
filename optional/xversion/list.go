package xversion

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 查询数据信息
//
//	offset	跳过条数
//	limit	查询条数
func List(offset, limit int) (int64, []Version, error) {
	if offset <= 0 {
		offset = 0
	}
	if limit <= 0 {
		limit = 20
	}
	// 数据库连接检测
	if _default.DB == nil {
		return 0, nil, errors.New("数据库未连接")
	}
	// 执行查询操作
	db := _default.DB.Table("version").
		Order("id DESC")
	var count int64
	err := db.Count(&count).Error
	if err != nil {
		return 0, nil, xlog.AE("表 version 的总条数查询失败", err)
	}
	model := []Version{}
	err = db.Offset(offset).Limit(limit).Find(&model).Error
	if err != nil {
		return 0, nil, xlog.AE("表 version 的数据列表查询失败", err)
	}
	return count, model, nil
}
