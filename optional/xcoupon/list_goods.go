package xcoupon

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 获取优惠券关联商品信息
//
//	id	优惠券ID
func ListGoods(id uint) ([]CouponGoods, error) {
	cg := []CouponGoods{}
	cache_key := xcache.Key("xcoupon.list.goods", id)
	if xcache.Exists(cache_key) {
		if err := xcache.GetStruct(cache_key, &cg); err == nil {
			return cg, nil
		}
		cg = []CouponGoods{}
	}
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	err := _default.DB.Table(_default.WhithCoupon).Where("coupon_id", id).Order("`levels` ASC").Find(&cg).Error
	if err != nil {
		return nil, xlog.AE("优惠券关联查询失败", err)
	}
	xcache.SetStruct(cache_key, cg)
	return cg, nil
}
