package xcoupon

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
	"gitee.com/xiaoyutab/xgotool/xnum"
)

// 创建优惠券所关联的商品
//
//	c	优惠券商品信息
func CreateGood(c *CouponGoods) error {
	if !xnum.InArray(c.Levels, []uint8{1, 2, 3}) {
		return errors.New("目前仅限创建1~3级商品关联")
	}
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	err := _default.DB.Table(_default.WhithCoupon).Create(c).Error
	if err != nil {
		return xlog.AE("优惠券关联失败", err)
	}
	return nil
}
