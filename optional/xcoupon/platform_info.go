package xcoupon

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 获取平台详情信息
//
//	id	平台ID
func PlatformInfo(id uint) (*Platform, error) {
	if id == 0 {
		return nil, errors.New("平台参数传输错误")
	}
	// 从缓存中进行查询数据
	cache_key := xcache.Key("xcoupon.platform.info", id)
	plat := Platform{}
	if xcache.Exists(cache_key) {
		if err := xcache.GetStruct(cache_key, &plat); err == nil {
			return &plat, nil
		}
	}
	// 从数据库中进行查询数据
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	err := _default.DB.Table(_default.PlatformTable).Where("is_deleted", 0).Where("id", id).Find(&plat).Error
	if err != nil {
		return nil, xlog.AE("平台信息查询错误", err)
	}
	if plat.Id == 0 {
		return nil, errors.New("数据未找到")
	}
	// 写入缓存
	xcache.SetStruct(cache_key, plat)
	return &plat, nil
}
