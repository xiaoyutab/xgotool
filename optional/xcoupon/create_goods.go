package xcoupon

import "gitee.com/xiaoyutab/xgotool/individual/xlog"

// 创建优惠券所关联的商品
//
//	c	优惠券商品列表信息
func CreateGoods(c []CouponGoods) error {
	if len(c) <= 0 {
		return nil
	}
	err := DeleteGoods(c[0].CouponId)
	if err != nil {
		return err
	}
	for i := 0; i < len(c); i++ {
		err = CreateGood(&c[i])
		if err != nil {
			xlog.Alert("优惠券关联创建失败", err)
		}
	}
	return nil
}
