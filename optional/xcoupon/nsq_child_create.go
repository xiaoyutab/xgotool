package xcoupon

import (
	"fmt"
	"time"

	"gitee.com/xiaoyutab/xgotool/individual/xlog"
	"gitee.com/xiaoyutab/xgotool/xstring"
)

// NSQ队列，创建优惠券队列
//
//	NSQ	xcoupon.child.create	<Coupon>
func NsqChildCreate(c *Coupon) error {
	// 如果子券状态不为初始状态则直接返回处理成功，避免重复创建
	if c.ChildOver != 0 {
		return nil
	}
	if c.AvtivationStatus == 0 {
		// 直接领取的优惠券，修改状态为正常状态，然后直接返回
		c.ChildOver = 2
		return Create(c)
	}
	fmt.Println("PNSQ-", c.Id, c.ChildOver, c.ChildOver)
	c.ChildOver = 1
	err := Create(c)
	if err != nil {
		return err
	}
	// 创建子优惠券
	for i := uint(0); i < c.Quota; i++ {
		child := CouponChild{
			CouponId:       c.Id,                                             // 券ID
			ActivationCode: xstring.RandomPass(16, true, true, false, false), // 激活码
			CreatedAt:      time.Now().Format(time.DateTime),
			UpdatedAt:      time.Now().Format(time.DateTime),
			UsedTime:       "1970-01-01 08:00:00",
			ActivationTime: "1970-01-01 08:00:00",
			ExpirationTime: "2099-12-31 23:59:59",
		}
		if c.ValidType == 1 {
			child.ExpirationTime = c.ValidEndTime
		}
		err = _default.DB.Table(_default.ChildCoupon).Create(&child).Error
		if err != nil {
			xlog.AE("优惠券子券创建失败", err)
		}
	}
	// 重新获取优惠券详情，避免期间有其他操作导致券信息变更
	c, err = Info(c.Id)
	if err != nil {
		return err
	}
	c.ChildOver = 2
	err = Create(c)
	if err != nil {
		return err
	}
	return nil
}
