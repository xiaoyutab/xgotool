package xcoupon

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 删除优惠券关联的商品
//
//	id	券ID
func DeleteGoods(id uint) error {
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	// 移除之前的券关联商品
	err := _default.DB.Table(_default.WhithCoupon).Where("coupon_id", id).Delete(&CouponGoods{}).Error
	if err != nil {
		return xlog.AE("券关联商品移除失败", err)
	}
	// 移除缓存
	xcache.Remove(xcache.Key("xcoupon.list.goods", id))
	return nil
}
