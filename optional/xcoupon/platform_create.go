package xcoupon

import (
	"errors"
	"time"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 创建/修改平台信息
//
//	p	平台详情信息
func PlatformCreate(p *Platform) error {
	if p.Uid == 0 {
		return errors.New("平台创建人不允许为空")
	}
	if p.Name == "" {
		return errors.New("平台名称不允许为空")
	}
	p.CreatedAt = time.Now().Format(time.DateTime)
	p.UpdatedAt = time.Now().Format(time.DateTime)
	if p.Id > 0 {
		// 是修改资料的话
		old, err := PlatformInfo(p.Id)
		if err != nil {
			return err
		}
		p.CreatedAt = old.CreatedAt
	}
	// 进行入库
	err := _default.DB.Table(_default.PlatformTable).Save(p).Error
	if err != nil {
		return xlog.AE("平台信息存储失败", err)
	}
	if p.IsDeleted == 1 {
		// 删除缓存
		xcache.Remove(xcache.Key("xcoupon.platform.info", p.Id))
	} else {
		xcache.SetStruct(xcache.Key("xcoupon.platform.info", p.Id), p)
	}
	return nil
}
