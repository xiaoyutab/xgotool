package xcoupon

import "errors"

// 删除批次信息
//
//	id	批次ID
func UsedDelete(id uint) error {
	if id == 0 {
		return errors.New("条目未找到")
	}
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	inf, err := UsedInfo(id)
	if err != nil {
		return err
	}
	inf.IsDeleted = 1
	return UsedCreate(inf)
}
