// 自建通用优惠券系统
package xcoupon

import (
	"gorm.io/gorm"
)

// 自建通用优惠券系统，共分为以下几种优惠券
// 1. 全平台通用满减券
// 2. 全平台专用满减券
// 3. 全平台通用折扣券
// 4. 全平台专用折扣券
// 11. 单平台通用满减券
// 12. 单平台专用满减券
// 13. 单平台通用折扣券
// 14. 单平台专用折扣券
const (
	// 1. 全平台通用满减券
	TYPE_ALL_REDUCTION = uint(1)
	// 2. 全平台专用满减券
	TYPE_ALL_REDUCTION_DEDICATED = uint(2)
	// 3. 全平台通用折扣券
	TYPE_ALL_DISCOUNT = uint(3)
	// 4. 全平台专用折扣券
	TYPE_ALL_DISCOUNT_DEDICATED = uint(4)
	// 11. 单平台通用满减券
	TYPE_SINGLE_REDUCTION = uint(11)
	// 12. 单平台专用满减券
	TYPE_SINGLE_REDUCTION_DEDICATED = uint(12)
	// 13. 单平台通用折扣券
	TYPE_SINGLE_DISCOUNT = uint(13)
	// 14. 单平台专用折扣券
	TYPE_SINGLE_DISCOUNT_DEDICATED = uint(14)
)

// 优惠券类型，用以判定是否允许的操作
var coupon_type = []uint{1, 2, 3, 4, 11, 12, 13, 14}

// 折扣券类型
var coupon_zhekou = []uint{3, 4, 13, 14}

// 获取全平台优惠券类型
var coupon_all_goods = []uint{1, 3, 11, 13}

type Config struct {
	DB             *gorm.DB
	PlatformTable  string // 平台表表名
	CouponTable    string // 优惠券主表
	UsedTable      string // 使用批次表
	ChildCoupon    string // 优惠券领取记录表
	WhithCoupon    string // 优惠券关联表
	ChildSerialPre string // 优惠券流水号前缀[后面会追加`年月日时分秒毫秒`的流水号]
}

var _default Config = Config{
	PlatformTable:  "platform",
	CouponTable:    "coupon",
	UsedTable:      "coupon_used",
	ChildCoupon:    "coupon_child",
	WhithCoupon:    "coupon_whith",
	ChildSerialPre: "CP",
}

// 入口配置
func Regedit(c *Config) {
	if c != nil {
		if c.DB != nil {
			_default.DB = c.DB
		}
		if c.PlatformTable != "" {
			_default.PlatformTable = c.PlatformTable
		}
		if c.CouponTable != "" {
			_default.CouponTable = c.CouponTable
		}
		if c.UsedTable != "" {
			_default.UsedTable = c.UsedTable
		}
	}
	if _default.DB != nil {
		_default.DB.AutoMigrate(&Platform{}, &Used{}, &Platform{}, &Coupon{}, &CouponChild{}, &CouponGoods{})
	}
}

// 平台所属表模型
type Platform struct {
	Id        uint   `gorm:"column:id;primaryKey;not null" form:"id" json:"id"`
	Uid       uint   `gorm:"column:uid;comment:创建人" form:"uid" json:"uid"`                                                   //创建人
	Name      string `gorm:"column:name;type:varchar(200);comment:平台名称" form:"name" json:"name"`                             //平台名称
	Desc      string `gorm:"column:desc;type:varchar(500);comment:平台描述" form:"desc" json:"desc"`                             //平台描述
	Status    uint8  `gorm:"column:status;type:tinyint unsigned;comment:状态 0-待审核 1-审核通过 2-审核拒绝" form:"status" json:"status"` //状态 0-待审核 1-审核通过 2-审核拒绝
	IsDeleted uint8  `gorm:"column:is_deleted;type:tinyint unsigned;comment:是否删除 1-已删除" form:"is_deleted" json:"is_deleted"` //是否删除 1-已删除
	CreatedAt string `gorm:"column:created_at;type:datetime;comment:添加时间" form:"created_at" json:"created_at"`               //添加时间
	UpdatedAt string `gorm:"column:updated_at;type:datetime;comment:修改时间" form:"updated_at" json:"updated_at"`               //修改时间
}

// 返回所属表名信息
func (c *Platform) TableName() string {
	return _default.PlatformTable
}

// 优惠券主表
// 优惠券无删除功能，其以状态来进行区分
type Coupon struct {
	Id               uint    `gorm:"column:id;primaryKey;autoIncrement;not null;comment:ID" form:"id" json:"id"`                                                   //ID
	Title            string  `gorm:"column:title;type:varchar(64);comment:优惠券标题（有图片则显示图片）：无门槛50元优惠券 | 单品最高减2000元" form:"title" json:"title"`                       //优惠券标题（有图片则显示图片）：无门槛50元优惠券 | 单品最高减2000元
	Icon             uint    `gorm:"column:icon;comment:图片,file表ID" form:"icon" json:"icon"`                                                                       //图片
	Used             uint    `gorm:"column:used;comment:叠加批次 0-不可叠加" form:"used" json:"used"`                                                                      //叠加批次 0-不可叠加
	Type             uint    `gorm:"column:type;comment:券类型" form:"type" json:"type"`                                                                              //券类型
	AvtivationStatus uint8   `gorm:"column:avtivation_status;type:tinyint unsigned;comment:发放方式 0-直接领取，1-激活码激活" form:"avtivation_status" json:"avtivation_status"` //发放方式 0-直接领取，1-激活码激活
	WithAmount       float64 `gorm:"column:with_amount;type:decimal(10,2);comment:满多少" form:"with_amount" json:"with_amount"`                                      //满多少
	UsedAmount       float64 `gorm:"column:used_amount;type:decimal(10,2);comment:减多少" form:"used_amount" json:"used_amount"`                                      //减多少
	Quota            uint    `gorm:"column:quota;comment:配额：发券数量" form:"quota" json:"quota"`                                                                       //配额：发券数量
	TakeCount        uint    `gorm:"column:take_count;comment:已领取的优惠券数量" form:"take_count" json:"take_count"`                                                      //已领取的优惠券数量
	StartType        uint8   `gorm:"column:start_type;type:tinyint unsigned;comment:发券时间类型 1-立即可领取 2-指定时间内可领取" form:"start_type" json:"start_type"`                //发券时间类型 1-立即可领取 2-指定时间内可领取
	StartTime        string  `gorm:"column:start_time;type:datetime;comment:发放开始时间" form:"start_time" json:"start_time"`                                           //发放开始时间
	EndTime          string  `gorm:"column:end_time;type:datetime;comment:发放结束时间" form:"end_time" json:"end_time"`                                                 //发放结束时间
	ValidType        uint8   `gorm:"column:valid_type;type:tinyint;comment:时效 1-绝对时效（领取后XXX-XXX时间段有效） 2-相对时效（领取后/激活后N天有效）" form:"valid_type" json:"valid_type"`    //时效 1-绝对时效（领取后XXX-XXX时间段有效） 2-相对时效（领取后/激活后N天有效）
	ValidStartTime   string  `gorm:"column:valid_start_time;type:datetime;comment:使用开始时间" form:"valid_start_time" json:"valid_start_time"`                         //使用开始时间
	ValidEndTime     string  `gorm:"column:valid_end_time;type:datetime;comment:使用结束时间" form:"valid_end_time" json:"valid_end_time"`                               //使用结束时间
	ValidDays        uint    `gorm:"column:valid_days;type:tinyint unsigned;comment:自领取之日起有效天数" form:"valid_days" json:"valid_days"`                               //自领取之日起有效天数
	Status           uint8   `gorm:"column:status;type:tinyint unsigned;comment:状态 1-正常 2-禁止领取 3-禁止领取且禁止使用" form:"status" json:"status"`                           //状态 1-正常 2-禁止领取 3-禁止领取且禁止使用
	Uid              uint    `gorm:"column:uid;comment:创建人" form:"uid" json:"uid"`                                                                                 //创建人
	CreatedAt        string  `gorm:"column:created_at;type:datetime;comment:创建时间" form:"created_at" json:"created_at"`                                             //创建时间
	UpdatedAt        string  `gorm:"column:updated_at;type:datetime;comment:修改时间" form:"updated_at" json:"updated_at"`                                             //修改时间
	ChildOver        uint8   `gorm:"column:child_over;type:tinyint unsigned;comment:子优惠券表数据创建状态：0-未创建，1-创建中(此状态禁止删除),2-创建完成" form:"child_over" json:"child_over"`  //子优惠券表数据创建状态：0-未创建，1-创建中(此状态禁止删除),2-创建完成
	Examine          uint8   `gorm:"column:examine;type:tinyint unsigned;comment:审核状态 0-未提交审核 1-未审核 2-审核中 3-审核通过 4-审核拒绝" form:"examine" json:"examine"`            //审核状态 0-未提交审核 1-未审核 2-审核中 3-审核通过 4-审核拒绝
	PeopleCount      uint    `gorm:"column:people_count;comment:每人限领取多少张 0-无限制" form:"people_count" json:"people_count"`                                           //每人限领取多少张 0-无限制
	Secret           string  `gorm:"column:secret;type:varchar(20);comment:批次秘钥（前端随机生成一个随机串，然后前台根据券的批次秘钥进行关联分享/领取）" form:"secret" json:"secret"`                   //批次秘钥（前端随机生成一个随机串，然后前台根据券的批次秘钥进行关联分享/领取）
	Memo             string  `gorm:"column:memo;type:varchar(64);comment:券备注" form:"memo" json:"memo"`                                                             //券备注
	Platform         uint    `gorm:"column:platform;comment:该优惠券所属平台" form:"platform" json:"platform"`                                                             //该优惠券所属平台
	InDetail         uint8   `gorm:"column:in_detail;type:tinyint unsigned;comment:是否在详情页显示" form:"in_detail" json:"in_detail"`                                    //是否在详情页显示
	InActivity       uint8   `gorm:"column:in_activity;type:tinyint unsigned;comment:是否在活动页显示" form:"in_activity" json:"in_activity"`                              //是否在活动页显示
}

// 返回所属表名信息
func (c *Coupon) TableName() string {
	return _default.CouponTable
}

// 批次存储表模型【仅同一批次的优惠券可叠加使用】
type Used struct {
	Id        uint   `gorm:"column:id;primaryKey;not null" form:"id" json:"id"`
	Uid       uint   `gorm:"column:uid;comment:创建人" form:"uid" json:"uid"`                                                   //创建人
	Name      string `gorm:"column:name;type:varchar(200);comment:批次名称" form:"name" json:"name"`                             //平台名称
	Desc      string `gorm:"column:desc;type:varchar(500);comment:批次描述" form:"desc" json:"desc"`                             //平台描述
	Status    uint8  `gorm:"column:status;type:tinyint unsigned;comment:状态 0-待审核 1-审核通过 2-审核拒绝" form:"status" json:"status"` //状态 0-待审核 1-审核通过 2-审核拒绝
	IsDeleted uint8  `gorm:"column:is_deleted;type:tinyint unsigned;comment:是否删除 1-已删除" form:"is_deleted" json:"is_deleted"` //是否删除 1-已删除
	Platform  uint   `gorm:"column:platform;comment:该优惠券所属平台" form:"platform" json:"platform"`                               //该优惠券所属平台
	CreatedAt string `gorm:"column:created_at;type:datetime;comment:添加时间" form:"created_at" json:"created_at"`               //添加时间
	UpdatedAt string `gorm:"column:updated_at;type:datetime;comment:修改时间" form:"updated_at" json:"updated_at"`               //修改时间
}

// 返回所属表名信息
func (c *Used) TableName() string {
	return _default.UsedTable
}

// 优惠券子表
type CouponChild struct {
	Id             uint64 `gorm:"column:id;type:bigint unsigned;primaryKey;autoIncrement;not null" form:"id" json:"id"`
	CouponId       uint   `gorm:"column:coupon_id;comment:优惠券编号，关联coupon表" form:"coupon_id" json:"coupon_id"`                                                                  //优惠券编号，关联coupon表
	SerialNo       string `gorm:"column:serial_no;type:varchar(45);comment:优惠券码，类似CP20220303144538202字样" form:"serial_no" json:"serial_no"`                                    //优惠券码，类似CP20220303144538202字样
	ActivationCode string `gorm:"column:activation_code;type:varchar(45);comment:激活码，类似TDUPY22XYAJ55XK7的随机字符串;index:activation" form:"activation_code" json:"activation_code"` //激活码，类似TDUPY22XYAJ55XK7的随机字符串
	IsActivation   uint8  `gorm:"column:is_activation;type:tinyint unsigned;comment:是否激活;index:activation" form:"is_activation" json:"is_activation"`                          //是否激活
	UsedUid        uint   `gorm:"column:used_uid;comment:激活/领取人ID" form:"used_uid" json:"used_uid"`                                                                            //激活/领取人ID
	ActivationTime string `gorm:"column:activation_time;type:datetime;comment:领取/激活时间" form:"activation_time" json:"activation_time"`                                          //领取/激活时间
	UsedTime       string `gorm:"column:used_time;type:datetime;comment:使用时间" form:"used_time" json:"used_time"`                                                               //使用时间
	ExpirationTime string `gorm:"column:expiration_time;type:datetime;comment:过期时间" form:"expiration_time" json:"expiration_time"`                                             //过期时间
	IsUsed         uint8  `gorm:"column:is_used;type:tinyint unsigned;comment:使用状态，0-未使用 1-锁定中 2-已使用" form:"is_used" json:"is_used"`                                           //使用状态，0-未使用 1-锁定中 2-已使用
	CreatedAt      string `gorm:"column:created_at;type:datetime" form:"created_at" json:"created_at"`
	UpdatedAt      string `gorm:"column:updated_at;type:datetime" form:"updated_at" json:"updated_at"`
}

// 返回所属表名信息
func (c *CouponChild) TableName() string {
	return _default.ChildCoupon
}

// 优惠券商品/店铺关联表
// 1级关联 -> 2级关联 -> 3级关联
// 对应：店铺->SPU->SKU
type CouponGoods struct {
	Id       uint64 `gorm:"column:id;type:bigint unsigned;primaryKey;autoIncrement;not null" form:"id" json:"id"`
	WithId   uint   `gorm:"column:with_id;not null;comment:关联商品ID/店铺ID" form:"with_id" json:"with_id"`    //关联商品ID/店铺ID
	CouponId uint   `gorm:"column:coupon_id;comment:关联优惠券主表ID" form:"coupon_id" json:"coupon_id"`         //关联优惠券主表ID
	Levels   uint8  `gorm:"column:levels;type:tinyint unsigned;comment:关联等级" form:"levels" json:"levels"` //关联等级
	Platform uint   `gorm:"column:platform;not null;comment:所属平台" form:"platform" json:"platform"`        //所属平台
}

// 返回所属表名信息
func (c *CouponGoods) TableName() string {
	return _default.WhithCoupon
}
