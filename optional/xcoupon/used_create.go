package xcoupon

import (
	"errors"
	"time"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 创建/保存优惠券批次信息
//
//	useds	批次信息
func UsedCreate(useds *Used) error {
	if useds.Uid == 0 {
		return errors.New("批次创建人不能为空")
	}
	if useds.Platform == 0 {
		return errors.New("所属平台不能为空")
	}
	// 时间确定
	useds.UpdatedAt = time.Now().Format(time.DateTime)
	if useds.Id > 0 {
		old, err := UsedInfo(useds.Id)
		if err != nil {
			return err
		}
		useds.CreatedAt = old.CreatedAt
	}
	if useds.CreatedAt == "" {
		useds.CreatedAt = time.Now().Format(time.DateTime)
	}
	// 进行数据存储
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	err := _default.DB.Table(_default.UsedTable).Save(useds).Error
	if err != nil {
		return xlog.AE("优惠券批次存储错误", err)
	}
	// 如果是删除的话
	if useds.IsDeleted == 1 {
		xcache.Remove(xcache.Key("xgotool.xcoupon.used.info", useds.Id))
	} else {
		xcache.SetStruct(xcache.Key("xgotool.xcoupon.used.info", useds.Id), useds)
	}
	return nil
}
