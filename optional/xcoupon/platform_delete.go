package xcoupon

// 删除平台标识信息
//
//	id	平台ID
func PlatformDelete(id uint) error {
	old, err := PlatformInfo(id)
	if err != nil {
		return err
	}
	old.IsDeleted = 1
	return PlatformCreate(old)
}
