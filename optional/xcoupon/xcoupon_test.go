package xcoupon_test

import (
	"testing"
	"time"

	"gitee.com/xiaoyutab/xgotool/optional/xcoupon"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func TestUsedCreate(t *testing.T) {
	// 注册数据库
	db, _ := gorm.Open(mysql.Open("admin:admin@tcp(localhost:3306)/gatway?charset=utf8"), &gorm.Config{})
	xcoupon.Regedit(&xcoupon.Config{
		DB: db,
	})
	// 创建Used分类
	err := xcoupon.UsedCreate(&xcoupon.Used{
		Id:        1,
		Uid:       1,
		Name:      "测试批次",
		Status:    1,
		IsDeleted: 0,
		Platform:  1,
	})
	if err != nil {
		t.Error(err)
	}
	err = xcoupon.PlatformCreate(&xcoupon.Platform{
		Id:     1,
		Name:   "官方平台",
		Uid:    1,
		Status: 1,
	})
	if err != nil {
		t.Error(err)
	}
	id, err := xcoupon.Platform2ids(1)
	if err != nil {
		t.Error(err)
	}
	t.Log(id)
	// 优惠券创建
	// err = xcoupon.Creates(&xcoupon.Coupon{
	// 	Uid:              1,
	// 	Type:             xcoupon.TYPE_SINGLE_REDUCTION_DEDICATED,
	// 	Used:             1,
	// 	AvtivationStatus: 1, // 激活码激活
	// 	WithAmount:       10,
	// 	UsedAmount:       9,
	// 	Quota:            10,
	// 	StartType:        1,
	// 	ValidType:        2,
	// 	ValidDays:        10,
	// 	Status:           1,
	// 	Examine:          3,
	// 	Platform:         1,
	// }, []xcoupon.CouponGoods{{
	// 	WithId:   1,
	// 	Levels:   1,
	// 	Platform: 1,
	// }})
	// if err != nil {
	// 	t.Error(err)
	// }
	// 领取优惠券
	// err = xcoupon.Receive(1, 1)
	// if err != nil {
	// 	t.Error(err)
	// }
	time.Sleep(time.Second)
}
