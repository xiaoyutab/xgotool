package xcoupon

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 获取优惠券详情信息
//
//	id	优惠券ID
func Info(id uint) (*Coupon, error) {
	if id <= 0 {
		return nil, errors.New("优惠券ID传输错误")
	}
	coupon := Coupon{}
	// 缓存中读取
	cache_key := xcache.Key("xgotool.xcoupon.info", id)
	if xcache.Exists(cache_key) {
		if err := xcache.GetStruct(cache_key, &coupon); err == nil {
			return &coupon, nil
		}
	}
	// 数据库中读取
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	err := _default.DB.Where("id", id).Find(&coupon).Error
	if err != nil {
		return nil, xlog.AE("优惠券查询失败", err)
	}
	if coupon.Id == 0 {
		return nil, errors.New("优惠券未找到")
	}
	xcache.SetStruct(cache_key, coupon)
	return &coupon, nil
}
