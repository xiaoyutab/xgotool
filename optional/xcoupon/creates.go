package xcoupon

import (
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
	"gitee.com/xiaoyutab/xgotool/xnum"
)

// 创建/存储优惠券和关联商品
func Creates(c *Coupon, g []CouponGoods) error {
	if c.Id > 0 {
		err := DeleteGoods(c.Id)
		if err != nil {
			return err
		}
	}
	// 存储优惠券
	err := Create(c)
	if err != nil {
		return err
	}
	if xnum.InArray(c.Type, coupon_all_goods) {
		// 全平台优惠券不再创建关联商品
		return nil
	}
	// 优惠券关联创建
	for i := 0; i < len(g); i++ {
		g[i].CouponId = c.Id
		g[i].Platform = c.Platform
		err = CreateGood(&g[i])
		if err != nil {
			xlog.AE("优惠券关联创建失败", err)
		}
	}
	return nil
}
