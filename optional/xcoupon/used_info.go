package xcoupon

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 批次详情查看
//
//	id	批次ID
func UsedInfo(id uint) (*Used, error) {
	if id <= 0 {
		return nil, errors.New("批次ID传输错误")
	}
	coupon := Used{}
	// 缓存中读取
	cache_key := xcache.Key("xgotool.xcoupon.used.info", id)
	if xcache.Exists(cache_key) {
		if err := xcache.GetStruct(cache_key, &coupon); err == nil {
			return &coupon, nil
		}
	}
	// 数据库中读取
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	err := _default.DB.Where("id", id).Where("is_deleted", 0).Find(&coupon).Error
	if err != nil {
		return nil, xlog.AE("优惠券批次查询失败", err)
	}
	if coupon.Id == 0 {
		return nil, errors.New("批次未找到")
	}
	xcache.SetStruct(cache_key, coupon)
	return &coupon, nil
}
