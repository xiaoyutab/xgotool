package xcoupon

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 获取批次列表服务
//
//	uid		用户ID
//	offset	跳过条数
//	limit	查询条数
func UsedList(uid uint, offset, limit int) (int64, []Used, error) {
	if _default.DB == nil {
		return 0, nil, errors.New("数据库未连接")
	}
	db := _default.DB.Table(_default.UsedTable).Where("uid", uid).Where("is_deleted", 0)
	var count int64
	err := db.Count(&count).Error
	if err != nil {
		return 0, nil, xlog.AE("优惠券批次条数查询失败", err)
	}
	if count <= 0 {
		return 0, []Used{}, nil
	}
	lis := []Used{}
	err = db.Order("id DESC").Limit(limit).Offset(offset).Find(&lis).Error
	if err != nil {
		return 0, nil, xlog.AE("优惠券批次列表查询失败", err)
	}
	// 写入缓存
	for i := 0; i < len(lis); i++ {
		xcache.SetStruct(xcache.Key("xgotool.xcoupon.used.info", lis[i].Id), lis[i])
	}
	return count, lis, nil
}
