// 用户模块
package xuser

import "gorm.io/gorm"

// 此模块包含：
// 1. 附加属性功能（其中固定属性直接使用方进行固定即可，如昵称、头像、性别等直接固定到最外层用户表）
// 2. 用户关联功能
//
// 配置项信息
type Config struct {
	DB                      *gorm.DB
	AdditionalPropertyTable string           // 附加属性表表名
	AdditionalType          map[uint]string  // 附加属性类型
	UserLinkTable           string           // 用户关系关联表表名
	UserLinkMap             map[uint8]string // 用户关联关系字典
}

// 默认配置
var _default Config = Config{
	AdditionalPropertyTable: "xuser_additional",
	AdditionalType: map[uint]string{
		1: "曾用名",
		2: "外号",
	},
	UserLinkTable: "xuser_link",
	UserLinkMap: map[uint8]string{
		1:  "父亲",
		2:  "母亲",
		3:  "姐姐",
		4:  "妹妹",
		5:  "哥哥",
		6:  "弟弟",
		7:  "同学",
		8:  "同事",
		9:  "邻居",
		10: "妻子",
		11: "丈夫",
	},
}

// 配置项的注入
func Regedit(c *Config) {
	if c == nil {
		return
	}
	if c.DB != nil {
		_default.DB = c.DB
	}
	if c.AdditionalPropertyTable != "" {
		_default.AdditionalPropertyTable = c.AdditionalPropertyTable
	}
	if c.UserLinkTable != "" {
		_default.UserLinkTable = c.UserLinkTable
	}
	if len(c.UserLinkMap) > 0 {
		for i, v := range c.UserLinkMap {
			_default.UserLinkMap[i] = v
		}
	}
	if len(c.AdditionalType) > 0 {
		for i, v := range c.AdditionalType {
			_default.AdditionalType[i] = v
		}
	}
	if _default.DB != nil {
		_default.DB.AutoMigrate(&UserAdditional{})
	}
}

// 用户附加属性表
type UserAdditional struct {
	Id        uint   `gorm:"column:id;primaryKey;autoIncrement;not null" form:"id" json:"id"`
	UserId    uint   `gorm:"column:user_id;comment:所属用户" form:"user_id" json:"user_id"`                                //所属用户
	TypeId    uint   `gorm:"column:type_id;comment:附加属性类型" form:"type_id" json:"type_id"`                              //附加属性类型
	Value     string `gorm:"column:value;type:varchar(200);comment:附加属性值" form:"value" json:"value"`                   //附加属性值
	IsDeleted uint8  `gorm:"column:is_deleted;type:tinyint unsigned;comment:是否删除" form:"is_deleted" json:"is_deleted"` //是否删除
	CreatedAt string `gorm:"column:created_at;type:datetime;comment:添加时间" form:"created_at" json:"created_at"`         //添加时间
}

// 返回所属表名信息
func (c *UserAdditional) TableName() string {
	return _default.AdditionalPropertyTable
}
