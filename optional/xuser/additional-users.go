package xuser

import (
	"gitee.com/xiaoyutab/xgotool/individual/xcache"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 根据用户附加属性获取对应的用户列表
//
//	tid	分类id,0-所有
//	val	附加属性名称
func AdditionalUsers(tid uint, val string) []uint {
	uids := []uint{}
	cache_key := xcache.Key("xuser.additional.users", tid, val)
	if xcache.Exists(cache_key) {
		if err := xcache.GetStruct(cache_key, &uids); err == nil {
			return uids
		}
		uids = []uint{}
	}
	// 查询分类下属性IDS
	db := _default.DB.Table(_default.AdditionalPropertyTable).Where("value", val)
	if tid > 0 {
		db = db.Where("type_id", tid)
	}
	err := db.Select("user_id").Find(&uids).Error
	if err != nil {
		xlog.Alert("附加属性中的用户获取失败", err)
		return nil
	}
	xcache.SetStruct(cache_key, uids)
	return uids
}
