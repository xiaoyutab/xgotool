package xuser

import (
	"gitee.com/xiaoyutab/xgotool/individual/xcache"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
	"gitee.com/xiaoyutab/xgotool/xnum"
)

// 移除用户附加属性
//
//	uid	用户ID
//	tid	类型ID
//	val	附加属性值
func RemoveAdditional(uid, tid uint, val string) error {
	tmp, err := GetAdditional(uid, tid)
	if err != nil {
		return err
	}
	if !xnum.InArray(val, tmp) {
		return nil
	}
	// 创建关联
	err = _default.DB.Table(_default.AdditionalPropertyTable).
		Where("user_id", uid).
		Where("type_id", tid).
		Where("value", val).
		Where("is_deleted", 0).
		Update("is_deleted", 1).
		Error
	if err != nil {
		return xlog.AE("用户附加属性移除失败", err)
	}
	xcache.SetStruct(xcache.Key("xuser.get.additional", uid, tid), xnum.RemoveStringArray(val, tmp))
	// 添加属性列表-0
	xcache.SetStruct(xcache.Key("xuser.additional.users", uint(0), val), xnum.RemoveUintArray(uid, AdditionalUsers(0, val)))
	// 添加属性列表-tid
	xcache.SetStruct(xcache.Key("xuser.additional.users", tid, val), xnum.RemoveUintArray(uid, AdditionalUsers(tid, val)))
	return nil
}
