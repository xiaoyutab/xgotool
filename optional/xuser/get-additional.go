package xuser

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xcache"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 获取用户附加属性
//
//	uid	用户ID
//	tid	类型ID
func GetAdditional(uid, tid uint) ([]string, error) {
	if uid == 0 || tid == 0 {
		return nil, errors.New("参数错误")
	}
	cache_key := xcache.Key("xuser.get.additional", uid, tid)
	if xcache.Exists(cache_key) {
		tmp := []string{}
		if err := xcache.GetStruct(cache_key, &tmp); err == nil {
			return tmp, nil
		}
	}
	tmp := []string{}
	if _default.DB == nil {
		return nil, errors.New("数据库未连接")
	}
	err := _default.DB.Table(_default.AdditionalPropertyTable).
		Where("user_id", uid).
		Where("type_id", tid).
		Where("is_deleted", 0).
		Select("value").
		Find(&tmp).
		Error
	if err != nil {
		return nil, xlog.AE("用户附加属性查询失败", err)
	}
	xcache.SetStruct(cache_key, tmp)
	return tmp, nil
}
