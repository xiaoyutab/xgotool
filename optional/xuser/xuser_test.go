package xuser_test

import (
	"testing"

	"gitee.com/xiaoyutab/xgotool/optional/xuser"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// 测试用户附加属性
func TestUserAdd(t *testing.T) {
	db, _ := gorm.Open(mysql.Open("admin:admin@tcp(localhost:3306)/log_analysis?charset=utf8"), &gorm.Config{})
	xuser.Regedit(&xuser.Config{
		DB: db,
	})
	xuser.CreateAdditional(1, 1, "测试")
	xuser.CreateAdditional(1, 1, "这是测试属性")
	t.Log(xuser.GetAdditional(1, 1))
	xuser.RemoveAdditional(1, 1, "测试")
	t.Log(xuser.GetAdditional(1, 1))
}
