package xauth

// 获取权限列表
//
//	fid		上级权限ID
//	id		筛选ID列表【一旦传入此条件则fid自动失效】
//	offset	跳过条数
//	limit	查询条数
func GetAuthorityList(fid uint, id []uint, offset, limit int) ([]Authority, error) {
	if _default.DB == nil {
		return nil, errFun("数据库未连接", nil)
	}
	mod := []Authority{}
	db := _default.DB.Table(_default.AuthorityName)
	if len(id) > 0 {
		db = db.Where("id IN ?", id)
	} else {
		db = db.Where("fid", fid)
	}
	err := db.Offset(offset).Limit(limit).Order("id DESC").Find(&mod).Error
	if err != nil {
		return nil, errFun("角色列表获取失败", err)
	}
	return mod, nil
}
