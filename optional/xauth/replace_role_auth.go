package xauth

import (
	"time"
)

// 替换角色的权限列表
// 此操作会删除掉该角色的之前的权限列表，使用新传入的权限列表进行替换
//
//	id		角色ID
//	auth	权限ID列表
func ReplaceRoleAuth(id uint, auth []uint) error {
	if id <= 0 {
		return errFun("角色ID不能为空", nil)
	}
	if _default.DB == nil {
		return errFun("数据库未连接", nil)
	}
	// 删除之前的权限列表
	err := _default.DB.Table(_default.RoleAuthName).Where("role_id", id).Delete(&RoleAuth{}).Error
	if err != nil {
		return errFun("移除角色之前的权限失败", err)
	}
	if len(auth) == 0 {
		return nil
	}
	// 添加新的权限列表
	for i := 0; i < len(auth); i++ {
		err := _default.DB.Table(_default.RoleAuthName).Create(&RoleAuth{
			RoleId:    id,
			AuthId:    auth[i],
			CreatedAt: time.Now().Format(time.DateTime),
		}).Error
		if err != nil {
			return errFun("角色权限插入失败", err)
		}
	}
	return nil
}
