package xauth

import (
	"strings"
	"time"
)

// 创建权限操作
//
//	c	待保存的权限详情
func CreateAuth(c *Authority) error {
	if c.Code == "" {
		return errFun("权限标识位不能为空", nil)
	}
	if len(strings.Split(c.Code, ":")) == 1 {
		return errFun("权限标识位格式不正确", nil)
	}
	if c.Type == 0 {
		return errFun("权限类型来源不能为空", nil)
	}
	if _default.DB == nil {
		return errFun("数据库未连接", nil)
	}
	c.UpdatedAt = time.Now().Format(time.DateTime)
	if c.CreatedAt == "" {
		c.CreatedAt = c.UpdatedAt
	}
	err := _default.DB.Table(_default.AuthorityName).Save(c).Error
	if err != nil {
		return errFun("权限存储错误", err)
	}
	// 清空权限缓存
	_cache = cache{}
	return nil
}
