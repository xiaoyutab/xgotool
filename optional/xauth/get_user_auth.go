package xauth

// 获取用户的权限信息
//
//	uid	用户ID
//	typ	权限来源
func GetUserAuth(uid uint, typ uint) []string {
	if uid == 0 {
		return nil
	}
	_cache.Lock()
	defer _cache.Unlock()
	if v, ok := _cache.UserAuth[key(uid, typ)]; ok {
		return v
	}
	mod := []string{}
	// 从数据库中进行查询
	var err error
	if uid == 1 {
		// 超管，直接获取全部的权限列表
		err = _default.DB.Table(_default.AuthorityName).Where("type", typ).Select("code").Find(&mod).Error
	} else {
		// 获取用户的角色列表
		rids := getRoleAuth(GetUserRole(uid))
		if len(rids) == 0 {
			return nil
		}
		err = _default.DB.Table(_default.AuthorityName).
			Where("id in ?", rids).
			Where("type", typ).
			Select("code").
			Find(&mod).
			Error
	}
	if err != nil {
		errFun("用户权限获取失败", err)
		return nil
	}
	_cache.UserAuth[key(uid, typ)] = mod
	return mod
}
