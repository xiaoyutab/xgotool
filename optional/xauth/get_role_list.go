package xauth

// 获取角色列表
//
//	id		角色ID列表
//	offset	跳过条数
//	limit	查询条数
func GetRoleList(id []uint, offset, limit int) ([]Role, error) {
	if _default.DB == nil {
		return nil, errFun("数据库未连接", nil)
	}
	mod := []Role{}
	db := _default.DB.Table(_default.RoleName)
	if len(id) > 0 {
		db = db.Where("id IN ?", id)
	}
	err := db.
		Offset(offset).
		Limit(limit).
		Order("id DESC").
		Find(&mod).
		Error
	if err != nil {
		return nil, errFun("角色列表获取失败", err)
	}
	return mod, nil
}
