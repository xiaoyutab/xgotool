package xauth

// 查询下级权限条数
//
//	id	权限ID
func getCountByFid(id uint) int64 {
	var count int64
	err := _default.DB.Table(_default.AuthorityName).Where("fid", id).Count(&count).Error
	if err != nil {
		errFun("下级条数查询失败", err)
	}
	return count
}
