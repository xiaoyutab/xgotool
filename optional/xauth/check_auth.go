package xauth

import (
	"gitee.com/xiaoyutab/xgotool/xnum"
)

// 检测用户是否存在某权限
//
//	uid		用户ID
//	auth	权限标识
//	typ		权限标识来源
func CheckAuth(uid uint, auth string, typ uint) bool {
	if uid == 0 {
		return false
	}
	if uid == 1 {
		return true
	}
	auths := GetUserAuth(uid, typ)
	if len(auths) == 0 {
		return false
	}
	return xnum.InArray(auth, auths)
}
