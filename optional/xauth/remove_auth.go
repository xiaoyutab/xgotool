package xauth

import (
	"errors"
)

// 移除权限标识
//
//	id	权限ID
func RemoveAuth(id uint) error {
	if _default.DB == nil {
		return errors.New("数据库未连接")
	}
	// 查询子权限是否为空
	var count int64
	err := _default.DB.Table(_default.AuthorityName).Where("fid", id).Count(&count).Error
	if err != nil {
		return err
	}
	if count > 0 {
		return errors.New("抱歉，该权限下的子权限不为空，暂无法删除")
	}
	// 移除权限ID
	err = _default.DB.Table(_default.AuthorityName).Where("id", id).Delete(&Authority{}).Error
	if err != nil {
		return err
	}
	// 移除关联角色
	err = _default.DB.Table(_default.RoleAuthName).Where("auth_id", id).Delete(&RoleAuth{}).Error
	if err != nil {
		return err
	}
	// 清理缓存
	_cache = cache{}
	return nil
}
