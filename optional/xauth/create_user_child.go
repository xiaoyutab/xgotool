package xauth

import (
	"time"
)

// 创建/修改用户关联
//
//	fid	上级用户ID 0-删除此条目记录 1-忽略此条目（超管用户拥有所有用户ID） 其余值则表示判断是否存在上级，如果存在就修改，不存在就替换
//	uid	子用户ID 1-超管用户，此用户不可设置上下级关系，一旦传入此ID则直接返回nil
//	typ	关联权限来源
func CreateUserChild(fid, uid, typ uint) error {
	if fid == 0 || uid == 0 {
		return errFun("用户ID不能为空", nil)
	}
	if typ == 0 {
		typ = 1
	}
	if uid == 1 || fid == 1 {
		return nil
	}
	if _default.DB == nil {
		return errFun("数据库未连接", nil)
	}
	// 清理此处的缓存信息
	_cache = cache{}
	// 获取用户上级ID
	sup_id := GetUserParent(uid, typ)
	if fid == 0 {
		// 如果上级ID为0，则表示要删除该条目信息
		err := _default.DB.Table(_default.UserChildName).Where("user_id", uid).Delete(&UserChild{}).Error
		if err != nil {
			return errFun("用户角色权限删除失败", err)
		}
		return nil
	}
	if sup_id > 0 {
		// 如果存在上级，则此需求就调整为编辑上级用户ID
		err := _default.DB.Table(_default.UserChildName).Where("user_id", uid).Update("superior_id", fid).Error
		if err != nil {
			return errFun("用户角色权限插入失败", err)
		}
		return nil
	}
	err := _default.DB.Table(_default.UserChildName).Create(&UserChild{
		SuperiorId: fid,
		UserId:     uid,
		Type:       typ,
		CreatedAt:  time.Now().Format(time.DateTime),
	}).Error
	if err != nil {
		return errFun("用户角色权限插入失败", err)
	}
	// 添加对应缓存操作
	return nil
}
