package xauth

import (
	"gitee.com/xiaoyutab/xgotool/xnum"
)

// 获取角色权限
//
//	rid	角色ID
func getRoleAuth(rid []uint) []uint {
	if len(rid) == 0 {
		return nil
	}
	_cache.Lock()
	defer _cache.Unlock()
	if v, ok := _cache.RoleAuth[key(rid)]; ok {
		return v
	}
	mod := []uint{}
	// 从数据库中进行查询
	err := _default.DB.Table(_default.RoleAuthName).Select("auth_id").Where("role_id in ?", rid).Find(&mod).Error
	if err != nil {
		errFun("角色权限获取失败", err)
		return nil
	}
	l := len(mod)
	if l == 0 {
		return nil
	}
	// 进行子权限查询【即如果授权主菜单ID的话，则子菜单ID自动授予权限】
	for {
		ms := []uint{}
		err := _default.DB.Table(_default.AuthorityName).Select("id").Where("id in ? or fid in ?", mod).Find(&ms).Error
		if err != nil {
			errFun("角色权限获取失败-子", err)
			break
		}
		if len(ms) == l {
			// 查询前后数据量，即无新增子ID信息
			break
		}
		// 将MS中新增的ID追加到mod权限中去
		for i := 0; i < len(ms); i++ {
			if !xnum.InArray(ms[i], mod) {
				mod = append(mod, ms[i])
			}
		}
	}
	_cache.RoleAuth[key(rid)] = mod
	return mod
}
