package xauth

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/individual/xgorm"
)

// 删除角色信息
//
//	id	待删除的角色ID
func RoleRemove(id uint) error {
	if id == 0 {
		return errors.New("参数错误")
	}
	// 查询关联权限是否为空
	var count int64
	err := xgorm.To(_default.DB).Table(_default.RoleAuthName).Where("role_id", id).Count(&count).Error
	if err != nil {
		return err
	}
	if count > 0 {
		err = xgorm.To(_default.DB).Table(_default.RoleAuthName).Where("role_id", id).Delete(&RoleAuth{}).Error
		if err != nil {
			return err
		}
	}
	// 查询关联用户是否为空
	err = xgorm.To(_default.DB).Table(_default.RoleUserName).Where("role_id", id).Count(&count).Error
	if err != nil {
		return err
	}
	if count > 0 {
		err = xgorm.To(_default.DB).Table(_default.RoleUserName).Where("role_id", id).Delete(&RoleUser{}).Error
		if err != nil {
			return err
		}
	}
	// 删除本角色信息
	err = xgorm.To(_default.DB).Table(_default.RoleName).Where("id", id).Delete(&Role{}).Error
	if err != nil {
		_cache = cache{}
	}
	return err
}
