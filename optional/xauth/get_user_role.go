package xauth

// 获取用户角色列表
//
//	uid	用户ID
func GetUserRole(uid uint) []uint {
	_cache.Lock()
	defer _cache.Unlock()
	if v, ok := _cache.UserRole[uid]; ok {
		return v
	}
	mod := []uint{}
	// 从数据库中进行查询
	err := _default.DB.Table(_default.RoleUserName).Select("role_id").Where("user_id", uid).Find(&mod).Error
	if err != nil {
		errFun("用户角色获取失败", err)
		return nil
	}
	_cache.UserRole[uid] = mod
	return mod
}
