package xauth

import (
	"time"
)

// 替换角色的权限列表
// 此操作会删除掉该角色的之前的权限列表，使用新传入的权限列表进行替换
//
//	uid	用户ID
//	rid	角色ID列表
func ReplaceUserRole(uid uint, rid []uint) error {
	if uid <= 0 {
		return errFun("用户ID不能为空", nil)
	}
	if _default.DB == nil {
		return errFun("数据库未连接", nil)
	}
	// 删除之前的权限列表
	err := _default.DB.Table(_default.RoleUserName).Where("user_id", uid).Delete(&RoleUser{}).Error
	if err != nil {
		return errFun("移除用户之前的角色", err)
	}
	if len(rid) == 0 {
		return nil
	}
	// 添加新的权限列表
	for i := 0; i < len(rid); i++ {
		err := _default.DB.Table(_default.RoleUserName).Create(&RoleUser{
			UserId:    uid,
			RoleId:    rid[i],
			CreatedAt: time.Now().Format(time.DateTime),
		}).Error
		if err != nil {
			return errFun("用户角色权限插入失败", err)
		}
	}
	return nil
}
