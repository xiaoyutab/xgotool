package xauth

// 获取角色列表接口
//
//	offset	跳过条数
//	limit	查询条数
func ListRole(offset, limit int) (int64, []Role, error) {
	if offset < 0 {
		offset = 0
	}
	if limit <= 0 {
		limit = 20
	}
	if _default.DB == nil {
		return 0, nil, errFun("权限模块数据库未连接", nil)
	}
	db := _default.DB.Table(_default.RoleName)
	var count int64
	if err := db.Count(&count).Error; err != nil {
		return 0, nil, errFun("权限条数查询失败", err)
	}
	lis := []Role{}
	if err := db.Order("id ASC").Offset(offset).Limit(limit).Find(&lis).Error; err != nil {
		return 0, nil, errFun("权限列表数据查询失败", err)
	}
	return count, lis, nil
}
