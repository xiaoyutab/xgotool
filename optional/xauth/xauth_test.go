package xauth_test

import (
	"encoding/json"
	"testing"
	"time"

	"gitee.com/xiaoyutab/xgotool/optional/xauth"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func TestMapEmpty(t *testing.T) {
	var a map[string]string
	t.Log(a, len(a))
}

func TestReg(t *testing.T) {
	db, _ := gorm.Open(mysql.Open("admin:admin@tcp(localhost:3306)/gatway?charset=utf8"), &gorm.Config{})
	xauth.Regedit(&xauth.Config{
		DB: db,
	})
	t.Log(xauth.GetUserChild(1, 1, false))
	time.Sleep(time.Second)
	// xauth.ReplaceUserRole(2, []uint{1})
	// t.Log(xauth.GetUserAuth(2, 1))
	// t.Log(xauth.CheckAuth(2, "menu:index", 1))
	// err := xauth.CreateUserChild(1, 1, 3)
	// if err != nil {
	// 	t.Error(err)
	// }
	// // 添加权限
	// err := xauth.CreateAuth(&xauth.Authority{
	// 	Code:  "menu:index",
	// 	Title: "首页",
	// 	Type:  1,
	// })
	// if err != nil {
	// 	t.Error(err)
	// }
	// // 添加角色
	// err = xauth.CreateRole(&xauth.Role{
	// 	Title: "测试角色",
	// })
	// if err != nil {
	// 	t.Error(err)
	// }
	// 添加权限/角色的关联
	// err := xauth.ReplaceRoleAuth(1, []uint{1})
	// if err != nil {
	// 	t.Error(err)
	// }
}

func TestJson(t *testing.T) {
	b, err := json.Marshal(int64(1))
	if err != nil {
		t.Error(err)
	}
	t.Log(b)
}
