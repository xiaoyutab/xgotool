package xauth

// 获取权限列表接口
//
//	typ		权限位置
//	fid		上级条目ID
//	offset	跳过条数
//	limit	查询条数
func ListAuth(typ, fid uint, offset, limit int) (int64, []Authority, error) {
	if offset < 0 {
		offset = 0
	}
	if limit <= 0 {
		limit = 20
	}
	if typ == 0 {
		typ = 1
	}
	if _default.DB == nil {
		return 0, nil, errFun("权限模块数据库未连接", nil)
	}
	db := _default.DB.Table(_default.AuthorityName).Where("type", typ).Where("fid", fid)
	var count int64
	if err := db.Count(&count).Error; err != nil {
		return 0, nil, errFun("权限条数查询失败", err)
	}
	lis := []Authority{}
	if err := db.Order("id ASC").Offset(offset).Limit(limit).Find(&lis).Error; err != nil {
		return 0, nil, errFun("权限列表数据查询失败", err)
	}
	for i := 0; i < len(lis); i++ {
		// 追加是否存在下级数据的菜单
		lis[i].HasChildren = getCountByFid(lis[i].Id) > 0
	}
	return count, lis, nil
}
