package xauth

// 获取我的上级用户
//
//	uid	用户ID
//	typ	授权来源ID
func GetUserParent(uid, typ uint) uint {
	if uid == 0 || uid == 1 {
		return 0
	}
	_cache.Lock()
	defer _cache.Unlock()
	if v, ok := _cache.UserParent[key(uid, typ)]; ok {
		return v
	}
	var mod uint
	if _default.DB == nil {
		return 0
	}
	// 从数据库中读取上级ID
	err := _default.DB.Table(_default.UserChildName).
		Where("user_id", uid).
		Where("type", typ).
		Order("id ASC").
		Limit(1).
		Select("superior_id").
		Find(&mod).
		Error
	if err != nil {
		errFun("用户上级ID获取失败", err)
	}
	_cache.UserParent[key(uid, typ)] = mod
	return mod
}
