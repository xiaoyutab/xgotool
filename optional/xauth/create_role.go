package xauth

import (
	"time"
)

// 创建角色信息
//
//	c	待创建的角色信息
func CreateRole(c *Role) error {
	if _default.DB == nil {
		return errFun("数据库未连接", nil)
	}
	if c.Title == "" {
		return errFun("角色标题不能为空", nil)
	}
	c.UpdatedAt = time.Now().Format(time.DateTime)
	if c.CreatedAt == "" {
		c.CreatedAt = c.UpdatedAt
	}
	err := _default.DB.Save(c).Error
	if err != nil {
		return errFun("角色创建失败", err)
	}
	return nil
}
