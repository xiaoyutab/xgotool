package xcmd_test

import (
	"os"
	"testing"

	"gitee.com/xiaoyutab/xgotool/optional/xcmd"
)

// 测试获取视频的时长信息
func TestVideo2times(t *testing.T) {
	tm, err := xcmd.Video2times("/mnt/c/Users/xiaoyutab/Videos/Captures/test.mp4")
	if err != nil {
		t.Error(err)
	}
	t.Log(tm)
}

// 测试获取视频的宽高信息
func TestVideo2wh(t *testing.T) {
	w, h, err := xcmd.Video2wh("/mnt/c/Users/xiaoyutab/Videos/Captures/test.mp4")
	if err != nil {
		t.Error(err)
	}
	t.Log(w, h)
}

// 测试生成二维码图片
func TestQrencode(t *testing.T) {
	err := xcmd.Qrcode("https://xiaoyutab.cn/wap/", "/mnt/c/Users/xiaoy/Downloads/qrcode.png", &xcmd.QrcodeConfig{
		Level: "H",
	})
	if err != nil {
		t.Error(err)
	}
}

// 生成视频封面图
func TestVideo2image(t *testing.T) {
	err := xcmd.Video2image("/mnt/c/Users/xiaoyutab/Videos/123.mp4", "/mnt/c/Users/xiaoyutab/Videos/123.jpg", 0)
	if err != nil {
		t.Error(err)
	}
	ti, err := xcmd.Video2times("/mnt/c/Users/xiaoyutab/Videos/123.mp3")
	if err != nil {
		t.Error(err)
	}
	t.Log(ti)
}

// 获取图片的平均颜色
func TestImage2averageColor(t *testing.T) {
	col, err := xcmd.Image2averageColor("/home/xiaoyutab/Pictures/123.webp")
	if err != nil {
		t.Error(err)
	}
	t.Logf("#%s", col)
	mt, err := xcmd.Image2mimeType("/home/xiaoyutab/Pictures/123.webp")
	if err != nil {
		t.Error(err)
	}
	t.Log(mt)
}

// 测试获取环境变量
func TestGetEnv(t *testing.T) {
	t.Log(os.LookupEnv("PATH"))
}

// 获取文件类型
func TestFile2type(t *testing.T) {
	inf, err := xcmd.File2type("/mnt/c/Users/xiaoyutab/Downloads/c721682a882d9e4b0013464c5ce1c477.jpg")
	if err != nil {
		t.Error(err)
	}
	t.Log(inf)
}

// 获取目录内文件列表
func TestGetFiles(t *testing.T) {
	inf, err := xcmd.Find("/home/xiaoyutab/Pictures", "-type", "f")
	if err != nil {
		t.Error(err)
	}
	t.Log(inf)
}

func TestImage2wh(t *testing.T) {
	t.Log(xcmd.Image2wh("/home/xiaoyutab/Pictures/Saved Pictures/2023-07-10_153835.png"))
}

// 测试二维码识别
func TestQrcodeIdentification(t *testing.T) {
	m, err := xcmd.QrcodeIdentification("https://qiniu-web-assets.dcloud.net.cn/unidoc/zh/uni-android.png")
	if err != nil {
		t.Error(err)
	}
	t.Log(m)
}

func TestTar(t *testing.T) {
	// _, err := xcmd.Tar("/mnt/c/Users/xiaoy/Downloads/temp.7z", []string{
	// 	"/mnt/c/Users/xiaoy/Downloads/root.db",
	// 	"/mnt/c/Users/xiaoy/Downloads/sql-backups_2024-01-17.db",
	// })
	// if err != nil {
	// 	t.Error(err)
	// }
	t.Log(xcmd.CheckCommand("cat"))
}

func TestSsh(t *testing.T) {
	t.Log(xcmd.SSH(&xcmd.SshConfig{
		Host:     "xiaoyutab.cn",
		IsPK:     true,
		Username: "xiaoyutab",
		Password: "/home/xiaoyutab/.ssh/id_rsa.pub",
	}).Exec("uname -a"))
}

func TestKey(t *testing.T) {
	ck := xcmd.NewPem().Size(2048).Run()
	if ck.Error != nil {
		t.Error(ck.Error)
	}
	t.Log(string(ck.Key), string(ck.Pub), string(ck.Pkcs8))
}

func TestSshPem(t *testing.T) {
	ck := xcmd.NewSsh().Size(2000).Run()
	if ck.Error != nil {
		t.Error(ck.Error)
	}
	t.Log(string(ck.PreRsa), string(ck.PubRsa))
}
