//go:build !windows

package xcmd

import (
	"os"
	"strings"
)

// 推送命令列表信息
//
//	cmd_check	待推入的命令列表信息
func pushConsole(cmd_check string) Cmds {
	// 命令存放位置
	path_count := len(_maps_.findPath)
	tmp := Cmds{Name: cmd_check}
	for j := 0; j < path_count; j++ {
		if _, err := os.Stat(_maps_.findPath[j] + "/" + tmp.Name); err == nil {
			inf := _maps_.findPath[j] + "/" + tmp.Name
			if strings.Contains(inf, "mnt") && (strings.Contains(inf, "windows") || strings.Contains(inf, "Windows") || strings.Contains(inf, "exe")) {
				// 说明找到的此磁盘为挂载到/mnt下的Windows磁盘【WSL环境】
				continue
			}
			tmp.Path = inf
			tmp.Exist = true
			break
		}
	}
	_maps_.Store(tmp.Name, tmp)
	return tmp
}
