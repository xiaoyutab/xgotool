package xcmd

import (
	"errors"
	"os"
)

// 图片格式转换，智能根据后缀推测图片格式
//
//	input	输入图片的路径
//	output	输出图片的路径
func Image2webp(input, output string) error {
	if err := CheckCommand("convert"); err != nil {
		return err
	}
	if _, err := os.Stat(output); err == nil {
		// 如果输出文件存在的话就直接返回成功
		return nil
	}
	if _, err := os.Stat(input); err != nil {
		return errors.New("input 图片未找到: " + input)
	}
	_, err := Exec(GetCommand("convert"), input, output)
	return err
}
