package xcmd

// 文件压缩（简易压缩，判断环境变量中的7z、tar等命令来进行压缩）
//
//	to	目标文件(会自动在后面追加.7z、.tzr.gz等后缀)
//	from	来源文件
func Tar(to string, from []string) (string, error) {
	if CheckCommand("7z") == nil {
		// 存在7z命令
		from = append([]string{"a", to + ".7z"}, from...)
		_, err := Exec(GetCommand("7z"), from...)
		if err != nil {
			return "", err
		}
		return to + ".7z", nil
	} else if CheckCommand("tar") == nil {
		// 存在tar命令
		from = append([]string{"zcvf", to + ".tar.gz"}, from...)
		_, err := Exec(GetCommand("tar"), from...)
		if err != nil {
			return "", err
		}
		return to + ".tar.gz", nil
	}
	return "", CheckCommand("7z", "tar")
}
