package xcmd

import (
	"errors"
	"fmt"
)

// 生成缩略图，画质较低
//
//	input	输入图片的路径
//	output	输出图片的路径
//	size	缩略图大小
func Thumb(input, output string, size uint) error {
	if err := CheckCommand("convert"); err != nil {
		return err
	}
	if size == 0 {
		return errors.New("缩略图大小不能为0")
	}
	_, err := Exec(GetCommand("convert"), "-sample", fmt.Sprintf("%dx%d!", size, size), "-quality", "20", input, output)
	return err
}

// 生成缩略图
//
// 和Thumb的区别为此处使用的-resize参数进行生成，效率稍慢，画质较高
func ThumbResize(input, output string, size uint) error {
	if err := CheckCommand("convert"); err != nil {
		return err
	}
	if size == 0 {
		return errors.New("缩略图大小不能为0")
	}
	_, err := Exec(GetCommand("convert"), "-resize", fmt.Sprintf("%dx%d!", size, size), "-quality", "20", input, output)
	return err
}
