package xcmd

import (
	"errors"
	"os"
	"strconv"
)

// 图像压缩，将图片压缩为指定质量的压缩图，宽高度不定
//
//	input	输入图片的路径
//	output	输出图片的路径
//	quality	压缩等级，0~100
func Quality(input, output string, quality int) error {
	if err := CheckCommand("convert"); err != nil {
		return err
	}
	// 如果传入图像压缩质量为0，则修改为5用以正常压缩
	if quality <= 0 || quality > 100 {
		quality = 10
	}
	if _, err := os.Stat(output); err == nil {
		// 如果输出文件存在的话就直接返回成功
		return nil
	}
	if _, err := os.Stat(input); err != nil {
		return errors.New("input 图片未找到: " + input)
	}
	_, err := Exec(GetCommand("convert"), "-resize", "1000x1000>", "", "-quality", strconv.Itoa(quality), input, output)
	return err
}
