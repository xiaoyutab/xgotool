//go:build windows

package xcmd

import (
	"os"
)

// 推送命令列表信息
//
//	cmd_check	待推入的命令列表信息
func pushConsole(cmd_check string) Cmds {
	// 命令存放位置
	path_count := len(_maps_.findPath)
	tmp := Cmds{Name: cmd_check}
	for j := 0; j < path_count; j++ {
		// 如果是Windows系统
		exts := []string{"exe", "bat", "ps1", "com", "vbs"}
		for k := 0; k < len(exts); k++ {
			if _, err := os.Stat(_maps_.findPath[j] + "/" + tmp.Name + "." + exts[k]); err == nil {
				tmp.Path = _maps_.findPath[j] + "/" + tmp.Name + "." + exts[k]
				tmp.Exist = true
				break
			}
		}
	}
	_maps_.Store(tmp.Name, tmp)
	return tmp
}
