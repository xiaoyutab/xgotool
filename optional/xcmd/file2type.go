package xcmd

import (
	"errors"
	"strings"
)

// 获取文件类型
//
//	path	文件路径
//
//	uint8返回类型确定：
//	0-未知 1-目录 2-Linux可执行程序 3-Linux下超链接 4-Linux Socket文件 5-Windows 可执行程序
//	10-JPEG图片文件 11-PNG图片文件 12-WEBP格式图片 13-GIF格式图片 14-BMP位图 15-TIFF格式图片 19-未知格式图片文件
//	20-ASCII编码的文本文件 21-UTF-8编码的文本文件 29-未知格式文本文件
//	31-7Z压缩文件 32-ZIP压缩文件 39-未知压缩文件
//	41-PDF文档 42-Word文档 43-Excel文档 44-PPT文档
//	51-WMV视频 52-MP4视频 53-AVI视频 54-FLV视频 55-MOV视频 59-未知格式视频
//	60-MP3音频文件 61-AAC音频文件 62-AIFF音频文件 63-FLAC音频文件 69-未知格式音频文件
func File2type(path string) (uint8, error) {
	if err := CheckCommand("file"); err != nil {
		return 0, err
	}
	cmd, err := Exec(GetCommand("file"), path)
	if err != nil {
		return 0, err
	}
	cmd = strings.TrimSpace(strings.Split(cmd, ":")[1])
	if cmd == "directory" {
		return 1, nil
	}
	// 常见文件的类型
	headers := map[string]uint8{
		"ELF":                    2,  // Linux可执行程序
		"symbolic link":          3,  // Linux超链接
		"socket":                 4,  // Linux Socket文件
		"PE32":                   5,  // Windows 可执行程序
		"JPEG":                   10, // JPEG图片文件
		"PNG":                    11, // PNG图片文件
		"GIF":                    13, // GIF格式图片
		"TIFF":                   15, // TIFF格式图片
		"ASCII":                  20, // ASCII编码的文本文件
		"UTF-8":                  21, // UTF-8编码的文本文件
		"7-zip":                  31, // 7Z压缩文件
		"Zip":                    32, // ZIP压缩文件
		"PDF":                    41, // PDF文档
		"Microsoft Word":         42, // Word文档
		"Microsoft Excel":        43, // Excel文档
		"Microsoft PowerPoint":   44, // PPT文档
		"Microsoft ASF":          51, // WMV视频
		"MP4 Base":               52, // MP4视频
		"AVI":                    53, // AVI视频
		"Macromedia Flash Video": 54, // FLV视频
		"Apple QuickTime":        55, // MOV视频
		"Matroska data":          59, // MKV等格式视频
		"Audio file with ID3":    60, // MP3音频文件
		"MPEG ADTS":              61, // AAC音频文件
		"IFF data":               62, // AIFF音频文件
		"FLAC audio":             63, // FLAC音频文件
		"Ogg data":               69, // OGG音频文件
		"WAVE audio":             69, // WAV音频文件
		"ISO Media":              69, // M4A音频文件
	}
	for i, v := range headers {
		if len(cmd) > len(i) && cmd[0:len(i)] == i {
			return v, nil
		}
	}
	// 不常见文件类型【依靠字符串搜索来确定是文本/图片等类型文件】
	headers = map[string]uint8{
		"text":        29, //文本文件
		"image":       19, // 图片文件
		"archive":     39, // 压缩文件
		"Web/P image": 12, // WEBP格式图片
		"PC bitmap":   14, // BMP位图
	}
	for i, v := range headers {
		if strings.Contains(cmd, i) {
			return v, nil
		}
	}
	return 0, nil
}

// 通过文件名后缀进行匹配文件类型
func File2typeByExt(path string) (uint8, error) {
	tmp_file := strings.ToLower(path)
	ext_type := map[uint8][]string{
		10: {".jpg", ".jpeg"},
		11: {".png"},
		12: {".webp"},
		13: {".gif"},
		14: {".bmp"},
		15: {".tif", ".tiff"},
		19: {".avif"},
		31: {".7z"},
		32: {".zip", ".jar"},
		41: {".pdf"},
		42: {".doc", ".docx"},
		43: {".xls", ".xlsx"},
		44: {".ppt", ".pptx"},
		51: {".wmv"},
		52: {".mp4", ".m4v"},
		53: {".avi"},
		54: {".flv"},
		55: {".mov"},
		60: {".mp3"},
		61: {".aac"},
		62: {".aif", ".aiff"},
		63: {".flac"},
		69: {".ogg", ".wav"},
	}
	for k, v := range ext_type {
		for _, ext := range v {
			if strings.HasSuffix(tmp_file, ext) {
				return k, nil
			}
		}
	}
	return 0, errors.New("暂无法通过后缀匹配文件")
}
