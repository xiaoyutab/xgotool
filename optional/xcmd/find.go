package xcmd

import (
	"errors"
	"os"
	"strings"
)

// 获取指定目录内的文件列表
func Find(path string, arg ...string) ([]string, error) {
	if err := CheckCommand("find"); err != nil {
		return nil, err
	}
	if path == "" {
		return nil, errors.New("目录不允许为空")
	}
	if path[len(path)-1] != '/' {
		path += "/"
	}
	if _, err := os.Stat(path); err != nil {
		return nil, err
	}
	arg = append([]string{path}, arg...)
	inf, err := Exec(GetCommand("find"), arg...)
	if err != nil {
		return nil, err
	}
	rets := []string{}
	pam := strings.Split(inf, "\n")
	for i := 0; i < len(pam); i++ {
		if pam[i] == path || pam[i] == "" {
			continue
		}
		rets = append(rets, pam[i][len(path):])
	}
	return rets, nil
}
