package xcmd

import (
	"fmt"
	"os"

	"gitee.com/xiaoyutab/xgotool/xstring"
)

// 证书相关结构[RSA加密用证书]
type Pem struct {
	Key   []byte // 证书内容Key
	Pub   []byte // 证书公钥内容
	Pkcs8 []byte // Pkcs8私钥内容【无加密】
	size  int    // 证书强度大小
	Error error  // 错误信息，若此值不为空就直接终止，不再往下走
}

// 新建Pem结构体
func NewPem() *Pem {
	return &Pem{
		size: 2048,
	}
}

// 设置证书强度大小，默认为2048
//
//	size	强度大小
func (p *Pem) Size(size int) *Pem {
	p.size = size
	return p
}

// 进行密钥生成
func (p *Pem) Run() *Pem {
	p.Error = CheckCommand("openssl")
	if p.Error != nil {
		return p
	}
	pre := xstring.RandomNo("key_", 3)
	// 生成key密钥 RSA PRIVATE KEY
	_, err := Exec(GetCommand("openssl"), "genrsa", "-out", "/tmp/"+pre+".key", fmt.Sprintf("%d", p.size))
	if err != nil {
		p.Error = err
		return p
	}
	defer os.Remove("/tmp/" + pre + ".key")
	if f, err := os.ReadFile("/tmp/" + pre + ".key"); err == nil {
		p.Key = f
	} else {
		p.Error = err
	}
	if p.Error != nil {
		return p
	}
	// 生成RSA公钥PUBLIC KEY
	_, err = Exec(GetCommand("openssl"), "rsa", "-in", "/tmp/"+pre+".key", "-pubout", "-out", "/tmp/"+pre+"_public.key")
	if err != nil {
		p.Error = err
		return p
	}
	defer os.Remove("/tmp/" + pre + "_public.key")
	if f, err := os.ReadFile("/tmp/" + pre + "_public.key"); err == nil {
		p.Pub = f
	} else {
		p.Error = err
	}
	if p.Error != nil {
		return p
	}
	// 私钥文件转PKCS8证书
	_, err = Exec(GetCommand("openssl"), "pkcs8", "-topk8", "-in", "/tmp/"+pre+".key", "-nocrypt", "-out", "/tmp/"+pre+"_pkcs8.key")
	if err != nil {
		p.Error = err
		return p
	}
	defer os.Remove("/tmp/" + pre + "_pkcs8.key")
	if f, err := os.ReadFile("/tmp/" + pre + "_pkcs8.key"); err == nil {
		p.Pkcs8 = f
	} else {
		p.Error = err
	}
	if p.Error != nil {
		return p
	}
	return p
}
