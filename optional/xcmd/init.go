// 运行CMD命令
// 此处的命令需要在Linux下执行，请注意
package xcmd

import (
	"errors"
	"os"
	"strings"
	"sync"
)

// 命令结构体【命令不再强制存在】
type Cmds struct {
	Name  string // 命令名称
	Path  string // 命令地址
	Exist bool   // 是否存在
}

type syncMp struct {
	findPath []string // 查找命令的地址路径
	sync.Map
	sync.Mutex
}

var _maps_ syncMp = syncMp{}

// 检测当前服务中的命令是否存在[基础命令检测]
func init() {
	_maps_.findPath = strings.Split(os.Getenv("PATH"), ":")
}

// 获取指定命令的相关信息
//
//	name	待获取详情的命令列表
func GetCommand(name string) string {
	return GetCommandInfo(name).Path
}

// 获取指定命令的相关信息
//
//	name	待获取详情的命令列表
func GetCommandInfo(name string) Cmds {
	defer func() {
		recover()
	}()
	if v, ok := _maps_.Load(name); ok {
		return v.(Cmds)
	}
	return pushConsole(name)
}

// 检测命令信息
//
//	name	待检测命令
func CheckCommand(name ...string) error {
	ins := map[string]string{
		// 软件映射  软件命令：所属软件名
		"identify": "imagemagick",
		"7z":       "p7zip",
		"convert":  "imagemagick",
		"ffmpeg":   "ffmpeg",
		"qrencode": "qrencode",
	}
	for i := 0; i < len(name); i++ {
		tmp := GetCommandInfo(name[i])
		if !tmp.Exist {
			ext := ""
			if v, ok := ins[name[i]]; ok {
				ext += "或执行 sudo apt-get install " + v + " 进行安装"
			}
			return errors.New("命令 " + name[i] + " 不存在，请确认您的操作系统下存在此程序" + ext)
		}
	}
	return nil
}
