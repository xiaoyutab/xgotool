package xcmd

import (
	"errors"
	"fmt"
	"os"

	"gitee.com/xiaoyutab/xgotool/xstring"
)

// SSH登录用的密钥对信息
type SshKey struct {
	PreRsa []byte // Rsa签名密钥内容-私钥
	PubRsa []byte // RSA签名密钥内容-公钥
	size   int    // 密钥长度
	Error  error  // 是否存在错误
	name   string // 追加姓名信息，默认为<uuid>@localhost
}

// 创建Ssh登录密钥对
func NewSsh() *SshKey {
	return &SshKey{
		name: xstring.UUID() + "@localhost",
		size: 4096,
	}
}

// 设置密钥的长度
//
//	size	强度数值
func (p *SshKey) Size(size int) *SshKey {
	p.size = size
	return p
}

// 设置生成的账户姓名信息
func (p *SshKey) Name(name string) *SshKey {
	p.name = name
	return p
}

// 进行SSH证书生成操作
func (p *SshKey) Run() *SshKey {
	p.Error = CheckCommand("ssh-keygen")
	if p.Error != nil {
		return p
	}
	if p.size < 1024 {
		p.Error = errors.New("证书最小密钥长度需要为1024")
		return p
	}
	pre := xstring.RandomNo("key_", 3)
	// 生成key密钥
	_, err := Exec(GetCommand("ssh-keygen"), "-t", "rsa", "-b", fmt.Sprintf("%d", p.size), "-f", "/tmp/"+pre, "-N", "", "-C", p.name)
	if err != nil {
		p.Error = err
		return p
	}
	defer os.Remove("/tmp/" + pre)
	defer os.Remove("/tmp/" + pre + ".pub")
	if f, err := os.ReadFile("/tmp/" + pre); err == nil {
		p.PreRsa = f
	} else {
		p.Error = err
	}
	if p.Error != nil {
		return p
	}
	if f, err := os.ReadFile("/tmp/" + pre + ".pub"); err == nil {
		p.PubRsa = f
	} else {
		p.Error = err
	}
	if p.Error != nil {
		return p
	}
	// 生成key公钥
	return p
}
