package xcmd

import (
	"io"
	"os/exec"
)

// Exec 执行Shell命令并等待结果输出【避免僵尸进程】
//
// 如果要使用管道进行输出，则需要使用bash进行嵌套，如：
//
// xgotool.Exec("bash", "-c", "ps aux | grep go")
//
//	name	命令参数
//	arg...	参数列表
func Exec(name string, arg ...string) (string, error) {
	cmd := exec.Command(name, arg...)
	//获取输出对象，可以从该对象中读取输出结果
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return "", err
	}
	// 保证关闭输出流
	defer stdout.Close()
	// 运行命令
	if err := cmd.Start(); err != nil {
		return "", err
	}
	defer cmd.Wait()
	opBytes, err := io.ReadAll(stdout)
	if err != nil {
		return "", err
	}
	return string(opBytes), nil
}
