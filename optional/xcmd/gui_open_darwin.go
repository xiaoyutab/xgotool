//go:build darwin

package xcmd

// GUI环境下打开连接
// 此处使用Linux环境下的方式来调用，其他环境使用拆分编译的形式来实现
func GuiOpen(url string) error {
	if len(url) < 4 || url[:4] != "http" {
		url = "http://" + url
	}
	_, err := Exec("open", url)
	if err != nil {
		return err
	}
	return nil
}
