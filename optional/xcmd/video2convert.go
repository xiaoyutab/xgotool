package xcmd

import (
	"errors"
	"fmt"
	"os"
	"strings"
)

// 视频获取封面图【获取时长为00:00:00秒的图片】
func Video2convert(input, output string) error {
	return Video2image(input, output, 0)
}

// 视频获取指定时长位置的图片
//
//	input	输入视频位置
//	output	输出图片位置
//	times	视频时长，单位：毫秒
func Video2image(input, output string, times uint) error {
	if err := CheckCommand("ffmpeg"); err != nil {
		return err
	}
	if input == "" {
		return errors.New("文件路径不能为空")
	}
	if output == "" {
		return errors.New("输出文件路径不能为空")
	}
	if _, err := os.Stat(output); err == nil {
		return errors.New("输出文件已存在")
	}
	if _, err := os.Stat(input); err != nil {
		return errors.New("输入文件不存在/无权限")
	}
	output = strings.ReplaceAll(output, " ", "\\ ")
	input = strings.ReplaceAll(input, " ", "\\ ")
	time_exts := fmt.Sprintf("%02d:%02d:%02d.%03d", times/1000/60/60, times/1000/60%60, times/1000%60, times%1000)
	_, err := Exec(GetCommand("ffmpeg"), "-ss", time_exts, "-i", input, "-f", "image2", "-frames:v", "1", "-y", output)
	if err != nil {
		return err
	}
	// 生成的位置
	if _, err := os.Stat(output); err != nil {
		return errors.New("生成失败")
	}
	return nil
}
