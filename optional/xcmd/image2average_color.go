package xcmd

import (
	"errors"
	"fmt"
	"os"
	"strings"
)

// 获取图片的平均颜色【值为16进制的颜色色值（6位长度无前缀#，如：C6BEB2）】
//
//	input	输入的图片文件
func Image2averageColor(input string) (string, error) {
	if err := CheckCommand("bash", "identify", "awk", "head", "grep"); err != nil {
		return "", err
	}
	// 输入路径为空
	if input == "" {
		return "", errors.New("图片输入路径不能为空")
	}
	// 输入文件不存在
	if _, err := os.Stat(input); err != nil {
		return "", err
	}
	input = strings.ReplaceAll(input, " ", "\\ ")
	// 命令拼接
	cmds := fmt.Sprintf(`%s -verbose %s 2>&1 | %s mean | %s -3 | %s '{match($0," ");print $2}' | %s '{printf("%%02X",$1)}'`,
		GetCommand("identify"), input, GetCommand("grep"), GetCommand("head"), GetCommand("awk"), GetCommand("awk"))
	// 命令执行
	out, err := Exec(GetCommand("bash"), "-c", cmds)
	if err != nil {
		return "", err
	}
	return out, nil
}
