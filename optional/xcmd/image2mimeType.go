package xcmd

import (
	"errors"
	"fmt"
	"os"
	"strings"
)

// 获取图片的Mime Type值，如：image/webp
//
//	input	输入的图片文件
func Image2mimeType(input string) (string, error) {
	if err := CheckCommand("awk", "sed", "grep", "bash", "identify"); err != nil {
		return "", err
	}
	// 输入路径为空
	if input == "" {
		return "", errors.New("图片输入路径不能为空")
	}
	// 输入文件不存在
	if _, err := os.Stat(input); err != nil {
		return "", err
	}
	input = strings.ReplaceAll(input, " ", "\\ ")
	// 命令拼接
	cmds := fmt.Sprintf(`%s -verbose %s 2>&1 | %s "Mime type" | %s -F ':' '{print $2}' | %s 's/\ //'`,
		GetCommand("identify"), input, GetCommand("grep"), GetCommand("awk"), GetCommand("sed"))
	// 命令执行
	out, err := Exec(GetCommand("bash"), "-c", cmds)
	if err != nil {
		return "", err
	}
	return out, nil
}
