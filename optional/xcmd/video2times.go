package xcmd

import (
	"errors"
	"os"
	"strings"

	"gitee.com/xiaoyutab/xgotool/xstring"
)

// 根据音频获取音频时长，返回单位：毫秒
// 内部会调用Video2times命令中的ffmpeg来获取音频时长
//
//	filepath	视频地址[绝对路径]
func Audio2times(filepath string) (uint, error) {
	return Video2times(filepath)
}

// 根据视频获取视频时长，返回单位：毫秒
//
//	filepath	视频地址[绝对路径]
func Video2times(filepath string) (uint, error) {
	if err := CheckCommand("bash", "sed", "ffmpeg", "cut", "grep"); err != nil {
		return 0, err
	}
	if filepath == "" {
		return 0, errors.New("文件路径不能为空")
	}
	if _, err := os.Stat(filepath); err != nil {
		// 文件不存在
		return 0, errors.New("文件不存在")
	}
	filepath = strings.ReplaceAll(filepath, " ", "\\ ")
	out, err := Exec(GetCommand("bash"), "-c", GetCommand("ffmpeg")+" -i "+filepath+" 2>&1 | "+
		GetCommand("grep")+" Duration | "+
		GetCommand("cut")+" -d ' ' -f 4 | "+
		GetCommand("sed")+" s/,//")
	if err != nil {
		return 0, err
	}
	if out == "" {
		// 无输出信息
		return 0, errors.New("命令输出信息为空")
	}
	// 00:01:51.92
	ms := strings.Split(out, ".")[len(strings.Split(out, "."))-1] // 获取毫秒 [92]
	times := strings.Split(out, ".")[0]                           // 时间 [00:01:51]
	hour := strings.Split(times, ":")[0]                          // 时 [00]
	min := strings.Split(times, ":")[1]                           // 分 [01]
	s := strings.Split(times, ":")[2]                             // 秒 [51]

	return xstring.ToUint(xstring.Pad(strings.TrimSpace(ms), 3, "000", xstring.PAD_RIGHT)) + // 毫秒
			xstring.ToUint(s)*1000 + // 秒
			xstring.ToUint(min)*60*1000 + // 分
			xstring.ToUint(hour)*60*60*1000, // 时
		nil
}
