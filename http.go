package xgotool

import "gitee.com/xiaoyutab/xgotool/https"

// GET请求
//
//	url		请求地址
//	param	请求参数
//	header	请求头，自动追加accept、Content-Type等属性
func Get(url string, param, header map[string]string) *https.CURL {
	return https.New(url).Param(param).Header(header).Get()
}

// Post请求
//
//	url		请求地址
//	param	请求参数
//	header	请求头，自动追加accept、Content-Type等属性
func Post(url string, param, header map[string]string) *https.CURL {
	return https.New(url).Param(param).Header(header).Post()
}

// Post请求[此请求会以json形式发送数据]
//
//	url		请求地址
//	param	请求参数
//	header	请求头，自动追加accept、Content-Type等属性
func PostJson(url string, param map[string]any, header map[string]string) *https.CURL {
	return https.New(url).ParamJson(param).Header(header).PostJson()
}
