package xstring

import (
	"encoding/json"
	"strings"
)

// json.Number转int类型
// 因此类型为json转为int64以后再次转为其他数值类型，可能会存在数值溢出的问题
//
//	c	待转换的类型
func JInt(c json.Number) int {
	return int(JInt64(c))
}

// json.Number转int8类型
// 因此类型为json转为int64以后再次转为其他数值类型，可能会存在数值溢出的问题
//
//	c	待转换的类型
func JInt8(c json.Number) int8 {
	return int8(JInt64(c))
}

// json.Number转int16类型
// 因此类型为json转为int64以后再次转为其他数值类型，可能会存在数值溢出的问题
//
//	c	待转换的类型
func JInt16(c json.Number) int16 {
	return int16(JInt64(c))
}

// json.Number转uint类型
// 因此类型为json转为int64以后再次转为其他数值类型，可能会存在数值溢出的问题
//
//	c	待转换的类型
func JUint(c json.Number) uint {
	return uint(JInt64(c))
}

// json.Number转uint8类型
// 因此类型为json转为int64以后再次转为其他数值类型，可能会存在数值溢出的问题
//
//	c	待转换的类型
func JUint8(c json.Number) uint8 {
	return uint8(JInt64(c))
}

// json.Number转uint16类型
// 因此类型为json转为int64以后再次转为其他数值类型，可能会存在数值溢出的问题
//
//	c	待转换的类型
func JUint16(c json.Number) uint16 {
	return uint16(JInt64(c))
}

// json.
// 因此类型为json转为int64以后再次转为其他数值类型，可能会存在数值溢出的问题
//
//	c	待转换的类型
func JInt64(c json.Number) int64 {
	// 验证如果为空字符串就直接返回0
	if c.String() == "" {
		return 0
	}
	// 如果是浮点数的话，就直接转换为浮点数再转为整数
	if strings.Contains(c.String(), ".") {
		num, err := c.Float64()
		if err != nil {
			if _default.DebugLog != nil {
				_default.DebugLog("json类型转换错误", err)
			}
		}
		return int64(num)
	}
	num, err := c.Int64()
	if err != nil {
		if _default.DebugLog != nil {
			_default.DebugLog("json类型转换错误", err)
		}
		return 0
	}
	return num
}

// json.Number转uint64类型
// 因此类型为json转为int64以后再次转为其他数值类型，可能会存在数值溢出的问题
//
//	c	待转换的类型
func JUint64(c json.Number) uint64 {
	return uint64(JInt64(c))
}

// json.Number转float32类型
// 因此类型为json转为float64以后再次转为其他数值类型，可能会存在数值溢出的问题
//
//	c	待转换的类型
func JFloat32(c json.Number) float32 {
	return float32(JFloat64(c))
}

// json.Number转float64类型
// 因此类型为json转为float64以后再次转为其他数值类型，可能会存在数值溢出的问题
//
//	c	待转换的类型
func JFloat64(c json.Number) float64 {
	num, err := c.Float64()
	if err != nil {
		if _default.DebugLog != nil {
			_default.DebugLog("json类型转换错误", err)
		}
		return 0
	}
	return num
}
