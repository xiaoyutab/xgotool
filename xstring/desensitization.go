package xstring

import "strings"

// 对象号码脱敏操作

// 手机号脱敏
//
//	t	待脱敏的手机号
func Phone(t string) string {
	return Format(t, "000 **** 0000")
}

// 手机号格式化
//
//	t	待格式化的手机号
func PhoneFormat(t string) string {
	return Format(t, "000 0000 0000")
}

// 银行卡号脱敏
//
//	t	待脱敏的银行卡号
func BankCard(t string) string {
	// 获取*数量
	lens := len(t) / 4
	f := []string{}
	if len(t)%4 != 0 {
		f = append(f, "****"[0:len(t)%4])
	}
	for i := 0; i < lens-1; i++ {
		f = append(f, "****")
	}
	f = append(f, "0000")
	return Format(t, strings.Join(f, " "))
}

// 银行卡号格式化
//
//	t	待格式化的银行卡号
func BankCardFormat(t string) string {
	if len(t) < 4 {
		return t
	}
	// 获取*数量
	lens := len(t) / 4
	f := []string{}
	if len(t)%4 != 0 {
		f = append(f, "0000"[0:len(t)%4])
	}
	for i := 0; i < lens-1; i++ {
		f = append(f, "0000")
	}
	f = append(f, "0000")
	return Format(t, strings.Join(f, " "))
}

// 身份证号脱敏
//
//	t	脱敏前的身份证号
func CardNo(t string) string {
	return Format(t, "000000 ******** 0000")
}

// 身份证号格式化
//
//	t	格式化前的身份证号
func CardNoFormat(t string) string {
	return Format(t, "000000 00000000 0000")
}
