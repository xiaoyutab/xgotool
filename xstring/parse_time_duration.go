package xstring

import (
	"time"
)

// ParseTimeDurations 解析时间字符串(支持多个时间拼接，如：1h30s)，支持以下单位：
// m, min, minutes
// h, hour, hours
// d, day, days
// w, week, weeks
// M, month, months
// y, year, years
// ms
// us
// ns
// s (默认单位)
//
//	str 时间字符串
func ParseTimeDurations(str string) time.Duration {
	spls := SplitUnit(str, false)
	num := time.Duration(0)
	for i := 0; i < len(spls); i++ {
		switch spls[i].Unit {
		case "m", "min", "minutes", "分钟", "分":
			num += time.Duration(spls[i].Num) * time.Minute
		case "h", "hour", "hours", "小时", "时":
			num += time.Duration(spls[i].Num) * time.Hour
		case "d", "day", "days", "天":
			num += time.Duration(spls[i].Num) * time.Hour * 24
		case "w", "week", "weeks", "周":
			num += time.Duration(spls[i].Num) * time.Hour * 24 * 7
		case "M", "month", "months", "月":
			num += time.Duration(spls[i].Num) * time.Hour * 24 * 30
		case "y", "year", "years", "年":
			num += time.Duration(spls[i].Num) * time.Hour * 24 * 365
		case "ms", "毫秒":
			num += time.Duration(spls[i].Num) * time.Millisecond
		case "us", "微秒":
			num += time.Duration(spls[i].Num) * time.Microsecond
		case "ns", "纳秒":
			num += time.Duration(spls[i].Num) * time.Nanosecond
		case "", "s", "秒":
			num += time.Duration(spls[i].Num) * time.Second
		}
	}
	return num
}
