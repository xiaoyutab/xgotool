package xstring

import (
	"math/rand"
	"strings"
	"time"
)

// 获取随机数
//
//	l	int	获取随机数长度
//	arg	string	可选参数，随机数的发生范围【只有第一个可选参数生效】
func Random(l int, arg ...string) string {
	str := "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	if len(arg) > 0 {
		str = arg[0]
	}
	bytes := []byte(str)
	result := []byte{}
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < l; i++ {
		result = append(result, bytes[r.Intn(len(bytes))])
	}
	return string(result)
}

// 随机密码生成
//
//	l	密码长度
//	num	是否含有数字
//	up	是否含有大写字母
//	dow	是否含有小写字母
//	sym	是否含有特殊符号
func RandomPass(l int, num, up, dow, sym bool) string {
	str := ""
	if num {
		str += "0123456789"
	}
	if up {
		str += "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	}
	if dow {
		str += "abcdefghijklmnopqrstuvwxyz"
	}
	if sym {
		str += "~!@#$%^&*()-+_=,."
	}
	return Random(l, str)
}

// 生成`年月日时分秒毫秒`的流水号
// 返回长度：14+len(cp)+l
//
//	cp	流水号前缀
//	l	毫秒位数，3-毫秒 6-微秒 9-纳秒，取值：1~9，不在此范围内则转化为3
func RandomNo(cp string, l int) string {
	if l < 1 || l > 9 {
		l = 3
	}
	s := time.Now().Format("20060102150405." + Pad("0", l, "0", PAD_RIGHT))
	return strings.ReplaceAll(cp+s, ".", "")
}
