package xstring_test

import (
	"encoding/json"
	"strings"
	"testing"
	"time"

	"gitee.com/xiaoyutab/xgotool/xstring"
)

func TestSplitUnit(t *testing.T) {
	b, _ := json.Marshal(xstring.SplitUnit("30分钟60时五零分", true))
	t.Log(string(b))
}

func TestAnyIsInteger(t *testing.T) {
	t.Log(xstring.IsInteger(1.01))
	// t.Log(xstring.JFloat64(json.Number("1.0")))
}

func TestPhoneFormat(t *testing.T) {
	t.Log(xstring.Format("410926199412233610", "000000 0000 00 00 0000"))
}

func TestBankCard(t *testing.T) {
	t.Log(xstring.CardNo("410926199512233618"))
}

func TestDes(t *testing.T) {
	byt := "sBERvxRdTAzjf1BUGoc6VZE_J7ZMN1BHIfqy4sC2Sl1mpRNcI5OEuCbVYi7l3w8+boOWS9cgnfI="
	byt = strings.ReplaceAll(byt, "_", "/")
	byt = xstring.UnBase64(byt)
	inf, _ := xstring.UnDes([]byte(byt), []byte("11556688"))
	t.Log(string(inf))
}

func TestCheck(t *testing.T) {
	t.Log(xstring.CheckDateTime("2023-09-20 13:44:04"))
	t.Log(xstring.CheckPhone("+8613345678911", true))
	t.Log(xstring.CheckPassword("Asad1234", true, true, true, true))
	t.Log(xstring.CheckUsername("1111"))
}

func TestCamelCase(t *testing.T) {
	t.Log(xstring.CamelCase("logs_asd_123_c"))
}

func TestMD5(t *testing.T) {
	t.Logf("%b", 10)
	t.Log(xstring.MD5("123456"))
	t.Logf("%X", xstring.Hex2Bin(xstring.MD5("123456")))
}

func TestRandomNo(t *testing.T) {
	t.Log(xstring.RandomNo("CP", 9))
}

func TestCheckVersion(t *testing.T) {
	t.Log(xstring.CheckVersion("v1.0.4", "1.0.4.4"))
}

func TestMac(t *testing.T) {
	t.Log(xstring.Mac())
}

func TestAes(t *testing.T) {
	// 加密
	aes_info, err := xstring.Des([]byte("asdeasafew"), []byte("QAZWSXED"))
	if err != nil {
		t.Error(err)
	}
	t.Log(aes_info)
	// 转成json进行传输
	j, err := json.Marshal(aes_info)
	if err != nil {
		t.Error(err)
	}
	t.Log(string(j))
	// json解析
	a := []byte{}
	json.Unmarshal(j, &a)
	// 解密
	info, err := xstring.UnDes(a, []byte("QAZWSXED"))
	if err != nil {
		t.Error(err)
	}
	t.Log(string(info))
}

func TestToTime(t *testing.T) {
	t.Log(xstring.ToTime("2024-08-09  10:18:10").Format(time.DateTime))
}

func TestJwts(t *testing.T) {
	token := "eyJhbGciOiJSUzI1NiIsImtpZCI6IjcyRENCNzE2RTE3NzAzMjQxQjM5QzU4NTlCQjNDNDI5IiwidHlwIjoiYXQrand0In0.eyJuYmYiOjE3MDE3NjkyMzAsImV4cCI6MTcwMjYzMzIzMCwiaXNzIjoiaHR0cHM6Ly9sb2dpbi5sYXd4cC5jb20iLCJjbGllbnRfaWQiOiJhcHAiLCJzdWIiOiIyMTAzMzU4NjE1MjEiLCJhdXRoX3RpbWUiOjE3MDE3NjkyMzAsImlkcCI6ImxvY2FsIiwiVXNlcklkIjoiMjEwMzM1ODYxNSIsIm5hbWUiOiJTd2FnZ2VyVG9rZW5fU2NvcmUyMV8xIiwiZ2l2ZW5fbmFtZSI6IuWktOWDjyIsImVtYWlsIjoiamJxYXBpQGxhd3hwLmNvbSIsImp0aSI6IjNBNjQwQkZEMDhCOUZGRjZGOENCMURCMjNCM0RFQzkxIiwiaWF0IjoxNzAxNzY5MjMwLCJzY29wZSI6WyJvcGVuaWQiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiXSwiYW1yIjpbImN1c3RvbSJdfQ."
	type us struct {
		UserId json.Number `json:"UserId"`
	}
	ut := us{}
	err := xstring.UnJwtNoKey(token, &ut)
	if err != nil {
		t.Error(err)
	}
	t.Log(ut)
}

func TestJsonNumberFloat(t *testing.T) {
	t.Log(xstring.JInt64(json.Number("4.076e+07")))
}

func TestSplitInt(t *testing.T) {
	t.Log(xstring.SplitFloat64("1, 2,3,4,,5", ","))
}

func TestNumber(t *testing.T) {
	b := 2.01
	t.Log(xstring.AString(b))
}

func TestTar(t *testing.T) {
	byt, err := xstring.Gz([]byte("ABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABC"))
	if err != nil {
		t.Error(err)
	}
	t.Logf("%x", byt)
	t.Log(xstring.BaseByte64(byt))
	// ntr, err := xstring.UnGz(xstring.Hex2Bin("1f8b08000000000000ff7274721e45a368148d22b211200000ffff7855852c44040000"))
	// if err != nil {
	// 	t.Error(err)
	// }
	// t.Log(string(ntr))
}

func TestURLFormat(t *testing.T) {
	str := xstring.UrlFormat(`https://so.toutiao.com//search//////?dvpf=pc&source=input&keyword=\/`)
	t.Log(str)
}

func TestCehckUsername(t *testing.T) {
	t.Log(xstring.CheckUsername("asd-_-"))
}

func TestAny2jsons(t *testing.T) {
	// 配置项信息
	type Configs struct {
		WebPort string `json:"web_port"` // WEB端口
		RootDNS string `json:"root_dns"` // root库的DNS连接
		Nsqs    struct {
			Status      bool   `json:"status"`       // 是否启用队列
			SetDomain   string `json:"set_domain"`   // 设置队列的地址
			SetTopic    string `json:"set_topic"`    // 设置Topic
			GetDomain   string `json:"get_domain"`   // 检测队列的地址
			GetTopic    string `json:"get_topic"`    // 监听Topic
			GetChannel  string `json:"get_channel"`  // 监听Channel
			MaxFlight   int64  `json:"max_flight"`   // 最大并行队列数量
			MaxAttempts int64  `json:"max_attempts"` // 最大错误重试次数
		} `json:"nsqs"` // NSQ队列配置
	}
	conf := Configs{}
	err := xstring.Any2Jsons([][]byte{
		[]byte(`{"web_port": ":8000"}`),
		[]byte(`{"root_dns": "./root.db","web_port": ":8080"}`),
		[]byte(`{"root_dns": "./root.sqlite","web_port": ":8081", "nsqs": {"status": true, "set_domain": "127.0.0.1:5590"}}`),
		[]byte(`{"root_dns": "./root.sqlite","web_port": ":8081", "nsqs": {"status": false, "set_domain": "127.0.0.1:5591"}}`),
	}, &conf)
	if err != nil {
		t.Error(err)
	}
	b, _ := json.Marshal(&conf)
	t.Log(string(b))
}

func TestXxx(t *testing.T) {
	ins := []map[string]any{{
		"a":     "10000000",
		"cc":    2,
		"tm":    "this is test applications",
		"times": "2024-08-15 13:11:48",
		"asd":   "品牌:OPPO,首销日期:2023-11-23,屏幕尺寸:6.7英寸英寸,运行内存:8G12G,机身内存:256G512G,电池容量:4800mAh(typ)",
		"ttt":   []string{"品牌1,品牌2", "品牌3,品牌4"},
	}}
	out := []map[string]any{}
	mps := map[string]string{
		"a":     "b|int|rep::A_{}_B",
		"tm":    "tm|low|up1",
		"times": "times|unix",
		"asd":   "asds|trim|spl:,|spl:{}",
		"ttt":   "trim",
	}
	err := xstring.J2jMap(ins, &out, mps, true)
	if err != nil {
		t.Error(err)
	}
	b, _ := json.Marshal(out)
	t.Log(string(b))
}

func TestTimes(t *testing.T) {
	ts := xstring.ToTime("2024-07-23T16:00:00.000+0000")
	t.Log(ts.Format(time.DateTime))
}

func TestAny2Json(t *testing.T) {
	t.Log(xstring.ToInt64("123456.4e2"))
}
