package xstring

import (
	"strings"

	"github.com/mssola/user_agent"
)

// 根据UA解析其中内容
type UaInfo struct {
	Mobile        bool   `json:"mobile"`         // 是否是移动设备
	Bot           bool   `json:"bot"`            // 是否是机器人程序
	Mozilla       string `json:"mozilla"`        // Mozilla版本
	Model         string `json:"model"`          // 手机型号
	Platform      string `json:"platform"`       // 所在平台
	OS            string `json:"os"`             // 操作系统
	EngineName    string `json:"engine_name"`    // 引擎名称
	EngineVersion string `json:"engine_version"` // 引擎版本
	BrowName      string `json:"brow_name"`      // 浏览器名称
	BrowVersion   string `json:"brow_version"`   // 浏览器版本
}

// 根据UA获取详情信息
//
//	user_agent_string	UA拆分前的UA头
func Ua2info(user_agent_string string) UaInfo {
	ua_info := UaInfo{}
	if user_agent_string == "" {
		return ua_info
	}
	ua := user_agent.New(user_agent_string)
	eng, ver := ua.Engine()
	bro, ber := ua.Browser()
	ua_info = UaInfo{
		Mobile:        ua.Mobile(),
		Bot:           ua.Bot(),
		Mozilla:       ua.Mozilla(),
		Model:         ua.Model(),
		Platform:      ua.Platform(),
		OS:            ua.OS(),
		EngineName:    eng,
		EngineVersion: ver,
		BrowName:      bro,
		BrowVersion:   ber,
	}
	if ua_info.BrowName == "Android" {
		if strings.LastIndex(user_agent_string, "MMWEBID") > 0 {
			ua_info.BrowName = "微信内打开"
		} else if strings.LastIndex(user_agent_string, "AlipayClient") > 0 {
			ua_info.BrowName = "支付宝内打开"
		} else if strings.LastIndex(user_agent_string, "QQ") > 0 {
			ua_info.BrowName = "QQ内打开"
		}
	}
	return ua_info
}
