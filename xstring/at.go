package xstring

import (
	"regexp"
	"strings"
)

type AtStruct struct {
	Label string `json:"label"` // 标签 拆分为 "~", "#", "&", "@", "/", "!", "$", "%", "*", "^"
	Value string `json:"value"` // 值
}

// 关键词标签内容提取
// 该方法会提取字符串中的关键词，对应正则为：@([\p{L}\d-]+)
// 提取后的内容会组合到 AtStruct 结构体中，结构体中包含标签和值两个字段
// 标签为标识符前缀，即 "~", "#", "&", "@", "/", "!", "$", "%", "*", "^"
// 值为标识符内容，即标识符的具体内容
//
//	str	待提取的字符串
func At(str string) []AtStruct {
	mods := []AtStruct{}
	tgs := []string{"~", "#", "&", "@", "/", "!", "$", "%", "*", "^"}
	for _, tg := range tgs {
		if !strings.Contains(str, tg) {
			continue
		}
		var err error
		var re *regexp.Regexp
		switch tg {
		case "$", "*", "^":
			re, err = regexp.Compile("\\" + tg + `([\p{L}\d-]+)`)
		default:
			re, err = regexp.Compile(tg + `([\p{L}\d-]+)`)
		}
		if err != nil {
			return mods
		}
		ts := re.FindAllString(str, -1)
		for _, t := range ts {
			mod := AtStruct{
				Label: tg,
				Value: t[1:],
			}
			mods = append(mods, mod)
		}
	}
	return mods
}
