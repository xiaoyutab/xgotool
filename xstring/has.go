package xstring

import (
	"unicode"
)

// 检测是否存在的方法

// 检测字符串中是否存在中文
//
//	str	待检测的字符串
func HasContains(str string) bool {
	for _, v := range str {
		if unicode.Is(unicode.Han, v) {
			return true
		}
	}
	return false
}

// 检测字符串中是否存在英文字母
//
//	str	待检测的字符串
func HasLetter(str string) bool {
	for _, v := range str {
		// 因中文汉字使用IsLetter也返回true，所以此处使用Unicode编码进行判断
		// 正常来说可以调用HasLetterUp和HasLetterLower进行判断是否存在英文字母，不过这样就相当于循环了两遍，效率有些低
		// 所以此处直接使用if...else if进行判断
		if v >= 0x41 && v <= 0x5a {
			// 大写字母
			return true
		} else if v >= 0x61 && v <= 0x7a {
			// 小写英文字母
			return true
		}
	}
	return false
}

// 检测字符串中是否存在英文字母-大写字母
//
//	str	待检测的字符串
func HasLetterUp(str string) bool {
	for _, v := range str {
		// 因中文汉字使用IsLetter也返回true，所以此处使用Unicode编码进行判断
		if v >= 0x41 && v <= 0x5a {
			// 大写字母
			return true
		}
	}
	return false
}

// 检测字符串中是否存在英文字母-小写字母
//
//	str	待检测的字符串
func HasLetterLower(str string) bool {
	for _, v := range str {
		// 因中文汉字使用IsLetter也返回true，所以此处使用Unicode编码进行判断
		if v >= 0x61 && v <= 0x7a {
			// 小写英文字母
			return true
		}
	}
	return false
}

// 检测字符串中是否存在空白符
//
//	str	待检测的字符串
func HasSpace(str string) bool {
	for _, v := range str {
		if unicode.IsSpace(v) {
			return true
		}
	}
	return false
}

// 检测字符串中是否存在Unicode标点符号
//
//	str	待检测的字符串
func HasPunct(str string) bool {
	for _, v := range str {
		if unicode.IsPunct(v) {
			return true
		}
	}
	return false
}

// 检测字符串中是否存在数字
//
//	str	待检测的字符串
func HasNumber(str string) bool {
	for _, v := range str {
		if unicode.IsNumber(v) {
			return true
		}
	}
	return false
}

// 快速检测字符串中是否存在中文
// 此处采用的为国标码GB18030和国际码Unicode的范围：0x4e00 ~ 0x9fff    0x3400 ~ 0x4dbf    0x20000 ~ 0x2a6df
// 详细字符集范围见下表：
// 字符集	字数	Unicode 编码
// 基本汉字	20902字	4E00-9FA5
// 基本汉字补充	90字	9FA6-9FFF
// 扩展A	6592字	3400-4DBF
// 扩展B	42720字	20000-2A6DF
// 扩展C	4154字	2A700-2B739
// 扩展D	222字	2B740-2B81D
// 扩展E	5762字	2B820-2CEA1
// 扩展F	7473字	2CEB0-2EBE0
// 扩展G	4939字	30000-3134A
// 扩展H	4192字	31350-323AF
// 康熙部首	214字	2F00-2FD5
// 部首扩展	115字①	2E80-2EF3
// 兼容汉字	472字②	F900-FAD9
// 兼容扩展	542字	2F800-2FA1D
// 汉字笔画	36字	31C0-31E3
// 汉字结构	12字	2FF0-2FFB
// 汉语注音	43字	3105-312F
// 注音扩展	32字	31A0-31BF
// 〇	1字	3007
//
//	str	待检测的字符串
func HasContainsQuest(str string) bool {
	for _, v := range str {
		if v >= 0x4e00 && v <= 0x9fff {
			return true
		}
		if v >= 0x3400 && v <= 0x4dbf {
			return true
		}
		if v >= 0x20000 && v <= 0x2a6df {
			return true
		}
	}
	return false
}

// 判断字符串中是否存在emoji表情【此处使用rune的取值范围来确定是不是emoji表情，中文最大值为0xffff】
// 此处采用的为国标码GB18030和国际码Unicode中都有收录emoji图形符号   0x2600 ~ 0x27ff    0x1f000 ~ 0x1f6ff
//
//	str	待判断的字符串
func HasEmoji(str string) bool {
	for _, v := range str {
		if v >= 0x2600 && v <= 0x27ff {
			return true
		}
		if v >= 0x1f000 && v <= 0x1f6ff {
			return true
		}
	}
	return false
}
