package xstring

import (
	"regexp"
	"strconv"
	"strings"
)

// 表情解码
// 将emoji表情替换成类似[\u1f415]的字符串
//
//	s	待解码的含emoji表情的字符串
func EmojiDecode(s string) string {
	//emoji表情的数据表达式
	re := regexp.MustCompile(`\[[\\u0-9a-zA-Z]+\]`)
	//提取emoji数据表达式
	reg := regexp.MustCompile(`\[\\u|]`)
	// 查询字符串中的emoji表情，以便进行替换
	src := re.FindAllString(s, -1)
	for i := 0; i < len(src); i++ {
		e := reg.ReplaceAllString(src[i], "")
		p, err := strconv.ParseInt(e, 16, 32)
		if err == nil {
			s = strings.Replace(s, src[i], string(rune(p)), -1)
		}
	}
	return s
}

// 表情编码
// 将类似[\u1f415]的解码emoji表情转换为原始的emoji表情
//
//	s	含有类似[\u1f415]字符串的字符串
func EmojiEncode(s string) string {
	ret := ""
	rs := []rune(s)
	for i := 0; i < len(rs); i++ {
		if len(string(rs[i])) == 4 {
			u := `[\u` + strconv.FormatInt(int64(rs[i]), 16) + `]`
			ret += u

		} else {
			ret += string(rs[i])
		}
	}
	return ret
}
