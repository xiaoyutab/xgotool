package xstring

import (
	"regexp"
	"strings"
)

// 去除html标签和空白符
//
//	src	待去除的网页代码
func HtmlTrim(src string) string {
	// 将HTML标签全转换成小写
	src = regexp.MustCompile("\\<[\\S\\s]+?\\>").ReplaceAllStringFunc(src, strings.ToLower)
	// 去除STYLE
	src = regexp.MustCompile("\\<style[\\S\\s]+?\\</style\\>").ReplaceAllString(src, "")
	// 去除SCRIPT
	src = regexp.MustCompile("\\<script[\\S\\s]+?\\</script\\>").ReplaceAllString(src, "")
	// 去除所有尖括号内的HTML代码，并换成换行符
	src = regexp.MustCompile("\\<[\\S\\s]+?\\>").ReplaceAllString(src, "\n")
	// 去除连续的换行符
	src = regexp.MustCompile("\\s{2,}").ReplaceAllString(src, "\n")
	// 去除空格
	return strings.TrimSpace(src)
}
