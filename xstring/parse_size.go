package xstring

// 字符串转换为整数文件大小，单位：B
//
//	str: 带单位的字符串，如 "1024 KB"、"1G"、"128M"
//
// 不支持小数输入，但是支持多单位拼接，如：1KB 1KB20B
func ParseSizes(str string) int64 {
	spls := SplitUnit(str, true)
	num := int64(0)
	for i := 0; i < len(spls); i++ {
		switch spls[i].Unit {
		case "k", "kb":
			num += spls[i].Num * 1024
		case "m", "mb":
			num += spls[i].Num * 1024 * 1024
		case "g", "gb":
			num += spls[i].Num * 1024 * 1024 * 1024
		case "t", "tb":
			num += spls[i].Num * 1024 * 1024 * 1024 * 1024
		case "p", "pb":
			num += spls[i].Num * 1024 * 1024 * 1024 * 1024 * 1024
		case "e", "eb":
			num += spls[i].Num * 1024 * 1024 * 1024 * 1024 * 1024 * 1024
		case "b", "":
			// 默认单位为字节
			num += spls[i].Num
		}
	}
	return num
}
