package xstring

import "strings"

// 数字格式化
//
//	number	待格式化的数字
//	format	格式化成的字符串，以0替代数字，*替换原符号，其他字节直接原样输出
func Format(number, format string) string {
	ret := ""
	n_length := len(number)
	m_length := strings.Count(format, "0") + strings.Count(format, "*")
	if n_length > m_length {
		ret = number[0 : n_length-m_length]
		number = number[n_length-m_length:]
	} else {
		number = Pad(number, m_length, "0", PAD_RIGHT)
	}
	if ret != "" {
		ret += " "
	}
	i := 0
	for _, v := range format {
		if v == '0' {
			ret += string(number[i])
			i++
		} else if v == '*' {
			ret += "*"
			i++
		} else {
			ret += string(v)
		}
	}
	return ret
}
