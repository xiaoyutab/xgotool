package xstring

import (
	"os"
	"os/user"
	"runtime"
	"strings"
)

// 获取设备ID
//
//	name	上级目录列表
func DerviceId(name ...string) (string, error) {
	u, err := user.Current()
	if err != nil {
		return "", err
	}
	path := u.HomeDir + "/.config/"
	if runtime.GOOS == "windows" {
		// 将文件目录分割符设置为/，避免Windows下创建失败问题
		path = strings.ReplaceAll(u.HomeDir, "\\", "/") + "/AppData/Local/"
	}
	if len(name) > 0 {
		// 拼接用户输入的路径
		pth := strings.Join(name, "/") + "/"
		// 特殊符号过滤
		for _, v := range []string{":", "\\", "*", "?", "\"", "'", "<", ">", "|", "&", ";", "{", "}", "(", ")"} {
			pth = strings.ReplaceAll(pth, v, "")
		}
		path += pth
	}
	err = os.MkdirAll(path, os.ModePerm)
	if err != nil {
		return "", err
	}
	// 拼接设备ID所在文件
	path += "/.dervice_id"
	if _, err := os.Stat(path); err == nil {
		// 文件存在，读取该文件并返回其内容
		// 文件存在，读取其中内容
		if tmp, err := os.ReadFile(path); err == nil {
			return string(tmp), nil
		} else {
			return "", err
		}
	}
	// 文件不存在，创建字符串并返回其内容
	dervice_id := UUID()
	err = os.WriteFile(path, []byte(dervice_id), 0666)
	if err != nil {
		return "", err
	}
	return dervice_id, nil
}
