package xstring

// 字符串填充位置
const (
	// 将字符串填充到左侧
	PAD_LEFT = 1
	// 将字符串填充到右侧
	PAD_RIGHT = 2
)

// 字符串填充到指定长度
//
//	input	string	原字符串
//	padLength	int	规定补齐后的字符串位数
//	padString	string	自定义填充字符串
//	padType	string	填充类型:PAD_LEFT(向左填充,自动补齐位数), 默认右侧
func Pad(input string, padLength int, padString string, padType int) string {
	output := ""
	inputLen := len(input)
	if inputLen >= padLength {
		return input
	}
	padStringLen := len(padString)
	needFillLen := padLength - inputLen
	if diffLen := padStringLen - needFillLen; diffLen > 0 {
		padString = padString[diffLen:]
	}
	for i := 1; i <= needFillLen; i += padStringLen {
		output += padString
	}
	// 填充位置
	if padType == PAD_LEFT {
		return output + input
	}
	return input + output
}
