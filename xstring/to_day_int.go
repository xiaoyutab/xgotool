package xstring

import "time"

// 获取当前时间的日期int类型数字【从unix开始到现在的天数】
//
//	t	待获取的时间戳
func ToDayInt(t time.Time) int {
	_, zn := t.UTC().Zone()
	return int((t.Unix()-int64(zn))/60/60-8) / 24
}
