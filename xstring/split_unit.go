package xstring

import (
	"strconv"
	"strings"
)

type SplitUnits struct {
	Num  int64  `json:"num"`  // 拆分出来的数量
	Unit string `json:"unit"` // 拆分出来的单位
}

// 拆分单位和金额数值
//
//	str		字符串
//	low		是否小写单位
func SplitUnit(str string, low bool) []SplitUnits {
	units := []SplitUnits{}
	mps := SplitUnits{}
	for _, v := range str {
		switch v {
		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
			if mps.Unit != "" {
				if low {
					mps.Unit = strings.ToLower(mps.Unit)
				}
				units = append(units, mps)
				mps = SplitUnits{}
			}
			// 提取数值
			if num, err := strconv.ParseInt(string(v), 10, 64); err == nil {
				mps.Num = mps.Num*10 + num
			}
		case ' ':
			continue
		case '一', '二', '三', '四', '五', '六', '七', '八', '九', '零', '〇', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖':
			num_map := map[rune]int64{
				'一': 1, '壹': 1,
				'二': 2, '贰': 2,
				'三': 3, '叁': 3,
				'四': 4, '肆': 4,
				'五': 5, '伍': 5,
				'六': 6, '陆': 6,
				'七': 7, '柒': 7,
				'八': 8, '捌': 8,
				'九': 9, '玖': 9,
				'零': 0, '〇': 0,
			}
			if mps.Unit != "" {
				if low {
					mps.Unit = strings.ToLower(mps.Unit)
				}
				units = append(units, mps)
				mps = SplitUnits{}
			}
			// 提取数值
			mps.Num = mps.Num*10 + num_map[v]
		default:
			mps.Unit += string(v)
		}
	}
	if low {
		mps.Unit = strings.ToLower(mps.Unit)
	}
	units = append(units, mps)
	return units
}
