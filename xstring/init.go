// xstring包
package xstring

// 配置项信息
type Config struct {
	DebugLog func(msg string, err error) // DEBUG日志记录
}

var _default Config = Config{}

// 配置项注入
func Regedit(c *Config) {
	if c == nil {
		return
	}
	if c.DebugLog != nil {
		_default.DebugLog = c.DebugLog
	}
}
