package xstring

import (
	"os"
	"path"
	"regexp"
	"strings"
	"time"
)

// 根据规则生成名称
//
//	rols: 规则字符串，支持变量替换，格式为{{变量名}}，变量名为map中的key
//	param: 变量参数，map类型，key为变量名，value为变量值
func Name(rols string, param ...map[string]string) string {
	if rols == "" {
		rols = "{{year}}{{month}}/{{day}}"
	}
	// 变量合并整理
	pms := map[string]string{
		"year":   time.Now().Format("2006"),
		"month":  time.Now().Format("01"),
		"day":    time.Now().Format("02"),
		"hour":   time.Now().Format("15"),
		"minute": time.Now().Format("04"),
		"second": time.Now().Format("05"),
		"tmd5":   MD5(time.Now().Format(time.RFC3339Nano)),
		"rmd5":   MD5(Random(20)),
	}
	for i := range param {
		for k, v := range param[i] {
			pms[k] = v
		}
	}
	// 变量替换
	for k, v := range pms {
		rols = strings.ReplaceAll(rols, "{{"+k+"}}", v)
	}
	// 将多余的 / 去除
	re, err := regexp.Compile("/+")
	if err == nil {
		rols = re.ReplaceAllString(rols, "/")
	}
	// 移除其中的空格
	rols = strings.ReplaceAll(rols, " ", "")
	// 进行目录创建
	if strings.Contains(rols, ".") {
		// 是文件，创建上级目录
		os.MkdirAll(path.Dir(rols), os.ModePerm)
	} else {
		os.MkdirAll(rols, os.ModePerm)
	}
	return rols
}
