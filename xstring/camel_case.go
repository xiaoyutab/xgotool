package xstring

import "strings"

// 数据库表名转模型名【大驼峰转换】
// PS: 此函数会将abc_def的表名转换为AbcDef的大驼峰命名
//
//	str	待转换的下划线命名的数据库表名称
func CamelCase(str string) string {
	c := ""
	// 目标切分
	mbs := strings.Split(str, "_")
	for i := 0; i < len(mbs); i++ {
		c += strings.ToUpper(mbs[i][:1]) + mbs[i][1:]
	}
	return c
}
