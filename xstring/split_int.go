package xstring

import "strings"

// 将字符串切分为整形数组
//
//	s	待切分字符串
//	sep	使用什么切分
func SplitInt(s, sep string) []int {
	out := strings.Split(s, sep)
	temp := []int{}
	for i := 0; i < len(out); i++ {
		if out[i] == "" {
			continue
		}
		temp = append(temp, ToInt(out[i]))
	}
	return temp
}

// 将字符串切分为整形数组
//
//	s	待切分字符串
//	sep	使用什么切分
func SplitUint(s, sep string) []uint {
	out := strings.Split(s, sep)
	temp := []uint{}
	for i := 0; i < len(out); i++ {
		if out[i] == "" {
			continue
		}
		temp = append(temp, ToUint(out[i]))
	}
	return temp
}

// 将字符串切分为整形数组
//
//	s	待切分字符串
//	sep	使用什么切分
func SplitInt8(s, sep string) []int8 {
	out := strings.Split(s, sep)
	temp := []int8{}
	for i := 0; i < len(out); i++ {
		if out[i] == "" {
			continue
		}
		temp = append(temp, ToInt8(out[i]))
	}
	return temp
}

// 将字符串切分为整形数组
//
//	s	待切分字符串
//	sep	使用什么切分
func SplitInt16(s, sep string) []int16 {
	out := strings.Split(s, sep)
	temp := []int16{}
	for i := 0; i < len(out); i++ {
		if out[i] == "" {
			continue
		}
		temp = append(temp, ToInt16(out[i]))
	}
	return temp
}

// 将字符串切分为整形数组
//
//	s	待切分字符串
//	sep	使用什么切分
func SplitInt32(s, sep string) []int32 {
	out := strings.Split(s, sep)
	temp := []int32{}
	for i := 0; i < len(out); i++ {
		if out[i] == "" {
			continue
		}
		temp = append(temp, ToInt32(out[i]))
	}
	return temp
}

// 将字符串切分为整形数组
//
//	s	待切分字符串
//	sep	使用什么切分
func SplitInt64(s, sep string) []int64 {
	out := strings.Split(s, sep)
	temp := []int64{}
	for i := 0; i < len(out); i++ {
		if out[i] == "" {
			continue
		}
		temp = append(temp, ToInt64(out[i]))
	}
	return temp
}

// 将字符串切分为整形数组
//
//	s	待切分字符串
//	sep	使用什么切分
func SplitUint8(s, sep string) []uint8 {
	out := strings.Split(s, sep)
	temp := []uint8{}
	for i := 0; i < len(out); i++ {
		if out[i] == "" {
			continue
		}
		temp = append(temp, ToUint8(out[i]))
	}
	return temp
}

// 将字符串切分为整形数组
//
//	s	待切分字符串
//	sep	使用什么切分
func SplitUint16(s, sep string) []uint16 {
	out := strings.Split(s, sep)
	temp := []uint16{}
	for i := 0; i < len(out); i++ {
		if out[i] == "" {
			continue
		}
		temp = append(temp, ToUint16(out[i]))
	}
	return temp
}

// 将字符串切分为整形数组
//
//	s	待切分字符串
//	sep	使用什么切分
func SplitUint32(s, sep string) []uint32 {
	out := strings.Split(s, sep)
	temp := []uint32{}
	for i := 0; i < len(out); i++ {
		if out[i] == "" {
			continue
		}
		temp = append(temp, ToUint32(out[i]))
	}
	return temp
}

// 将字符串切分为整形数组
//
//	s	待切分字符串
//	sep	使用什么切分
func SplitUint64(s, sep string) []uint64 {
	out := strings.Split(s, sep)
	temp := []uint64{}
	for i := 0; i < len(out); i++ {
		if out[i] == "" {
			continue
		}
		temp = append(temp, ToUint64(out[i]))
	}
	return temp
}

// 将字符串切分为浮点数组
//
//	s	待切分字符串
//	sep	使用什么切分
func SplitFloat32(s, sep string) []float32 {
	out := strings.Split(s, sep)
	temp := []float32{}
	for i := 0; i < len(out); i++ {
		if out[i] == "" {
			continue
		}
		temp = append(temp, ToFloat32(out[i]))
	}
	return temp
}

// 将字符串切分为浮点数组
//
//	s	待切分字符串
//	sep	使用什么切分
func SplitFloat64(s, sep string) []float64 {
	out := strings.Split(s, sep)
	temp := []float64{}
	for i := 0; i < len(out); i++ {
		if out[i] == "" {
			continue
		}
		temp = append(temp, ToFloat64(out[i]))
	}
	return temp
}
