package xstring

import (
	"crypto/rand"
	"fmt"
)

// 生成UUID
//
// 基于 `crypto/rand` 密钥库进行随机生成
func UUID() string {
	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		return ""
	}
	return fmt.Sprintf("%x-%x-%x-%x-%x",
		b[0:4], b[4:6], b[6:8], b[8:10], b[10:])
}
