package xstring

import (
	"bytes"
	"compress/gzip"
	"errors"
	"fmt"
	"io"
)

// 字符串压缩
//
//	str	待压缩字符串 - 使用gzip算法进行压缩
//
// return 压缩后的字符串的HEX值
func Gz(str []byte) ([]byte, error) {
	var buffer bytes.Buffer
	writer := gzip.NewWriter(&buffer)
	_, err := writer.Write(str)
	defer writer.Close()
	if err != nil {
		return nil, err
	}
	err = writer.Close()
	if err != nil {
		return nil, err
	}
	return buffer.Bytes(), nil
}

// 字符串解压缩
func UnGz(str []byte) ([]byte, error) {
	if len(str) < 4 {
		return nil, errors.New("压缩字节流格式错误")
	}
	switch fmt.Sprintf("%x", str[:4]) {
	case "1f8b0800":
		// tar.gz格式压缩文件流，采用gzip进行解压
		reader, err := gzip.NewReader(bytes.NewReader(str))
		if err != nil {
			return nil, err
		}
		defer reader.Close()
		return io.ReadAll(reader)
	case "504B0304":
		// ZIP
		return nil, errors.New("暂不支持zip格式的压缩")
	case "52617221":
		// RAR
		return nil, errors.New("暂不支持rar格式的压缩")
	case "377abcaf":
		// 7z
		return nil, errors.New("暂不支持7z格式的压缩")
	case "425a6839":
		// bz2
		return nil, errors.New("暂不支持bz2格式的压缩")
	case "fd377a58":
		// xz
		return nil, errors.New("暂不支持xz格式的压缩")
	}
	return nil, errors.New("该压缩格式未识别，请校准后再次尝试")
}
