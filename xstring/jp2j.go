package xstring

import "regexp"

// JSONP代码转换为JSON代码(若不符合JSONP格式规则，则原样返回)
//
//	jsonp	JSONP代码
func Jp2j(jsonp string) string {
	// 正则表达式匹配JSONP格式，匹配到的内容为json字符串
	re := regexp.MustCompile(`^([^\(]+)\((.+)\)$`)
	match := re.FindStringSubmatch(jsonp)
	if len(match) != 3 {
		return jsonp
	}
	// 直接提取其中内容，不使用json进行串行化处理
	return match[2]
}
