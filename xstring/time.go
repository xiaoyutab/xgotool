package xstring

import (
	"errors"
	"time"
)

// 周uint定位
var WeekList = map[uint8]string{
	0: "日",
	1: "一",
	2: "二",
	3: "三",
	4: "四",
	5: "五",
	6: "六",
}

// 计算两个时间的时间差
// str1和str2均为yyyy-mm-dd hh:mm:ss格式的时间才行
//
//	str1	日期时间参数1
//	str2	日期时间参数2
func TimeDiff(str1, str2 string) (time.Duration, error) {
	s1, err := time.ParseInLocation(time.DateTime, str1, time.Local)
	if err != nil {
		return time.Duration(0), err
	}
	s2, err := time.ParseInLocation(time.DateTime, str2, time.Local)
	if err != nil {
		return time.Duration(0), err
	}
	return s1.Sub(s2), nil
}

// 计算两个时间的时间差【第一个日期比第二个日期大则为正数】
// str1和str2均为yyyy-mm-dd格式的时间才行
//
//	str1	日期参数1
//	str2	日期参数2
func TimeDateDiff(str1, str2 string) (time.Duration, error) {
	s1, err := time.ParseInLocation(time.DateOnly, str1, time.Local)
	if err != nil {
		return time.Duration(0), err
	}
	s2, err := time.ParseInLocation(time.DateOnly, str2, time.Local)
	if err != nil {
		return time.Duration(0), err
	}
	return s1.Sub(s2), nil
}

// 计算传入日期到现在的时间差[单位-年，用于计算年龄]
// str1需为yyyy-mm-dd格式的时间才行
//
//	str1	用户出生日期
func TimeDateAge(str1 string) (uint, error) {
	s1, err := time.ParseInLocation(time.DateOnly, str1, time.Local)
	if err != nil {
		return 0, err
	}
	age := time.Now().Year() - s1.Year()
	if age <= 0 {
		return 0, errors.New("传入时间需要为当前时间之前")
	}
	if time.Now().Month() > s1.Month() {
		// 如果当前月份超过了生日的月份
		// 如：当前4月，生日1月
		age++
	} else if time.Now().Month() < s1.Month() {
		// 如果当前月份不到生日的月份
		// 如：当前1月，生日12月
		age--
	}
	return uint(age), nil
}
