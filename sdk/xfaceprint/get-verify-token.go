package xfaceprint

import (
	"encoding/json"
	"fmt"

	"gitee.com/xiaoyutab/xgotool/https"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
	"gitee.com/xiaoyutab/xgotool/xstring"
)

// 获取百度鉴权的verify_token
func GetVerifyToken() string {
	if _default.PlanId == "" {
		return ""
	}
	type results struct {
		VerifyToken string `json:"verify_token" form:"verify_token"`
	}
	type request_token struct {
		LogId     string      `json:"log_id" form:"log_id"`
		Success   bool        `json:"success" form:"success"`
		Result    results     `json:"result" form:"result"`
		ErrorCode json.Number `json:"error_code" form:"error_code"`
		ErrorMsg  string      `json:"error_msg" form:"error_msg"`
	}
	temp := request_token{}
	err := https.New(_default.Domain + "rpc/2.0/brain/solution/faceprint/verifyToken/generate?access_token=" + GetAccessToken()).
		ParamJson(map[string]any{
			"plan_id": _default.PlanId,
		}).
		PostJson().
		Json(&temp)
	if err != nil {
		xlog.Panic("百度token请求失败", err)
		return ""
	}
	if !temp.Success {
		xlog.Panic("百度verify_token获取失败", fmt.Errorf("百度鉴权失败：[%d]%s", xstring.JUint(temp.ErrorCode), temp.ErrorMsg))
		return ""
	}
	return temp.Result.VerifyToken
}
