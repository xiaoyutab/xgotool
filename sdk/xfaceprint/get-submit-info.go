package xfaceprint

import (
	"fmt"
	"net/url"
)

// 拼接H5连接信息
//
//	v_token	批次Token/批次ID
//	suc_url	成功后的跳转地址，会自动追加&verify_token=xxxxx，所以若地址为绝对地址时请追加?
//	err_url	成功后的跳转地址，会自动追加&verify_token=xxxxx，所以若地址为绝对地址时请追加?
func GetSubmitInfo(v_token, suc_url, err_url string) *IdCardSubmitStruct {
	return &IdCardSubmitStruct{
		SuccessUrl: suc_url + "&verify_token=" + v_token,
		ErrorUrl:   err_url + "&verify_token=" + v_token,
		H5Url: fmt.Sprintf("https://brain.baidu.com/face/print/?token=%s&successUrl=%s&failedUrl=%s",
			v_token,
			url.QueryEscape(suc_url+"&verify_token="+v_token), url.QueryEscape(err_url+"&verify_token="+v_token),
		),
		VerifyToken: v_token,
	}
}
