package xfaceprint

import (
	"gitee.com/xiaoyutab/xgotool/https"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 获取认证图片
//
//	verify_token	识别token
func GetResultSimple(verify_token string) (string, error) {
	type results struct {
		Image string `json:"image" form:"image"`
	}

	type temp_struct struct {
		Success bool    `json:"success" form:"success"`
		Result  results `json:"result" form:"result"`
		LogId   string  `json:"log_id" form:"log_id"`
	}
	temp := temp_struct{}
	err := https.New(_default.Domain + "rpc/2.0/brain/solution/faceprint/result/simple?access_token=" + GetAccessToken()).
		ParamJson(map[string]any{
			"verify_token": verify_token,
		}).
		PostJson().
		Json(&temp)
	if err != nil {
		return "", xlog.AE("认证图片获取失败", err)
	}
	return temp.Result.Image, nil
}
