package xfaceprint

import (
	"encoding/json"
	"errors"

	"gitee.com/xiaoyutab/xgotool/https"
)

type ResultDetailResultIdcardConfirm struct {
	IdcardNumber string `json:"idcard_number" form:"idcard_number"` // 身份证号
	Name         string `json:"name" form:"name"`                   // 姓名
}

type ResultDetailResultIdcardImages struct {
	FrontBase64 string `json:"front_base64" form:"front_base64"` // 身份证图片的正面信息
	BackBase64  string `json:"back_base64" form:"back_base64"`   // 身份证图片的反面信息 当人脸实名认证控制台设置为使用OCR识别且为国徽面+人像面时返回此参数信息
}

type ResultDetailResultIdcardOcrResult struct {
	IssueAuthority string `json:"issue_authority" form:"issue_authority"` // 身份证签发机关
	Address        string `json:"address" form:"address"`                 // 地址
	Nation         string `json:"nation" form:"nation"`                   // 民族
	ExpireTime     string `json:"expire_time" form:"expire_time"`         // 身份证失效日期
	Name           string `json:"name" form:"name"`                       // 姓名
	IssueTime      string `json:"issue_time" form:"issue_time"`           // 身份证生效日期
	IdCardNumber   string `json:"id_card_number" form:"id_card_number"`   // 身份证号
	Birthday       string `json:"birthday" form:"birthday"`               // 生日
	Gender         string `json:"gender" form:"gender"`                   // 性别
}

type ResultDetailResultVerifyResult struct {
	LivenessScore json.Number `json:"liveness_score" form:"liveness_score"` // 活体检测分数：在线图片/动作活体：活体验证通过时返回活体分数，不通过则返回0。炫瞳活体：活体通过/不通过均会返回0
	Spoofing      json.Number `json:"spoofing" form:"spoofing"`             // 合成图分数 若未进行合成图检测，则返回0 若进行活体检测，则返回合成图检测分值
	Score         json.Number `json:"score" form:"score"`                   // 人脸实名认证
}

type ResultDetailResult struct {
	VerifyResult    ResultDetailResultVerifyResult    `json:"verify_result" form:"verify_result"`         // 认证返还信息
	IdcardOcrResult ResultDetailResultIdcardOcrResult `json:"idcard_ocr_result" form:"idcard_ocr_result"` // 返回采集的身份证信息 当人脸实名认证控制台设置为使用OCR识别时返回此参数信息
	IdcardImages    ResultDetailResultIdcardImages    `json:"idcard_images" form:"idcard_images"`         // 返回采集的身份证图片信息 当人脸实名认证控制台设置为使用OCR识别时返回此参数信息
	IdcardConfirm   ResultDetailResultIdcardConfirm   `json:"idcard_confirm" form:"idcard_confirm"`       // 用户二次确认的身份证信息
}

type resultDetail struct {
	Success bool                `json:"success" form:"success"`
	Result  *ResultDetailResult `json:"result" form:"result"`
	LogId   string              `json:"log_id" form:"log_id"`
}

// 获取认证结果
//
//	verify_token	识别token
func GetResultDetail(verify_token string) (*ResultDetailResult, error) {
	temp := resultDetail{}
	err := https.New(_default.Domain + "rpc/2.0/brain/solution/faceprint/result/detail?access_token=" + GetAccessToken()).
		ParamJson(map[string]any{
			"verify_token": verify_token,
		}).
		PostJson().
		Json(&temp)
	if err != nil {
		return nil, err
	}
	if !temp.Success {
		return nil, errors.New("认证结果获取错误，识别码：" + verify_token)
	}
	return temp.Result, nil
}
