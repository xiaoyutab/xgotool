package xfaceprint

import (
	"encoding/json"
	"errors"
	"strings"

	"gitee.com/xiaoyutab/xgotool/https"
	"gitee.com/xiaoyutab/xgotool/xnum"
	"gitee.com/xiaoyutab/xgotool/xstring"
)

// APP端直接上传图片到百度进行识别
//
//	id_card_name	姓名
//	id_card_number	身份证号
//	image_type		图片类型，支持：BASE64、URL、FACE_TOKEN
//	image_data		图片值
func SubmitApp(id_card_name, id_card_number, image_type, image_data string) (float64, error) {
	image_type = strings.ToUpper(image_type)
	if image_type == "" {
		image_type = "BASE64"
	}
	if !xnum.InArray(image_type, []string{"BASE64", "URL", "FACE_TOKEN"}) {
		return 0, errors.New("图片类型不支持")
	}
	// 长度过低时直接返回错误
	if len(image_data) < 4 {
		return 0, errors.New("图片数据错误")
	}
	// 长度在200位以内、且以http开头的话，直接将type修改为URL类型
	if len(image_data) < 200 && image_data[0:4] == "http" {
		image_type = "URL"
	}
	type jsonStructResult struct {
		Score        json.Number `json:"score" form:"score"`                 // 与公安数据源人脸图相似度可能性，用于验证生活照与公安数据源人脸图是否为同一人，有正常分数时为[0~100]，推荐阈值80，超过即判断为同一人
		VerifyStatus json.Number `json:"verify_status" form:"verify_status"` // 认证状态，取值如下：0 ： 正常 1 ： 身份证号与姓名不匹配或该身份证号不存在 2 ： 公安网图片不存在或质量过低
	}
	type jsonStruct struct {
		LogId     json.Number      `json:"log_id" form:"log_id"`         // 调用的日志id
		ErrorCode json.Number      `json:"error_code" form:"error_code"` // 错误码
		ErrorMsg  string           `json:"error_msg" form:"error_msg"`   // 错误信息标识
		Result    jsonStructResult `json:"result" form:"result"`         // 认证返回的结果
		DecImage  string           `json:"dec_image" form:"dec_image"`   // 对SDK传入的加密图片进行解密。仅APP场景且进行了图片加密时，此参数返回解密后的人脸图片信息
		RiskLevel string           `json:"risk_level" form:"risk_level"` // 判断设备是否发生过风险行为来判断风险级别，取值（数值由高到低）：1 – 高危 2 – 嫌疑 3 – 普通 4 – 正常
		RiskTag   []string         `json:"risk_tag" form:"risk_tag"`     // 风险标签，若判断为有风险，则会有风险标签json 数组告知风险类型, 例如：general_inject
	}
	// 请求百度获取相应结果
	tmp := jsonStruct{}
	err := https.New(_default.Domain + "rest/2.0/face/v4/mingjing/verify?access_token=" + GetAccessToken()).
		ParamJson(map[string]any{
			"image_type":     image_type,
			"image":          image_data,
			"id_card_number": id_card_number,
			"name":           id_card_name,
		}).
		PostJson().
		Json(&tmp)
	if err != nil {
		return 0, err
	}
	if xstring.JInt(tmp.ErrorCode) != 0 {
		return 0, errors.New("接口方返回错误：" + tmp.ErrorMsg)
	}
	return xstring.JFloat64(tmp.Result.Score), nil
}
