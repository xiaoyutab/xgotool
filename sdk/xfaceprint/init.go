// 百度人脸识别/人脸认证SDK
package xfaceprint

// 百度人脸识别配置项
type Config struct {
	Domain    string // 人脸识别接口域名
	AppId     string // 百度开发者平台的AppId
	AppKey    string // 百度开发者平台的AK
	SecretKey string // 百度开发者平台的密钥信息
	PlanId    string // 获取鉴权ID
}

var _default Config = Config{
	Domain: "https://aip.baidubce.com/",
}

// 配置项注入
func Regedit(c *Config) {
	if c == nil {
		return
	}
	if c.Domain != "" {
		_default.Domain = c.Domain
	}
	if c.AppId != "" {
		_default.AppId = c.AppId
	}
	if c.AppKey != "" {
		_default.AppKey = c.AppKey
	}
	if c.SecretKey != "" {
		_default.SecretKey = c.SecretKey
	}
	if c.PlanId != "" {
		_default.PlanId = c.PlanId
	}
}
