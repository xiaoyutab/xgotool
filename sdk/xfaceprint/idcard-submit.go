package xfaceprint

type IdCardSubmitStruct struct {
	SuccessUrl  string `json:"success_url" form:"success_url"`   // 成功的回跳地址
	ErrorUrl    string `json:"error_url" form:"error_url"`       // 失败的回跳地址
	H5Url       string `json:"h5_url" form:"h5_url"`             // 认证H5地址
	VerifyToken string `json:"verify_token" form:"verify_token"` // 请求批次
}

// 用户信息提交接口
//
//	id_name	姓名
//	id_no	身份证号
//	c_type	身份证类型 0-大陆居民二代身份证 4-港澳台居民居住证
//	suc_url	成功后的跳转地址，会自动追加&verify_token=xxxxx，所以若地址为绝对地址时请追加?
//	err_url	成功后的跳转地址，会自动追加&verify_token=xxxxx，所以若地址为绝对地址时请追加?
func IdcardSubmit(id_name, id_no string, c_type uint, suc_url, err_url string) (*IdCardSubmitStruct, error) {
	verify_token := GetVerifyToken()
	err := SubmitInfo(verify_token, id_name, id_no, c_type)
	if err != nil {
		return nil, err
	}
	// 拼接H5认证地址
	return GetSubmitInfo(verify_token, suc_url, err_url), nil
}
