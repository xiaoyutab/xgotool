package xfaceprint

import (
	"encoding/json"
	"errors"

	"gitee.com/xiaoyutab/xgotool/https"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 提交用户个人资料信息
//
//	v_token	批次Token/批次ID
//	id_name	姓名
//	id_no	身份证号
//	c_type	身份证类型 0-大陆居民二代身份证 4-港澳台居民居住证
func SubmitInfo(v_token, id_name, id_no string, c_type uint) error {
	if v_token == "" {
		return errors.New("认证批次获取失败")
	}
	type tempStruct struct {
		Result  json.Number `json:"result" form:"result"`
		LogId   string      `json:"log_id" form:"log_id"`
		Success bool        `json:"success" form:"success"`
	}
	temp := tempStruct{}
	err := https.New(_default.Domain + "rpc/2.0/brain/solution/faceprint/idcard/submit?access_token=" + GetAccessToken()).ParamJson(map[string]any{
		"verify_token":     v_token,
		"id_name":          id_name,
		"id_no":            id_no,
		"certificate_type": c_type,
	}).PostJson().Json(&temp)
	if err != nil {
		return err
	}
	if !temp.Success {
		return xlog.AE("用户信息提交失败", errors.New("接口返回错误，请查询对应日志: "+temp.LogId))
	}
	return nil
}
