package xocrbaidu

import (
	"fmt"
	"time"

	"gitee.com/xiaoyutab/xgotool/https"
	"gitee.com/xiaoyutab/xgotool/individual/xcache"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 获取百度AI中心的token标识
func GetToken() string {
	cache_key := xcache.Key("sdk.xocr.baidu.gettoken")
	if xcache.Exists(cache_key) {
		return xcache.GetString(cache_key)
	}
	type request_token struct {
		AccessToken      string `json:"access_token"`      // 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
		ExpiresIn        int    `json:"expires_in"`        // 必填，若三方返回的此值json对应错误，请手动重赋值给此值
		RefreshToken     string `json:"refresh_token"`     // 用户刷新access_token
		Scope            string `json:"scope"`             // 用户授权的作用域，使用逗号（,）分隔
		Error            string `json:"error"`             // 错误码
		ErrorDescription string `json:"error_description"` // 错误描述信息
	}
	temp := request_token{}
	err := https.New(_default.Domain + "oauth/2.0/token").Param(map[string]string{
		"grant_type":    "client_credentials",
		"client_id":     _default.AppKey,
		"client_secret": _default.SecretKey,
	}).Get().Json(&temp)
	if err != nil {
		xlog.Panic("百度token请求失败", err)
		return ""
	}
	if temp.Error != "" {
		xlog.Panic("百度token获取失败", fmt.Errorf("百度鉴权失败代码：%s, 详情请参阅： https://ai.baidu.com/ai-doc/REFERENCE/Ck3dwjhhu", temp.Error))
		return ""
	}
	xcache.SetStructExt(cache_key, temp.AccessToken, time.Hour*24*7) // 缓存一周【有效期30天，但为了兼容考虑不进行缓存那么长时间】
	return temp.AccessToken
}
