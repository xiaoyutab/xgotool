// 百度OCR识图功能SDK
package xocrbaidu

import (
	"gitee.com/xiaoyutab/xgotool/individual/xcron"
	"gitee.com/xiaoyutab/xgotool/xnum"
)

const (
	SizeGeneralBasic           = iota // 通用文字识别（标准版）
	SizeGeneral                       // 通用文字识别（标准含位置版）
	SizeAccurateBasic                 // 通用文字识别（高精度版）
	SizeAccurate                      // 通用文字识别（高精度含位置版）
	SizeWebimage                      // 网络图片文字识别
	SizeIdcard                        // 身份证识别
	SizeDrivingLicense                // 驾驶证识别
	SizeVehicleLicense                // 行驶证识别
	SizeBusinessLicense               // 营业执照识别
	SizeLicensePlate                  // 车牌识别
	SizeRequest                       // 表格文字识别-提交请求
	SizeReceipt                       // 通用票据识别
	SizeQrcode                        // 二维码识别【总量，无每天清零】
	SizeHandwriting                   // 手写文字识别
	SizePassport                      // 护照识别【总量，无每天清零】
	SizeVatInvoice                    // 增值税发票识别
	SizeNumbers                       // 数字识别
	SizeBusinessCard                  // 名片识别
	SizeTrainTicket                   // 火车票识别
	SizeTaxiReceipt                   // 出租车票识别
	SizeVinCode                       // VIN码识别
	SizeQuotaInvoice                  // 定额发票识别
	SizeBirthCertificate              // 出生证明识别
	SizeHouseholdRegister             // 户口本识别
	SizeHKMacauExitentrypermit        // 港澳通行证识别
	SizeTaiwanExitentrypermit         // 台湾通行证识别
)

// 总量tab，不会每日清空的列表
var _all_size = []int{
	SizeQrcode, SizePassport, SizeHouseholdRegister, SizeQuotaInvoice, SizeBirthCertificate,
}

// 百度识图配置项
type Config struct {
	Domain    string               // 百度翻译的域名前缀
	AppId     string               // 百度开发者平台的AppId
	AppKey    string               // 百度开发者平台的AK
	SecretKey string               // 百度开发者平台的密钥信息
	Size      map[int]FreeSizeInfo // 免费次数配置/已使用次数限制
}

// 次数配置、次数回调配置
type FreeSizeInfo struct {
	FreeSize   uint64                  // 免费次数
	NowSize    uint64                  // 当前已用次数【每天清零】
	BeyondSize uint64                  // 超出次数
	ExceedStop bool                    // 超出免费翻译字符量以后是否停止翻译
	Func       func(int, FreeSizeInfo) // 次数调整后的回调
}

var _default Config = Config{
	Domain: "https://aip.baidubce.com/",
}

// 配置项注入
func Regedit(c *Config) {
	if c == nil {
		return
	}
	if c.Domain != "" {
		_default.Domain = c.Domain
	}
	if c.AppId != "" {
		_default.AppId = c.AppId
	}
	if c.AppKey != "" {
		_default.AppKey = c.AppKey
	}
	if c.SecretKey != "" {
		_default.SecretKey = c.SecretKey
	}
	if len(c.Size) > 0 {
		for i, v := range c.Size {
			_default.Size[i] = v
		}
	}
	// 每日清空的cron注入
	xcron.SpecNext("1 0 * * *", xcron.CronTab{
		Key:  "sdk.xocr.baidu.now.reset",
		Name: "重置请求次数",
		Desc: "每天0点1分重置今日的已使用次数，用于对账和次数控制",
		Func: func() {
			for i, v := range _default.Size {
				if !xnum.InArray(i, _all_size) {
					v.Func(i, v)
				}
			}
		},
	})
}
