package xamap

import (
	"encoding/json"

	"gitee.com/xiaoyutab/xgotool/https"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
	"gitee.com/xiaoyutab/xgotool/xnum"
)

// 坐标地址转换
func Convert(gps []xnum.GPS, coordsys string) ([]xnum.GPS, error) {
	if !xnum.InArray(coordsys, []string{COORDSYS_GPS, COORDSYS_MAPBAR, COORDSYS_BAIDU}) {
		return gps, nil
	}
	// 地址转换
	if len(gps) > 40 {
		gps = gps[0:39] // 限制最大40个GPS坐标范围
	}
	type inf_temp struct {
		Infocode  json.Number `json:"infocode"`
		Locations string      `json:"locations"`
	}
	inf := inf_temp{}
	err := https.New(geturl("assistant/coordinate/convert")).
		Param(params(map[string]string{
			"locations": GPS2S(gps, "|"),
		})).
		Get().
		Json(&inf)
	if err != nil {
		return nil, xlog.EE("高德地图IP转换接口请求失败", err)
	}
	// infocode判定
	err = infocode(inf.Infocode)
	if err != nil {
		return nil, err
	}
	return S2GPSAll(inf.Locations), nil
}
