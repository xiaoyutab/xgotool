package xamap

import (
	"fmt"
	"strings"

	"gitee.com/xiaoyutab/xgotool/xnum"
	"gitee.com/xiaoyutab/xgotool/xstring"
)

// 字符串转GPS坐标系
//
//	inf	待转换的字符串形式的坐标
func S2GPS(inf string) xnum.GPS {
	if inf == "" {
		return xnum.GPS{}
	}
	temp := strings.Split(inf, ",")
	if len(temp) != 2 {
		return xnum.GPS{}
	}
	return xnum.GPS{
		Longitude: xstring.ToFloat64(temp[0]), // 经度
		Latitude:  xstring.ToFloat64(temp[1]), // 纬度
	}
}

// 字符串转GPS坐标系，兼容;分割和|分割
//
//	inf	待分割转换的字符串形式的坐标
func S2GPSAll(inf string) []xnum.GPS {
	rec := strings.Split(inf, ";") // 经纬度切割
	if len(rec) == 1 {
		rec = strings.Split(inf, "|") // 经纬度切割
	}
	gps := []xnum.GPS{}
	for i := 0; i < len(rec); i++ {
		temp := S2GPS(rec[i])
		if temp.Latitude == 0 || temp.Longitude == 0 {
			continue
		}
		gps = append(gps, temp)
	}
	return gps
}

// GPS转字符串拼接（单坐标点固定使用,拼接）
//
//	gps	坐标点列表
//	j	多个坐标点之间的拼接字符串
func GPS2S(gps []xnum.GPS, j string) string {
	temp := []string{}
	for i := 0; i < len(gps); i++ {
		if gps[i].Latitude == 0 || gps[i].Longitude == 0 {
			continue
		}
		temp = append(temp, fmt.Sprintf("%f,%f", gps[i].Longitude, gps[i].Latitude))
	}
	return strings.Join(temp, j)
}
