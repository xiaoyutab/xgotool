package xamap

import (
	"encoding/json"
	"fmt"

	"gitee.com/xiaoyutab/xgotool/https"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
)

// 返回的结果结构体
type weather_forecast_quest struct {
	Status    string      `json:"status"`    // 成功状态
	Count     json.Number `json:"count"`     // 返回结果总数目
	Info      string      `json:"info"`      // 结果标识
	Infocode  json.Number `json:"infocode"`  // infocode标识
	Forecasts []Forecasts `json:"forecasts"` // 预报天气
	Lives     []Lives     `json:"lives"`     // 实况天气
}

// 省市信息
type Forecasts struct {
	City       string  `json:"city"`       // 城市名称
	Adcode     string  `json:"adcode"`     // 城市编码
	Province   string  `json:"province"`   // 省份名称
	Reporttime string  `json:"reporttime"` // 预报发布时间
	Casts      []Casts `json:"casts"`      // 预报数据list结构，元素cast,按顺序为当天、第二天、第三天的预报数据
}

// 天气预报
type Casts struct {
	Date           string `json:"date"`            // 日期
	Week           string `json:"week"`            // 星期几
	Dayweather     string `json:"dayweather"`      // 白天天气现象
	Nightweather   string `json:"nightweather"`    // 晚上天气现象
	Daytemp        string `json:"daytemp"`         // 白天温度
	Nighttemp      string `json:"nighttemp"`       // 晚上温度
	Daywind        string `json:"daywind"`         // 白天风向
	Nightwind      string `json:"nightwind"`       // 晚上风向
	Daypower       string `json:"daypower"`        // 白天风力
	Nightpower     string `json:"nightpower"`      // 晚上风力
	DaytempFloat   string `json:"daytemp_float"`   // 白天温度【浮点数格式】
	NighttempFloat string `json:"nighttemp_float"` // 晚上温度【浮点数格式】
}

// 实时天气信息
type Lives struct {
	Province         string `json:"province"`          // 省份名
	City             string `json:"city"`              // 城市名
	Adcode           string `json:"adcode"`            // 区域编码
	Weather          string `json:"weather"`           // 天气现象（汉字描述）
	Temperature      string `json:"temperature"`       // 实时气温，单位：摄氏度
	Winddirection    string `json:"winddirection"`     // 风向描述
	Windpower        string `json:"windpower"`         // 风力级别，单位：级
	Humidity         string `json:"humidity"`          // 空气湿度
	Reporttime       string `json:"reporttime"`        // 数据发布的时间
	TemperatureFloat string `json:"temperature_float"` // 实时气温，单位：摄氏度【浮点数格式】
	HumidityFloat    string `json:"humidity_float"`    // 空气湿度【浮点数格式】
}

// 获取实时天气信息
//
//	adcode	要查询的城市code
func WeatherLives(adcode int) ([]Lives, error) {
	tmp := weather_forecast_quest{}
	err := https.New(geturl("weather/weatherInfo")).
		Param(params(map[string]string{
			"city":       fmt.Sprintf("%d", adcode),
			"extensions": "base",
			"output":     "JSON",
		})).
		Get().
		Json(&tmp)
	if err != nil {
		return nil, xlog.EE("高德实时天气获取失败", err)
	}
	err = infocode(tmp.Infocode)
	if err != nil {
		return nil, err
	}
	return tmp.Lives, nil
}

// 获取预报天气信息
//
//	adcode	要查询的城市code
func WeatherForecast(adcode int) ([]Forecasts, error) {
	tmp := weather_forecast_quest{}
	err := https.New(geturl("weather/weatherInfo")).Param(map[string]string{
		"city":       fmt.Sprintf("%d", adcode),
		"extensions": "all",
		"output":     "JSON",
	}).Get().Json(&tmp)
	if err != nil {
		return nil, xlog.EE("高德天气预报获取失败", err)
	}
	err = infocode(tmp.Infocode)
	if err != nil {
		return nil, err
	}
	return tmp.Forecasts, nil
}
