// 高德地图SDK
package xamap

import (
	"encoding/json"
	"fmt"

	_ "embed"

	"gitee.com/xiaoyutab/xgotool/xstring"
)

//go:embed AMap_adcode_citycode.xlsx
var citycode []byte

// 配置项结构信息
type Config struct {
	WebKey      string `json:"key"`      // 高德地图申请的WebKey
	WebSecurity string `json:"security"` // 高德地图申请的security密钥【可为空】
	Domain      string `json:"domain"`   // 高德地图域名前缀，默认为：https://restapi.amap.com/v3/
}

var _default Config = Config{
	WebKey: "",
	Domain: "https://restapi.amap.com/v3/",
}

const (
	COORDSYS_GPS    = "gps"    // 坐标系-GPS坐标系
	COORDSYS_MAPBAR = "mapbar" // 坐标系-图为先坐标系
	COORDSYS_BAIDU  = "baidu"  // 坐标系-百度坐标系
)

// 配置项注入
func Regedit(c *Config) {
	if c != nil {
		if c.WebKey != "" {
			_default.WebKey = c.WebKey
		}
		if c.Domain != "" {
			_default.Domain = c.Domain
		}
	}
}

// 获取完整请求地址信息
//
//	url	url后缀信息
func geturl(url string) string {
	if _default.Domain[len(_default.Domain)-1:] != "/" {
		_default.Domain += "/"
	}
	if url[0:1] == "/" {
		url = url[1:]
	}
	return _default.Domain + url
}

// 追加参数中的sig签名
//
//	param	待追加的参数
func params(param map[string]string) map[string]string {
	param["key"] = _default.WebKey
	if _default.WebSecurity != "" {
		param["sig"] = _default.WebSecurity
	}
	return param
}

// infocode状态码识别
func infocode(code json.Number) error {
	if xstring.JInt(code) == 10000 {
		return nil
	}
	return fmt.Errorf("infocode返回错误[%d]，详情请查看：https://lbs.amap.com/api/webservice/guide/tools/info", xstring.JInt(code))
}
