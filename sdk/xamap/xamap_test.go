package xamap_test

import (
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"gitee.com/xiaoyutab/xgotool/https"
	"gitee.com/xiaoyutab/xgotool/sdk/xamap"
	"gitee.com/xiaoyutab/xgotool/xnum"
)

func Reg() {
	https.SetQuestLog(func(c *https.CURL) {
		b, _ := json.Marshal(c)
		fmt.Println(string(b))
	})
	xamap.Regedit(&xamap.Config{
		WebKey: "689c8ce3142186b1ad2313cd6a1215d8",
	})
}

// 输入提示
func TestInputtips(t *testing.T) {
	Reg()
	tips, err := xamap.Inputtips("北京市")
	if err != nil {
		t.Error(err)
	}
	t.Log(tips)
	time.Sleep(time.Second)
}

// 获取天气信息
func TestWeather(t *testing.T) {
	Reg()
	w, err := xamap.WeatherForecast(110101)
	if err != nil {
		t.Error(err)
	}
	t.Log(w)
	time.Sleep(time.Second * 1)
}

// 坐标系转换
func TestConvert(t *testing.T) {
	Reg()
	info, err := xamap.Convert([]xnum.GPS{{
		Longitude: 116.481499,
		Latitude:  39.990475,
	}, {
		Longitude: 116.481499,
		Latitude:  39.990375,
	}}, xamap.COORDSYS_GPS)
	if err != nil {
		t.Error(err)
	}
	t.Log(info)
	time.Sleep(time.Second * 1)
}

// 获取IP详情信息
func TestIp2info(t *testing.T) {
	Reg()
	info, err := xamap.Ip2info("114.247.50.2")
	if err != nil {
		t.Error(err)
	}
	t.Log(info)
	time.Sleep(time.Second * 1)
}
