package xamap

import (
	"encoding/json"

	"gitee.com/xiaoyutab/xgotool/https"
	"gitee.com/xiaoyutab/xgotool/individual/xlog"
	"gitee.com/xiaoyutab/xgotool/xnum"
	"gitee.com/xiaoyutab/xgotool/xstring"
)

// 提示结构体
type Tips struct {
	ID       string   `json:"id"`       // 返回数据ID
	Name     string   `json:"name"`     // tip名称
	District string   `json:"district"` // 所属区域[省+市+区（直辖市为“市+区”）]
	Adcode   int      `json:"adcode"`   // 区域编码
	Location xnum.GPS `json:"location"` // tip中心点坐标
	Address  string   `json:"address"`  // 详细地址
	Typecode int      `json:"typecode"` // 提示所属的POI类型
}

// 输入提示
func Inputtips(keywords string) ([]Tips, error) {
	type tips struct {
		ID       string `json:"id"`
		Name     string `json:"name"`
		District string `json:"district"`
		Adcode   string `json:"adcode"`
		Location string `json:"location"`
		Address  string `json:"address"`
		Typecode string `json:"typecode"`
		City     string `json:"city"`
	}
	type request struct {
		Tips     []tips      `json:"tips"`
		Status   string      `json:"status"`
		Info     string      `json:"info"`
		Infocode json.Number `json:"infocode"`
		Count    string      `json:"count"`
	}
	tmp := request{}
	// 获取请求结果
	err := https.New(geturl("assistant/inputtips")).
		Param(params(map[string]string{
			"keywords": keywords,
		})).
		Get().
		Replace("[]", `""`).
		Json(&tmp)
	if err != nil {
		return nil, xlog.EE("高德输入提示获取失败", err)
	}
	err = infocode(tmp.Infocode)
	if err != nil {
		return nil, err
	}
	// 转化为输出格式
	temp := []Tips{}
	for i := 0; i < len(tmp.Tips); i++ {
		temp = append(temp, Tips{
			ID:       xstring.AString(tmp.Tips[i].ID),
			Name:     tmp.Tips[i].Name,
			District: tmp.Tips[i].District,
			Adcode:   xstring.ToInt(tmp.Tips[i].Adcode),
			Location: S2GPS(xstring.AString(tmp.Tips[i].Location)),
			Address:  xstring.AString(tmp.Tips[i].Address),
			Typecode: xstring.ToInt(tmp.Tips[i].Typecode),
		})
	}
	return temp, nil
}
