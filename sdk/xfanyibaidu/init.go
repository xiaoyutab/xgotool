// 百度翻译
package xfanyibaidu

import "gitee.com/xiaoyutab/xgotool/individual/xcron"

// 配置项信息
type Config struct {
	Domain     string           // 百度翻译的域名前缀
	AppId      string           // 百度开发者平台的AppId（需开通翻译功能）
	SecretKey  string           // 百度开发者平台的密钥信息（需开通翻译功能）
	FreeSize   uint64           // 免费翻译的字符量
	ExceedStop bool             // 超出免费翻译字符量以后是否停止翻译
	nowSize    uint64           // 当前已用翻译的字符量
	NowFunc    func(now uint64) // 已用字符量统计回调
}

var _default Config = Config{
	Domain:     "https://fanyi-api.baidu.com/",
	ExceedStop: false,
	FreeSize:   1000000,
}

// 配置项注入
//
//	c	待注入的配置项
func Regedit(c *Config) {
	if c == nil {
		return
	}
	if c.Domain != "" {
		_default.Domain = c.Domain
	}
	if c.AppId != "" {
		_default.AppId = c.AppId
	}
	if c.SecretKey != "" {
		_default.SecretKey = c.SecretKey
	}
	if c.FreeSize > 0 {
		_default.FreeSize = c.FreeSize
	}
	if c.ExceedStop {
		_default.ExceedStop = c.ExceedStop
	}
	if c.NowFunc != nil {
		_default.NowFunc = c.NowFunc
	}
	xcron.SpecNext("1 0 1 * *", xcron.CronTab{
		Key:  "sdk.xfanyibaidu.reset.now.size",
		Name: "重置每月可翻译字数",
		Desc: "每月1日的0时1分重置翻译次数为0",
		Func: func() {
			_default.nowSize = 0
			if _default.NowFunc != nil {
				_default.NowFunc(0)
			}
		},
	})
}

// 切换ExceedStop状态为传入状态
//
//	c	要切换成什么状态
func SwitchExceedStop(c bool) {
	_default.ExceedStop = c
}

// 设置当前字节量
//
//	c	当前已使用字节量
func SwitchNowSize(c uint64) {
	_default.nowSize = c
}
