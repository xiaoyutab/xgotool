package xfanyibaidu

import (
	"errors"
	"fmt"

	"gitee.com/xiaoyutab/xgotool/https"
	"gitee.com/xiaoyutab/xgotool/xstring"
)

// 通用文本翻译
//
//	src		待翻译的文本
//	from	来源语种
//	to		待翻译为的语种
func Translate(src, from, to string) ([]string, error) {
	if src == "" {
		return nil, errors.New("待翻译文本不允许为空")
	}
	// 如果超过6000byte就直接截取6000byte【百度翻译方限制长度】
	if len(src) > 6000 {
		src = src[:6000]
	}
	if from == "" {
		from = "auto"
	}
	if to == "" {
		to = "zh"
	}
	salt := xstring.Random(10)
	type result_struct struct {
		ErrorCode   string     `json:"error_code"` // 错误代码
		ErrorMsg    string     `json:"error_msg"`  // 错误消息
		From        string     `json:"from"`       // 来源语种
		To          string     `json:"to"`         // 转换语种
		TransResult []struct { // 转换结果列表
			Src string `json:"src"` // 来源
			Dst string `json:"dst"` // 转换结果
		} `json:"trans_result"`
	}
	temp := result_struct{}
	err := https.New(_default.Domain + "api/trans/vip/translate").Param(map[string]string{
		"q":     src,
		"from":  from,
		"to":    to,
		"appid": _default.AppId,
		"salt":  salt,
		"sign":  xstring.MD5(_default.AppId + src + salt + _default.SecretKey),
	}).Post().Json(&temp)
	if err != nil {
		return nil, err
	}
	if temp.ErrorCode != "" {
		return nil, fmt.Errorf("翻译结果出错，错误代码：%s, 错误简称：%s, 详情请前往 http://api.fanyi.baidu.com/doc/21 进行查看", temp.ErrorCode, temp.ErrorMsg)
	}
	// 翻译文本量添加
	_default.nowSize += uint64(len(src))
	// 翻译文本量回调添加
	if _default.NowFunc != nil {
		go _default.NowFunc(_default.nowSize)
	}
	mb := []string{}
	if len(temp.TransResult) >= 0 {
		for i := 0; i < len(temp.TransResult); i++ {
			mb = append(mb, temp.TransResult[i].Dst)
		}
	}
	return mb, nil
}

// 自动翻译为中文
//
//	src	待翻译的文本
func TranslateZh(src string) ([]string, error) {
	return Translate(src, "auto", "zh")
}
