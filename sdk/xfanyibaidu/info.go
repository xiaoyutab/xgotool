package xfanyibaidu

// 当前账户详情信息【不含AppSec等敏感信息】
type AccountInfos struct {
	AppId      string `json:"appid"`       // 账户APPID
	FreeSize   uint64 `json:"free_size"`   // 免费字符量
	ExceedStop bool   `json:"exceed_stop"` // 超出免费字符量后是否停止翻译
	NowSize    uint64 `json:"now_size"`    // 当前已使用翻译字符量
}

// 获取当前账户的详细信息
func Info() *AccountInfos {
	return &AccountInfos{
		AppId:      _default.AppId,
		FreeSize:   _default.FreeSize,
		ExceedStop: _default.ExceedStop,
		NowSize:    _default.nowSize,
	}
}
