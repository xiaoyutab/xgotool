// SDK相关服务，用于封装一些常用的但是没有专属SDK的相关服务
package sdk

// 目前支持的SDK服务有：
// 1. 高德SDK`xamap`
// 2. 短链接SDK`xduanlianjie`
// 3. 百度人脸识别/人脸认证SDK`xfaceprint`
// 4. 百度翻译`xfanyibaidu`
// 5. 谷歌密钥生成操作`xgoogauth`
// 6. 百度OCR识图功能`xocrbaidu`
// 7. gorm增强SDK【用于增强gorm的个性化功能】`xgorm`
// 目前常用但是有官方SDK的，此处不进行封装
// 1. 阿里云短信发送SDK
// 2. 阿里云OSS文件上传SDK
// 3. 腾讯云OSS文件上传SDK
//
// PS: 此处服务均依赖于本地的https服务，所以使用时请尽量注册https的日志注册等相关服务
// 主要依赖服务有以下几点：
// 1. gitee.com/xiaoyutab/xgotool/https
// 2. gitee.com/xiaoyutab/xgotool/optional/xlog
// 3. gitee.com/xiaoyutab/xgotool/xnum
// 4. gitee.com/xiaoyutab/xgotool/xstring
// 5. gitee.com/xiaoyutab/xgotool/optional/xcron
// 6. gitee.com/xiaoyutab/xgotool/optional/xcache
