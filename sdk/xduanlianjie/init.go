// 三方-短链接相关的SDK
//
// 官网地址：https://www.duanlianjie.net/
package xduanlianjie

type Config struct {
	Key       string // 短网址的授权KEY
	DomainUrl string // 生成的接口地址
}

var _default Config = Config{
	DomainUrl: "https://www.duanlianjie.net/api/url/add",
}

// 注入配置项
func Regedit(c *Config) {
	if c == nil {
		return
	}
	if c.Key != "" {
		_default.Key = c.Key
	}
	if c.DomainUrl != "" {
		_default.DomainUrl = ""
	}
}
