package xduanlianjie

import (
	"errors"

	"gitee.com/xiaoyutab/xgotool/https"
)

// 访问次数限制
type UrlLimit struct {
	Type  uint8 `json:"boundstype"` // 访问次数限制方式 0-按总访问量 1-按独立访客访问量
	Count uint  `json:"bounds"`     // 限制的访问次数
}

// URL链接配置
type UrlConfig struct {
	Url      string    `json:"url"`      // 要生成的网址
	Password string    `json:"password"` // 访问密码
	Expiry   string    `json:"expiry"`   // 短网址有效期，日期格式
	Counts   *UrlLimit `json:"limit"`    // 访问次数限制
}

// 获取URL链接
//
//	c	网址配置
func GetConfig(c *UrlConfig) (string, error) {
	if c.Url == "" {
		return "", errors.New("待生成的网址不能为空")
	}
	if len(c.Url) < 12 {
		// 如果网址本来就短，则直接返回原地址，不进行缩短
		return c.Url, nil
	}
	// 定义返回值信息
	type short struct {
		Error int    `json:"error"`
		Short string `json:"short"`
		Msg   string `json:"msg"`
	}
	s := short{}
	err := https.New(_default.DomainUrl).
		ParamJsonAny(c).
		HeaderKV("Authorization", "Token "+_default.Key).
		PostJson().
		Json(&s)
	if err != nil {
		return "", err
	}
	if s.Error != 0 {
		return "", errors.New(s.Msg)
	}
	return s.Short, nil
}

// 获取URL链接
//
//	url	待转换的长网址
func GetUrl(url string) (string, error) {
	return GetConfig(&UrlConfig{
		Url: url,
	})
}

// 获取URL链接
//
//	longurl	待转换的长网址
//	date	有效期截止日期
func GetUrlDate(longurl, date string) (string, error) {
	return GetConfig(&UrlConfig{
		Url:    longurl,
		Expiry: date,
	})
}
