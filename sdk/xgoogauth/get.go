package xgoogauth

import (
	"fmt"
	"strings"
	"time"
)

// 根据秘钥获取随机数字验证码
//
//	secret	秘钥标识
func Get(secret string) string {
	return GetLv(secret, 0)
}

// 根据秘钥获取随机数字验证码
//
//	secret	秘钥标识
//	lv		获取层级 0-当前密钥 -1-前x秒密钥 -2-前2x秒的密钥 1-后x秒的密钥...
func GetLv(secret string, lv int) string {
	if secret == "" {
		return ""
	}
	tm := time.Now().Unix() + int64(lv)*CodeTime
	secretUpper := strings.ToUpper(secret)
	secretKey, err := base32decode(secretUpper)
	if err != nil {
		_default.LogErrorFunc("谷歌秘钥Code获取失败", err)
		return ""
	}
	number := oneTimePassword(secretKey, toBytes(tm/CodeTime))
	return fmt.Sprintf("%06d", number)
}
