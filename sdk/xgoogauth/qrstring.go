package xgoogauth

import (
	"fmt"
)

// 获取生成二维码的规范化字符串
//
//	user	标题
//	secret	秘钥
//
// 返回格式：otpauth://totp/<user>?secret=<secret>
func Qrstring(user, secret string) string {
	return fmt.Sprintf("otpauth://totp/%s?secret=%s", user, secret)
}
