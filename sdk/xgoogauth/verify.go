package xgoogauth

// 验证输入的验证码是否正确
//
//	secret	秘钥
//	code	用户输入的验证码
func Verify(secret, code string) bool {
	_code := Get(secret)
	if _code == "" {
		return false
	}
	return _code == code
}
