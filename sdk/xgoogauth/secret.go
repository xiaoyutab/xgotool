package xgoogauth

import (
	"bytes"
	"encoding/binary"
	"strings"
)

// 生成秘钥
func Secret() string {
	var buf bytes.Buffer
	binary.Write(&buf, binary.BigEndian, un())
	return strings.ToUpper(base32encode(hmacSha1(buf.Bytes(), nil)))
}
